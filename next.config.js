const withBundleAnalyzer = require("@next/bundle-analyzer")({
    enabled: process.env.ANALYZE === "true",
});

module.exports = withBundleAnalyzer(
    {
        distDir: "build",
        publicRuntimeConfig: {
            // add your public runtime environment variables here with NEXT_PUBLIC_*** prefix
        },
        webpack: (config) => {
            // extend your webpack configuration here
            config.externals.html2canvas = "html2canvas";

            return config;
        }
    }
);

// const withPWA = require("next-pwa");
//
// module.exports = withPWA(
//     {
//         reactStrictMode: true,
//         swcMinify: false,
//         pwa: {
//             dest: "public",
//             register: true,
//             skipWaiting: true,
//         },
//     },
//     withBundleAnalyzer(
//         {
//             distDir: "build",
//             publicRuntimeConfig: {
//                 // add your public runtime environment variables here with NEXT_PUBLIC_*** prefix
//             },
//             webpack: (config) => {
//                 // extend your webpack configuration here
//                 return config;
//             }
//         }
//     )
// );