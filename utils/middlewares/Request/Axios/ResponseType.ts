export enum ResponseType {
    ARRAY_BUFFER = 'arraybuffer',
    DOCUMENT = 'document',
    JSON = 'json',
    TEXT = 'text',
    STREAM = 'stream',
    BLOB = 'blob'
}

