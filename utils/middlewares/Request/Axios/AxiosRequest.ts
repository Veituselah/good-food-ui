import axios from "axios";
import {ContentType, Request, RequestMethod, ResponseType} from "../../../../internal"

export class AxiosRequest extends Request {

    constructor(
        url: string,
        requestMethod: RequestMethod = RequestMethod.GET,
        token: string | null,
        data: Object = {},
        contentType: ContentType = ContentType.JSON,
        private _responseType: ResponseType = ResponseType.JSON
    ) {
        super(url, requestMethod, token, data, contentType);
    }

    async doRequest() {

        const config: any = {
            url: this.url,
            method: this.requestMethod,
            responseType: this.responseType,
            data: this.data,
            rejectUnauthorized: false,
            headers: {
                'Content-Type': this.contentType,
            }
        };

        if (this.token) {
            config.headers['Authorization'] = 'Bearer ' + this.token;
        }

        return axios(config);
    }

    get responseType(): ResponseType {
        return this._responseType;
    }

    set responseType(value: ResponseType) {
        this._responseType = value;
    }
}

export default AxiosRequest