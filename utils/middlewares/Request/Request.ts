import {ContentType, RequestMethod} from "../../../internal"

export abstract class Request {

    protected constructor(
        private _url: string,
        private _requestMethod: RequestMethod = RequestMethod.GET,
        private _token: string | null,
        private _data: Object = {},
        private _contentType: ContentType = ContentType.JSON,
    ) {
    }

    abstract doRequest(): Promise<any>

    get token(): string | null {
        return this._token;
    }

    get url(): string {
        return this._url;
    }

    set url(value: string) {
        this._url = value;
    }

    get requestMethod(): RequestMethod {
        return this._requestMethod;
    }

    set requestMethod(value: RequestMethod) {
        this._requestMethod = value;
    }

    get data(): Object {
        return this._data;
    }

    set data(value: Object) {
        this._data = value;
    }

    get contentType(): ContentType {
        return this._contentType;
    }

    set contentType(value: ContentType) {
        this._contentType = value;
    }

}

