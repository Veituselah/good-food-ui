export enum ContentType {
    JSON = 'application/json',
    MERGE_PATCH_JSON = 'application/merge-patch+json'
}

