import {
    CustomerModel,
    FranchiseEmployeeModel,
    FranchisorEmployeeModel,
    Message,
    MessageGravity,
    UserInterface,
    UserModel
} from "../../../internal"
import {NextRequest, NextResponse} from "next/server";
import jwtDecode from "jwt-decode";
import {NextCookies} from "next/dist/server/web/spec-extension/cookies";
import {Dispatch} from "redux";
import {loginAdmin, loginCustomer} from "../../../redux/actions/context";
import {addLog} from "../../../redux/actions/logger";

export class Context {

    public static readonly CUSTOMER_COOKIE_NAME = 'customer';
    public static readonly ADMIN_COOKIE_NAME = 'admin';
    public static readonly CART_COOKIE_NAME = 'cart';

    constructor(
        protected _isServerSide: boolean,
        protected _request?: NextRequest,
        protected _response?: NextResponse
    ) {

        if (_isServerSide && (!_request || !_response)) {
            throw new Error("To get cookie from server side you must set 'request' and 'response' parameters");
        }
    }

    /**
     * Retourne le client stocké dans les cookies
     */
    public getCustomer(): CustomerModel | null {
        return this.getUser(Context.CUSTOMER_COOKIE_NAME) as CustomerModel;
    }

    /**
     * Retourne l'admin stocké dans les cookies
     */
    public getAdmin(): UserInterface | null {
        return this.getUser(Context.ADMIN_COOKIE_NAME);
    }

    public getAdminToken(): string | null {
        return this.getCookie(Context.ADMIN_COOKIE_NAME);
    }

    public getCustomerToken(): string | null {
        return this.getCookie(Context.CUSTOMER_COOKIE_NAME);
    }

    public getCartIdentifier(): string | null {
        return this.getCookie(Context.CART_COOKIE_NAME);
    }

    public setCartIdentifier(id: string | null) {
        this.setCookie(Context.CART_COOKIE_NAME, id);
    }

    public removeCart() {
        this.clearCookie(Context.CART_COOKIE_NAME);
    }

    public currentUserCanAccessToAdmin(): boolean {
        let user = this.getAdmin();

        if (user) {
            return user.canAccessToAdmin();
        }

        return false;
    }

    public currentUserIsCustomer(): boolean {
        let user = this.getCustomer();

        if (user) {
            return user.isCustomer();
        }

        return false;
    }

    public saveUser(token: string | null, dispatch: Dispatch) {
        let data: any = jwtDecode(token ?? '');

        if (token) {

            if (UserModel.isCustomer(data)) {

                this.setCookie(Context.CUSTOMER_COOKIE_NAME, token);

                dispatch(loginCustomer(token));

                dispatch((addLog({
                    message: "Vous avez été connecté avec succès.",
                    gravity: MessageGravity.SUCCESS,
                })));

            } else if (UserModel.canAccessToAdmin(data)) {

                this.setCookie(Context.ADMIN_COOKIE_NAME, token);

                dispatch(loginAdmin(token));

                dispatch((addLog({
                    title: "Vous avez été connecté avec succès.",
                    message: "Vous allez être redirigé d'ici quelques instants.",
                    gravity: MessageGravity.SUCCESS,
                })));

                return true;

            } else {

                let message: Message = {
                    gravity: MessageGravity.ERROR,
                    message: "Erreur pendant la connnexion. Veuillez contacter un administrateur si le problème persiste."
                }

                dispatch(addLog(message));

            }

        }

        return false;
    }

    public logout(customer: boolean, admin: boolean) {

        if (customer) {
            this.clearCookie(Context.CUSTOMER_COOKIE_NAME);
        }

        if (admin) {
            this.clearCookie(Context.ADMIN_COOKIE_NAME);

        }
    }

    protected getUser(cookieName: string): UserModel | null {

        let cookie = this.getCookie(cookieName);

        if (cookie) {

            try {

                let decoded: any = jwtDecode(cookie);

                if (decoded && decoded.exp > Date.now() / 1000) {

                    let user = new UserModel();

                    switch (cookieName) {
                        case Context.ADMIN_COOKIE_NAME :
                            if (decoded.restaurant) {
                                user = new FranchiseEmployeeModel();
                            } else {
                                user = new FranchisorEmployeeModel();
                            }
                            break;
                        case Context.CUSTOMER_COOKIE_NAME:
                            user = new CustomerModel();
                            break;
                    }

                    user.hydrateFromJson(decoded);
                    return user;
                }

            } catch (e) {
                console.error(e);
            }
        }

        return null;
    }

    get isServerSide(): boolean {
        return this._isServerSide;
    }

    set isServerSide(value: boolean) {
        this._isServerSide = value;
    }

    get request(): NextRequest | undefined {
        return this._request;
    }

    set request(value: NextRequest | undefined) {
        this._request = value;
    }

    get response(): NextResponse | undefined {
        return this._response;
    }

    set response(value: NextResponse | undefined) {
        this._response = value;
    }

    protected getCookie(cookie: string): string | null {

        let returnValue: any = null;

        if (this.isServerSide) {

            let cookies = this.request?.cookies;

            if (cookies instanceof NextCookies) {
                returnValue = cookies?.get(cookie) ?? null;
            } else if (cookies) {
                returnValue = cookies[cookie];
            }

        } else {

            let name = cookie + "=";
            let decodedCookie = decodeURIComponent(document.cookie);
            let ca = decodedCookie.split(';');

            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    returnValue = c.substring(name.length, c.length);
                }
            }

        }

        return returnValue;
    }

    protected setCookie(cookie: string, value: string | null, nbDay: number = 7) {

        let maxAge = nbDay * 24 * 60 * 60 * 1000;

        if (this.isServerSide && this.response) {

            if (value) {
                this.response?.cookies.set(cookie, value, {
                        maxAge: maxAge,
                        sameSite: 'strict',
                    }
                );
            } else {
                this.response.cookies?.clear();
            }

        } else {
            const d = new Date();
            d.setTime(d.getTime() + (maxAge));
            let expires = "expires=" + d.toUTCString();
            document.cookie = cookie + "=" + value + ";" + expires + ";path=/";
        }

    }

    protected clearCookie(cookie: string) {
        this.setCookie(cookie, null, 0);
    }

}

