import {MessageGravity} from "../../../internal"

export interface Message {
    key?: string,
    title?: string,
    message: string,
    duration?: number,
    gravity?: MessageGravity,
    field?: string
}

export const generateUniqueRandomKey = (): string => Date.now().toString() + (Math.random() * 100).toString();
