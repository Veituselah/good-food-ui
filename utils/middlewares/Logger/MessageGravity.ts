export enum MessageGravity {
    INFO,
    WARNING,
    ERROR,
    SUCCESS
}