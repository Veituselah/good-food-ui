export interface JsonableInterface{
    prepareJSON(full: boolean): object;
}