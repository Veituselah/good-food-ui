export const URL_PARAM_START_SEPRATOR = '{{';
export const URL_PARAM_END_SEPRATOR = '}}';

export interface UrlInterface {
    url: string;
    urlParams: Array<UrlParamInterface>;
}

export interface UrlParamInterface {
    param: string,
    pathToProperty: Array<string>
}

export class UrlBuilder {

    constructor(
        private url: UrlInterface,
        private object: any
    ) {

    }


    buildUrl(): string {

        let url = this.url.url;

        for (let param of this.url.urlParams) {

            let paramName = URL_PARAM_START_SEPRATOR + param.param + URL_PARAM_END_SEPRATOR;
            let value = this.object;

            for (let property of param.pathToProperty) {
                value = value[property] ?? null;
            }

            let regex = new RegExp(paramName, 'g');

            url = url.replace(regex, value);

        }

        return url;

    }

}