import {Role} from "../../internal";

export interface UserInterface {

    isGranted(role: Role): boolean;

    isCustomer(): boolean;

    canAccessToAdmin(): boolean;

}

