export interface SavableCookieInterface{

    hydrateFromCookie(cookie: string|null): void;

}