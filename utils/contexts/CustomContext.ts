import {createContext} from "react";
import {Context} from "../../internal";

const context = new Context(false);

export const CustomContext = createContext(context);
