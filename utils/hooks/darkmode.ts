import {useEffect, useState} from 'react';

export const DARK_THEME: string = 'dark';
export const LIGHT_THEME: string = 'light';

function useDarkMode(initialTheme: string = LIGHT_THEME): [string, (value: unknown) => void] {

    const [theme, setTheme] = useState(typeof window !== 'undefined' ? (localStorage.theme ?? initialTheme) : DARK_THEME);

    const colorThemeToRemove = theme === DARK_THEME ? LIGHT_THEME : DARK_THEME;

    useEffect(() => {

        const root = window.document.documentElement;

        root.classList.remove(colorThemeToRemove);
        root.classList.add(theme.toString());

        if (window) {
            localStorage.setItem('theme', theme.toString());
        }
    }, [theme]);

    return [theme, setTheme];
}

export default useDarkMode;
