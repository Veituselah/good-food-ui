import {EmployeeHandler, FranchisorEmployeeModel} from "../../../../../internal"

export class FranchisorEmployeeHandler<T extends FranchisorEmployeeModel> extends EmployeeHandler<T> {

    getModel(): any {
        return FranchisorEmployeeModel;
    }

    public getPrefixRoute(): string {
        return "franchisor_employees";
    }

}

