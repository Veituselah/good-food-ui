import {EmployeeHandler, FranchiseEmployeeModel} from "../../../../../internal"

export class FranchiseEmployeeHandler<T extends FranchiseEmployeeModel> extends EmployeeHandler<T> {

    getModel(): any {
        return FranchiseEmployeeModel;
    }

    public getPrefixRoute(): string {
        return "franchisor_employees";
    }

}

