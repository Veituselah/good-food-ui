import {EmployeeModel, UserHandler} from "../../../../../internal"

export class EmployeeHandler<T extends EmployeeModel> extends UserHandler<T> {

    getModel(): any {
        return EmployeeModel;
    }

    public getPrefixRoute(): string {
        return "employees";
    }

}

