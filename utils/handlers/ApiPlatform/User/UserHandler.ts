import {ApiPlatformHandler, ModelIdentifierNotSettedError, UserModel} from "../../../../internal"

export class UserHandler<T extends UserModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return UserModel;
    }

    public getPrefixRoute(): string {
        return "users";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.uuid) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.uuid;
    }

}

