import {ApiPlatformHandler, ModelIdentifierNotSettedError, CommentModel} from "../../../../internal"

export class CommentHandler<T extends CommentModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return CommentModel;
    }

    public getPrefixRoute(): string {
        return "comments";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

