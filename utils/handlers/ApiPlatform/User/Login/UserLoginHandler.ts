import {ApiPlatformHandler, AxiosRequest, RequestMethod, UserLoginModel} from "../../../../../internal"

export class UserLoginHandler<T extends UserLoginModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return UserLoginModel;
    }

    async login(data: T): Promise<string | null> {

        let req = new AxiosRequest(
            this.getCollectionRoute(),
            RequestMethod.POST,
            null,
            data.prepareJSON()
        );

        let token = null;

        await req.doRequest().then((resp) => {
            token = resp?.data?.token;
        });

        return token;
    }

    public getPrefixRoute() {
        return 'login';
    }

    protected getRouteIdentifierForItem(data: T): string {
        return "";
    }

}

