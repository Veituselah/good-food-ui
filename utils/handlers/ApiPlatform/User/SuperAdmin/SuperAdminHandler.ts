import {SuperAdminModel, UserHandler} from "../../../../../internal"

export class SuperAdminHandler<T extends SuperAdminModel> extends UserHandler<T> {

    getModel(): any {
        return SuperAdminModel;
    }

    public getPrefixRoute(): string {
        return "super_admins";
    }

}

