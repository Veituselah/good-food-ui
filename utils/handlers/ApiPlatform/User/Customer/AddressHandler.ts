import {AddressModel, ApiPlatformHandler, ModelIdentifierNotSettedError,} from "../../../../../internal"

export class AddressHandler<T extends AddressModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return AddressModel;
    }

    public getPrefixRoute(): string {
        return "addresses";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

