import {
    AddressHandler,
    AddressModel,
    AxiosRequest,
    Collection, CommentHandler, CommentModel,
    CustomerModel, OrderHandler, OrderModel,
    RequestMethod, ReservationHandler, ReservationModel, RestaurantModel,
    UserHandler
} from "../../../../../internal"

export class CustomerHandler<T extends CustomerModel> extends UserHandler<T> {

    static NB_SUB_RESOURCE_PER_PAGE = 5

    getModel(): any {
        return CustomerModel;
    }

    public getPrefixRoute(): string {
        return "customers";
    }

    public async getCustomerOrders(
        customerIri: string, page = 1,
        searchedRef: string | null = null,
        itemsPerPage = CustomerHandler.NB_SUB_RESOURCE_PER_PAGE,
    ): Promise<Collection<OrderModel>> {

        let orderHandler = new OrderHandler();

        let url = this.getItemRouteFromIri(customerIri) + '/' + orderHandler.getPrefixRoute() + '?itemsPerPage=' + itemsPerPage + '&page=' + page;

        if (searchedRef) {
            url += '&reference=' + searchedRef;
        }

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest();

        return orderHandler.parseCollectionResponseToCollection(resp, page);

    }

    public async getCustomerReservations(
        customerIri: string, page = 1,
        itemsPerPage = CustomerHandler.NB_SUB_RESOURCE_PER_PAGE,
    ): Promise<Collection<ReservationModel>> {

        let reservationHandler = new ReservationHandler();

        let url = this.getItemRouteFromIri(customerIri) + '/' + reservationHandler.getPrefixRoute() + '?itemsPerPage=' + itemsPerPage + '&page=' + page;

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest();

        return reservationHandler.parseCollectionResponseToCollection(resp, page);

    }

    public async getCustomerAddresses(customerIri: string, page = 1, itemsPerPage = CustomerHandler.NB_SUB_RESOURCE_PER_PAGE): Promise<Collection<AddressModel>> {

        let addressHandler = new AddressHandler();

        let url = this.getItemRouteFromIri(customerIri) + '/' + addressHandler.getPrefixRoute() + '?itemsPerPage=' + itemsPerPage + '&page=' + page;

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest();

        return addressHandler.parseCollectionResponseToCollection(resp, page);

    }

    public async getCustomerComments(customerIri: string, page = 1, itemsPerPage = CustomerHandler.NB_SUB_RESOURCE_PER_PAGE): Promise<Collection<CommentModel>> {

        let commentHandler = new CommentHandler();

        let url = this.getItemRouteFromIri(customerIri) + '/' + commentHandler.getPrefixRoute() + '?itemsPerPage=' + itemsPerPage + '&page=' + page;

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest();

        return commentHandler.parseCollectionResponseToCollection(resp, page);

    }

    public async getCart(): Promise<OrderModel> {

        let url = this.getCollectionRoute() + '/cart';

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest();

        const orderHandler = new OrderHandler();

        let model = orderHandler.getModelInstance();
        model.hydrateFromJson(resp.data);

        return model;

    }


    public async getAllInOrders(): Promise<Collection<CustomerModel>> {

        let url = this.getCollectionRoute() + '/in_orders';

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest()

        return this.parseCollectionResponseToCollection(resp);

    }

    public async getAllInReservations(): Promise<Collection<CustomerModel>> {

        let url = this.getCollectionRoute() + '/in_reservations';

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest()

        return this.parseCollectionResponseToCollection(resp);

    }

}

