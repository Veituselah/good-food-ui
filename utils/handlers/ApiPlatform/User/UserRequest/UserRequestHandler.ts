import {ApiPlatformHandler, AxiosRequest, ContentType, RequestMethod, UserRequestModel} from "../../../../../internal"

export abstract class UserRequestHandler<T extends UserRequestModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return UserRequestModel;
    }


    public async validate(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model)  + '/validate',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON(false),
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }


}

