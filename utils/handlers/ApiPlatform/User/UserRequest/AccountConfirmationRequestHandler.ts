import {
    AccountConfirmationRequestModel,
    ModelIdentifierNotSettedError,
    UserRequestHandler
} from "../../../../../internal"

export class AccountConfirmationRequestHandler<T extends AccountConfirmationRequestModel> extends UserRequestHandler<T> {

    getModel(): any {
        return AccountConfirmationRequestModel;
    }

    public getPrefixRoute(): string {
        return "account_confirmation_requests";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

