import {ModelIdentifierNotSettedError, NewPasswordRequestModel, UserRequestHandler} from "../../../../../internal"

export class NewPasswordRequestHandler<T extends NewPasswordRequestModel> extends UserRequestHandler<T> {

    getModel(): any {
        return NewPasswordRequestModel;
    }

    public getPrefixRoute(): string {
        return "new_password_requests";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

