import {
    ApiHandler,
    ApiPlatformModel,
    AxiosRequest,
    Collection,
    ContentType,
    OrderFilter,
    RequestMethod,
    SearchFilter
} from "../../../internal"
import {ExistsFilter} from "../../filters/ApiPlatform/ExistsFilter";

export abstract class ApiPlatformHandler<T extends ApiPlatformModel> extends ApiHandler<T> {

    public static readonly CONTEXT_INDEX_JSON_LD = '@context';
    public static readonly ID_INDEX_JSON_LD = '@id';
    public static readonly TYPE_INDEX_JSON_LD = '@type';
    public static readonly HYDRA_TITLE_INDEX = 'hydra:title';
    public static readonly HYDRA_COLLECTION_TYPE_VALUE_JSON_LD = 'hydra:Collection';
    public static readonly HYDRA_TOTAL_ITEMS_INDEX_JSON_LD = 'hydra:totalItems';
    public static readonly HYDRA_VIEW_INDEX_JSON_LD = 'hydra:view';
    public static readonly HYDRA_PARTIAL_COLLECTION_VIEW_TYPE_VALUE_JSON_LD = 'hydra:PartialCollectionView';
    public static readonly HYDRA_DESCRIPTION = 'hydra:description';
    public static readonly HYDRA_FIRST_INDEX_JSON_LD = 'hydra:first';
    public static readonly HYDRA_LAST_INDEX_JSON_LD = 'hydra:last';
    public static readonly HYDRA_NEXT_INDEX_JSON_LD = 'hydra:next';
    public static readonly HYDRA_PREV_INDEX_JSON_LD = 'hydra:previous';
    public static readonly HYDRA_MEMBER_INDEX_JSON_LD = 'hydra:member';

    /**
     * Retourne le préfix des routes associées aux flux gérés par la classe
     * Example : customers (Full path = API_URL/customers)
     *
     * @protected
     */
    public abstract getPrefixRoute(): string;

    /**
     * Retourne la chaîne de caractère permettnt d'identifier l'entité
     * Example : 84ef5-ds5ez-5sd8e-eza45-83ez2 (Full path = API_URL/customers/84ef5-ds5ez-5sd8e-eza45-83ez2)
     *
     * @protected
     */
    protected abstract getRouteIdentifierForItem(data: T): string;

    public getCollectionRoute(): string {
        return ApiHandler.API_URL + this.getBaseResourceRoute()
    }

    public getBaseResourceRoute(): string{
        return '/' + this.getPrefixRoute()
    }

    public getItemRoute(data: T) {
        return this.getCollectionRoute() + '/'+ this.getRouteIdentifierForItem(data);
    }

    public buildIri(identifier: any){
        return '/api' + this.getBaseResourceRoute() + '/' + identifier;
    }

    public getItemRouteFromIri(iri: string) {
        return ApiHandler.API_DOMAIN + iri;
    }

    public async getAll(orderBy?: Array<OrderFilter>, searchFilters?: Array<SearchFilter>, page = 1, disablePartial = false, existsFilters?: Array<ExistsFilter>): Promise<Collection<T>> {

        let url = this.getCollectionRoute() + '?';
        let urlParams = [];

        if (orderBy) {
            urlParams.push(this.buildOrderFilterQueryForUrl(orderBy));
        }

        if (searchFilters) {
            urlParams.push(this.buildSearchFilterQueryForUrl(searchFilters));
        }

        if (existsFilters) {
            urlParams.push(this.buildExistsFilterQueryForUrl(existsFilters));
        }

        urlParams.push('pagination=' + !disablePartial)

        page = (!page || page < 1 ? 1 : page);


        if (page > 1) {
            urlParams.push('page=' + page);
        }

        url += this.buildParamUrl(urlParams);

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest()

        return this.parseCollectionResponseToCollection(resp, page);
    }

    public async delete(iri: string): Promise<boolean> {

        let req = new AxiosRequest(
            this.getItemRouteFromIri(iri),
            RequestMethod.DELETE,
            this._token
        );

        let resp = await req.doRequest()

        return resp.status === 204;

    }

    public async get(iri: string): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRouteFromIri(iri),
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest()

        let model = this.getModelInstance();
        model.hydrateFromJson(resp.data);

        return model;

    }

    public async create(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getCollectionRoute(),
            RequestMethod.POST,
            this._token,
            model.prepareJSON ? model.prepareJSON(false): model
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public async patch(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model),
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false): model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public parseCollectionResponseToCollection(resp: any, page = 1): Collection<T> {

        let totalItems = resp.data[ApiPlatformHandler.HYDRA_TOTAL_ITEMS_INDEX_JSON_LD];

        let view = resp?.data[ApiPlatformHandler.HYDRA_VIEW_INDEX_JSON_LD] ?? [];
        let firstPage =
            view[ApiPlatformHandler.HYDRA_FIRST_INDEX_JSON_LD] ?
                this.getPageNumberFromPageUrl(ApiHandler.API_URL + '/' + view[ApiPlatformHandler.HYDRA_FIRST_INDEX_JSON_LD]) :
                1;
        let lastPage =
            view[ApiPlatformHandler.HYDRA_LAST_INDEX_JSON_LD] ?
                this.getPageNumberFromPageUrl(ApiHandler.API_URL + '/' + view[ApiPlatformHandler.HYDRA_LAST_INDEX_JSON_LD]) :
                1;
        let nextPage =
            view[ApiPlatformHandler.HYDRA_NEXT_INDEX_JSON_LD] ?
                this.getPageNumberFromPageUrl(ApiHandler.API_URL + '/' + view[ApiPlatformHandler.HYDRA_NEXT_INDEX_JSON_LD]) :
                null;
        let prevPage =
            view[ApiPlatformHandler.HYDRA_PREV_INDEX_JSON_LD] ?
                this.getPageNumberFromPageUrl(ApiHandler.API_URL + '/' + view[ApiPlatformHandler.HYDRA_PREV_INDEX_JSON_LD]) :
                null;

        let data = new Array<T>();

        for (let item of resp.data[ApiPlatformHandler.HYDRA_MEMBER_INDEX_JSON_LD] ?? []) {
            let model = this.getModelInstance();
            model.hydrateFromJson(item);
            data.push(model);
        }

        return new Collection<T>(
            totalItems,
            data,
            page,
            firstPage,
            lastPage,
            nextPage,
            prevPage
        );
    }

    protected buildOrderFilterQueryForUrl(orderFilters: Array<OrderFilter>) {

        let params = '';

        for (let filter of orderFilters) {
            params += (params ? '&' : '') + filter.toString();
        }

        return params;

    }

    protected buildSearchFilterQueryForUrl(searchFilters: Array<SearchFilter>) {

        let params = '';

        for (let filter of searchFilters) {
            params += (params ? '&' : '') + filter.toString();
        }

        return params;

    }

    protected buildExistsFilterQueryForUrl(existsFilters: Array<ExistsFilter>) {

        let params = '';

        for (let filter of existsFilters) {
            params += (params ? '&' : '') + filter.toString();
        }

        return params;

    }

    protected buildParamUrl(params: Array<string>) {

        let url = '';

        params.forEach((param: string) => {
            url += (url ? '&' : '') + param;
        });

        return url;

    }

    protected getPageNumberFromPageUrl(pageUrl: string): number {

        let page = 1;

        if (pageUrl) {
            let urlObj = new URL(pageUrl);
            page = Number.parseInt(urlObj.searchParams.get('page') ?? '1');
        }

        return page;

    }

}