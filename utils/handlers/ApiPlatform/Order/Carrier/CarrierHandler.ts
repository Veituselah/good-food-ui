import {ApiPlatformHandler, CarrierModel, ModelIdentifierNotSettedError} from "../../../../../internal"

export class CarrierHandler<T extends CarrierModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return CarrierModel;
    }

    public getPrefixRoute(): string {
        return "carriers";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.reference) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.reference;
    }

}

