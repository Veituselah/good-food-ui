import {ApiPlatformHandler, CarrierDataModel, ModelIdentifierNotSettedError} from "../../../../../internal"

export class CarrierDataHandler<T extends CarrierDataModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return CarrierDataModel;
    }

    public getPrefixRoute(): string {
        return "carrier_datas";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

