import {ApiPlatformHandler, ModelIdentifierNotSettedError, OrderLineModel} from "../../../../../internal"

export class OrderLineHandler<T extends OrderLineModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return OrderLineModel;
    }

    public getPrefixRoute(): string {
        return "order_lines";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

