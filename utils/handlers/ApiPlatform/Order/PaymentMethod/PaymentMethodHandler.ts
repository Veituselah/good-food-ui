import {ApiPlatformHandler, ModelIdentifierNotSettedError, PaymentMethodModel} from "../../../../../internal"

export class PaymentMethodHandler<T extends PaymentMethodModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return PaymentMethodModel;
    }

    public getPrefixRoute(): string {
        return "payment_methods";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

