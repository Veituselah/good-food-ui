import {ApiPlatformHandler, ModelIdentifierNotSettedError, VoucherModel} from "../../../../../internal"

export class VoucherHandler<T extends VoucherModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return VoucherModel;
    }

    public getPrefixRoute(): string {
        return "vouchers";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.code) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.code;
    }

}

