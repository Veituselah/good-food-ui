import {ApiPlatformHandler, ModelIdentifierNotSettedError, VoucherDataModel} from "../../../../../internal"

export class VoucherDataHandler<T extends VoucherDataModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return VoucherDataModel;
    }

    public getPrefixRoute(): string {
        return "voucher_datas";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

