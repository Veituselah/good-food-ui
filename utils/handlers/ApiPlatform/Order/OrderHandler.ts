import {
    ApiPlatformHandler,
    AxiosRequest, ContentType,
    ModelIdentifierNotSettedError,
    OrderModel,
    RequestMethod
} from "../../../../internal"

export class OrderHandler<T extends OrderModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return OrderModel;
    }

    public getPrefixRoute(): string {
        return "orders";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.reference) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.reference;
    }

    public async pay(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/pay',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public async cancel(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/cancel',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public async delivered(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/delivered',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public async processing(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/processing',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public async in_delivery(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/in_delivery',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public async available_in_restaurant(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/available_in_restaurant',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

    public async refunded(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/refunded',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

}

