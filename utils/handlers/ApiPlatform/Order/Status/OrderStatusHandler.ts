import {ApiPlatformHandler, ModelIdentifierNotSettedError, OrderStatusModel} from "../../../../../internal"

export class OrderStatusHandler<T extends OrderStatusModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return OrderStatusModel;
    }

    public getPrefixRoute(): string {
        return "order_statuses";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

