import {
    ApiPlatformHandler,
    AxiosRequest,
    Collection,
    CommentHandler,
    CommentModel, CountryModel,
    ModelIdentifierNotSettedError,
    RequestMethod,
    RestaurantModel
} from "../../../../internal"

export class RestaurantHandler<T extends RestaurantModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return RestaurantModel;
    }

    public getPrefixRoute(): string {
        return "restaurants";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

    public async getRestaurantAroundLocation(longitude: number, latitude: number): Promise<any> {

        let req = new AxiosRequest(
            this.getCollectionRoute() + '/around_location/' + longitude + '/' + latitude,
            RequestMethod.GET,
            this._token,
        );

        return req.doRequest();
    }

    public async getComments(iri: string): Promise<Collection<CommentModel>>{

        let commentHandler = new CommentHandler();

       let url =  this.getItemRouteFromIri(iri) + '/' + commentHandler.getPrefixRoute();

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest();

        return commentHandler.parseCollectionResponseToCollection(resp, 1);

    }

    public async getAllInOrders(): Promise<Collection<RestaurantModel>> {

        let url = this.getCollectionRoute() + '/in_orders';

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest()

        return this.parseCollectionResponseToCollection(resp);

    }

    public async getAllInReservations(): Promise<Collection<RestaurantModel>> {

        let url = this.getCollectionRoute() + '/in_reservations';

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest()

        return this.parseCollectionResponseToCollection(resp);

    }

}

