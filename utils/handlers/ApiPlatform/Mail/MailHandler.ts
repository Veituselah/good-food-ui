import {ApiPlatformHandler, MailModel, ModelIdentifierNotSettedError} from "../../../../internal"

export class MailHandler<T extends MailModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return MailModel;
    }

    public getPrefixRoute(): string {
        return "mails";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

