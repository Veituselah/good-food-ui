import {ApiPlatformHandler, ModelIdentifierNotSettedError, ProductMediaModel} from "../../../../internal"

export class ProductMediaHandler<T extends ProductMediaModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return ProductMediaModel;
    }

    public getPrefixRoute(): string {
        return "product_medias";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data?.product?.slug || !data?.media?.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.media.id.toString() + '/' + data.product.slug;
    }

}

