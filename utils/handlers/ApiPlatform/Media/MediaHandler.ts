import {ApiPlatformHandler, MediaModel, ModelIdentifierNotSettedError} from "../../../../internal"

export class MediaHandler<T extends MediaModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return MediaModel;
    }

    public getPrefixRoute(): string {
        return "medias";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

