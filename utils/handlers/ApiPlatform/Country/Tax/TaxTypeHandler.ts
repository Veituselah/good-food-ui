import {ApiPlatformHandler, ModelIdentifierNotSettedError, TaxTypeModel} from "../../../../../internal"

export class TaxTypeHandler<T extends TaxTypeModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return TaxTypeModel;
    }

    public getPrefixRoute(): string {
        return "tax_types";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

