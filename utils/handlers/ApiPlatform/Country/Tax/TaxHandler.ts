import {ApiPlatformHandler, ModelIdentifierNotSettedError, TaxModel} from "../../../../../internal"

export class TaxHandler<T extends TaxModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return TaxModel;
    }

    public getPrefixRoute(): string {
        return "taxes";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

