import {ApiPlatformHandler, CurrencyModel, ModelIdentifierNotSettedError,} from "../../../../internal";

export class CurrencyHandler<T extends CurrencyModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return CurrencyModel;
    }

    public getPrefixRoute(): string {
        return "currencies";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.isoCode) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.isoCode;
    }

}