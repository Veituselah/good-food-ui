import {
    ApiPlatformHandler,
    AxiosRequest,
    Collection,
    CountryModel,
    ModelIdentifierNotSettedError,
    RequestMethod
} from "../../../../internal"

export class CountryHandler<T extends CountryModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return CountryModel;
    }

    public getPrefixRoute(): string {
        return "countries";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.isoCode) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.isoCode;
    }

    public async getAllInRestaurants(): Promise<Collection<CountryModel>> {

        let url = this.getCollectionRoute() + '/in_restaurants';

        let req = new AxiosRequest(
            url,
            RequestMethod.GET,
            this._token
        );

        let resp = await req.doRequest()

        return this.parseCollectionResponseToCollection(resp);

    }

}

