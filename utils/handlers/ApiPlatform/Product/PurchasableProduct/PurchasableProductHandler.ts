import {ProductHandler, PurchasableProductModel} from "../../../../../internal"

export class PurchasableProductHandler<T extends PurchasableProductModel> extends ProductHandler<T> {

    getModel(): any {
        return PurchasableProductModel;
    }

    public getPrefixRoute(): string {
        return "purchasable_products";
    }
}

