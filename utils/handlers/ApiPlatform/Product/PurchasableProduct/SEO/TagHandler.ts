import {ApiPlatformHandler, ModelIdentifierNotSettedError, TagModel} from "../../../../../../internal"

export class TagHandler<T extends TagModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return TagModel;
    }

    public getPrefixRoute(): string {
        return "tags";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

