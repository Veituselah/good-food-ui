import {DishModel, FoodHandler,} from "../../../../../../internal"

export class DishHandler<T extends DishModel> extends FoodHandler<T> {

    getModel(): any {
        return DishModel;
    }

    public getPrefixRoute(): string {
        return "dishes";
    }

}

