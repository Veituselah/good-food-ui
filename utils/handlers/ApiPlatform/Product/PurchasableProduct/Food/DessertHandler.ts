import {DessertModel, FoodHandler,} from "../../../../../../internal"

export class DessertHandler<T extends DessertModel> extends FoodHandler<T> {

    getModel(): any {
        return DessertModel;
    }

    public getPrefixRoute(): string {
        return "desserts";
    }

}

