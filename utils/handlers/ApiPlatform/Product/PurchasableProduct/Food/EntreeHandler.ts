import {EntreeModel, FoodHandler,} from "../../../../../../internal"

export class EntreeHandler<T extends EntreeModel> extends FoodHandler<T> {

    getModel(): any {
        return EntreeModel;
    }

    public getPrefixRoute(): string {
        return "entrees";
    }

}

