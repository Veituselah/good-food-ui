import {FoodModel, PurchasableProductHandler} from "../../../../../../internal"

export class FoodHandler<T extends FoodModel> extends PurchasableProductHandler<T> {

    getModel(): any {
        return FoodModel;
    }

    public getPrefixRoute(): string {
        return "foods";
    }

}

