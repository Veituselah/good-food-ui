import {ApiPlatformHandler, ExtraModel, ModelIdentifierNotSettedError} from "../../../../../../../internal"

export class ExtraHandler<T extends ExtraModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return ExtraModel;
    }

    public getPrefixRoute(): string {
        return "extras";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

}

