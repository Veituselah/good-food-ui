import {ApiPlatformHandler, ExtraGroupModel, ModelIdentifierNotSettedError} from "../../../../../../../internal"

export class ExtraGroupHandler<T extends ExtraGroupModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return ExtraGroupModel;
    }

    public getPrefixRoute(): string {
        return "extra_groups";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

