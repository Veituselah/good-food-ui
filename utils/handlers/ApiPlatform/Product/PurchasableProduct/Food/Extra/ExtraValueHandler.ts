import {ApiPlatformHandler, ExtraValueModel, ModelIdentifierNotSettedError} from "../../../../../../../internal"

export class ExtraValueHandler<T extends ExtraValueModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return ExtraValueModel;
    }

    public getPrefixRoute(): string {
        return "extra_values";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

