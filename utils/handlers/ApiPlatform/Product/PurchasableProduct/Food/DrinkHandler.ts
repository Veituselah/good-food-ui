import {DrinkModel, FoodHandler,} from "../../../../../../internal"

export class DrinkHandler<T extends DrinkModel> extends FoodHandler<T> {

    getModel(): any {
        return DrinkModel;
    }

    public getPrefixRoute(): string {
        return "drinks";
    }

}

