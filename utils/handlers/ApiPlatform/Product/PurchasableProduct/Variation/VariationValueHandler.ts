import {ApiPlatformHandler, ModelIdentifierNotSettedError, VariationValueModel} from "../../../../../../internal"

export class VariationValueHandler<T extends VariationValueModel> extends ApiPlatformHandler<T> {


    getModel(): any {
        return VariationValueModel;
    }

    public getPrefixRoute(): string {
        return "variation_values";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

