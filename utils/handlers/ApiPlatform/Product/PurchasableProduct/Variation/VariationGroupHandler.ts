import {ApiPlatformHandler, ModelIdentifierNotSettedError, VariationGroupModel} from "../../../../../../internal"

export class VariationGroupHandler<T extends VariationGroupModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return VariationGroupModel;
    }

    public getPrefixRoute(): string {
        return "variation_groups";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

