import {PurchasableProductHandler, VariationModel} from "../../../../../../internal"

export class VariationHandler<T extends VariationModel> extends PurchasableProductHandler<T> {

    getModel(): any {
        return VariationModel;
    }

    public getPrefixRoute(): string {
        return "variationss";
    }

}

