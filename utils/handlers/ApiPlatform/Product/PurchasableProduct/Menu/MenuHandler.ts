import {MenuModel, PurchasableProductHandler} from "../../../../../../internal"

export class MenuHandler<T extends MenuModel> extends PurchasableProductHandler<T> {

    getModel(): any {
        return MenuModel;
    }

    public getPrefixRoute(): string {
        return "menus";
    }

}

