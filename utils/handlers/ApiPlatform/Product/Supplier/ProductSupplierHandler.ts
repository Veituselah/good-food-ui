import {ApiPlatformHandler, ModelIdentifierNotSettedError, ProductSupplierModel} from "../../../../../internal"

export class ProductSupplierHandler<T extends ProductSupplierModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return ProductSupplierModel;
    }

    public getPrefixRoute(): string {
        return "product_suppliers";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data?.product?.slug || !data?.supplier?.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.product.slug + '/' + data.supplier.slug;
    }

}

