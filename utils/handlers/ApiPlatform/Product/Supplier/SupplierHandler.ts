import {ApiPlatformHandler, ModelIdentifierNotSettedError, SupplierModel} from "../../../../../internal"

export class SupplierHandler<T extends SupplierModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return SupplierModel;
    }

    public getPrefixRoute(): string {
        return "suppliers";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

