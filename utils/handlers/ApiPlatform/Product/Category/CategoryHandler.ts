import {ApiPlatformHandler, CategoryModel, ModelIdentifierNotSettedError} from "../../../../../internal"

export class CategoryHandler<T extends CategoryModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return CategoryModel;
    }

    public getPrefixRoute(): string {
        return "categories";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

