import {ApiPlatformHandler, ModelIdentifierNotSettedError, StockModel} from "../../../../../internal"

export class StockHandler<T extends StockModel> extends ApiPlatformHandler<T> {


    getModel(): any {
        return StockModel;
    }

    public getPrefixRoute(): string {
        return "stocks";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data?.product?.slug || !data?.restaurant?.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.product.slug + '/' + data.restaurant?.slug;
    }

}

