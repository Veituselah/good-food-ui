import {ApiPlatformHandler, ModelIdentifierNotSettedError, ProductModel} from "../../../../internal"

export class ProductHandler<T extends ProductModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return ProductModel;
    }

    public getPrefixRoute(): string {
        return "products";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.slug) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.slug;
    }

}

