import {
    ApiPlatformHandler,
    AxiosRequest,
    ContentType,
    ModelIdentifierNotSettedError,
    RequestMethod,
    ReservationModel
} from "../../../internal"

export class ReservationHandler<T extends ReservationModel> extends ApiPlatformHandler<T> {

    getModel(): any {
        return ReservationModel;
    }

    public getPrefixRoute(): string {
        return "reservations";
    }

    protected getRouteIdentifierForItem(data: T): string {
        if (!data.id) {
            throw new ModelIdentifierNotSettedError();
        }

        return data.id.toString();
    }

    public async cancel(model: T): Promise<T> {

        let req = new AxiosRequest(
            this.getItemRoute(model) + '/cancel',
            RequestMethod.PATCH,
            this._token,
            model.prepareJSON ? model.prepareJSON(false) : model,
            ContentType.MERGE_PATCH_JSON
        );

        let resp = await req.doRequest()

        let modelResp: T = this.getModelInstance();
        modelResp.hydrateFromJson(resp.data);

        return modelResp;
    }

}

