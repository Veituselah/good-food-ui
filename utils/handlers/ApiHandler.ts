import {Collection, Model, OrderFilter, SearchFilter} from "../../internal"

export abstract class ApiHandler<T extends Model> {

    constructor(
        protected _token: string | null = null
    ) {
    }

    get token(): string | null {
        return this._token;
    }

    set token(token: string | null) {
        this._token = token;
    }

    public static readonly API_DOMAIN = process.env.NEXT_PUBLIC_API_URL;
    public static readonly API_URL = ApiHandler.API_DOMAIN + '/api';

    public abstract getModel(): any;

    public getModelInstance(): T {
        let model = this.getModel();
        return new model();
    }

    public abstract getAll(orderBy?: Array<OrderFilter>, searchFilters?: Array<SearchFilter>, page?: number): Promise<Collection<T>>;

    public abstract get(iri: string): Promise<T>;

    public abstract create(model: T): Promise<T>

    public abstract patch(mordel: T): Promise<T>

    public abstract delete(iri: string): Promise<boolean>

    public abstract getCollectionRoute(): string
}

