import {JsonableInterface, Model} from "../../internal";

export class Collection<T extends Model> implements JsonableInterface {


    public constructor(
        protected _totalItems: number = 0,
        protected _data: Array<T> = new Array<T>(),
        protected _currentPage = 1,
        protected _firstPage: number | null = null,
        protected _lastPage: number | null = null,
        protected _nextPage: number | null = null,
        protected _previousPage: number | null = null,
    ) {
    }

    get totalItems(): number {
        return this._totalItems;
    }

    get data(): Array<T> {
        return this._data;
    }

    get firstPage(): number | null {
        return this._firstPage;
    }

    get lastPage(): number | null {
        return this._lastPage;
    }

    get nextPage(): number | null {
        return this._nextPage;
    }

    get previousPage(): number | null {
        return this._previousPage;
    }

    get currentPage(): number {
        return this._currentPage;
    }

    public prepareJSON(full: boolean): object {

        let dataJSON = [];

        for (let item of this._data) {
            dataJSON.push(item.prepareJSON(full));
        }

        return {
            totalItems: this._totalItems,
            data: dataJSON,
            currentPage: this._currentPage,
            firstPage: this._firstPage,
            lastPage: this._lastPage,
            nextPage: this._nextPage,
            previousPage: this._previousPage,
        }
    }

    static getPageFromUrl(url: string) : number{

        const queryparams = url.split('?')[1];
        const params = queryparams.split('&');

        params.forEach(function(d) {
            const pair = d.split('=');
            if(pair[0] === 'page'){
                return pair[1]
            }
        });

        return 0;
    }
}