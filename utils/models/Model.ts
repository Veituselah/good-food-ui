import {JsonableInterface} from "../../internal";

export abstract class Model implements JsonableInterface{

    public abstract hydrateFromJson(data: any): void;

    public abstract prepareJSON(full: boolean): object;

}

