import {ApiPlatformModel, CarrierDataModel} from "../../../../../internal"

export class CarrierModel extends ApiPlatformModel {

    private _active?: boolean;
    private _deleted?: boolean;
    private _name?: string;
    private _reference?: string;
    private _siteUrl?: string;
    private _carrierData: Array<CarrierDataModel> = new Array<CarrierDataModel>();

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get reference(): string | undefined {
        return this._reference;
    }

    set reference(value: string | undefined) {
        this._reference = value;
    }

    get siteUrl(): string | undefined {
        return this._siteUrl;
    }

    set siteUrl(value: string | undefined) {
        this._siteUrl = value;
    }

    get carrierData(): Array<CarrierDataModel> {
        return this._carrierData;
    }

    set carrierData(value: Array<CarrierDataModel>) {
        this._carrierData = value;
    }

    hydrateFromJson(data: any) {

        this._active = data.active ?? null;
        this._deleted = data.deleted ?? null;
        this._name = data.name ?? null;
        this._reference = data.reference ?? null;
        this._siteUrl = data.siteUrl ?? null;
        this._carrierData = data.carrierData ?? null;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            active: this._active ?? null,
            deleted: this._deleted ?? null,
            name: this._name ?? null,
            reference: this._reference ?? null,
            siteUrl: this._siteUrl ?? null,
            carrierData: (this._carrierData ?? []).map((carrier: CarrierDataModel) => full ? carrier.prepareJSON(full) : carrier.iri),
        };
    }
}

