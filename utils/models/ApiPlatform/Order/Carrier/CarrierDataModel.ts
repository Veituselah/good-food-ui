import {ApiPlatformModel, CarrierModel, CurrencyModel, TaxTypeModel} from "../../../../../internal"

export class CarrierDataModel extends ApiPlatformModel {

    protected _id?: number;
    protected _createdAt?: Date;
    protected _carrier?: CarrierModel;
    protected _currency?: CurrencyModel;
    protected _delay?: string;
    protected _shippingCost?: number;
    protected _tax?: TaxTypeModel;

    get id(): number | undefined {
        return this._id;
    }

    get carrier(): CarrierModel | undefined {
        return this._carrier;
    }

    set carrier(value: CarrierModel | undefined) {
        this._carrier = value;
    }

    get currency(): CurrencyModel | undefined {
        return this._currency;
    }

    set currency(value: CurrencyModel | undefined) {
        this._currency = value;
    }

    get delay(): string | undefined {
        return this._delay;
    }

    set delay(value: string | undefined) {
        this._delay = value;
    }

    get shippingCost(): number | undefined {
        return this._shippingCost;
    }

    set shippingCost(value: number | undefined) {
        this._shippingCost = value;
    }

    get tax(): TaxTypeModel | undefined {
        return this._tax;
    }

    set tax(value: TaxTypeModel | undefined) {
        this._tax = value;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._carrier = data.carrier ? new CarrierModel().hydrateFromJson(data.carrier) : undefined;
        this._currency = data.currency ? new CurrencyModel().hydrateFromJson(data.currency) : undefined;
        this._delay = data.delay ?? undefined;
        this._shippingCost = data.shippingCost ?? undefined;
        this._tax = data.tax ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            carrier: this._carrier ? (full ? this._carrier.prepareJSON(full) : this._carrier.iri) : null,
            currency: this._currency ? (full ? this._currency.prepareJSON(full) : this._currency.iri) : null,
            delay: this._delay ?? null,
            shippingCost: this._shippingCost ?? null,
            tax: this._tax ?? null,
        };
    }
}

