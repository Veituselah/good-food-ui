import {
    AddressModel,
    ApiPlatformModel,
    CarrierDataModel, CurrencyModel,
    CustomerModel,
    OrderLineModel,
    OrderStatusModel,
    PaymentMethodModel,
    RestaurantModel,
    SavableCookieInterface,
    VoucherModel
} from "../../../../internal"

export class OrderModel extends ApiPlatformModel implements SavableCookieInterface {

    private _reference?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _orderedAt?: Date;
    private _refundedAt?: Date;
    private _total?: number;
    private _totalTaxExcluded?: number;
    private _taxAmount?: number;
    private _lastPaymentTryAt?: Date;
    private _customer?: CustomerModel;
    private _billingAddress?: AddressModel;
    private _carrier?: CarrierDataModel;
    private _deliveryAddress?: AddressModel;
    private _orderLines: Array<OrderLineModel> = new Array<OrderLineModel>();
    private _paymentIdentifier?: string;
    private _paymentMethod?: PaymentMethodModel;
    private _restaurant?: RestaurantModel;
    private _currency?: CurrencyModel;
    private _status?: OrderStatusModel;
    private _vouchers: Array<VoucherModel> = new Array<VoucherModel>();

    get reference(): string | undefined {
        return this._reference;
    }

    set reference(value: string | undefined) {
        this._reference = value;
    }

    get total(): number | undefined {
        return this._total;
    }

    set total(value: number | undefined) {
        this._total = value;
    }

    get taxAmount(): number | undefined {
        return this._taxAmount;
    }

    set taxAmount(value: number | undefined) {
        this._taxAmount = value;
    }

    get totalTaxExcluded(): number | undefined {
        return this._totalTaxExcluded;
    }

    set totalTaxExcluded(value: number | undefined) {
        this._totalTaxExcluded = value;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get orderedAt(): Date | undefined {
        return this._orderedAt;
    }

    set orderedAt(orderedAt: Date | undefined) {
        this._orderedAt = orderedAt;
    }

    get lastPaymentTryAt(): Date | undefined {
        return this._lastPaymentTryAt;
    }

    get refundedAt(): Date | undefined {
        return this._refundedAt;
    }

    get customer(): CustomerModel | undefined {
        return this._customer;
    }

    set customer(value: CustomerModel | undefined) {
        this._customer = value;
    }

    get billingAddress(): AddressModel | undefined {
        return this._billingAddress;
    }

    set billingAddress(value: AddressModel | undefined) {
        this._billingAddress = value;
    }

    get carrier(): CarrierDataModel | undefined {
        return this._carrier;
    }

    set carrier(value: CarrierDataModel | undefined) {
        this._carrier = value;
    }

    get deliveryAddress(): AddressModel | undefined {
        return this._deliveryAddress;
    }

    set deliveryAddress(value: AddressModel | undefined) {
        this._deliveryAddress = value;
    }

    get orderLines(): Array<any> {
        return this._orderLines;
    }

    set orderLines(value: Array<any>) {
        this._orderLines = value;
    }

    get paymentIdentifier(): string | undefined {
        return this._paymentIdentifier;
    }

    set paymentIdentifier(value: string | undefined) {
        this._paymentIdentifier = value;
    }

    get paymentMethod(): PaymentMethodModel | undefined {
        return this._paymentMethod;
    }

    set paymentMethod(value: PaymentMethodModel | undefined) {
        this._paymentMethod = value;
    }

    get restaurant(): RestaurantModel | undefined {
        return this._restaurant;
    }

    set restaurant(value: RestaurantModel | undefined) {
        this._restaurant = value;
    }

    get currency(): CurrencyModel | undefined {
        return this._currency;
    }

    set currency(value: CurrencyModel | undefined) {
        this._currency = value;
    }

    get status(): OrderStatusModel | undefined {
        return this._status;
    }

    set status(value: OrderStatusModel | undefined) {
        this._status = value;
    }

    get vouchers(): Array<VoucherModel> {
        return this._vouchers;
    }

    set vouchers(value: Array<VoucherModel>) {
        this._vouchers = value;
    }

    hydrateFromCookie(cookie: string | null) {
        if (cookie) {
            let data = JSON.parse(cookie);
            this.hydrateFromJson(data);
        }

        return this;
    }

    hydrateFromJson(data: any) {

        this._reference = data.reference ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._orderedAt = data.orderedAt ? new Date(data.orderedAt) : undefined;
        this._refundedAt = data.refundedAt ? new Date(data.refundedAt) : undefined;
        this._lastPaymentTryAt = data.lastPaymentTryAt ? new Date(data.lastPaymentTryAt) : undefined;
        this._total = data.total ? data.total : undefined;
        this._taxAmount = data.taxAmount ? data.taxAmount : undefined;
        this._totalTaxExcluded = data.totalTaxExcluded ? data.totalTaxExcluded : undefined;

        this._customer = data.customer ? new CustomerModel().hydrateFromJson(data.customer) : undefined;
        this._billingAddress = data.billingAddress ? new AddressModel().hydrateFromJson(data.billingAddress) : undefined;
        this._carrier = data.carrier ? new CarrierDataModel().hydrateFromJson(data.carrier) : undefined;
        this._deliveryAddress = data.deliveryAddress ? new AddressModel().hydrateFromJson(data.deliveryAddress) : undefined;

        this._orderLines = (data.orderLines ?? []).map((line: any) => new OrderLineModel().hydrateFromJson(line));

        this._paymentIdentifier = data.paymentIdentifier ?? undefined;
        this._paymentMethod = data.paymentMethod ? PaymentMethodModel.getPaymentMethodInstanceAndHydrateFromJson(data.paymentMethod) : undefined;
        this._restaurant = data.restaurant ? new RestaurantModel().hydrateFromJson(data.restaurant) : undefined;
        this._currency = data.currency ? new CurrencyModel().hydrateFromJson(data.currency) : undefined;
        this._status = data.status ? new OrderStatusModel().hydrateFromJson(data.status) : undefined;

        this._vouchers = (data.vouchers ?? []).map((voucher: any) => new VoucherModel().hydrateFromJson(voucher));

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            reference: this._reference ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            orderedAt: this._orderedAt ? this._orderedAt.toISOString() : null,
            refundedAt: this._refundedAt ? this._refundedAt.toISOString() : null,
            lastPaymentTryAt: this._lastPaymentTryAt ? this._lastPaymentTryAt.toISOString() : null,
            total: this._total ? this._total : null,
            totalTaxExcluded: this._totalTaxExcluded ? this._totalTaxExcluded : null,
            taxAmount: this._taxAmount ? this._taxAmount : null,
            customer: this._customer ? (full ? this._customer.prepareJSON(full) : this._customer.iri) : null,
            billingAddress: this._billingAddress ? (full ? this._billingAddress.prepareJSON(full) : this._billingAddress.iri) : null,
            carrier: this._carrier ? (full ? this._carrier.prepareJSON(full) : this._carrier.iri) : null,
            deliveryAddress: this._deliveryAddress ? (full ? this._deliveryAddress.prepareJSON(full) : this._deliveryAddress.iri) : null,
            orderLines: (this._orderLines ?? []).map((orderLine: OrderLineModel) => full ? orderLine.prepareJSON(full) : orderLine.iri),
            paymentIdentifier: this._paymentIdentifier ?? null,
            paymentMethod: this._paymentMethod ? (full ? this._paymentMethod.prepareJSON(full) : this._paymentMethod.iri) : null,
            restaurant: this._restaurant ? (full ? this._restaurant?.prepareJSON(full) : this._restaurant?.iri) : null,
            currency: this._currency ? (full ? this._currency?.prepareJSON(full) : this._currency?.iri) : null,
            status: this._status ? (full ? this._status.prepareJSON(full) : this._status.iri) : null,
            vouchers: (this._vouchers ?? []).map((voucher: VoucherModel) => full ? voucher.prepareJSON(full) : voucher.iri),
        };
    }

    public static formatPrice(price: any): string {

        price = parseFloat(price);

        return price.toFixed(2)
    }

}

