import {
    ApiPlatformModel,
    CurrencyModel,
    CustomerModel,
    ProductModel,
    PurchasableProductModel,
    VoucherModel
} from "../../../../../internal"

export class VoucherDataModel extends ApiPlatformModel {

    private _id?: number;
    private _amountReductionTaxExcluded?: number;
    private _availableFrom?: Date;
    private _availableTo?: Date;
    private _currency?: CurrencyModel;
    private _freeShippingCost?: boolean;
    private _name?: string;
    private _onlyForCustomers: Array<CustomerModel> = new Array<CustomerModel>();
    private _onlyForProducts: Array<ProductModel> = new Array<ProductModel>();
    private _percentageReductionTaxExcluded?: number;
    private _voucher?: VoucherModel;

    get id(): number | undefined {
        return this._id;
    }

    get amountReductionTaxExcluded(): number | undefined {
        return this._amountReductionTaxExcluded;
    }

    set amountReductionTaxExcluded(value: number | undefined) {
        this._amountReductionTaxExcluded = value;
    }

    get availableFrom(): Date | undefined {
        return this._availableFrom;
    }

    set availableFrom(value: Date | undefined) {
        this._availableFrom = value;
    }

    get availableTo(): Date | undefined {
        return this._availableTo;
    }

    set availableTo(value: Date | undefined) {
        this._availableTo = value;
    }

    get currency(): CurrencyModel | undefined {
        return this._currency;
    }

    set currency(value: CurrencyModel | undefined) {
        this._currency = value;
    }

    get freeShippingCost(): boolean | undefined {
        return this._freeShippingCost;
    }

    set freeShippingCost(value: boolean | undefined) {
        this._freeShippingCost = value;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get onlyForCustomers(): Array<CustomerModel> {
        return this._onlyForCustomers;
    }

    set onlyForCustomers(value: Array<CustomerModel>) {
        this._onlyForCustomers = value;
    }

    get onlyForProducts(): Array<ProductModel> {
        return this._onlyForProducts;
    }

    set onlyForProducts(value: Array<ProductModel>) {
        this._onlyForProducts = value;
    }

    get percentageReductionTaxExcluded(): number | undefined {
        return this._percentageReductionTaxExcluded;
    }

    set percentageReductionTaxExcluded(value: number | undefined) {
        this._percentageReductionTaxExcluded = value;
    }

    get voucher(): VoucherModel | undefined {
        return this._voucher;
    }

    set voucher(value: VoucherModel | undefined) {
        this._voucher = value;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? undefined;
        this._amountReductionTaxExcluded = data.amountReductionTaxExcluded ?? undefined;
        this._availableFrom = data.availableFrom ? new Date(data.availableFrom) : undefined;
        this._availableTo = data.availableTo ? new Date(data.availableTo) : undefined;
        this._currency = data.currency ? new CurrencyModel().hydrateFromJson(data.currency) : undefined;
        this._freeShippingCost = data.freeShippingCost ?? undefined;
        this._name = data.name ?? undefined;
        this._onlyForCustomers = (data.onlyForCustomers ?? []).map((customer: any) => new CustomerModel().hydrateFromJson(customer));
        this._onlyForProducts = (data.onlyForProducts ?? []).map((product: any) => PurchasableProductModel.getPurchasableProductInstanceAndHydrateFromJSON(product));
        this._percentageReductionTaxExcluded = data.percentageReductionTaxExcluded ?? undefined;
        this._voucher = data.voucher ? new VoucherModel().hydrateFromJson(data.voucher) : undefined;
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            amountReductionTaxExcluded: this._amountReductionTaxExcluded ?? null,
            availableFrom: this._availableFrom ? this._availableFrom.toISOString() : null,
            availableTo: this._availableTo ? this._availableTo.toISOString() : null,
            currency: this._currency ? (full ? this._currency.prepareJSON(full) : this._currency.iri) : null,
            freeShippingCost: this._freeShippingCost ?? null,
            name: this._name ?? null,
            onlyForCustomers: (this._onlyForCustomers ?? []).map((customer: any) => full ? customer.prepareJSON(full) : customer.iri),
            onlyForProducts: (this._onlyForProducts ?? []).map((product: any) => full ? product.prepareJSON(full) : product.iri),
            percentageReductionTaxExcluded: this._percentageReductionTaxExcluded ?? null,
            voucher: this._voucher ?? null,
        };
    }
}

