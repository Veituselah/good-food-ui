import {ApiPlatformModel, VoucherDataModel} from "../../../../../internal"

export class VoucherModel extends ApiPlatformModel {

    private _createdAt?: Date;
    private _active?: boolean;
    private _code?: string;
    private _voucherData: Array<VoucherDataModel> = new Array<VoucherDataModel>();

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get code(): string | undefined {
        return this._code;
    }

    set code(value: string | undefined) {
        this._code = value;
    }

    get voucherData(): Array<VoucherDataModel> {
        return this._voucherData;
    }

    set voucherData(value: Array<VoucherDataModel>) {
        this._voucherData = value;
    }

    hydrateFromJson(data: any) {

        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._active = data.active ?? false;
        this._code = data.code ?? undefined;
        this._voucherData = (data.voucherData ?? []).map((voucher: any) => new VoucherDataModel().hydrateFromJson(voucher));

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            active: this._active ?? null,
            code: this._code ?? null,
            voucherData: this._voucherData.map(voucherData => full ? voucherData.prepareJSON(full) : voucherData.iri),
        };
    }
}

