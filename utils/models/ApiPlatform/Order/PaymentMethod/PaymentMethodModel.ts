import {ApiPlatformModel} from "../../../../../internal"

export abstract class PaymentMethodModel extends ApiPlatformModel {

    protected _slug?: string;
    protected _createdAt?: Date;
    protected _updatedAt?: Date;
    protected _active?: boolean;
    protected _name?: string;

    get slug(): string | undefined {
        return this._slug;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    hydrateFromJson(data: any) {

        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._name = data.name ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            name: this._name ?? null,
        };
    }

    static getPaymentMethodInstanceAndHydrateFromJson(data: any): PaymentMethodModel | undefined {

        switch (data.context) {
            // TODO : Get payment method in terms of context
        }

        return undefined;
    }
}

