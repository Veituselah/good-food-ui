import {
    ApiPlatformModel,
    CurrencyModel,
    ExtraModel,
    FoodModel,
    OrderModel,
    PurchasableProductModel
} from "../../../../../internal"

export class OrderLineModel extends ApiPlatformModel {

    private _id?: number;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _commentary?: string;
    private _currency?: CurrencyModel;
    private _depth?: number;
    private _description?: string;
    private _ean13?: string;
    private _foodExtraChoices: Array<ExtraModel> = new Array<ExtraModel>();
    private _height?: number;
    private _menuFoodChoices: Array<FoodModel> = new Array<FoodModel>();
    private _name?: string;
    private _orderReference?: OrderModel;
    private _product?: PurchasableProductModel;
    private _qty?: number;
    private _total?: number;
    private _totalTaxExcluded?: number;
    private _taxAmount?: number;
    private _taxRate?: number;
    private _unitPriceTaxExcluded?: number;
    private _priceTaxIncluded?: number;
    private _upc?: string;
    private _weight?: number;
    private _width?: number;

    get id(): number | undefined {
        return this._id;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get commentary(): string | undefined {
        return this._commentary;
    }

    set commentary(value: string | undefined) {
        this._commentary = value;
    }

    get currency(): CurrencyModel | undefined {
        return this._currency;
    }

    set currency(value: CurrencyModel | undefined) {
        this._currency = value;
    }

    get depth(): number | undefined {
        return this._depth;
    }

    set depth(value: number | undefined) {
        this._depth = value;
    }

    get description(): string | undefined {
        return this._description;
    }

    set description(value: string | undefined) {
        this._description = value;
    }

    get ean13(): string | undefined {
        return this._ean13;
    }

    set ean13(value: string | undefined) {
        this._ean13 = value;
    }

    get foodExtraChoices(): Array<ExtraModel> {
        return this._foodExtraChoices;
    }

    set foodExtraChoices(value: Array<ExtraModel>) {
        this._foodExtraChoices = value;
    }

    get height(): number | undefined {
        return this._height;
    }

    set height(value: number | undefined) {
        this._height = value;
    }

    get menuFoodChoices(): Array<FoodModel> {
        return this._menuFoodChoices;
    }

    set menuFoodChoices(value: Array<FoodModel>) {
        this._menuFoodChoices = value;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get orderReference(): OrderModel | undefined {
        return this._orderReference;
    }

    set orderReference(value: OrderModel | undefined) {
        this._orderReference = value;
    }

    get product(): PurchasableProductModel | undefined {
        return this._product;
    }

    set product(value: PurchasableProductModel | undefined) {
        this._product = value;
    }

    get qty(): number | undefined {
        return this._qty;
    }

    set qty(value: number | undefined) {
        this._qty = value;
    }

    get total(): number | undefined {
        return this._total;
    }

    set total(value: number | undefined) {
        this._total = value;
    }


    get totalTaxExcluded(): number | undefined {
        return this._totalTaxExcluded;
    }

    set totalTaxExcluded(value: number | undefined) {
        this._totalTaxExcluded = value;
    }

    get taxRate(): number | undefined {
        return this._taxRate;
    }

    set taxRate(value: number | undefined) {
        this._taxRate = value;
    }

    get taxAmount(): number | undefined {
        return this._taxAmount;
    }

    set taxAmount(value: number | undefined) {
        this._taxAmount = value;
    }

    get unitPriceTaxExcluded(): number | undefined {
        return this._unitPriceTaxExcluded;
    }

    set unitPriceTaxExcluded(value: number | undefined) {
        this._unitPriceTaxExcluded = value;
    }

    get priceTaxIncluded(): number | undefined {
        return this._priceTaxIncluded;
    }

    set priceTaxIncluded(value: number | undefined) {
        this._priceTaxIncluded = value;
    }

    get upc(): string | undefined {
        return this._upc;
    }

    set upc(value: string | undefined) {
        this._upc = value;
    }

    get weight(): number | undefined {
        return this._weight;
    }

    set weight(value: number | undefined) {
        this._weight = value;
    }

    get width(): number | undefined {
        return this._width;
    }

    set width(value: number | undefined) {
        this._width = value;
    }

    hydrateFromJson(data: any) {

        this._id = data.id ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._commentary = data.commentary ?? undefined;
        this._currency = data.currency ? new CurrencyModel().hydrateFromJson(data.currency) : undefined;
        this._depth = data.depth ?? undefined;
        this._description = data.description ?? undefined;
        this._ean13 = data.ean13 ?? undefined;
        this._foodExtraChoices = (data.foodExtraChoices ?? []).map((extraChoice: any) => new ExtraModel().hydrateFromJson(extraChoice));
        this._height = data.height ?? undefined;
        this._menuFoodChoices = (data.menuFoodChoices ?? []).map((foodChoice: any) => FoodModel.getFoodInstanceAndHydrateFromJSON(foodChoice));
        this._name = data.name ?? undefined;

        let order = new OrderModel();
        order.reference = data.orderReference;

        let product = new PurchasableProductModel();
        product.hydrateFromJson(data.product);

        this._orderReference = data.orderReference ? order : undefined;
        this._product = data.product ? product  : undefined;
        this._qty = data.qty ?? undefined;
        this._total = data.total ?? undefined;
        this._totalTaxExcluded = data.totalTaxExcluded ?? undefined;
        this._taxRate = data.taxRate ?? undefined;
        this._taxAmount = data.taxAmount ?? undefined;
        this._unitPriceTaxExcluded = data.unitPriceTaxExcluded ?? undefined;
        this._priceTaxIncluded = data.priceTaxIncluded ?? undefined;
        this._upc = data.upc ?? undefined;
        this._weight = data.weight ?? undefined;
        this._width = data.width ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,    commentary: this._commentary ?? null,
            currency: this._currency ? (full ? this._currency.prepareJSON(full) : this._currency.iri) : null,
            depth: this._depth ?? null,
            description: this._description ?? null,
            ean13: this._ean13 ?? null,
            foodExtraChoices: (this._foodExtraChoices ?? []).map((extraChoice: any) => extraChoice.iri),
            height: this._height ?? null,
            menuFoodChoices: (this._menuFoodChoices ?? []).map((foodChoice: any) => foodChoice.iri),
            name: this._name ?? null,
            orderReference: this._orderReference ? (full ? this._orderReference.prepareJSON(full) : this._orderReference.iri) : null,
            product: this._product ? (full ? this._product.prepareJSON(full) : this._product.iri) : null,
            qty: this._qty ?? null,
            total: this._total ?? null,
            totalTaxExcluded: this._totalTaxExcluded ?? null,
            taxRate: this._taxRate ?? null,
            taxAmount: this._taxAmount ?? null,
            unitPriceTaxExcluded: this._unitPriceTaxExcluded ?? null,
            priceTaxIncluded: this._priceTaxIncluded ?? null,
            upc: this._upc ?? null,
            weight: this._weight ?? null,
            width: this._width ?? null,
        };
    }
}

