import {ApiPlatformModel, CustomerModel, PurchasableProductModel, RestaurantModel} from "../../../internal"

export class ReservationModel extends ApiPlatformModel {

    private _id?: number;
    private _reservedFor?: Date;
    private _canceledAt?: Date;
    private _restaurant?: RestaurantModel;
    private _purchasableProduct?: PurchasableProductModel;
    private _customer?: CustomerModel;
    private _nbPerson?: number;
    private _reservedDayPart?: string;
    private _phoneNumber?: string;

    get restaurant(): RestaurantModel | undefined {
        return this._restaurant;
    }

    set restaurant(value: RestaurantModel | undefined) {
        this._restaurant = value;
    }

    get purchasableProduct(): PurchasableProductModel | undefined {
        return this._purchasableProduct;
    }

    set purchasableProduct(value: PurchasableProductModel | undefined) {
        this._purchasableProduct = value;
    }

    get customer(): CustomerModel | undefined {
        return this._customer;
    }

    set customer(value: CustomerModel | undefined) {
        this._customer = value;
    }

    get nbPerson(): number | undefined {
        return this._nbPerson;
    }

    set nbPerson(value: number | undefined) {
        this._nbPerson = value;
    }

    get reservedDayPart(): string | undefined {
        return this._reservedDayPart;
    }

    set reservedDayPart(value: string | undefined) {
        this._reservedDayPart = value;
    }


    get phoneNumber(): string | undefined {
        return this._phoneNumber;
    }

    set phoneNumber(value: string | undefined) {
        this._phoneNumber = value;
    }

    get id(): number | undefined {
        return this._id;
    }

    get reservedFor(): Date | undefined {
        return this._reservedFor;
    }

    set reservedFor(reservedFor: Date | undefined) {
        this._reservedFor = reservedFor;
    }

    get canceledAt(): Date | undefined {
        return this._canceledAt;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? null;
        this._reservedFor = data.reservedFor ? new Date(data.reservedFor) : undefined;
        this._canceledAt = data.canceledAt ? new Date(data.canceledAt) : undefined;
        this._nbPerson = data.nbPerson ??  undefined;
        this._reservedDayPart = data.reservedDayPart ??  undefined;
        this._phoneNumber = data.phoneNumber ??  undefined;

        this._customer = data.customer ? new CustomerModel() : undefined;
        if (this._customer) {
            this._customer.hydrateFromJson(data.customer);
        }

        this._restaurant = data.restaurant ? new RestaurantModel() : undefined;
        if (this._restaurant) {
            this._restaurant.hydrateFromJson(data.restaurant);
        }

        super.hydrateFromJson(data);

        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            reservedFor: this._reservedFor ? this._reservedFor.toISOString() : null,
            canceledAt: this._canceledAt ? this._canceledAt.toISOString() : null,
            restaurant: this.restaurant ? (full && this.restaurant.prepareJSON ? this.restaurant.prepareJSON(full) : this.restaurant.iri) : null,
            purchasableProduct: this.purchasableProduct ? (full && this.purchasableProduct.prepareJSON ? this.purchasableProduct.prepareJSON(full) : this.purchasableProduct.iri) : null,
            customer: this.customer ? (full && this.customer.prepareJSON ? this.customer.prepareJSON(full) : this.customer.iri) : null,
            nbPerson: this.nbPerson ?? null,
            reservedDayPart: this.reservedDayPart ?? null,
            phoneNumber: this.phoneNumber ?? null,
        };
    }

}


