import {ApiPlatformModel, TaxModel} from "../../../../../internal"

export class TaxTypeModel extends ApiPlatformModel {

    private _slug?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _name?: string;
    private _taxes: Array<TaxModel> = new Array<TaxModel>();

    get slug(): string | undefined {
        return this._slug;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get deleted(): boolean | undefined {
        return this._deleted;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get taxes(): Array<TaxModel> {
        return this._taxes;
    }

    set taxes(value: Array<TaxModel>) {
        this._taxes = value;
    }

    hydrateFromJson(data: any) {
        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._name = data.name ?? undefined;
        this._taxes = (data.taxes ?? []).map((tax: any) => new TaxModel().hydrateFromJson(tax));

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            name: this._name ?? null,
            taxes: (this._taxes ?? []).map((tax: any) => full ? tax.prepareJSON(full) : tax.iri),
        };
    }
}

