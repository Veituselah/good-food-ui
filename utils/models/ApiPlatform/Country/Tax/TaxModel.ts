import {ApiPlatformModel, CountryModel, TaxTypeModel} from "../../../../../internal"

export class TaxModel extends ApiPlatformModel {

    private _slug?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _country?: CountryModel;
    private _name?: string;
    private _rate?: number;
    private _type?: TaxTypeModel;

    get slug(): string | undefined {
        return this._slug;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get deleted(): boolean | undefined {
        return this._deleted;
    }

    get country(): CountryModel | undefined {
        return this._country;
    }

    set country(value: CountryModel | undefined) {
        this._country = value;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get rate(): number | undefined {
        return this._rate;
    }

    set rate(value: number | undefined) {
        this._rate = value;
    }

    get type(): TaxTypeModel | undefined {
        return this._type;
    }

    set type(value: TaxTypeModel | undefined) {
        this._type = value;
    }

    hydrateFromJson(data: any) {
        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._name = data.name ?? undefined;
        this._country = data.country ? new CountryModel().hydrateFromJson(data.country) : undefined;
        this._rate = data.rate ?? undefined;
        this._type = data.type ? new TaxTypeModel().hydrateFromJson(data.type) : undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            name: this._name ?? null,
            country: this._country ? (full ? this._country.prepareJSON(full) : this._country.iri) : null,
            rate: this._rate ?? null,
            type: this._type ? (full ? this._type.prepareJSON(full) : this._type.iri) : null,
        };
    }
}


