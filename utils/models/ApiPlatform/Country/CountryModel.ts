import {ApiPlatformModel, CurrencyModel, TaxModel} from "../../../../internal"

export class CountryModel extends ApiPlatformModel {

    private _isoCode?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _defaultCurrency?: CurrencyModel;
    private _name?: string;
    private _phonePrefix?: string;
    private _taxes: Array<TaxModel> = new Array<TaxModel>();

    get isoCode(): string | undefined {
        return this._isoCode;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get deleted(): boolean | undefined {
        return this._deleted;
    }

    get defaultCurrency(): CurrencyModel | undefined {
        return this._defaultCurrency;
    }

    set defaultCurrency(value: CurrencyModel | undefined) {
        this._defaultCurrency = value;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get phonePrefix(): string | undefined {
        return this._phonePrefix;
    }

    set phonePrefix(value: string | undefined) {
        this._phonePrefix = value;
    }

    get taxes(): Array<TaxModel> {
        return this._taxes;
    }

    set taxes(value: Array<TaxModel>) {
        this._taxes = value;
    }

    hydrateFromJson(data: any) {
        this._isoCode = data.isoCode ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._name = data.name ?? undefined;
        this._defaultCurrency = data.defaultCurrency ? new CurrencyModel().hydrateFromJson(data.defaultCurrency) : undefined;
        this._phonePrefix = data.phonePrefix ?? undefined;
        this._taxes = (data.taxes ?? []).map((tax: any) => new TaxModel().hydrateFromJson(tax));

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            isoCode: this._isoCode ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            name: this._name ?? null,
            defaultCurrency: this._defaultCurrency ? (full ? this._defaultCurrency.prepareJSON(full) : this._defaultCurrency.iri) : null,
            phonePrefix: this._phonePrefix ?? null,
            taxes: (this._taxes ?? []).map((tax: TaxModel) => full ? tax.prepareJSON(full) : tax.iri),
        };
    }
}

