import {ApiPlatformModel} from "../../../../internal"

export class CurrencyModel extends ApiPlatformModel {

    private _isoCode?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _name?: string;
    private _rateForOneEuro?: number;
    private _symbol?: string;

    get isoCode(): string | undefined {
        return this._isoCode;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get deleted(): boolean | undefined {
        return this._deleted;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get rateForOneEuro(): number | undefined {
        return this._rateForOneEuro;
    }

    set rateForOneEuro(value: number | undefined) {
        this._rateForOneEuro = value;
    }

    get symbol(): string | undefined {
        return this._symbol;
    }

    set symbol(value: string | undefined) {
        this._symbol = value;
    }

    hydrateFromJson(data: any) {
        this._isoCode = data.isoCode ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._name = data.name ?? undefined;
        this._rateForOneEuro = data.rateForOneEuro ?? undefined;
        this._symbol = data.symbol ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            isoCode: this._isoCode ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            deleted: this._deleted ?? false,
            name: this._name ?? null,
            rateForOneEuro: this._rateForOneEuro ?? null,
            symbol: this._symbol ?? null,

        };
    }
}