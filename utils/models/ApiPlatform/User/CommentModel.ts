import {ApiPlatformModel, CustomerModel, PurchasableProductModel, RestaurantModel} from "../../../../internal"

export class CommentModel extends ApiPlatformModel {

    private _id?: number;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _note?: number;
    private _restaurant?: RestaurantModel ;
    private _message?: string;
    private _purchasableProduct?: PurchasableProductModel;
    private _customer?: CustomerModel;

    get message(): string | undefined {
        return this._message;
    }

    set message(value: string | undefined) {
        this._message = value;
    }

    get restaurant(): RestaurantModel  | undefined {
        return this._restaurant;
    }

    set restaurant(value: RestaurantModel  | undefined) {
        this._restaurant = value;
    }

    get purchasableProduct(): PurchasableProductModel | undefined {
        return this._purchasableProduct;
    }

    set purchasableProduct(value: PurchasableProductModel | undefined) {
        this._purchasableProduct = value;
    }

    get customer(): CustomerModel | undefined {
        return this._customer;
    }

    set customer(value: CustomerModel | undefined) {
        this._customer = value;
    }

    get id(): number | undefined {
        return this._id;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get note(): number {
        return this._note ?? 0;
    }

    set note(note: number) {
        this._note = note;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? null;
        this._note = data.note ?? null;
        this._message = data.message ?? null;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._note = data.note ?? false;

        this._purchasableProduct = data.purchasableProduct ? new PurchasableProductModel() : undefined;
        if (this._purchasableProduct) {
            this._purchasableProduct.hydrateFromJson(data.purchasableProduct);
        }

        this._customer = data.customer ? new CustomerModel() : undefined;
        if (this._customer) {
            this._customer.hydrateFromJson(data.customer);
        }

        this._restaurant = data.restaurant ? new RestaurantModel() : undefined;
        if (this._restaurant) {
            this._restaurant.hydrateFromJson(data.restaurant);
        }

        super.hydrateFromJson(data);

        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            note: this._note ?? false,
            message: this._message ?? null,
            restaurant: this.restaurant ? (full && this.restaurant.prepareJSON ? this.restaurant.prepareJSON(full) : this.restaurant.iri) : null,
            purchasableProduct: this.purchasableProduct ? (full && this.purchasableProduct.prepareJSON ? this.purchasableProduct.prepareJSON(full) : this.purchasableProduct.iri) : null,
            customer: this.customer ? (full && this.customer.prepareJSON ? this.customer.prepareJSON(full) : this.customer.iri) : null,
        };
    }


}


