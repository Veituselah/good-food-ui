import {RestaurantModel, UserModel} from "../../../../../internal"

export abstract class EmployeeModel extends UserModel {
    public static readonly ROLE_EMPLOYEE = 'ROLE_EMPLOYEE';

    private _restaurant?: RestaurantModel;

    get restaurant(): RestaurantModel | undefined {
        return this._restaurant;
    }

    set restaurant(value: RestaurantModel | undefined) {
        this._restaurant = value;
    }

    hydrateFromJson(data: any) {
        this.restaurant = data.restaurant ?? null;
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            restaurant: this.restaurant ?? null,
        };
    }

}

