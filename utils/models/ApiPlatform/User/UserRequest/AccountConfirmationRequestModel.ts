import {CustomerModel, UserRequestModel} from "../../../../../internal"

export class AccountConfirmationRequestModel extends UserRequestModel {

    private _customerTarget?: CustomerModel;

    get customerTarget(): CustomerModel | undefined {
        return this._customerTarget;
    }

    set customerTarget(value: CustomerModel | undefined) {
        this._customerTarget = value;
    }

    hydrateFromJson(data: any) {
        this._customerTarget = data.customerTarget ?? null;
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            customerTarget: this._customerTarget ?? null
        };
    }
}

