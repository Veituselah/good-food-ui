import {ApiPlatformModel} from "../../../../../internal"

export abstract class UserRequestModel extends ApiPlatformModel {

    private _id?: number;
    private _createdAt?: Date;
    private _expiredAt?: Date;
    private _alreadyUsed: boolean = false;

    get id(): number | undefined {
        return this._id;
    }

    set id(id: number | undefined)  {
        this._id = id;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get expiredAt(): Date | undefined {
        return this._expiredAt;
    }

    get alreadyUsed(): boolean {
        return this._alreadyUsed;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._expiredAt = data.expiredAt ? new Date(data.expiredAt) : undefined;
        this._alreadyUsed = data.alreadyUsed ?? false;
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            createdAt: this._createdAt?.toISOString(),
            expiredAt: this._expiredAt?.toISOString(),
            alreadyUsed: this._alreadyUsed ?? false,
        };
    }
}

