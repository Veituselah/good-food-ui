import {UserModel, UserRequestModel} from "../../../../../internal"

export class NewPasswordRequestModel extends UserRequestModel {

    private _password?: string;
    private _userTarget?: UserModel;
    private _email?: string;

    get password(): string | undefined {
        return this._password;
    }

    set password(value: string | undefined) {
        this._password = value;
    }

    get userTarget(): UserModel | undefined {
        return this._userTarget;
    }

    set userTarget(value: UserModel | undefined) {
        this._userTarget = value;
    }

    get email(): string | undefined {
        return this._email;
    }

    set email(value: string | undefined) {
        this._email = value;
    }

    hydrateFromJson(data: any) {
        this._password = data.password ?? null;
        this._userTarget = data.userTarget ?? null;
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            password: this._password ?? null,
            userTarget: this._userTarget ?? null,
            email: this._email ?? null
        };
    }
}

