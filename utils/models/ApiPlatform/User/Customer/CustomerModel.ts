import {AddressModel, OrderModel, UserModel, VoucherModel} from "../../../../../internal"

export class CustomerModel extends UserModel {

    private _confirmedAt?: Date;
    private _addresses: Array<AddressModel> = new Array<AddressModel>();
    private _birthdate?: Date;
    private _orders: Array<OrderModel> = new Array<OrderModel>();
    private _voucher: Array<VoucherModel> = new Array<VoucherModel>();
    private _nbComments?: number;
    private _nbOrders?: number;
    private _nbAddresses?: number;
    private _meanComments?: number;
    private _noteDivision: Array<any> = [];

    get addresses(): Array<AddressModel> {
        return this._addresses;
    }

    set addresses(value: Array<AddressModel>) {
        this._addresses = value;
    }

    get birthdate(): Date | undefined {
        return this._birthdate;
    }

    set birthdate(value: Date | undefined) {
        this._birthdate = value;
    }

    get orders(): Array<OrderModel> {
        return this._orders;
    }

    set orders(value: Array<OrderModel>) {
        this._orders = value;
    }

    get voucher(): Array<VoucherModel> {
        return this._voucher;
    }

    set voucher(value: Array<VoucherModel>) {
        this._voucher = value;
    }

    get confirmedAt(): Date | undefined {
        return this._confirmedAt;
    }

    get nbOrders(): number | undefined {
        return this._nbOrders;
    }

    set nbOrders(value: number | undefined) {
        this._nbOrders = value;
    }

    get nbComments(): number | undefined {
        return this._nbComments;
    }

    set nbComments(value: number | undefined) {
        this._nbComments = value;
    }

    get nbAddresses(): number | undefined {
        return this._nbAddresses;
    }

    set nbAddresses(value: number | undefined) {
        this._nbAddresses = value;
    }

    get meanComments(): number | undefined {
        return this._meanComments;
    }

    set meanComments(value: number | undefined) {
        this._meanComments = value;
    }

    get noteDivision(): Array<any> {
        return this._noteDivision;
    }

    set noteDivision(value: Array<any>) {
        this._noteDivision = value;
    }

    hydrateFromJson(data: any) {

        this._birthdate = data.birthdate ? new Date(data.birthdate) : undefined;

        this._addresses = (data.addresses ?? []).map((address: any) => new AddressModel().hydrateFromJson(address));
        this._orders = (data.orders ?? []).map((order: any) => new OrderModel().hydrateFromJson(order));
        this._voucher = (data.voucher ?? []).map((voucher: any) => new VoucherModel().hydrateFromJson(voucher));

        this._nbOrders = data.nbOrders ?? undefined;
        this._nbComments = data.nbComments ?? undefined;
        this._nbAddresses = data.nbAddresses ?? undefined;
        this._meanComments = data.meanComments ?? undefined;
        this._noteDivision = data.noteDivision ?? [];

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            birthdate: this?._birthdate?.toISOString() ?? null,
            addresses: (this._addresses ?? []).map(address => full ? address.prepareJSON(full) : address.iri),
            orders: (this._orders ?? []).map(order => full ? order.prepareJSON(full) : order.iri),
            voucher: (this._voucher ?? []).map(voucher => full ? voucher.prepareJSON(full) : voucher.iri),
            nbOrders: this?._nbOrders ?? null,
            nbComments: this?._nbComments ?? null,
            nbAddresses: this?._nbAddresses ?? null,
            meanComments: this?._meanComments ?? null,
            noteDivision: this?._noteDivision ?? null,
        };
    }

}

