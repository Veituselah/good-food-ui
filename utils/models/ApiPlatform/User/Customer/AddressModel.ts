import {ApiPlatformModel, CountryModel, CustomerModel} from "../../../../../internal"

export class AddressModel extends ApiPlatformModel {

    private _slug?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _deleted?: boolean;
    private _customer?: CustomerModel;
    private _name?: string;
    private _additionalAddress?: string;
    private _city?: string;
    private _country?: CountryModel;
    private _phoneNumber?: string;
    private _street?: string;
    private _zipCode?: string;

    get slug(): string | undefined {
        return this._slug;
    }

    get customer(): CustomerModel | undefined {
        return this._customer;
    }

    set customer(value: CustomerModel | undefined) {
        this._customer = value;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get additionalAddress(): string | undefined {
        return this._additionalAddress;
    }

    set additionalAddress(value: string | undefined) {
        this._additionalAddress = value;
    }

    get city(): string | undefined {
        return this._city;
    }

    set city(value: string | undefined) {
        this._city = value;
    }

    get country(): CountryModel | undefined {
        return this._country;
    }

    set country(value: CountryModel | undefined) {
        this._country = value;
    }

    get phoneNumber(): string | undefined {
        return this._phoneNumber;
    }

    set phoneNumber(value: string | undefined) {
        this._phoneNumber = value;
    }

    get street(): string | undefined {
        return this._street;
    }

    set street(value: string | undefined) {
        this._street = value;
    }

    get zipCode(): string | undefined {
        return this._zipCode;
    }

    set zipCode(value: string | undefined) {
        this._zipCode = value;
    }

    hydrateFromJson(data: any) {
        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._deleted = data.deleted ?? false;

        this._customer = data.customer ? new CustomerModel() : undefined;
        if (this._customer) {
            this._customer.hydrateFromJson(data.customer);
        }

        this._name = data.name ?? undefined;
        this._additionalAddress = data.additionalAddress ?? undefined;
        this._city = data.city ?? undefined;

        this._country = data.country ? new CountryModel() : undefined;
        if (this._country) {
            this._country.hydrateFromJson(data.country);
        }

        this._phoneNumber = data.phoneNumber ?? null;
        this._street = data.street ?? null;
        this._zipCode = data.zipCode ?? null;
        super.hydrateFromJson({data});
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            deleted: this._deleted ?? false,
            customer: this._customer ? (full ? this._customer.prepareJSON(full) : this._customer.iri) : null,
            name: this._name ?? null,
            additionalAddress: this._additionalAddress ?? null,
            city: this._city ?? null,
            country: this._country ? (full ? this._country.prepareJSON(full) : this._country.iri) : null,
            phoneNumber: this._phoneNumber ?? null,
            street: this._street ?? null,
            zipCode: this._zipCode ?? null,
        };
    }
}


