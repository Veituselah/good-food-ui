import {ApiPlatformModel} from "../../../../../internal"

export class UserLoginModel extends ApiPlatformModel {

    private _username?: string;
    private _password?: string;

    get username(): string | undefined {
        return this._username;
    }

    set username(value: string | undefined) {
        this._username = value;
    }

    get password(): string | undefined {
        return this._password;
    }

    set password(value: string | undefined) {
        this._password = value;
    }

    hydrateFromJson(data: object): this {
        return this
    }

    prepareJSON(): object {
        return {
            username: this._username,
            password: this._password,
        }
    }
}

