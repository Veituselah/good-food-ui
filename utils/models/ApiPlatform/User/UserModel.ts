import {ApiPlatformModel, Role, UserInterface} from "../../../../internal"

export class UserModel extends ApiPlatformModel implements UserInterface {

    public static ROLES_PARENT: any = {};

    static get ROLE_HIERACHY(): any {
        return {
            ROLE_LOGISTICIAN: [Role.ROLE_FRANCHISE],
            ROLE_RESTORATOR: [Role.ROLE_FRANCHISE],
            ROLE_LEAD: [Role.ROLE_RESTORATOR, Role.ROLE_LOGISTICIAN, Role.ROLE_FRANCHISE],
            ROLE_EDITOR: [Role.ROLE_FRANCHISOR],
            ROLE_ACCOUNTANT: [Role.ROLE_FRANCHISOR],
            ROLE_HR: [Role.ROLE_FRANCHISOR],
            ROLE_ADMIN: [Role.ROLE_LEAD, Role.ROLE_EDITOR, Role.ROLE_ACCOUNTANT, Role.ROLE_HR, Role.ROLE_FRANCHISOR],
            ROLE_SUPER_ADMIN: [Role.ROLE_ADMIN],
        };
    }

    static get FO_ROLES(): string[] {
        return [
            Role.ROLE_CUSTOMER
        ];
    }

    static get BO_ROLES(): string[] {
        return [
            Role.ROLE_EMPLOYEE,
            Role.ROLE_FRANCHISOR,
            Role.ROLE_EDITOR,
            Role.ROLE_ACCOUNTANT,
            Role.ROLE_HR,
            Role.ROLE_ADMIN,
            Role.ROLE_FRANCHISE,
            Role.ROLE_LOGISTICIAN,
            Role.ROLE_RESTORATOR,
            Role.ROLE_LEAD,
            Role.ROLE_SUPER_ADMIN,
        ];
    }

    private _uuid?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active: boolean = false;
    private _deleted: boolean = false;
    private _email?: string;
    private _firstname?: string;
    private _lastname?: string;
    private _plainPassword?: string;
    private _roles: Array<string> = new Array<string>();

    get email(): string | undefined {
        return this._email;
    }

    set email(value: string | undefined) {
        this._email = value;
    }

    get firstname(): string | undefined {
        return this._firstname;
    }

    set firstname(value: string | undefined) {
        this._firstname = value;
    }

    get lastname(): string | undefined {
        return this._lastname;
    }

    set lastname(value: string | undefined) {
        this._lastname = value;
    }

    get plainPassword(): string | undefined {
        return this._plainPassword;
    }

    set plainPassword(value: string | undefined) {
        this._plainPassword = value;
    }

    get roles(): Array<string> {
        return this._roles;
    }

    set roles(value: Array<string>) {
        this._roles = value;
    }

    get uuid(): string | undefined {
        return this._uuid;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean {
        return this._active ?? false;
    }

    get deleted(): boolean {
        return this._deleted ?? false;
    }

    hydrateFromJson(data: any) {
        this._uuid = data.uuid ?? null;
        this._active = data.active ?? null;
        this._deleted = data.deleted ?? null;
        this._email = data.email ?? null;
        this._firstname = data.firstname ?? null;
        this._lastname = data.lastname ?? null;
        this._roles = data.roles ?? null;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;

        super.hydrateFromJson(data);

        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            uuid: this._uuid ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            deleted: this._deleted ?? false,
            email: this._email ?? null,
            firstname: this._firstname ?? null,
            lastname: this._lastname ?? null,
            plainPassword: this._plainPassword ?? null,
            roles: this._roles ?? []
        };
    }

    isGranted(role: string): boolean {
        let parentsRole: string[] = UserModel.ROLES_PARENT[role] ?? this.getParentsRoleInHierarchy(role);
        UserModel.ROLES_PARENT[role] = parentsRole;
        return this.roles.some(searchedRole => parentsRole.indexOf(searchedRole) > -1);
    }

    protected getSubsRoleInHierarchy(role: string): string[] {
        let directSubRoles: string[] = UserModel.ROLE_HIERACHY[role] ?? [];

        let subRoles: string[] = directSubRoles;

        for (let subRoleID in directSubRoles) {
            let subRole = directSubRoles[subRoleID];
            subRoles.concat(this.getSubsRoleInHierarchy(subRole));
        }

        return subRoles;
    }

    protected getParentsRoleInHierarchy(role: string): string[] {

        let parents: Array<string> = new Array<string>();

        for (let roleIndex in UserModel.ROLE_HIERACHY) {
            let subRoles = UserModel.ROLE_HIERACHY[roleIndex];

            if (subRoles.includes(role)) {
                parents = this.getParentsRoleInHierarchy(roleIndex);
            }

        }

        parents.push(role);

        return parents;
    }

    static isCustomer(user: UserModel): boolean {
        return (user?.roles ?? []).some(value => UserModel.FO_ROLES.includes(value));
    }

    static canAccessToAdmin(user: UserModel): boolean {
        return (user?.roles ?? []).some(value => UserModel.BO_ROLES.includes(value));
    }

    canAccessToAdmin(): boolean {
        return UserModel.canAccessToAdmin(this);
    }

    isCustomer(): boolean {
        return UserModel.isCustomer(this);
    }

}

