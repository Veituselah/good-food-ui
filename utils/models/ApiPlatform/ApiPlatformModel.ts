import {ApiPlatformHandler, Message, MessageGravity, Model} from "../../../internal"
import {Dispatch} from "redux";
import {addLog} from "../../../redux/actions/logger";

export abstract class ApiPlatformModel extends Model {

    private _context?: string;
    private _iri?: string;

    hydrateFromJson(data: any) {
        this._context = data['@context'];
        this._iri = data['@id'];
        return this;
    }

    get context(): string | undefined {
        return this._context;
    }

    get iri(): string | undefined {
        return this._iri;
    }

    set iri(iri: string | undefined) {
        this._iri = iri;
    }

    public static showFailedResponseError(error: any, dispatch: Dispatch) {

        const errStr: string | null = this.getFailedError(error);

        if (errStr) {
            dispatch(addLog({
                title: 'Une erreur est survenue',
                message: errStr,
                gravity: MessageGravity.ERROR,
            }))
        }

    }

    public static getFailedError(error: any): string | null {

        console.log(error);

        const data = error?.response?.data ?? {};
        const status = error?.response?.status ?? null;

        if (!status) {
            return error.toString() + ":\n" + error.stackTrace;
        }

        switch (status) {
            case 400:

                if (data[ApiPlatformHandler.HYDRA_DESCRIPTION] && data[ApiPlatformHandler.HYDRA_TITLE_INDEX]) {
                    return data[ApiPlatformHandler.HYDRA_TITLE_INDEX] + ' : ' + data[ApiPlatformHandler.HYDRA_DESCRIPTION];
                }

                return 'Veuillez réessayer plus tard';

            case 401 :
                return data?.message;

            case 403 :
                return 'Vous ne pouvez pas réaliser cette action';

            case 404 :
                return 'La ressource que vous recherchez n\'a pas été trouvée';

            case 500:
                return data?.message;

        }

        return null;

    }

    public static parseConstraintVioliationListIntoInputError(response: any): Array<Message> {

        let data: any = response?.data ?? {};
        let violations: any = data?.violations ?? [];
        let inputErrors: Array<Message> = [];

        for (let violation of violations) {

            inputErrors.push({
                message: violation.message,
                gravity: MessageGravity.ERROR,
                field: violation.propertyPath,
            })

        }

        return inputErrors;

    }
}

