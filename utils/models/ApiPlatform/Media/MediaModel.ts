import {ApiPlatformModel} from "../../../../internal"

export class MediaModel extends ApiPlatformModel {

    private _id?: number;
    private _createdAt?: Date;
    private _contentUrl?: string;
    private _filePath?: string;
    private _mimeType?: string;

    get id(): number | undefined | null {
        return this._id;
    }

    get createdAt(): Date | undefined | null {
        return this._createdAt;
    }

    get contentUrl(): string | undefined | null {
        return this._contentUrl;
    }

    get filePath(): string | undefined | null {
        return this._filePath;
    }

    get mimeType(): string | undefined | null {
        return this._mimeType;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._contentUrl = data.contentUrl ?? undefined;
        this._filePath = data.filePath ?? undefined;
        this._mimeType = data.mimeType ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            contentUrl: this._contentUrl ?? null,
            filePath: this._filePath ?? null,
            mimeType: this._mimeType ?? null,
        };
    }
}

