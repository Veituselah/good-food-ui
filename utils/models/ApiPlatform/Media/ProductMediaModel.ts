import {ApiPlatformModel, MediaModel, ProductModel} from "../../../../internal"

export class ProductMediaModel extends ApiPlatformModel {

    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _media?: MediaModel;
    private _mediaDefault?: boolean;
    private _product?: ProductModel;

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get media(): MediaModel | undefined {
        return this._media;
    }

    set media(value: MediaModel | undefined) {
        this._media = value;
    }

    get mediaDefault(): boolean | undefined {
        return this._mediaDefault;
    }

    set mediaDefault(value: boolean | undefined) {
        this._mediaDefault = value;
    }

    get product(): ProductModel | undefined {
        return this._product;
    }

    set product(value: ProductModel | undefined) {
        this._product = value;
    }

    hydrateFromJson(data: any) {
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._media = data.media ? new MediaModel().hydrateFromJson(data.media) : undefined;
        this._mediaDefault = data.mediaDefault ?? false;
        this._product = data.product ? ProductModel.getProductInstanceAndHydrateFromJSON(data.product) : undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            media: this._media ? (full ? this._media.prepareJSON(full) : this._media.iri) : null,
            mediaDefault: this._mediaDefault ?? false,
            product: this._product ? (full ? this._product.prepareJSON(full) : this._product.iri) : null,
        };
    }
}

