import {
    ApiPlatformModel, CommentModel,
    CountryModel,
    CurrencyModel,
    ExtraGroupModel,
    FranchiseEmployeeModel,
    MediaModel,
    OrderModel,
    ProductModel,
    PurchasableProductModel
} from "../../../../internal"

export class RestaurantModel extends ApiPlatformModel {

    private _slug?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _name?: string;
    private _description?: string;
    private _image?: MediaModel;
    private _latitude?: number;
    private _longitude?: number;
    private _additionalAddress?: string;
    private _city?: string;
    private _country?: CountryModel;
    private _defaultCurrency?: CurrencyModel;
    private _phoneNumber?: string;
    private _street?: string;
    private _zipCode?: string;
    private _employees: Array<FranchiseEmployeeModel> = new Array<FranchiseEmployeeModel>();
    private _leaders: Array<FranchiseEmployeeModel> = new Array<FranchiseEmployeeModel>();
    private _extraGroups: Array<ExtraGroupModel> = new Array<ExtraGroupModel>();
    private _orders: Array<OrderModel> = new Array<OrderModel>();
    private _products: Array<PurchasableProductModel> = new Array<PurchasableProductModel>();
    private _comments: Array<CommentModel> = new Array<CommentModel>();
    private _meanNote?: number;

    get slug(): string | undefined {
        return this._slug;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get deleted(): boolean | undefined {
        return this._deleted;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get description(): string | undefined {
        return this._description;
    }

    set description(value: string | undefined) {
        this._description = value;
    }

    get image(): MediaModel | undefined {
        return this._image;
    }

    set image(value: MediaModel | undefined) {
        this._image = value;
    }

    get latitude(): number | undefined {
        return this._latitude;
    }

    set latitude(value: number | undefined) {
        this._latitude = value;
    }

    get longitude(): number | undefined {
        return this._longitude;
    }

    set longitude(value: number | undefined) {
        this._longitude = value;
    }

    get additionalAddress(): string | undefined {
        return this._additionalAddress;
    }

    set additionalAddress(value: string | undefined) {
        this._additionalAddress = value;
    }

    get city(): string | undefined {
        return this._city;
    }

    set city(value: string | undefined) {
        this._city = value;
    }

    get country(): CountryModel | undefined {
        return this._country;
    }

    set country(value: CountryModel | undefined) {
        this._country = value;
    }

    get defaultCurrency(): CurrencyModel | undefined {
        return this._defaultCurrency;
    }

    set defaultCurrency(value: CurrencyModel | undefined) {
        this._defaultCurrency = value;
    }

    get phoneNumber(): string | undefined {
        return this._phoneNumber;
    }

    set phoneNumber(value: string | undefined) {
        this._phoneNumber = value;
    }

    get street(): string | undefined {
        return this._street;
    }

    set street(value: string | undefined) {
        this._street = value;
    }

    get zipCode(): string | undefined {
        return this._zipCode;
    }

    set zipCode(value: string | undefined) {
        this._zipCode = value;
    }

    get employees(): Array<FranchiseEmployeeModel> {
        return this._employees;
    }

    set employees(value: Array<FranchiseEmployeeModel>) {
        this._employees = value;
    }

    get leaders(): Array<FranchiseEmployeeModel> {
        return this._leaders;
    }

    set leaders(value: Array<FranchiseEmployeeModel>) {
        this._leaders = value;
    }

    get extraGroups(): Array<ExtraGroupModel> {
        return this._extraGroups;
    }

    set extraGroups(value: Array<ExtraGroupModel>) {
        this._extraGroups = value;
    }

    get orders(): Array<OrderModel> {
        return this._orders;
    }

    set orders(value: Array<OrderModel>) {
        this._orders = value;
    }

    get products(): Array<PurchasableProductModel> {
        return this._products;
    }

    set products(value: Array<PurchasableProductModel>) {
        this._products = value;
    }

    get address(): string {
        return this.street + ' ' +
            (this.additionalAddress ? +'(' + this.additionalAddress + ') ' : '') +
            this.zipCode + ' ' +
            this.city;
    }

    get comments(): Array<CommentModel> {
        return this._comments;
    }

    set comments(value: Array<CommentModel>) {
        this._comments = value;
    }

    get meanNote(): number | undefined {
        return this._meanNote;
    }

    set meanNote(value: number | undefined) {
        this._meanNote = value;
    }

    hydrateFromJson(data: any) {

        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._name = data.name ?? undefined;
        this._description = data.description ?? undefined;

        if (data.image) {
            let media = new MediaModel();
            media.hydrateFromJson(data.image);
            this._image = data.image ? media : undefined;
        }

        this._latitude = data.latitude ?? undefined;
        this._longitude = data.longitude ?? undefined;
        this._additionalAddress = data.additionalAddress ?? undefined;
        this._city = data.city ?? undefined;
        this._country = data.country ? new CountryModel().hydrateFromJson(data.country) : undefined;
        this._defaultCurrency = data.defaultCurrency ? new CurrencyModel().hydrateFromJson(data.defaultCurrency) : undefined;
        this._phoneNumber = data.phoneNumber ?? undefined;
        this._street = data.street ?? undefined;
        this._zipCode = data.zipCode ?? undefined;
        this._employees = (data.employees ?? []).map((employee: any) => new FranchiseEmployeeModel().hydrateFromJson(employee));
        this._leaders = (data.leaders ?? []).map((leader: any) => new FranchiseEmployeeModel().hydrateFromJson(leader));
        this._extraGroups = (data.extraGroups ?? []).map((extraGroup: any) => new ExtraGroupModel().hydrateFromJson(extraGroup));
        this._orders = (data.orders ?? []).map((order: any) => new OrderModel().hydrateFromJson(order));
        this._products = (data.products ?? []).map((product: any) => ProductModel.getProductInstanceAndHydrateFromJSON(product));

        this._comments = data.comments ? (data.comments ?? []).map((comment: any) => {
            const com = new CommentModel();
            if(comment instanceof String){
                com.iri = comment.toString();
            } else {
                com.hydrateFromJson(comment);
            }
            return com;
        }) : [];
        this._meanNote = data.meanNote ?? undefined;


        super.hydrateFromJson(data);

        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            deleted: this._deleted ?? false,
            name: this._name ?? null,
            description: this._description ?? null,
            image: this._image ? this.image?.iri : null,
            latitude: this._latitude ?? null,
            longitude: this._longitude ?? null,
            additionalAddress: this._additionalAddress ?? null,
            city: this._city ?? null,
            country: this._country ? (full ? this._country.prepareJSON(full) : this._country.iri) : null,
            defaultCurrency: this._defaultCurrency ? (full ? this._defaultCurrency.prepareJSON(full) : this._defaultCurrency.iri) : null,
            phoneNumber: this._phoneNumber ?? null,
            street: this._street ?? null,
            zipCode: this._zipCode ?? null,
            employees: this._employees ? this._employees.map(employee => full ? employee.prepareJSON(full) : employee.iri) : null,
            leaders: this._leaders ? this._leaders.map(leader => full ? leader.prepareJSON(full) : leader.iri) : null,
            extraGroups: this._extraGroups ? this._extraGroups.map(extraGroup => full ? extraGroup.prepareJSON(full) : extraGroup.iri) : null,
            orders: this._orders ? this._orders.map(order => full ? order.prepareJSON(full) : order.iri) : null,
            products: this._products ? this._products.map(product => full ? product.prepareJSON(full) : product.iri) : null,
            comments: (this._comments ?? []).map((comment: CommentModel) => full ? comment.prepareJSON(full) : comment.iri),
            meanNote: this._meanNote ?? null,
        };
    }
}

