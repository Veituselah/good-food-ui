import {ApiPlatformModel} from "../../../../internal"

export class MailModel extends ApiPlatformModel {

    private _id?: number;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _debug: Array<string> = new Array<string>();
    private _failedAt?: Date;
    private _lastAttemptSendAt?: Date;
    private _addresses: Array<string> = new Array<string>();
    private _content?: string;
    private _sender?: string;
    private _subject?: string;

    get id(): number | undefined {
        return this._id;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get debug(): Array<string> {
        return this._debug;
    }

    get failedAt(): Date | undefined {
        return this._failedAt;
    }

    get lastAttemptSendAt(): Date | undefined {
        return this._lastAttemptSendAt;
    }

    get addresses(): Array<string> {
        return this._addresses;
    }

    set addresses(value: Array<string>) {
        this._addresses = value;
    }

    get content(): string | undefined {
        return this._content;
    }

    set content(value: string | undefined) {
        this._content = value;
    }

    get sender(): string | undefined {
        return this._sender;
    }

    set sender(value: string | undefined) {
        this._sender = value;
    }

    get subject(): string | undefined {
        return this._subject;
    }

    set subject(value: string | undefined) {
        this._subject = value;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._debug = data.debug ?? false;
        this._failedAt = data.failedAt ? new Date(data.failedAt) : undefined;
        this._lastAttemptSendAt = data.lastAttemptSendAt ? new Date(data.lastAttemptSendAt) : undefined;
        this._addresses = data.addresses ?? [];
        this._content = data.content ?? undefined;
        this._sender = data.sender ?? undefined;
        this._subject = data.subject ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            debug: this._debug ?? false,
            failedAt: this._failedAt ? this._failedAt.toISOString() : null,
            lastAttemptSendAt: this._lastAttemptSendAt ? this._lastAttemptSendAt.toISOString() : null,
            addresses: this._addresses ?? [],
            content: this._content ?? null,
            sender: this._sender ?? null,
            subject: this._subject ?? null,
        };
    }
}

