import {DessertModel, DishModel, DrinkModel, EntreeModel, PurchasableProductModel} from "../../../../../../internal"

export class MenuModel extends PurchasableProductModel {

    private _dessertChoices: Array<DessertModel> = new Array<DessertModel>();
    private _dishChoices: Array<DishModel> = new Array<DishModel>();
    private _drinkChoices: Array<DrinkModel> = new Array<DrinkModel>();
    private _entreeChoices: Array<EntreeModel> = new Array<EntreeModel>();

    get dessertChoices(): Array<DessertModel> {
        return this._dessertChoices;
    }

    set dessertChoices(value: Array<DessertModel>) {
        this._dessertChoices = value;
    }

    get dishChoices(): Array<DishModel> {
        return this._dishChoices;
    }

    set dishChoices(value: Array<DishModel>) {
        this._dishChoices = value;
    }

    get drinkChoices(): Array<DrinkModel> {
        return this._drinkChoices;
    }

    set drinkChoices(value: Array<DrinkModel>) {
        this._drinkChoices = value;
    }

    get entreeChoices(): Array<EntreeModel> {
        return this._entreeChoices;
    }

    set entreeChoices(value: Array<EntreeModel>) {
        this._entreeChoices = value;
    }

    hydrateFromJson(data: any) {
        this._dessertChoices = (data.dessertChoices ?? []).map((dessertChoice: any) => new DessertModel().hydrateFromJson(dessertChoice));
        this._dishChoices = (data.dishChoices ?? []).map((dishChoice: any) => new DishModel().hydrateFromJson(dishChoice));
        this._drinkChoices = (data.drinkChoices ?? []).map((drinkChoice: any) => new DrinkModel().hydrateFromJson(drinkChoice));
        this._entreeChoices = (data.entreeChoices ?? []).map((entreeChoice: any) => new EntreeModel().hydrateFromJson(entreeChoice));

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            dessertChoices: (this._dessertChoices ?? []).map((dessertChoice: DessertModel) => full ? dessertChoice.prepareJSON(full) : dessertChoice.iri),
            dishChoices: (this._dishChoices ?? []).map((dishChoice: DishModel) => full ? dishChoice.prepareJSON(full) : dishChoice.iri),
            drinkChoices: (this._drinkChoices ?? []).map((drinkChoice: DrinkModel) => full ? drinkChoice.prepareJSON(full) : drinkChoice.iri),
            entreeChoices: (this._entreeChoices ?? []).map((entreeChoice: EntreeModel) => full ? entreeChoice.prepareJSON(full) : entreeChoice.iri),
        };
    }
}


