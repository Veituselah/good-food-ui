import {FoodModel, MenuModel} from "../../../../../../internal"

export class DessertModel extends FoodModel {

    private _useInMenus: Array<MenuModel> = new Array<MenuModel>();

    get useInMenus(): Array<MenuModel> {
        return this._useInMenus;
    }

    set useInMenus(value: Array<MenuModel>) {
        this._useInMenus = value;
    }

    hydrateFromJson(data: any) {
        this._useInMenus = (data.useInMenus ?? []).map((useInMenu: any) => new MenuModel().hydrateFromJson(useInMenu))
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            useInMenu: (this._useInMenus ?? []).map((useInMenu: MenuModel) => full ? useInMenu.prepareJSON(full) : useInMenu.iri)
        };
    }
}

