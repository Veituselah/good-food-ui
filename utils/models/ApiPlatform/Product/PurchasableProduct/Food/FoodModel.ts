import {ExtraModel, PurchasableProductModel} from "../../../../../../internal";

export class FoodModel extends PurchasableProductModel {

    private _extras: Array<ExtraModel> = new Array<ExtraModel>();

    get extras(): Array<ExtraModel> {
        return this._extras;
    }

    set extras(value: Array<ExtraModel>) {
        this._extras = value;
    }

    hydrateFromJson(data: any) {
        this._extras = (data.extras ?? []).map((extra: any) => new ExtraModel().hydrateFromJson(extra))
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            extra: (this._extras ?? []).map((extra: ExtraModel) => full ? extra.prepareJSON(full) : extra.iri)
        };
    }

    public static getFoodInstanceAndHydrateFromJSON(data: any): FoodModel | undefined {
        switch (data.context) {
            //    TODO : Créer la bonne instance et la retourner
        }
        return undefined;
    }
}

