import {ApiPlatformModel, ExtraValueModel, RestaurantModel} from "../../../../../../../internal"

export class ExtraGroupModel extends ApiPlatformModel {

    private _slug?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _description?: string;
    private _extraValues: Array<ExtraValueModel> = new Array<ExtraValueModel>();
    private _name?: string;
    private _onlyInRestaurants: Array<RestaurantModel> = new Array<RestaurantModel>();
    private _position?: number;

    get slug(): string | undefined {
        return this._slug;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get deleted(): boolean | undefined {
        return this._deleted;
    }

    get description(): string | undefined {
        return this._description;
    }

    set description(value: string | undefined) {
        this._description = value;
    }

    get extraValues(): Array<ExtraValueModel> {
        return this._extraValues;
    }

    set extraValues(value: Array<ExtraValueModel>) {
        this._extraValues = value;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get onlyInRestaurants(): Array<RestaurantModel> {
        return this._onlyInRestaurants;
    }

    set onlyInRestaurants(value: Array<RestaurantModel>) {
        this._onlyInRestaurants = value;
    }

    get position(): number | undefined {
        return this._position;
    }

    set position(value: number | undefined) {
        this._position = value;
    }

    hydrateFromJson(data: any) {
        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._description = data.description ?? undefined;
        this._extraValues = (data.extraValues ?? []).map((extraValue: any) => new ExtraValueModel().hydrateFromJson(extraValue));
        this._onlyInRestaurants = (data.onlyInRestaurants ?? []).map((restaurant: any) => new RestaurantModel().hydrateFromJson(restaurant));
        this._name = data.name ?? undefined;
        this._position = data.position ?? undefined;
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            deleted: this._deleted ?? false,
            description: this._description ?? null,
            extraValues: (this._extraValues ?? []).map((extraValue: any) => full ? extraValue.prepareJSON(full) : extraValue.iri),
            onlyInRestaurants: (this._onlyInRestaurants ?? []).map((restaurant: any) => full ? restaurant?.prepareJSON(full) : restaurant?.iri),
            name: this._name ?? null,
            position: this._position ?? null,
        };
    }
}

