import {ApiPlatformModel, ExtraValueModel, FoodModel} from "../../../../../../../internal"

export class ExtraModel extends ApiPlatformModel {

    private _id?: number;
    private _extraValue?: ExtraValueModel;
    private _multiselect?: boolean;
    private _product?: FoodModel;

    get extraValue(): ExtraValueModel | undefined {
        return this._extraValue;
    }

    set extraValue(value: ExtraValueModel | undefined) {
        this._extraValue = value;
    }

    get multiselect(): boolean | undefined {
        return this._multiselect;
    }

    set multiselect(value: boolean | undefined) {
        this._multiselect = value;
    }

    get product(): FoodModel | undefined {
        return this._product;
    }

    set product(value: FoodModel | undefined) {
        this._product = value;
    }

    get id(): number | undefined {
        return this._id;
    }


    hydrateFromJson(data: any) {
        this._id = data.id ?? null;
        this._extraValue = data.extraValue ?? null;
        this._multiselect = data.multiselect ?? null;
        this._product = data.product ? FoodModel.getFoodInstanceAndHydrateFromJSON(data.product) : undefined;
        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            extraValue: this._extraValue ?? null,
            multiselect: this._multiselect ?? null,
            product: this._product ? (full ? this._product.prepareJSON(full) : this._product.iri) : null,
        };
    }
}

