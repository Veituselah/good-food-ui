import {ApiPlatformModel} from "../../../../../../internal"

export class TagModel extends ApiPlatformModel {

    private _id?: number;
    private _createdAt?: Date;
    private _value?: string;

    get id(): number | undefined {
        return this._id;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get value(): string | undefined {
        return this._value;
    }

    set value(value: string | undefined) {
        this._value = value;
    }

    hydrateFromJson(data: any) {
        this._id = data.id ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._value = data.value ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            id: this._id ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            value: this._value ?? null,
        };
    }
}

