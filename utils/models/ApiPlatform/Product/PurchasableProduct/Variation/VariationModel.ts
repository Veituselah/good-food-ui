import {PurchasableProductModel, VariationValueModel} from "../../../../../../internal"

export class VariationModel extends PurchasableProductModel {

    private _variableProduct?: PurchasableProductModel;
    private _variationsValues: Array<VariationValueModel> = new Array<VariationValueModel>();

    get variableProduct(): PurchasableProductModel | undefined {
        return this._variableProduct;
    }

    set variableProduct(value: PurchasableProductModel | undefined) {
        this._variableProduct = value;
    }

    get variationsValues(): Array<VariationValueModel> {
        return this._variationsValues;
    }

    set variationsValues(value: Array<VariationValueModel>) {
        this._variationsValues = value;
    }

    hydrateFromJson(data: any) {
        this._variableProduct = data.variableProduct ? PurchasableProductModel.getPurchasableProductInstanceAndHydrateFromJSON(data.variableProduct) : undefined;
        this._variationsValues = (data.variationsValues ?? []).map((variationValue: any) => new VariationValueModel().hydrateFromJson(variationValue));

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            variableProduct: this._variableProduct ? this._variableProduct.iri : null,
            variationsValues: this._variationsValues ? this._variationsValues.map((variationValue: VariationValueModel) => full ? variationValue.prepareJSON(full) : variationValue.iri) : null,
        };
    }
}

