import {
    CategoryModel,
    CommentModel,
    CurrencyModel,
    ProductModel,
    TagModel, TaxTypeModel,
    VariationModel
} from "../../../../../internal"

export class PurchasableProductModel extends ProductModel {

    private _availableFrom?: Date;
    private _availableTo?: Date;
    private _categories: Array<CategoryModel> = new Array<CategoryModel>();
    private _comments: Array<CommentModel> = new Array<CommentModel>();
    private _meanNote?: number;
    private _currency?: CurrencyModel;
    private _taxType?: TaxTypeModel;
    private _ecotax?: number;
    private _minQtyInStockToSell?: number;
    private _onSale?: boolean;
    private _priceTaxExcluded?: number;
    private _price?: number;
    private _mainCategory?: string;
    private _tags: Array<TagModel> = new Array<TagModel>();
    private _variations: Array<VariationModel> = new Array<VariationModel>();

    get availableFrom(): Date | undefined {
        return this._availableFrom;
    }

    set availableFrom(value: Date | undefined) {
        this._availableFrom = value;
    }

    get availableTo(): Date | undefined {
        return this._availableTo;
    }

    set availableTo(value: Date | undefined) {
        this._availableTo = value;
    }

    get categories(): Array<CategoryModel> {
        return this._categories;
    }

    set categories(value: Array<CategoryModel>) {
        this._categories = value;
    }

    get currency(): CurrencyModel | undefined {
        return this._currency;
    }

    set currency(value: CurrencyModel | undefined) {
        this._currency = value;
    }

    get taxType(): TaxTypeModel | undefined {
        return this._taxType;
    }

    set taxType(value: TaxTypeModel | undefined) {
        this._taxType = value;
    }

    get ecotax(): number | undefined {
        return this._ecotax;
    }

    set ecotax(value: number | undefined) {
        this._ecotax = value;
    }

    get minQtyInStockToSell(): number | undefined {
        return this._minQtyInStockToSell;
    }

    set minQtyInStockToSell(value: number | undefined) {
        this._minQtyInStockToSell = value;
    }

    get onSale(): boolean | undefined {
        return this._onSale;
    }

    set onSale(value: boolean | undefined) {
        this._onSale = value;
    }

    get priceTaxExcluded(): number | undefined {
        return this._priceTaxExcluded;
    }

    set priceTaxExcluded(value: number | undefined) {
        this._priceTaxExcluded = value;
    }

    get price(): number | undefined {
        return this._price;
    }

    set price(value: number | undefined) {
        this._price = value;
    }

    get tags(): Array<TagModel> {
        return this._tags;
    }

    set tags(value: Array<TagModel>) {
        this._tags = value;
    }

    get variations(): Array<VariationModel> {
        return this._variations;
    }

    set variations(value: Array<VariationModel>) {
        this._variations = value;
    }

    get comments(): Array<CommentModel> {
        return this._comments;
    }

    set comments(value: Array<CommentModel>) {
        this._comments = value;
    }

    get meanNote(): number | undefined {
        return this._meanNote;
    }

    set meanNote(value: number | undefined) {
        this._meanNote = value;
    }

    get mainCategory(): string | undefined {
        return this._mainCategory;
    }

    set mainCategory(value: string | undefined) {
        this._mainCategory = value;
    }

    public static getPurchasableProductInstanceAndHydrateFromJSON(data: any): PurchasableProductModel | undefined {
        switch (data.context) {
            //    TODO : Créer la bonne instance et la retourner
        }
        return undefined;
    }

    hydrateFromJson(data: any) {
        this._availableFrom = data.availableFrom ? new Date(data.availableFrom) : undefined;
        this._availableTo = data.availableTo ? new Date(data.availableTo) : undefined;

        this._categories = data.categories ? (this._categories ?? []).map((category: any) => {
            const cat = new CategoryModel();
            cat.hydrateFromJson(category);
            return cat;
        }) : [];

        this._comments = data.comments ? (this._comments ?? []).map((comment: any) => {
            const com = new CommentModel();
            com.hydrateFromJson(comment);
            return com;
        }) : [];
        this._meanNote = data.meanNote ?? undefined;

        this._currency = data.currency ? new CurrencyModel() : undefined;
        if (this._currency) {
            this._currency.hydrateFromJson(data.currency);
        }

        this._taxType = data.taxType ? new TaxTypeModel() : undefined;
        if (this._taxType) {
            this._taxType.hydrateFromJson(data.taxType);
        }

        this._ecotax = data.ecotax ?? undefined;
        this._minQtyInStockToSell = data.minQtyInStockToSell ?? undefined;
        this._onSale = data.onSale ?? undefined;
        this._priceTaxExcluded = data.priceTaxExcluded ?? undefined;
        this._price = data.price ?? undefined;
        this._mainCategory = data.mainCategory ?? undefined;
        this._tags = data.tags ? (data.tags ?? []).map((tag: any) => new TagModel().hydrateFromJson(tag)) : [];
        this._variations = data.variations ? (data.variations ?? []).map((variation: any) => new VariationModel().hydrateFromJson(variation)) : [];

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            ...super.prepareJSON(full),
            availableFrom: this._availableFrom ? this._availableFrom.toISOString() : null,
            availableTo: this._availableTo ? this._availableTo.toISOString() : null,
            categories: (this._categories ?? []).map((category: CategoryModel) => full ? category.prepareJSON(full) : category.iri),
            currency: this._currency ? (full ? this._currency.prepareJSON(full) : this._currency.iri) : null,
            taxType: this._taxType ? (full ? this._taxType.prepareJSON(full) : this._taxType.iri) : null,
            ecotax: this._ecotax ?? null,
            minQtyInStockToSell: this._minQtyInStockToSell ?? null,
            onSale: this._onSale ?? null,
            priceTaxExcluded: this._priceTaxExcluded ?? null,
            price: this._price ?? null,
            tags: (this._tags ?? []).map((tag: TagModel) => full ? tag.prepareJSON(full) : tag.iri),
            variations: (this._variations ?? []).map((variation: VariationModel) => full ? variation.prepareJSON(full) : variation.iri),
            comments: (this._comments ?? []).map((comment: CommentModel) => full ? comment.prepareJSON(full) : comment.iri),
            meanNote: this._meanNote ?? null,
            mainCategory: this._mainCategory ?? null,
        };
    }
}

