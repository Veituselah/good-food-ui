import {ApiPlatformModel, MediaModel, PurchasableProductModel} from "../../../../../internal"

export class CategoryModel extends ApiPlatformModel {

    private _slug?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _children: Array<CategoryModel> = new Array<CategoryModel>();
    private _image?: MediaModel;
    private _name?: string;
    private _parent?: CategoryModel;
    private _products: Array<PurchasableProductModel> = new Array<PurchasableProductModel>();

    get slug(): string | undefined {
        return this._slug;
    }

    get children(): Array<CategoryModel> {
        return this._children;
    }

    set children(value: Array<CategoryModel>) {
        this._children = value;
    }

    get image(): MediaModel | undefined {
        return this._image;
    }

    set image(value: MediaModel | undefined) {
        this._image = value;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get parent(): CategoryModel | undefined {
        return this._parent;
    }

    set parent(value: CategoryModel | undefined) {
        this._parent = value;
    }

    get products(): Array<PurchasableProductModel> {
        return this._products;
    }

    set products(value: Array<PurchasableProductModel>) {
        this._products = value;
    }

    hydrateFromJson(data: any) {

        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._name = data.name ?? undefined;
        this._children = (data.children ?? []).map((category: any) => new CategoryModel().hydrateFromJson(category));
        this._image = data.image ? new MediaModel().hydrateFromJson(data.image) : undefined;
        this._parent = data.parent ? new CategoryModel().hydrateFromJson(data.parent) : undefined;
        this._products = (data.products ?? []).map((product: any) => PurchasableProductModel.getPurchasableProductInstanceAndHydrateFromJSON(product));

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            deleted: this._deleted ?? false,
            children: (this._children ?? []).map((category: any) => full ? category.prepareJSON(full) : category.iri),
            image: this._image ? (full ? this._image.prepareJSON(full) : this._image.iri) : null,
            parent: this._parent ? (full ? this._parent.prepareJSON(full) : this._parent.iri) : null,
            products: (this._products ?? []).map((product: any) => full ? product.prepareJSON(full) : product.iri),
        };
    }

}

