import {ApiPlatformModel, ProductSupplierModel} from "../../../../../internal"

export class SupplierModel extends ApiPlatformModel {

    private _slug?: string;
    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _name?: string;
    private _products: Array<ProductSupplierModel> = new Array<ProductSupplierModel>();

    get slug(): string | undefined {
        return this._slug;
    }

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get active(): boolean | undefined {
        return this._active;
    }

    get deleted(): boolean | undefined {
        return this._deleted;
    }

    get name(): string | undefined {
        return this._name;
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get products(): Array<ProductSupplierModel> {
        return this._products;
    }

    set products(value: Array<ProductSupplierModel>) {
        this._products = value;
    }

    hydrateFromJson(data: any) {
        this._slug = data.slug ?? undefined;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._name = data.name ?? null;
        this._products = (data.products ?? []).map((product: any) => new ProductSupplierModel().hydrateFromJson(product));

        super.hydrateFromJson(data);
return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            deleted: this._deleted ?? false,
            name: this._name ?? null,
            products: (this._products ?? []).map((product: ProductSupplierModel) => full ? product.prepareJSON(full) : product.iri)
        };
    }
}


