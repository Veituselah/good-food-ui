import {
    ApiPlatformModel,
    CurrencyModel,
    ProductModel,
    PurchasableProductModel,
    SupplierModel
} from "../../../../../internal"

export class ProductSupplierModel extends ApiPlatformModel {

    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _currency?: CurrencyModel;
    private _product?: ProductModel;
    private _supplier?: SupplierModel;
    private _supplierPrixTaxExcluded?: number;
    private _supplierReference?: string;

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get currency(): CurrencyModel | undefined {
        return this._currency;
    }

    set currency(value: CurrencyModel | undefined) {
        this._currency = value;
    }

    get product(): ProductModel | undefined {
        return this._product;
    }

    set product(value: ProductModel | undefined) {
        this._product = value;
    }

    get supplier(): SupplierModel | undefined {
        return this._supplier;
    }

    set supplier(value: SupplierModel | undefined) {
        this._supplier = value;
    }

    get supplierPrixTaxExcluded(): number | undefined {
        return this._supplierPrixTaxExcluded;
    }

    set supplierPrixTaxExcluded(value: number | undefined) {
        this._supplierPrixTaxExcluded = value;
    }

    get supplierReference(): string | undefined {
        return this._supplierReference;
    }

    set supplierReference(value: string | undefined) {
        this._supplierReference = value;
    }

    hydrateFromJson(data: any) {
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;

        this._currency = data.currency ? new CurrencyModel() : undefined;
        if (this._currency) {
            this._currency.hydrateFromJson(data.currency);
        }

        this._product = data.product ? PurchasableProductModel.getProductInstanceAndHydrateFromJSON(data.product) : undefined;

        this._supplier = data.supplier ? new SupplierModel() : undefined;
        if (this._supplier) {
            this._supplier.hydrateFromJson(data.supplier);
        }

        this._supplierPrixTaxExcluded = data.supplierPrixTaxExcluded ?? undefined;
        this._supplierReference = data.supplierReference ?? undefined;

        super.hydrateFromJson(data);
return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            currency: this._currency ? (full ? this._currency.prepareJSON(full) : this._currency.iri) : null,
            product: this._product ? (full ? this._product.prepareJSON(full) : this._product.iri) : null,
            supplier: this._supplier ? (full ? this._supplier.prepareJSON(full) : this._supplier.iri) : null,
            supplierPrixTaxExcluded: this._supplierPrixTaxExcluded ?? null,
            supplierReference: this._supplierReference ?? null,
        };
    }
}


