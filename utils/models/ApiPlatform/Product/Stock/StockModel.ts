import {ApiPlatformModel, ProductModel, RestaurantModel} from "../../../../../internal"

export class StockModel extends ApiPlatformModel {

    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _lowStockAlert?: number;
    private _product?: ProductModel;
    private _qty?: number;
    private _restaurant?: RestaurantModel;

    get createdAt(): Date | undefined {
        return this._createdAt;
    }

    get updatedAt(): Date | undefined {
        return this._updatedAt;
    }

    get lowStockAlert(): number | undefined {
        return this._lowStockAlert;
    }

    set lowStockAlert(value: number | undefined) {
        this._lowStockAlert = value;
    }

    get product(): ProductModel | undefined {
        return this._product;
    }

    set product(value: ProductModel | undefined) {
        this._product = value;
    }

    get qty(): number | undefined {
        return this._qty;
    }

    set qty(value: number | undefined) {
        this._qty = value;
    }

    get restaurant(): RestaurantModel | undefined {
        return this._restaurant;
    }

    set restaurant(value: RestaurantModel | undefined) {
        this._restaurant = value;
    }

    hydrateFromJson(data: any) {
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._lowStockAlert = data.lowStockAlert ?? undefined;
        this._product = data.product ? ProductModel.getProductInstanceAndHydrateFromJSON(data.product) : undefined;
        this._qty = data.qty ?? undefined;
        this._restaurant = data.restaurant ?? undefined;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            lowStockAlert: this._lowStockAlert ?? null,
            product: this._product ? (full ? this._product.prepareJSON(full) : this._product.iri) : null,
            qty: this._qty ?? null,
            restaurant: this._restaurant ? (full ? this._restaurant?.prepareJSON(full) : this._restaurant?.iri) : null,
        };
    }
}

