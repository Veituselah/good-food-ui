import {ApiPlatformModel, ProductMediaModel, RestaurantModel, SupplierModel} from "../../../../internal"

export abstract class ProductModel extends ApiPlatformModel {

    private _createdAt?: Date;
    private _updatedAt?: Date;
    private _active?: boolean;
    private _deleted?: boolean;
    private _slug?: string;
    private _reference?: string;
    private _depth?: number;
    private _description?: string;
    private _ean13?: string;
    private _height?: number;
    private _medias: Array<ProductMediaModel> = new Array<ProductMediaModel>();
    private _name?: string;
    private _onlyInRestaurants: Array<RestaurantModel> = new Array<RestaurantModel>();
    private _suppliers: Array<SupplierModel> = new Array<SupplierModel>();
    private _upc?: string;
    private _weight?: number;
    private _width?: number;

    get slug(): string | undefined  {
        return this._slug;
    }

    get reference(): string {
        return this._reference ?? '';
    }

    set reference(value: string | undefined)  {
        this._reference = value;
    }

    get depth(): number  {
        return this._depth ?? 0;
    }

    set depth(value: number | undefined) {
        this._depth = value;
    }

    get description(): string  {
        return this._description ?? '';
    }

    set description(value: string | undefined) {
        this._description = value;
    }

    get ean13(): string  {
        return this._ean13 ?? '';
    }

    set ean13(value: string | undefined) {
        this._ean13 = value;
    }

    get height(): number  {
        return this._height ?? 0;
    }

    set height(value: number | undefined) {
        this._height = value;
    }

    get medias(): Array<ProductMediaModel> {
        return this._medias;
    }

    set medias(value: Array<ProductMediaModel>) {
        this._medias = value;
    }

    get name(): string  {
        return this._name ?? '';
    }

    set name(value: string | undefined) {
        this._name = value;
    }

    get onlyInRestaurants(): Array<RestaurantModel> {
        return this._onlyInRestaurants;
    }

    set onlyInRestaurants(value: Array<RestaurantModel>) {
        this._onlyInRestaurants = value;
    }

    get suppliers(): Array<SupplierModel> {
        return this._suppliers;
    }

    set suppliers(value: Array<SupplierModel>) {
        this._suppliers = value;
    }

    get upc(): string  {
        return this._upc ?? '';
    }

    set upc(value: string | undefined) {
        this._upc = value;
    }

    get weight(): number  {
        return this._weight ?? 0;
    }

    set weight(value: number | undefined) {
        this._weight = value;
    }

    get width(): number  {
        return this._width ?? 0;
    }

    set width(value: number | undefined) {
        this._width = value;
    }

    public static getProductInstanceAndHydrateFromJSON(data: any): ProductModel | undefined {
        switch (data.context) {
            //    TODO : Créer la bonne instance et la retourner
        }

        return undefined;
    }

    hydrateFromJson(data: any) {

        this._slug = data.slug ?? null;
        this._createdAt = data.createdAt ? new Date(data.createdAt) : undefined;
        this._updatedAt = data.updatedAt ? new Date(data.updatedAt) : undefined;
        this._active = data.active ?? false;
        this._deleted = data.deleted ?? false;
        this._reference = data.reference ?? null;
        this._depth = data.depth ?? null;
        this._description = data.description ?? null;
        this._ean13 = data.ean13 ?? null;
        this._height = data.height ?? null;
        this._medias = (data.medias ?? []).map((media: any) => new ProductMediaModel().hydrateFromJson(media));
        this._name = data.name ?? null;
        this._onlyInRestaurants = (data.onlyInRestaurants ?? []).map((restaurant: any) => new RestaurantModel().hydrateFromJson(restaurant));
        this._suppliers = (data.suppliers ?? []).map((supplier: any) => new SupplierModel().hydrateFromJson(supplier));
        this._upc = data.upc ?? null;
        this._weight = data.weight ?? null;
        this._width = data.width ?? null;

        super.hydrateFromJson(data);
        return this;
    }

    prepareJSON(full: boolean): object {
        return {
            iri: this.iri ?? null,
            slug: this._slug ?? null,
            createdAt: this._createdAt ? this._createdAt.toISOString() : null,
            updatedAt: this._updatedAt ? this._updatedAt.toISOString() : null,
            active: this._active ?? false,
            deleted: this._deleted ?? false,
            reference: this._reference ?? null,
            depth: this._depth ?? null,
            description: this._description ?? null,
            ean13: this._ean13 ?? null,
            height: this._height ?? null,
            medias: (this._medias ?? []).map((media: ProductMediaModel) => full ? media.prepareJSON(full) : media.iri),
            name: this._name ?? null,
            onlyInRestaurants: (this._onlyInRestaurants ?? []).map((restaurant: RestaurantModel) => full ? restaurant?.prepareJSON(full) : restaurant?.iri),
            suppliers: (this._suppliers ?? []).map((supplier: SupplierModel) => full ? supplier.prepareJSON(full) : supplier.iri),
            upc: this._upc ?? null,
            weight: this._weight ?? null,
            width: this._width ?? null,

        };
    }


}

