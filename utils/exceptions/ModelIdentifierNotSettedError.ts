export class ModelIdentifierNotSettedError extends Error {

    constructor() {
        super("The identifier of the object is not setted");
    }

}