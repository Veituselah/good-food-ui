export class NonExistantEndpointError extends Error {

    constructor(endpoint: string) {
        super("Non-existant endpoint" + (endpoint ? " '" + endpoint + "'" : ''));
    }

}

