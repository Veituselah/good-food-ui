import {ApiPlatformFilter} from "../../../internal";

export class OrderFilter extends ApiPlatformFilter {

    constructor(
        fieldName: string,
        private _type = OrderFilterTypeEnum.ASC
    ) {
        super(fieldName);
    }

    get type(): OrderFilterTypeEnum {
        return this._type;
    }

    switchOrderFilterType() {
        if (this._type === OrderFilterTypeEnum.ASC) {
            this._type = OrderFilterTypeEnum.DESC;
        } else {
            this._type = OrderFilterTypeEnum.ASC;
        }
    }

    toString(): string {
        return 'order[' + this._fieldName + ']=' + this._type;
    }
}

export enum OrderFilterTypeEnum {
    ASC = 'asc',
    DESC = 'desc'
}