import {ApiPlatformFilter} from "../../../internal";

export class SearchFilter extends ApiPlatformFilter {

    constructor(
        fieldName: string,
        protected _value?: any
    ) {
        super(fieldName);
    }

    get value(): any {
        return this._value;
    }

    set value(value: any) {
        this._value = value;
    }

    toString(): string {
        let str = '';

        if (this._value) {
            if (Array.isArray(this._value)) {

                for (let value of this._value) {
                    str += (str ? '&' : '') + this._fieldName + '[]=' + value;
                }

            } else if (this._value instanceof Object) {

                for (let filter in this._value) {

                    let value = this._value[filter];

                    str += (str ? '&' : '') + this._fieldName + '[' + filter + ']=' + value;
                }

            } else {
                str = this._fieldName + '=' + this._value;
            }
        }

        return str;
    }
}