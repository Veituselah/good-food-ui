import {ApiPlatformFilter} from "../../../internal";

export class ExistsFilter extends ApiPlatformFilter {

    constructor(
        fieldName: string,
        private _exists: boolean
    ) {
        super(fieldName);
    }

    get exists(): boolean {
        return this._exists;
    }

    set exists(exists: boolean) {
        this._exists = exists;
    }

    toString(): string {
        return 'exists[' + this._fieldName + ']=' + (this._exists ? 'true' : 'false');
    }
}
