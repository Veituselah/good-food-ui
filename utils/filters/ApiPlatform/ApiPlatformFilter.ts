import {Filter} from "../../../internal";

export abstract class ApiPlatformFilter extends Filter {

    constructor(
        protected _fieldName: string
    ) {
        super();
    }

    get fieldName(): string {
        return this._fieldName;
    }

    public abstract toString(): string;
}