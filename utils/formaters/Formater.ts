export abstract class Formater {

    abstract format(value: any, params: any): string;

    abstract canBeFormated(value: any): boolean;

}