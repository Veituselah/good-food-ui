import {Formater} from "../../internal";

export class DateFormater extends Formater {

    format(dateValue: string | Date, params = []) {

        let date;

        if (typeof dateValue === 'string') {
            date = new Date(dateValue);
        } else {
            date = dateValue;
        }

        const dayUTC = ("0" + date.getUTCDate()).slice(-2);;
        const monthUTC = ("0" + date.getUTCMonth()).slice(-2);;
        const yearUTC = date.getUTCFullYear();

        const hour = ("0" + date.getUTCHours()).slice(-2);
        const minutes = ("0" + date.getUTCMinutes()).slice(-2);

        return `${dayUTC}/${monthUTC}/${yearUTC} ${hour}:${minutes} UTC`;

        // let day = date.getDate();
        // let month = date.getMonth();
        // let year = date.getFullYear();
        // let hours = date.getHours();
        // let minutes = date.getMinutes();
        // let secondes = date.getSeconds();
        //
        // return (day < 10 ? '0' : '') + day + '/' +
        //     (month < 10 ? '0' : '') + month + '/' +
        //     year + ' ' +
        //     (hours < 10 ? '0' : '') + hours + ':' +
        //     (minutes < 10 ? '0' : '') + minutes + ':' +
        //     (secondes < 10 ? '0' : '') + secondes;

    }

    canBeFormated(value: any): boolean {
        return value instanceof Date || (
            isNaN(value) &&
            !isNaN(
                Date.parse(value)
            )
        );
    }

}