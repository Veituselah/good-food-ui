import {CountryModel, Formater} from "../../internal";

type Params = {
    country: CountryModel
}

export class PhoneFormater extends Formater {

    public static readonly REGEX_E164 = /^\+?[1-9]\d{1,14}$/;
    public static readonly REPLACE_PREFIX_BY = '0';

    format(
        value: string,
        params: Params
    ): string {
        return value.replace(params?.country?.phonePrefix ?? '+33', PhoneFormater.REPLACE_PREFIX_BY).replace(/(.{2})/g, "$1 ");
    }

    public canBeFormated(value: string): boolean {
        return new RegExp(PhoneFormater.REGEX_E164).test(value);
    }

}