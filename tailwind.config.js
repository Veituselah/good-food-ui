module.exports = {
    important: true,
    // Active dark mode on class basis
    darkMode: 'class',
    i18n: {
        locales: ['fr-FR'],
        defaultLocale: 'fr-FR',
    },
    purge: {
        content: ['./pages/**/*.tsx', './components/**/*.tsx'],
        // These options are passed through directly to PurgeCSS
    },
    theme: {
        extend: {
            backgroundImage: (theme) => ({
                check: "url('/icons/check.svg')",
                landscape: "url('/images/landscape/2.jpg')",
            }),
            height: theme => ({
                "screen/1.5": "65vh",
                "screen/2": "50vh",
                "screen/3": "calc(100vh / 3)",
                "screen/4": "calc(100vh / 4)",
                "screen/5": "calc(100vh / 5)",
            }),
            colors: {
                org: {
                    50: "#FFF7EB",
                    100: "#FFF1DB",
                    200: "#FFE0B3",
                    300: "#FFCE85",
                    400: "#FFB84D",
                    500: "#FF9B00",
                    600: "#EB8D00",
                    700: "#CC7A00",
                    800: "#A86500",
                    900: "#7A4900"
                },
                gre: {
                    50: "#F7FFF0",
                    100: "#EFFFE0",
                    200: "#E2FFC7",
                    300: "#D2FFA8",
                    400: "#C2FF8A",
                    500: "#B4FF6E",
                    600: "#8EFF24",
                    700: "#6ADB00",
                    800: "#479400",
                    900: "#234700"
                },
                neutral: {
                    50: "#F2F2F2",
                    100: "#E8E8E8",
                    200: "#CCCCCC",
                    300: "#ADADAD",
                    400: "#858585",
                    500: "#333333",
                    600: "#303030",
                    700: "#262626",
                    800: "#1A1A1A",
                    900: "#1A1A1A"
                }
            }
        },
    },
    variants: {
        extend: {
            backgroundColor: ['dark'],
            borderColor: ['checked'],
            display: ['dark'],
            inset: ['checked'],
            scrollbar: ['dark'],
            zIndex: ['hover', 'active'],
        },
    },
    plugins: [
        require("tailwindcss-scrollbar")
    ],
    future: {
        purgeLayersByDefault: true,
    },
};