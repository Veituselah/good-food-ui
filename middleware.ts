import {NextRequest, NextResponse} from "next/server";
import {Context} from "./internal";

let response: NextResponse;
let request: NextRequest;
let pathUrl: string;
let context: Context;

export function middleware(req: NextRequest) {

    request = req;
    response = NextResponse.next();
    pathUrl = req.nextUrl.pathname;
    context = new Context(true, req, response);

    if (pathUrl.startsWith('/admin')) {
        return adminMiddleware();
    }

    if (pathUrl.startsWith('/invoice')) {
        return mixMiddleware();
    }

    if (
        pathUrl.startsWith('/cart') ||
        pathUrl.startsWith('/account')
    ) {
        return customerMiddleware();
    }

}

export const config = {
    matcher: [
        '/admin/:path*',
        '/cart/:path*',
        '/account/:path*',
        '/invoice/:path*',
    ]
}

function redirect(url: string) {
    return NextResponse.redirect(
        new URL(url, request.url)
    );
}

function isAdmin() {
    return context.currentUserCanAccessToAdmin() && context.getAdmin();
}

function isCustomer() {
    return context.currentUserIsCustomer() && context.getCustomer();
}

function adminMiddleware() {

    if (!isAdmin()) {
        return redirect('/');
    }

}

function customerMiddleware() {

    if (!isCustomer()) {
        return redirect('/');
    }

}

function mixMiddleware() {

    if (!isCustomer() && !isAdmin()) {
        return redirect('/');
    }

}
