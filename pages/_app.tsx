import App, {AppProps} from 'next/app';
import '../global.css';
import {Context, CustomContext, MessageGravity} from "../internal";
import {wrapper} from "../redux/store";
import {useDispatch} from "react-redux";
import {loginAdmin, loginCustomer, logoutAdmin, logoutCustomer} from "../redux/actions/context";
import {useEffect} from "react";
import {useRouter} from "next/router";
import {addLog} from "../redux/actions/logger";
import HttpsRedirect from 'react-https-redirect';

let context = new Context(false);

function MyApp({Component, pageProps}: AppProps) {

    const dispatch = useDispatch();

    const router = useRouter();

    useEffect(() => {

        if (context) {

            if (context.getCustomer()) {
                dispatch(loginCustomer(context.getCustomerToken() as string))
            } else if(context.getCustomerToken()){

                dispatch(addLog({
                    gravity: MessageGravity.INFO,
                    message: 'Votre session est arrivé à expiration, veuillez vous reconnectez'
                }))
                router.reload();
                dispatch(logoutCustomer(context));
            }

            if (context.getAdmin()) {
                dispatch(loginAdmin(context.getAdminToken() as string))
            } else if(context.getAdminToken()){
                router.reload();
                dispatch(logoutAdmin(context))
            }

        }

    }, [])


    return (
        <HttpsRedirect>
        <CustomContext.Provider value={context}>
            <Component {...pageProps} />
        </CustomContext.Provider>
        </HttpsRedirect>
    );
}

MyApp.getInitialProps = async (appContext: any) => {

    const appProps = await App.getInitialProps(appContext);

    if (appContext.ctx.req && appContext.ctx.res) {
        context = new Context(true, appContext.ctx.req, appContext.ctx.res);
    }

    return {
        ...appProps
    };
}

export default wrapper.withRedux(MyApp)
