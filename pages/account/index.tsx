import HomeLayout from '../../components/layouts/HomeLayout'
import CardAddresses from '../../components/elements/cards/CardAddresses'
import CardOrders from '../../components/elements/cards/CardOrders'
import CardProfile from '../../components/elements/cards/CardProfile'
import CardReviews from '../../components/elements/cards/CardReviews'
import FormUserData from '../../components/forms/FormUserData'
import {Context} from "../../utils/middlewares/Context/Context";
import jwtDecode from "jwt-decode";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {CountryHandler} from "../../utils/handlers/ApiPlatform/Country/CountryHandler";
import {OrderFilter, OrderFilterTypeEnum} from "../../utils/filters/ApiPlatform/OrderFilter";
import {useDispatch} from "react-redux";
import {setProfileCustomer} from "../../redux/actions/profile";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import CardReservations from "../../components/elements/cards/CardReservations";

type Props = {
    customer: any,
    errors: any,
    countries?: any,
}

const AccountPage = ({customer, countries, errors}: Props) => {

    const dispatch = useDispatch();

    dispatch(setProfileCustomer(customer))

    if (errors && errors?.length > 0) {
        for (let error of errors) {
            dispatch(addLog({
                gravity: MessageGravity.ERROR,
                message: error
            }))
        }
    }

    return (
        <HomeLayout>
            <div className="flex flex-wrap flex-col lg:flex-row w-full xs:p-4">
                <div className="flex flex-wrap flex-col w-full lg:w-1/2 pt-4 md:p-4 space-y-4">
                    <CardProfile/>
                    <FormUserData/>
                    <CardReservations/>
                </div>
                <div className="flex flex-wrap flex-col w-full lg:w-1/2 py-4 md:p-4 space-y-4">
                    <CardOrders/>
                    <CardAddresses countries={countries}/>
                    <CardReviews/>
                </div>
            </div>
        </HomeLayout>
    );
};

export const getServerSideProps = async (appContext: any) => {

    let context = new Context(false);

    if (appContext.req && appContext.res) {
        context = new Context(true, appContext.req, appContext.res);
    }

    let customerToken = context.getCustomerToken();

    let props: any = {
        errors: []
    }

    if (customerToken) {

        await getCustomerData(customerToken, props);
        await getCountries(customerToken, props);

    }

    return {
        props
    }
}

const getIriFromToken = (customerToken: string): string => {
    const customerData: any = jwtDecode(customerToken);
    const customerHandler = new CustomerHandler(customerToken);
    return customerHandler.buildIri(customerData.uuid);
}

export const getCustomerData = async (customerToken: string, props: any) => {

    const customerHandler = new CustomerHandler(customerToken);
    const iri = getIriFromToken(customerToken);

    try {

        const customer = await customerHandler.get(iri);
        props.customer = customer.prepareJSON(true);

    } catch (e: any) {
        props.errors.push(ApiPlatformModel.getFailedError(e));
    }

}

export const getCustomerAddresses = async (customerToken: string, props: any) => {

    const customerHandler = new CustomerHandler(customerToken);
    const iri = getIriFromToken(customerToken);

    try {

        const addresses = await customerHandler.getCustomerAddresses(iri);
        props.addresses = addresses.prepareJSON(true);

    } catch (e: any) {
        props.errors.push(ApiPlatformModel.getFailedError(e));
    }

    await getCountries(customerToken, props);

}

export const getCountries = async (customerToken: string, props: any) => {

    const countryHandler = new CountryHandler(customerToken);
    const orderFilter = new OrderFilter('name', OrderFilterTypeEnum.ASC);

    try {

        let countries = await countryHandler.getAll([orderFilter], [], 1, true);
        props.countries = countries.prepareJSON(true);

    } catch (e: any) {
        props.errors.push(ApiPlatformModel.getFailedError(e));
    }

}

export const getCustomerOrders = async (customerToken: string, props: any) => {

    const customerHandler = new CustomerHandler(customerToken);
    const iri = getIriFromToken(customerToken);

    try {

        const orders = await customerHandler.getCustomerOrders(iri);
        props.orders = orders.prepareJSON(true);

    } catch (e: any) {
        props.errors.push(ApiPlatformModel.getFailedError(e));
    }

}

export const getCustomerComments = async (customerToken: string, props: any) => {

    const customerHandler = new CustomerHandler(customerToken);
    const iri = getIriFromToken(customerToken);

    try {

        const comments = await customerHandler.getCustomerComments(iri);
        props.comments = comments.prepareJSON(true);

    } catch (e: any) {
        props.errors.push(ApiPlatformModel.getFailedError(e));
    }

}

export default AccountPage;
export const ACCOUNT_PATH = '/account';