import {FC, useEffect, useState} from 'react';
import Button from '../../components/elements/buttons/Button'
import FormCartValidation from '../../components/forms/FormCartValidation'
import FormPayment from '../../components/forms/FormPayment'
import FormCheckoutAddresses from '../../components/forms/FormCheckoutAddresses'
import HomeLayout from '../../components/layouts/HomeLayout'
import ProgressBarCheckout from '../../components/elements/progress/ProgressBarCheckout'
import {SvgDots} from '../../components/elements/icon/SvgItems'
import {useDispatch, useSelector} from "react-redux";
import {getCart} from "../../redux/selectors/cart";
import {CardCartLine} from "../../components/elements/cards/CardCartLine";
import {OrderModel} from "../../utils/models/ApiPlatform/Order/OrderModel";
import {Context} from "../../utils/middlewares/Context/Context";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {Item} from "../../components/elements/checkout/Item";
import {setAddresses, setCountries, setCustomer} from "../../redux/actions/checkout";
import {getCustomerAddresses, getCustomerData} from "../account";

const STEPS: any = [
    {
        title: "Validation du panier",
        component: <FormCartValidation/>
    },
    {
        title: "Service et adresses",
        component: <FormCheckoutAddresses/>
    },
    {
        title: "Méthode de paiement",
        component: <FormPayment/>
    },
    {
        title: "Confirmation de commande",
        component: <>Nos équipes de livraison officient dans un rayon de 5km autour de nos restaurants. Nos livreurs
            sont dotés d 'un équipement complet visant à garantir la fraîcheur et la température de votre repas pour
            son arrivée auprès de vous.</>
    }
]

const CartPage: FC = ({addresses, errors, countries, customer}: any) => {

    const dispatch = useDispatch();

    useEffect(() => {

        dispatch(setAddresses(addresses?.data ?? []));
        dispatch(setCountries(countries?.data ?? []));
        dispatch(setCustomer(customer));

        if (errors && errors?.length > 0) {
            for (let error of errors) {
                dispatch(addLog({
                    gravity: MessageGravity.ERROR,
                    message: error
                }))
            }
        }

    }, [])

    const cartState = useSelector(getCart);
    const cart = cartState?.cart;
    const nbProduct = cart?.orderLines?.length ?? 0;
    const symbol = cart?.currency?.symbol ?? '€';

    // const VOUCHER = 'TEST';
    // const SHIPPING_PRICE = 10;
    //
    const [applyVoucher, setApplyVoucher] = useState<boolean>(false)
    const [errorVoucher, setErrorVoucher] = useState<boolean>(false)

    const applyVoucherFunc = (e: any) => {

        e.preventDefault();

        const validDiscount = false;
        // let form = e.target;
        // let elements = form.elements;
        // const discount = elements['discount-code'] ?? null;
        // const validDiscount = discount.value === VOUCHER;
        setApplyVoucher(validDiscount);
        setErrorVoucher(!validDiscount);

    }

    return (
        <HomeLayout>
            <div className="flex flex-wrap flex-col md:flex-row  md:h-screen">
                {cart?.orderLines?.length > 0 ?
                    <>
                        <div className="w-full pt-8 md:mt-8">
                            <h2 className="max-w-lg font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                                <span className="relative inline-block">
                                    <SvgDots/>
                                    <div className="relative text-center pb-4 md:pb-0">Confirmation de commande</div>
                                </span>
                            </h2>
                        </div>
                        <div className="w-full md:w-1/3">
                            <div className="container px-8 mx-auto">
                                <div className="w-full overflow-x-auto">
                                    <h3 className="text-xl font-bold tracking-wider">Votre panier
                                        contient {nbProduct} produit{nbProduct > 1 ? 's' : ''} </h3>
                                    <div
                                        className="relative flex flex-col py-6 px-4 gap-6 divide-y divide-neutral-400 overflow-y-auto">
                                        {nbProduct > 0 && cart?.orderLines.map(
                                            (orderLine: any) => <CardCartLine orderLine={orderLine} cart={cart}
                                                                              key={orderLine.id}/>
                                        )}
                                    </div>
                                    <form className="mt-10 rounded-2xl w-full bg-white dark:bg-neutral-800" onSubmit={applyVoucherFunc}>
                                        <div className="sticky bottom-0 flex-none p-6">
                                            <div>
                                                <label className="block text-sm font-medium">
                                                    Code promotionnel
                                                </label>
                                                <div className="flex space-x-4 mt-1">
                                                    <input type="text" id="discount-code" name="discount-code" required className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-200 dark:bg-neutral-500 text-gray-500 aa-input"/>
                                                    <Button color="gre" label="Utiliser" submit={true}/>
                                                </div>
                                            </div>
                                            {applyVoucher && (
                                                <span className="block text-sm text-green-600">
                                          Remise appliquée avec succès
                                        </span>
                                            )}
                                            {errorVoucher && (
                                                <span className="block text-sm text-red-600">
                                          Remise non applicable
                                        </span>
                                            )}
                                            <dl className="text-sm font-medium space-y-6 pt-2">
                                                <div className="flex justify-between">
                                                    <dt>Sous-total</dt>
                                                    <dd className="">{OrderModel.formatPrice(cart?.totalTaxExcluded ?? 0) + symbol}</dd>
                                                </div>
                                                {applyVoucher && (
                                                    <div className="flex justify-between">
                                                        <dt className="flex">
                                                            Remise
                                                        </dt>
                                                        <dd className="">-{OrderModel.formatPrice(cart?.voucherAmount ?? 0) + symbol}</dd>
                                                    </div>
                                                )}
                                                <div className="flex justify-between">
                                                    <dt>T.V.A.</dt>
                                                    <dd className="">{OrderModel.formatPrice(cart?.taxAmount ?? 0) + symbol}</dd>
                                                </div>
                                                <div className="flex justify-between">
                                                    <dt>Livraison</dt>
                                                    <dd className="">{OrderModel.formatPrice(cart?.shipping ?? 0) + symbol}</dd>
                                                </div>
                                                <div
                                                    className="flex items-center justify-between border-t border-org-500 dark:border-gre-500 pt-6">
                                                    <dt>Total (TTC)</dt>
                                                    <dd className="text-base text-org-500 dark:text-gre-500">{OrderModel.formatPrice(cart?.total ?? 0) + symbol}</dd>
                                                </div>
                                            </dl>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="w-full md:w-2/3">
                            <div className="p-4">
                                <ProgressBarCheckout/>
                                <div className="space-y-4 h-screen/1.5 overflow-y-auto scrollbar scrollbar-w-2 scrollbar-thumb-neutral-400 scrollbar-track-neutral-100 dark:scrollbar-track-neutral-800">
                                    {STEPS.map((step: any, index: number) =>
                                        <Item title={step.title} key={index} step={index}>
                                            {step.component}
                                        </Item>
                                    )}
                                </div>
                            </div>
                        </div>
                    </> :
                    <h2 className="m-auto font-sans p-4 text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                        Votre panier est vide... Revenez une fois que vous l'aurez rempli :p !
                    </h2>
                }
            </div>
        </HomeLayout>
    );
};

export const getServerSideProps = async (appContext: any) => {

    let context = new Context(false);

    if (appContext.req && appContext.res) {
        context = new Context(true, appContext.req, appContext.res);
    }

    let customerToken = context.getCustomerToken();

    let props: any = {
        errors: []
    }

    if (customerToken) {

        await getCustomerData(customerToken, props);
        await getCustomerAddresses(customerToken, props);

    }

    return {
        props
    }
}

export default CartPage;
export const CART_PATH = '/cart';