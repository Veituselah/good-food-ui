import {OrderModel} from "../../utils/models/ApiPlatform/Order/OrderModel"
import {SvgDownload, SvgInvoice} from '../../components/elements/icon/SvgItems'
import HomeLayout from '../../components/layouts/HomeLayout'
import {Context} from "../../utils/middlewares/Context/Context";
import {OrderHandler} from "../../utils/handlers/ApiPlatform/Order/OrderHandler";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {NOT_FOUND_PATH} from "../404";
import {ERROR_PATH} from "../500";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {useDispatch} from "react-redux";
import React, {createRef, useEffect, useState} from "react";
import {OrderLineModel} from "../../utils/models/ApiPlatform/Order/Line/OrderLineModel";
import Link from "next/link";
import {PRODUCT_PATH} from "../product/[reference]";
import {HOME_PATH} from "../index";
import {jsPDF} from "jspdf";
import html2canvas from "html2canvas";
import {hideLoader, showLoader} from "../../redux/actions/loader";

const InvoicePage = ({order, errors}: any) => {

    const dispatch = useDispatch();

    if (errors && errors?.length > 0) {
        for (let error of errors) {
            dispatch(addLog({
                gravity: MessageGravity.ERROR,
                message: error
            }))
        }
    }

    const invoiceRef = order?.reference + '-INV';
    const [orderedAt, setOrderedAt] = useState<string>('');
    const [payedAt, setPayedAt] = useState<string>('');
    const currency = order?.currency?.symbol ?? '€';

    useEffect(() => {

        const orderedAtDate = new Date(order?.orderedAt);
        setOrderedAt(order?.orderedAt ? orderedAtDate?.toLocaleString() : 'Non commandée');

        const payedAtDate = new Date(order?.lastPaymentTryAt);
        setPayedAt(order?.lastPaymentTryAt ? payedAtDate?.toLocaleString() : 'Non réglée');
    })

    const pdfContent = createRef<any>();

    const downloadPdf = () => {

        const element = pdfContent?.current as HTMLElement;
        dispatch(showLoader('Chargement du pdf en cours...'))

        html2canvas(element).then(function () {
            const HTML_Width = element.offsetWidth;
            const HTML_Height = element.offsetHeight;
            const top_left_margin = 15;
            const PDF_Width = HTML_Width + (top_left_margin * 2);
            const PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
            const canvas_image_width = HTML_Width;
            const canvas_image_height = HTML_Height;

            const totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

            html2canvas(element).then(function (canvas) {
                const imgData = canvas.toDataURL("image/jpeg", 1.0);
                const pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);

                pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);

                for (let i = 1; i <= totalPDFPages; i++) {
                    pdf.addPage('A4', 'p');
                    pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
                }

                pdf.save(invoiceRef + ".pdf");
            });
        }).catch(() => {
            dispatch(addLog({
                message: 'Une erreur est survenue pendant la génération du pdf'
            }))
        }).finally(() => {
            dispatch(hideLoader())
        });
    }

    return (
        <HomeLayout>

            <div className={"flex flex-col items-center"}>

                <button type="button" onClick={downloadPdf}
                        className="my-8 h-8 p-2 justify-center items-center transition ease-in duration-200 text-center text-base font-semibold shadow-md rounded-lg bg-org-500 dark:bg-gre-500 hover:bg-org-600 dark:hover:bg-gre-600 focus:ring-org-700 dark:focus:ring-gre-500 focus:ring-offset-org-200 dark:focus:ring-offset-gre-200 text-neutral-700">
                    <SvgDownload/>
                </button>

                <div
                    ref={pdfContent}
                    className="mb-16 max-w-2xl bg-white dark:bg-neutral-800 shadow-md border-2 border-neutral-200 dark:border-neutral-500">
                    <div className="relative p-4 page">
                        {/* <div className="download-pdf " title="Save PDF">
                        <a download="invoice.pdf" href="blob:https://tuanpham-dev.github.io/fff03fb6-86bc-48bd-99fd-44341c9125d4">
                        </a>
                    </div> */}
                        <div className="flex flex-row">
                            <div className="flex flex-col block w-1/2">
                                <div className="relative inline-block mb-5">
                                    <a href="/" aria-label="Se rendre sur le site" title="Good Food">
                                        <img className="h-8 w-auto sm:h-12 hidden dark:block" src="/icons/GoodFoodD.png"
                                             alt="site"/>
                                        <img className="h-8 w-auto sm:h-12 block dark:hidden" src="/icons/GoodFoodL.png"
                                             alt="site"/>
                                    </a>
                                </div>
                                <div className="flex flex-col pt-2 pr-5 space-y-2">
                                    <p className="inline-block font-semibold uppercase text-org-500 dark:text-gre-500">Good
                                        Food</p>
                                    <p className="inline-block">{order?.restaurant?.name}</p>
                                    <p className="inline-block">{order?.restaurant?.street}, {order?.restaurant?.zipCode + ' ' + order?.restaurant?.city}</p>
                                </div>
                            </div>
                            <div className="block w-1/2">
                                <p className="w-full inline-block text-org-500 dark:text-gre-500">
                                    <SvgInvoice/>
                                </p>
                            </div>
                        </div>
                        <div className="flex flex-row mt-12">
                            <div className="flex flex-col w-7/12 pr-5 space-y-2">
                                <p className="inline-block font-semibold  text-org-500 dark:text-gre-500"
                                   placeholder="">Facturé à :</p>
                                <p className="inline-block">{order?.customer?.lastname + ' ' + order?.customer?.firstname}</p>
                                <p className="inline-block">{order?.billingAddress?.street}, {order?.billingAddress?.zipCode + ' ' + order?.billingAddress?.city}</p>
                            </div>
                            <div className="flex flex-col w-5/12 space-y-2">
                                <div className="flex flex-row mb-5">
                                    <div className="w-5/12">
                                        <p className="font-semibold">Facture n°</p>
                                    </div>
                                    <div className="w-7/12">
                                        <p>{invoiceRef}</p>
                                    </div>
                                </div>
                                <div className="flex flex-row mb-5">
                                    <div className="w-5/12">
                                        <p className="font-semibold">Achat du</p>
                                    </div>
                                    <div className="w-7/12">
                                        <p>{orderedAt}</p>
                                    </div>
                                </div>
                                <div className="flex flex-row mb-5">
                                    <div className="w-5/12">
                                        <p className="font-semibold">Réglée le</p>
                                    </div>
                                    <div className="w-7/12">
                                        <p>{payedAt}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div
                            className="flex flex-row mt-8 bg-neutral-100 dark:bg-neutral-500 text-gre-700 dark:text-org-500">
                            <div className="w-1/2 p-2">
                                <p className="font-semibold">Produit</p>
                            </div>
                            <div className="w-1/6 p-2">
                                <p className="font-semibold">Qté</p>
                            </div>
                            <div className="w-1/6 p-2">
                                <p className="font-semibold">T.U.</p>
                            </div>
                            <div className="w-1/6 p-2">
                                <p className="font-semibold">Montant</p>
                            </div>
                        </div>

                        {order?.orderLines?.map((orderLine: OrderLineModel) => {
                            return (
                                <div key={orderLine.id} className="flex flex-row">
                                    <div className="w-1/2 p-2 cursor-pointer">
                                        <Link href={PRODUCT_PATH + orderLine?.product?.reference}>
                                            <p>{orderLine?.name}</p>
                                        </Link>
                                    </div>
                                    <div className="w-1/6 p-2">
                                        <p>{orderLine?.qty}</p>
                                    </div>
                                    <div className="w-1/6 p-2">
                                        <p>{OrderModel.formatPrice(orderLine?.priceTaxIncluded) + currency}</p>
                                    </div>
                                    <div className="w-1/6 p-2 pr-5 text-right">
                                    <span
                                        className="w-full font-semibold">{OrderModel.formatPrice(orderLine?.total) + currency}</span>
                                    </div>
                                </div>
                            )
                        })}

                        <div className="flex flex-row mt-5">
                            <div className="flex flex-col block w-1/2"/>
                            <div className="flex flex-col block w-1/2 space-y-2">
                                <div className="flex flex-row">
                                    <div className="w-1/2 px-5">
                                        <p className="font-semibold">Sous-total</p>
                                    </div>
                                    <div className="w-1/2 px-5 text-right">
                                    <span
                                        className="w-full font-semibold">{OrderModel.formatPrice(order?.totalTaxExcluded ?? 0) + currency}</span>
                                    </div>
                                </div>
                                <div className="flex flex-row">
                                    <div className="w-1/2 px-5">
                                        <p className="font-semibold">T.V.A.</p>
                                    </div>
                                    <div className="w-1/2 px-5 text-right">
                                    <span
                                        className="w-full font-semibold">{OrderModel.formatPrice(order?.taxAmount) + currency}</span>
                                    </div>
                                </div>
                                {order?.voucherAmount &&
                                    <div className="flex flex-row">
                                        <div className="w-1/2 px-5">
                                            <p className="font-semibold">Remise</p>
                                        </div>
                                        <div className="w-1/2 px-5 text-right">
                                    <span
                                        className="w-full font-semibold">-{OrderModel.formatPrice(order?.voucherAmount ?? 0) + currency}</span>
                                        </div>
                                    </div>
                                }
                                <div
                                    className="flex flex-row bg-neutral-100 dark:bg-neutral-500 text-org-500 dark:text-gre-500">
                                    <div className="w-1/2 p-5">
                                        <p className="font-semibold uppercase">Total</p>
                                    </div>
                                    <div className="w-1/2 p-5 flex">
                                        {/*<span className="font-semibold">{currency}</span>*/}
                                        <span
                                            className="w-full font-semibold text-right">{OrderModel.formatPrice(order?.total) + currency}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="flex flex-col my-8">
                            <p className="font-semibold">Informations</p>
                            <p className="w-full h-8">Les équipes de votre franchise favorite vous remercient pour votre
                                commande et vous souhaitent un bon appétit !</p>
                        </div>
                    </div>
                </div>

            </div>
            {/* <p tabindex="-1" aria-hidden="true" style="min-height: 0px !important; max-height: none !important; height: 0px !important; visibility: hidden !important; overflow: hidden !important; position: absolute !important; z-index: -1000 !important; top: 0px !important; right: 0px !important; border-width: 1px; box-sizing: border-box; font-family: Nunito, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; line-height: normal; padding: 4px 12px 4px 0px; tab-size: 8; text-indent: 0px; text-rendering: auto; text-transform: none; width: 630px; word-break: normal;">
</p> */}

        </HomeLayout>
    );
};

export default InvoicePage;

export async function getServerSideProps(appContext: any) {

    let context = new Context(false);

    if (appContext.req && appContext.res) {
        context = new Context(true, appContext.req, appContext.res);
    }

    let token = context.getCustomerToken() ?? context.getAdminToken();

    const orderHandler = new OrderHandler(token);
    let reference = appContext?.query?.order_id;
    let orderIri = orderHandler.buildIri(reference);

    let props: any = {
        order: {},
        token: token ?? null,
        errors: []
    };

    await orderHandler.get(orderIri).then(async (order: OrderModel) => {

        props.order = order?.prepareJSON(true);

    }).catch(function (err: any) {

        let response = err.response;

        switch (response?.status) {

            case 404:
                return {
                    redirect: {
                        permanent: false,
                        destination: NOT_FOUND_PATH
                    }
                }

            case 500:
                return {
                    redirect: {
                        permanent: false,
                        destination: ERROR_PATH
                    }
                }

            case 401:
                return {
                    redirect: {
                        permanent: false,
                        destination: HOME_PATH
                    }
                }

        }

        props.errors?.push(ApiPlatformModel.getFailedError(err));

    })

    return {
        props: props, // will be passed to the page component as props
    }
}

export const INVOICE_PATH = '/invoice/';