import {FC, useState} from 'react';
import {SvgDots, SvgQuestion} from '../../components/elements/icon/SvgItems';
import HomeLayout from '../../components/layouts/HomeLayout';

type Props = {
    title: string,
    children: any
}

const Item = ({title, children}: Props) => {
    const [isOpen, setIsOpen] = useState(false);
    return (
        <div className="border rounded-lg shadow-sm bg-neutral-50 dark:bg-neutral-800">
            <button
                type="button"
                aria-label="Open item"
                title="Open item"
                className="flex items-center justify-between w-full p-4 focus:outline-none"
                onClick={() => setIsOpen(!isOpen)}
            >
                <p className="text-lg font-medium text-gre-700 dark:text-org-500">{title}</p>
                <div className="flex items-center justify-center w-8 h-8 border rounded-full text-org-500 dark:text-gre-500">
                    <svg
                        viewBox="0 0 24 24"
                        className={`w-3 transition-transform duration-200 ${
                            isOpen ? 'transform rotate-180' : ''
                        }`}
                    >
                        <polyline
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeMiterlimit="10"
                            points="2,7 12,17 22,7"
                            strokeLinejoin="round"
                        />
                    </svg>
                </div>
            </button>
            {isOpen && (
                <div className="p-4 pt-0">
                    <p className="ctext-neutral-700 dark:text-neutral-100">{children}</p>
                </div>
            )}
        </div>
    );
};

const FAQPage: FC = () => {
    return (
        <HomeLayout>
            <div className="flex flex-wrap w-full shadow">
                <div
                    className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                    <div className="max-w-xl sm:mx-auto lg:max-w-2xl=">
                        <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                            <div>
                                <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider uppercase rounded-full">
                                    F.A.Q.
                                </p>
                            </div>
                            <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                            <span className="relative inline-block">
                                <SvgDots/>
                                <span className="relative">Questions fréquentes</span>
                            </span>
                            </h2>
                            <p className="text-base md:text-lg">
                                Nous nous posons souvent les mêmes questions. Jetez un oeil à notre F.A.Q., quelqu'un ne vous a peut-être devancé et nous a offert l'opportunité de vous apporter la réponse attendue.
                            </p>
                        </div>
                    </div>
                    <div className="max-w-screen-xl sm:mx-auto">
                        <div className="grid grid-cols-1 gap-16 row-gap-8 lg:grid-cols-2">
                            <div className="space-y-8 text-org-500 dark:text-gre-500">
                                <SvgQuestion/>
                                <div>
                                    <p className="mb-4 text-xl font-medium">
                                        Good Food, qu'est-ce-que c'est ?
                                    </p>
                                    <p className="text-neutral-700 dark:text-neutral-100">
                                        Good Food est une franchise de service de restauration de burgers élaborés disponibles à la livraison, au retrait ou à la consommation depuis ses différentes enseignes.
                                        <br/>
                                        <br/>
                                        Commandez ce qui vous fait envie, quand vous voulez, sans vous sentir coupable. Découvrez régulièrement de nouvelles recettes, il y en a pour tous les goûts !
                                    </p>
                                </div>
                                <div>
                                    <p className="mb-4 text-xl font-medium">Combien coûte Good Food ?</p>
                                    <p className="text-neutral-700 dark:text-neutral-100">
                                        Bénéficiez de nos services à tout moment sans frais supplémentaires. Vous ne payez que votre repas !
                                    </p>
                                </div>
                            </div>
                            <div className="space-y-4">
                                <div>
                                    <p className="mb-4 text-xl font-medium text-org-500 dark:text-gre-500">
                                        D'où puis-je bénéficier de Good Food ?
                                    </p>
                                    <p className="text-neutral-700 dark:text-neutral-100">
                                        Good Food, c'est où vous voulez, quand vous voulez ! Nous offrons nos services de livraison dans un rayon de 5km autour de nos établissements situés en Angleterre, Belgique et France.
                                        <br/>
                                        <br/>
                                        Vous pouvez aussi commander ou réserver dans nos différents établissements pour profiter de vos menus favoris, même lors de vos déplacements !
                                    </p>
                                </div>
                                <Item title="Commencer à utiliser l'application Good Food">
                                    Pour débuter en notre compagnie, cliquez sur le bouton "Connexion" présent en haut des différentes pages de l'application.
                                    <br />
                                    <br />
                                    Procédez alors à votre inscription en complétant le formulaire d'accueil. Une fois votre inscription validée, vous pourrez effectuer votre première commande.
                                </Item>
                                <Item title="Quels sont les horaires d'ouverture des restaurants">
                                    Les horaires de nos différents restaurants sont variables selon l'étendue de leur activité sur le lieu d'implantation.
                                    <br />
                                    <br />
                                    Pour en savoir plus, rendez vous sur la page du restaurant dont vous souhaitez connaîtres le horaires et jours d'ouverture.
                                </Item>
                                <Item title="Quelle est la provenance de nos produits ?">
                                    Nos différents produits cuisinés sont issus des marchés agroalimentaires locaux, de petits producteurs de vos régions. 
                                    <br />
                                    <br />
                                    Nous attachons une telle importance à cette marque de qualité que nos viandes de chinois nous arrivent tout droit du pays sous barquettes réfrigérées.
                                </Item>
                                <Item title="Je suis allergique à certains aliments">
                                    Afin de vous éviter tout désagrément de santé, chaque page de produit proposé au menu recense les différents allergènes connus.
                                    <br />
                                    <br />
                                    En cas de doute, nous vous invitons à nous faire parvenir vos questions depuis la page {' '}
                                    <a href="/terms" className="font-bold transition-colors duration-300 hover:text-org-500 dark:hover:text-gre-500 hover:underline">
                                        contact
                                    </a>
                                    {' '}de cette application et de ne prendre aucun risque pouvant engager votre santé.
                                </Item>
                                <Item title="Quelle est votre politique de transport et de remboursement ?">
                                    Nos équipes de livraison officient dans un rayon de 5km autour de nos restaurants. Nos livreurs sont dotés d'un équipement complet visant à garantir la fraîcheur et la température de votre repas pour son arrivée auprès de vous.
                                    <br />
                                    <br />
                                    En cas de désagrément ou désaccord, nous vous invitons à suivre la démarche appropriée renseignée dans nos {' '}
                                    <a href="/terms" className="font-bold transition-colors duration-300 hover:text-org-500 dark:hover:text-gre-500 hover:underline">
                                        CGV
                                    </a>.
                                </Item>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </HomeLayout>
    );
};

export default FAQPage;
