import { FC } from 'react';
import FormContact from '../../components/forms/FormContact';
import HomeLayout from '../../components/layouts/HomeLayout';
import {SvgContact} from '../../components/elements/icon/SvgItems';

const ContactPage: FC = () => {
    return (
        <HomeLayout>
            <div className="flex flex-wrap w-full shadow">
                <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                    <div className="max-w-screen-xl sm:mx-auto">
                        <div className="grid grid-cols-1 gap-16 row-gap-8 lg:grid-cols-2">
                            <div className="space-y-8 text-org-500 dark:text-gre-500">
                                <SvgContact />
                            </div>
                            <div className="space-y-4">
                                <FormContact />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </HomeLayout>
    );
};

export default ContactPage;
