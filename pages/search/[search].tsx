import {SvgDots, SvgLocation} from '../../components/elements/icon/SvgItems'
import FormSearchbar from '../../components/forms/FormSearchbar'
import HomeLayout from '../../components/layouts/HomeLayout'
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {RestaurantHandler} from "../../utils/handlers/ApiPlatform/Restaurant/RestaurantHandler";
import {OrderFilter, OrderFilterTypeEnum} from "../../utils/filters/ApiPlatform/OrderFilter";
import {SearchFilter} from "../../utils/filters/ApiPlatform/SearchFilter";
import CardRestaurant from "../../components/elements/cards/CardRestaurant";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {useDispatch} from "react-redux";
import {endRequest, newRequest} from "../../redux/actions/request";

const SearchPage = () => {

    const router = useRouter();

    const [searchRestaurant, setSearchRestaurant] = useState(router?.query?.search);
    const [orderByField, setOrderByField] = useState<any>();
    const [restaurants, setRestaurants] = useState<any>({});

    const dispatch = useDispatch();

    useEffect(() => {

        const restaurantHandler = new RestaurantHandler();
        const orderBy = []
        const searchFilters = [
            new SearchFilter('search_by_name_or_city', searchRestaurant)
        ];

        if (orderByField) {

            let orderByData = orderByField.toString().split('-');
            orderBy.push(new OrderFilter(orderByData[0], orderByData[1] as OrderFilterTypeEnum));

        }

        dispatch(newRequest());

        restaurantHandler.getAll(orderBy, searchFilters).then((restaurants: any) => {
            setRestaurants(restaurants);
        }).catch((error: any) => {
            ApiPlatformModel.showFailedResponseError(error, dispatch);
        }).finally(() => {
            dispatch(endRequest());
        })

    }, [searchRestaurant, orderByField])

    return (
        <HomeLayout>
            <div className="flex flex-wrap w-full shadow md:min-h-screen/1.5">
                <div className="w-full md:w-1/2 px-4 lg:px-8 pt-16 lg:pt-20">
                    <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                        <div>
                            <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider uppercase rounded-full">
                                Nos restaurants
                            </p>
                        </div>
                        <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                        <span className="relative inline-block">
                            <SvgDots/>
                            <span className="relative">Résultats de recherche</span>
                        </span>
                        </h2>
                        <p className="text-base md:text-lg">
                            Retrouvez facilement toutes nos enseignes en Europe ! Pour affiner votre recherche, nous
                            vous invitons à utiliser notre système de filtres.
                        </p>
                    </div>
                </div>
                <div className="w-full md:w-1/2 px-4">
                    <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                        <SvgLocation/>
                    </div>
                </div>
                <div className="pb-16 lg:px-8 lg:pb-20 mx-auto w-full">
                    <nav
                        className="relative w-full flex flex-wrap items-center justify-between rounded-lg p-4 bg-white dark:bg-neutral-800 shadow-lg">
                        <div
                            className="relative flex flex-col md:flex-row md:w-1/2 items-center justify-center space-x-2">
                            <p className="font-bold">Nouvelle recherche :</p>
                            <FormSearchbar defaultSearch={searchRestaurant} onSearch={setSearchRestaurant} placeholder={"Je recherche un restaurant (par nom ou ville)"}/>
                        </div>
                        <div className="bg-neutral-50 inline-flex rounded-3xl select-none">
                            <select
                                className="block w-full py-1.5 px-4 leading-normal rounded-l-3xl bg-white dark:bg-neutral-500 border dark:border-neutral-500 aa-input"
                                name="searchfilter"
                                value={orderByField}
                                onChange={(e: any) => setOrderByField(e.target.value)}
                            >
                                <option value="">
                                    Trier par
                                </option>
                                <option value={"name-" + OrderFilterTypeEnum.ASC}>
                                    Nom - A à Z
                                </option>
                                <option value={"name-" + OrderFilterTypeEnum.DESC}>
                                    Nom - Z à A
                                </option>
                                <option value={"country.name-" + OrderFilterTypeEnum.ASC}>
                                    Ville - A à Z
                                </option>
                                <option value={"country.name-" + OrderFilterTypeEnum.DESC}>
                                    Ville - Z à A
                                </option>
                            </select>
                            <div
                                // className="py-1.5 px-4 border-r dark:bg-neutral-700 hover:bg-neutral-300 active:hover:bg-neutral-400 dark:border-neutral-500 cursor-pointer flex items-center"
                                className="py-1.5 px-4 rounded-r-3xl dark:bg-neutral-700 hover:bg-neutral-300 active:hover:bg-neutral-400 dark:border-neutral-500 cursor-pointer flex items-center"
                            >
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star"
                                         className="w-4 text-org-500 dark:text-gre-500 mr-2" role="img"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/>
                                    </svg>
                                    Notation
                            </div>
                            {/*<div*/}
                            {/*    className="bg-white dark:bg-neutral-500 py-1.5 px-4 rounded-r-3xl hover:text-blue-500 cursor-pointer border dark:border-neutral-500"*/}
                            {/*>*/}
                            {/*    Ouvert*/}
                            {/*</div>*/}
                        </div>
                    </nav>
                    <div className={"p-8 min-h-screen"}>
                        {restaurants?.data?.length > 0 ?
                            <>
                                <span className={"text-xl"}>
                                    Voici les {restaurants?.data?.length} premiers résultats de votre recherche. Pour affiner vos résultats, nous vous invitons à utiliser nos filtres ou relancer la recherche avec de nouveaux mots-clé.
                                </span>
                                <div
                                    className="mt-8 flex flex-row flex-wrap space-x-4 space-y-4 items-end justify-between">
                                    {restaurants.data.map((restaurant: any, index: number) =>
                                        <CardRestaurant key={restaurant.slug} restaurant={restaurant}
                                                        defaultImage={index % 5}/>
                                    )}
                                </div>
                            </> :
                            <span className={"text-xl"}>Aucun établissement ne correspond à votre recherche... Merci de réessayer avec d'autres termes !</span>
                        }
                    </div>
                </div>
            </div>
        </HomeLayout>
    );
};

export default SearchPage;
export const SEARCH_PATH = '/search/';