import {useRouter} from 'next/router'
import Button from '../../components/elements/buttons/Button'
import HomeLayout from "../../components/layouts/HomeLayout"

const ERROR_MESSAGE = "Vous ne pensiez pas cela possible, et pourtant ! Vous voilà perdu sur un site où la plus grande décision de votre vie consiste à élire le burger de votre coeur. Même si la page que vous recherchez n'existe pas, l'équipe de Good Food tient à vous rassurer, nos livreurs connaissent le chemin.";
const CODE = 404;

const NotFoundPage = () => {
    const router = useRouter()
    return (
        <HomeLayout title={CODE.toString()}>
            <div className="flex flex-wrap w-full shadow">
                <div className="mx-auto">
                    <div className="px-4 pt-16 sm:max-w-xl mx-auto md:max-w-full lg:max-w-screen-xl md:px-12 lg:px-8 lg:pt-20">
                        <div className="flex flex-row md:mx-auto sm:text-center space-y-4 items-center justify-center">
                            <h2 className="max-w-lg font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                                <span className="flex flex-col relative inline-block mr-4">
                                    <span className="relative">Erreur</span>
                                    <span className="sm:flex-row relative text-gre-700 dark:text-org-500">{CODE.toString()}</span>
                                </span>
                            </h2>
                            <p className="border-l-8 border-org-500 dark:border-gre-500 text-base md:text-lg">
                                {ERROR_MESSAGE}
                            </p>
                        </div>
                        <div className="pt-4 w-full sm:w-1/2 md:w-1/4">
                            <Button color="gre" onClick={() => router.back()} label="Revenir en sécurité"/>
                        </div>
                    </div>    
                    <a className="-mb-8 md:-mb-16 flex w-screen items-center">
                        <img className="w-auto block dark:hidden" src="/icons/Error404AnimatedOrg.svg" alt="site"/>
                        <img className="w-auto hidden dark:block" src="/icons/Error404AnimatedGre.svg" alt="site"/>
                    </a>
                </div>
            </div>
        </HomeLayout>
    )
}
export default NotFoundPage;
export const NOT_FOUND_PATH = '/' + CODE;
