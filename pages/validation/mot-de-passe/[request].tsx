import {FC} from 'react'
import {SvgDots} from '../../../components/elements/icon/SvgItems'
import HomeLayout from '../../../components/layouts/HomeLayout'
import FormChangePassword from '../../../components/forms/FormChangePassword'

const ValidatorPage: FC = () => {
    return (
        <HomeLayout>
            <div className="flex flex-wrap w-full shadow">
                <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                    <div className="max-w-xl sm:mx-auto lg:max-w-2xl=">
                        <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                            <div>
                                <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider uppercase rounded-full">
                                    Validation de
                                </p>
                            </div>
                            <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                            <span className="relative inline-block">
                                <SvgDots/>
                                <span className="relative">votre nouveau mot de passe</span>
                            </span>
                            </h2>
                        </div>
                    </div>

                    <div className="space-y-4">
                        <FormChangePassword/>
                    </div>
                </div>
            </div>
            {/*<SvgPassValid/>*/}
            {/*<SvgPassWrong/>*/}
        </HomeLayout>
    );
};

export default ValidatorPage;
