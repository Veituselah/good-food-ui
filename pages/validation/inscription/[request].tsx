import {SvgDots, SvgLostPath, SvgMailJoin} from '../../../components/elements/icon/SvgItems'
import HomeLayout from '../../../components/layouts/HomeLayout'
import {AccountConfirmationRequestHandler} from "../../../utils/handlers/ApiPlatform/User/UserRequest/AccountConfirmationRequestHandler";
import {AccountConfirmationRequestModel} from "../../../utils/models/ApiPlatform/User/UserRequest/AccountConfirmationRequestModel";
import {ApiPlatformHandler} from "../../../utils/handlers/ApiPlatform/ApiPlatformHandler";
import {NOT_FOUND_PATH} from "../../404";
import {HOME_PATH} from "../../index";
import {useEffect} from "react";
import {useRouter} from "next/router";

type Props = {
    error?: any
}

const ValidatorRegisterPage = ({error}: Props) => {

    const router = useRouter();

    useEffect(() => {
        if(!error){
            setTimeout(() => {
                router.push(HOME_PATH)
            }, 3000)
        }
    })

    return (
        <HomeLayout>
            <div className="flex flex-wrap w-full shadow">

                <div
                    className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                    <div className="max-w-xl sm:mx-auto lg:max-w-2xl=">
                        <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                            <div>
                                <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider uppercase rounded-full">
                                    Validation de
                                </p>
                            </div>
                            <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                            <span className="relative inline-block">
                                <SvgDots/>
                                <span className="relative">votre inscription</span>
                            </span>
                            </h2>

                            {
                                error ?
                                    (
                                        <p className="text-red-500 md:text-lg">
                                            Malhereusement la confirmation de création de votre compte a échoué :<br/>
                                            {error.toString()}
                                        </p>
                                    ) :
                                    (
                                        <p className="text-green-500 md:text-lg">
                                            Votre compte a bien été confirmé, vous pouve désormais vous connecter ! (Vous allez être redirigé vers l'accueil dans quelques instants)
                                        </p>
                                    )
                            }

                            <br/>

                            <p className="text-base md:text-lg">
                                Nous nous posons souvent les mêmes questions. Jetez un oeil à notre F.A.Q., quelqu'un ne
                                vous a peut-être devancé et nous a offert l'opportunité de vous apporter la réponse
                                attendue.
                            </p>
                        </div>
                    </div>

                    <div className="max-w-screen-xl sm:mx-auto">

                        <div className="grid grid-cols-1 gap-16 row-gap-8 lg:grid-cols-2">

                            {error ?
                                <SvgLostPath/>
                                :
                                <SvgMailJoin/>
                            }

                            <div className="space-y-8 text-org-500 dark:text-gre-500">
                                <div>
                                    <p className="mb-4 text-xl font-medium">
                                        Good Food, qu'est-ce-que c'est ?
                                    </p>
                                    <p className="text-neutral-700 dark:text-neutral-100">
                                        Good Food est une franchise de service de restauration de burgers élaborés
                                        disponibles à la livraison, au retrait ou à la consommation depuis ses
                                        différentes enseignes.
                                        <br/>
                                        <br/>
                                        Commandez ce qui vous fait envie, quand vous voulez, sans vous sentir coupable.
                                        Découvrez régulièrement de nouvelles recettes, il y en a pour tous les goûts !
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </HomeLayout>
    );
};

export async function getServerSideProps(context: any) {

    const props: Props = {}

    let accountHandler = new AccountConfirmationRequestHandler();

    let model = new AccountConfirmationRequestModel();
    model.id = context?.query?.request;

    try {

        await accountHandler.validate(model);

    } catch (e: any) {

        let response = e.response;

        switch (response?.status) {

            case 400:
                props.error = response.data[ApiPlatformHandler.HYDRA_DESCRIPTION];
                break;
            case 404:
                return {
                    redirect: {
                        permanent: false,
                        destination: NOT_FOUND_PATH
                    }
                }

            case 500:
                return {
                    redirect: {
                        permanent: false,
                        destination: NOT_FOUND_PATH
                    }
                }

            case 401:
                return {
                    redirect: {
                        permanent: false,
                        destination: HOME_PATH
                    }
                }

            default:
                props.error = 'Une erreur est survenue pendant le chargement de la page';
                break;

        }

    }

    return {props};

}

export default ValidatorRegisterPage;
