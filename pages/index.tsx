import React from 'react';
import HomeLayout from '../components/layouts/HomeLayout';
import Map from "../components/elements/maps/Map";
import dynamic from "next/dynamic";

export const IndexPage = () => {

    const Map = React.useMemo(() => dynamic(
        () => import('../components/elements/maps/Map'), // replace '@components/map' with your component's location
        {
            loading: () => <p>Chargement de la map en cours</p>,
            ssr: false // This line is important. It's what prevents server-side render
        }
    ), [/* list variables which should trigger a re-render here */])
    return (
        <HomeLayout>
            <Map/>
        </HomeLayout>
    );
};

export default IndexPage;
export const HOME_PATH = '/';
