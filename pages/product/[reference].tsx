import React from "react"
import {ButtonAddToCart} from "../../components/elements/buttons/ButtonAddToCart"
import CalculatedStars from '../../components/elements/calculated/CalculatedStars'
import HomeLayout from '../../components/layouts/HomeLayout'
import {Context} from "../../utils/middlewares/Context/Context";
import {ProductModel} from "../../utils/models/ApiPlatform/Product/ProductModel";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {NOT_FOUND_PATH} from "../404";
import {ERROR_PATH} from "../500";
import {HOME_PATH} from "../index";
import {useDispatch, useSelector} from "react-redux";
import {getCart} from "../../redux/selectors/cart";
import {
    PurchasableProductHandler
} from "../../utils/handlers/ApiPlatform/Product/PurchasableProduct/PurchasableProductHandler";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {OrderModel} from "../../utils/models/ApiPlatform/Order/OrderModel";

const ProductPage = ({product, errors}: any) => {

    const cartState = useSelector(getCart);
    const dispatch = useDispatch();

    if (errors && errors?.length > 0) {
        for (let error of errors) {
            dispatch(addLog({
                gravity: MessageGravity.ERROR,
                message: error
            }))
        }
    }

    const restaurant = cartState?.cart?.restaurant;

    const media = product?.medias?.length > 0 ? product.medias[0] : '/images/food/burger.jpg';

    let price = product.priceTaxExcluded;

    for (let tax of product?.taxType?.taxes ?? []) {

        if (tax?.country?.isoCode === restaurant?.country?.isoCode) {
            price *= 1 + parseFloat(tax?.rate);
        }

    }

    return (
        <HomeLayout>
            <div className="pt-6">
                <div className="mt-6 max-w-2xl mx-auto sm:px-6 lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-3 lg:gap-x-8">
                    <div className="hidden aspect-w-3 aspect-h-4 rounded-lg overflow-hidden lg:block">
                        <img src={media} alt="Two each of gray, white, and black shirts laying flat."
                             className="w-full h-full object-center object-cover"/>
                    </div>
                    <div className="hidden lg:grid lg:grid-cols-1 lg:gap-y-8">
                        <div className="aspect-w-3 aspect-h-2 rounded-lg overflow-hidden">
                            <img src={media} alt="Model wearing plain black basic tee."
                                 className="w-full h-full object-center object-cover"/>
                        </div>
                        <div className="aspect-w-3 aspect-h-2 rounded-lg overflow-hidden">
                            <img src={media} alt="Model wearing plain gray basic tee."
                                 className="w-full h-full object-center object-cover"/>
                        </div>
                    </div>
                    <div className="aspect-w-4 aspect-h-5 sm:rounded-lg sm:overflow-hidden lg:aspect-w-3 lg:aspect-h-4">
                        <img src={media} alt="Model wearing plain white basic tee."
                             className="w-full h-full object-center object-cover"/>
                    </div>
                </div>
                <div
                    className="max-w-2xl mx-auto pt-10 pb-16 px-4 sm:px-6 lg:max-w-7xl lg:pt-16 lg:pb-24 lg:px-8 lg:grid lg:grid-cols-3 lg:grid-rows-[auto,auto,1fr] lg:gap-x-8">
                    <div className="lg:col-span-2 lg:border-r lg:border-org-500 lg:dark:border-gre-500 lg:pr-8">
                        <h1 className="text-2xl font-bold tracking-tight sm:tracking-tight sm:text-3xl">{product?.name}</h1>
                        {(product?.mainCategory ?? 'Non catégorisé') + (product?.categories?.length > 0 ? ' : ' + product?.categories[0] : '')}
                    </div>
                    <div className="mt-4 lg:mt-0 lg:row-span-3">
                        <div className="mt-6">
                            <div className="flex items-center text-right">
                                <CalculatedStars note={product?.meanNote}/>
                                <p className="w-full ml-3 text-sm text-left font-medium hover:text-indigo-500 cursor-pointer">{product?.comments?.length ?? 0} avis</p>
                                <p className="w-full text-3xl font-semibold align-right text-org-500">
                                    {OrderModel.formatPrice(price)} {product?.currency?.symbol ?? '€'}<br/>
                                </p>
                            </div>
                            <span className="w-full text-xs font-semibold align-right text-gray-500">(Prix pouvant varier en fonction des restaurants)</span>
                            <ButtonAddToCart product={product}/>
                        </div>
                        <form className="mt-10">
                            <div>
                                <h3 className="text-sm font-medium">Sauce</h3>
                                <fieldset className="mt-4">
                                    <div className="flex items-center space-x-3">
                                        {product?.description}
                                        <label
                                            className="-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none ring-gray-400">
                                            <input type="radio" name="sauce-choice" value="Blanche" className="sr-only"
                                                   aria-labelledby="color-choice-0-label"/>
                                            <span id="color-choice-0-label" className="sr-only">Blanche</span>
                                            <span aria-hidden="true"
                                                  className="h-8 w-8 bg-white border border-black border-opacity-10 rounded-full"></span>
                                        </label>
                                        <label
                                            className="-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none ring-gray-400">
                                            <input type="radio" name="sauce-choice" value="Poivre" className="sr-only"
                                                   aria-labelledby="color-choice-1-label"/>
                                            <span id="color-choice-1-label" className="sr-only">Poivre</span>
                                            <span aria-hidden="true"
                                                  className="h-8 w-8 bg-gray-200 border border-black border-opacity-10 rounded-full"></span>
                                        </label>
                                        <label
                                            className="-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none ring-gray-900">
                                            <input type="radio" name="sauce-choice" value="Bleu" className="sr-only"
                                                   aria-labelledby="color-choice-2-label"/>
                                            <span id="color-choice-2-label" className="sr-only">Bleu</span>
                                            <span aria-hidden="true"
                                                  className="h-8 w-8 bg-blue-900 border border-black border-opacity-10 rounded-full"></span>
                                        </label>
                                        <label
                                            className="-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none ring-gray-900">
                                            <input type="radio" name="sauce-choice" value="Chocolat" className="sr-only"
                                                   aria-labelledby="color-choice-2-label"/>
                                            <span id="color-choice-2-label" className="sr-only">Chocolat</span>
                                            <span aria-hidden="true"
                                                  className="h-8 w-8 bg-gray-900 border border-black border-opacity-10 rounded-full"></span>
                                        </label>
                                        <label
                                            className="-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none ring-gray-900">
                                            <input type="radio" name="sauce-choice" value="Mayonnaise"
                                                   className="sr-only" aria-labelledby="color-choice-2-label"/>
                                            <span id="color-choice-2-label" className="sr-only">Mayonnaise</span>
                                            <span aria-hidden="true"
                                                  className="h-8 w-8 bg-yellow-300 border border-black border-opacity-10 rounded-full"></span>
                                        </label>
                                        <label
                                            className="-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none ring-gray-900">
                                            <input type="radio" name="sauce-choice" value="Ketchup" className="sr-only"
                                                   aria-labelledby="color-choice-2-label"/>
                                            <span id="color-choice-2-label" className="sr-only">Ketchup</span>
                                            <span aria-hidden="true"
                                                  className="h-8 w-8 bg-red-900 border border-black border-opacity-10 rounded-full"></span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                            <div className="mt-10">
                                <div className="flex items-center justify-between">
                                    <h3 className="text-sm font-medium">Cuisson de la viande</h3>
                                    <a href="#"
                                       className="text-sm font-medium text-gre-700 dark:text-org-500 hover:text-gre-500 dark:hover:text-org-700">Guide
                                        des cuissons</a>
                                </div>
                                <fieldset className="mt-4 text-center">
                                    <div className="grid grid-cols-4 gap-4 sm:grid-cols-8 lg:grid-cols-4">
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-white dark:bg-neutral-500 shadow-sm cursor-pointer">
                                            <input type="radio" name="size-choice" value="Bleue" className="sr-only"/>
                                            <span id="size-choice-3-label">Bleue</span>
                                            <span className="absolute -inset-px rounded-md pointer-events-none"
                                                  aria-hidden="true"></span>
                                        </label>
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-white dark:bg-neutral-500 shadow-sm cursor-pointer">
                                            <input type="radio" name="size-choice" value="Saignante"
                                                   className="sr-only"/>
                                            <span id="size-choice-3-label">Saignant</span>
                                            <span className="absolute -inset-px rounded-md pointer-events-none"
                                                  aria-hidden="true"></span>
                                        </label>
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-white dark:bg-neutral-500 shadow-sm cursor-pointer">
                                            <input type="radio" name="size-choice" value="A point" className="sr-only"/>
                                            <span id="size-choice-4-label">à point</span>
                                            <span className="absolute -inset-px rounded-md pointer-events-none"
                                                  aria-hidden="true"></span>
                                        </label>
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-white dark:bg-neutral-500 shadow-sm cursor-pointer">
                                            <input type="radio" name="size-choice" value="Bien cuit"
                                                   className="sr-only"/>
                                            <span id="size-choice-5-label">Bien cuit</span>
                                            <span className="absolute -inset-px rounded-md pointer-events-none"
                                                  aria-hidden="true"></span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                            <div className="mt-10">
                                <div className="flex items-center justify-between">
                                    <h3 className="text-sm font-medium">Portion</h3>
                                    <a href="#"
                                       className="text-sm font-medium text-gre-700 dark:text-org-500 hover:text-gre-500 dark:hover:text-org-700">Guide
                                        des portions</a>
                                </div>
                                <fieldset className="mt-4 text-center">
                                    <div className="grid grid-cols-4 gap-4 sm:grid-cols-8 lg:grid-cols-4">
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-neutral-50 dark:bg-neutral-700 text-neutral-400 cursor-not-allowed">
                                            <input type="radio" name="size-choice" value="XXS" disabled
                                                   className="sr-only" aria-labelledby="size-choice-0-label"/>
                                            <span id="size-choice-0-label"> Enfant </span>
                                            <span aria-hidden="true"
                                                  className="absolute -inset-px rounded-md border-2 dark:border-neutral-700 pointer-events-none">
                                                <svg
                                                    className="absolute inset-0 w-full h-full text-neutral-400 stroke-2"
                                                    viewBox="0 0 100 100" preserveAspectRatio="none"
                                                    stroke="currentColor">
                                                <line x1="0" y1="100" x2="100" y2="0"
                                                      vectorEffect="non-scaling-stroke"/>
                                                </svg>
                                            </span>
                                        </label>
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-white dark:bg-neutral-500 shadow-sm cursor-pointer">
                                            <input type="radio" name="size-choice" value="M" className="sr-only"
                                                   aria-labelledby="size-choice-3-label"/>
                                            <span id="size-choice-3-label"> M </span>
                                            <span className="absolute -inset-px rounded-md pointer-events-none"
                                                  aria-hidden="true"></span>
                                        </label>
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-white dark:bg-neutral-500 shadow-sm cursor-pointer">
                                            <input type="radio" name="size-choice" value="L" className="sr-only"
                                                   aria-labelledby="size-choice-4-label"/>
                                            <span id="size-choice-4-label"> L </span>
                                            <span className="absolute -inset-px rounded-md pointer-events-none"
                                                  aria-hidden="true"></span>
                                        </label>
                                        <label
                                            className="group relative border dark:border-neutral-700 rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-neutral-100 focus:outline-none sm:flex-1 sm:py-6 bg-white dark:bg-neutral-500 shadow-sm cursor-pointer">
                                            <input type="radio" name="size-choice" value="XL" className="sr-only"
                                                   aria-labelledby="size-choice-5-label"/>
                                            <span id="size-choice-5-label"> XL </span>
                                            <span className="absolute -inset-px rounded-md pointer-events-none"
                                                  aria-hidden="true"></span>
                                        </label>
                                    </div>
                                </fieldset>
                            </div>
                        </form>
                    </div>
                    <div
                        className="py-10 lg:pt-6 lg:pb-16 lg:col-start-1 lg:col-span-2 lg:border-r lg:border-org-500 lg:dark:border-gre-500 lg:pr-8">
                        <div>
                            <div className="space-y-6">
                                <p className="text-base">
                                    {product?.description}
                                    The Basic Tee 6-Pack allows you to fully
                                    express your vibrant personality with three grayscale options. Feeling adventurous?
                                    Put on a heather gray tee. Want to be a trendsetter? Try our exclusive
                                    colorway: &quot;Black&quot;. Need to add an extra pop of color to your outfit? Our
                                    white tee has you covered.
                                </p>
                            </div>
                        </div>
                        <div className="mt-10">
                            <h3 className="font-semibold">Vous avez aimé :</h3>
                            <div className="mt-4">
                                <ul role="list" className="pl-4 list-disc space-y-2">
                                    <li><span className="">La fraîcheur de nos produits</span></li>
                                    <li><span className="">La cuisson parfaite de votre viande</span></li>
                                    <li><span className="">Le fromage fondant à souhait</span></li>
                                    <li><span className="">Le sourire de votre livreur(se)</span></li>
                                </ul>
                            </div>
                        </div>
                        <div className="mt-10">
                            <h2 className="font-semibold">La note de l'équipe :</h2>
                            <div className="mt-4 space-y-6">
                                <p>Nous mettons toute notre passion et notre énergie dans la conception de nos
                                    différents plats. Grâce à nos 20 ans d'expérience et de retours de votre part, nous
                                    perfectionnement quotidiennement notre carte. L'équipe de Good Food vous remercie
                                    encore pour vos conseils et votre fidélité.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </HomeLayout>
    );
};

export default ProductPage;
export const PRODUCT_PATH = '/product/';

export const getServerSideProps = async (appContext: any) => {

    let context = new Context(false);

    if (appContext.req && appContext.res) {
        context = new Context(true, appContext.req, appContext.res);
    }

    let customerToken = context.getCustomerToken();

    const productHandler = new PurchasableProductHandler(customerToken);

    let reference = appContext?.query?.reference;
    let productIri = productHandler.buildIri(reference);

    let props: any = {
        product: {},
        errors: new Array<string>(),
    };

    await productHandler.get(productIri).then(async (product: ProductModel) => {

        props.product = product?.prepareJSON(true);


    }).catch(function (err: any) {

        let response = err.response;

        switch (response?.status) {

            case 404:
                return {
                    redirect: {
                        permanent: false,
                        destination: NOT_FOUND_PATH
                    }
                }

            case 500:
                return {
                    redirect: {
                        permanent: false,
                        destination: ERROR_PATH
                    }
                }

            case 401:
                return {
                    redirect: {
                        permanent: false,
                        destination: HOME_PATH
                    }
                }

        }

        props.errors?.push(ApiPlatformModel.getFailedError(err));

    })

    return {
        props: props, // will be passed to the page component as props
    }
}
