import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps, TableSearchFilterType} from "../../../components/elements/table/AdminTable";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {
    Collection,
    Context,
    ReservationHandler,
    ReservationModel,
    CustomContext,
    Role,
    MessageGravity,
    ApiPlatformModel,
    RestaurantModel,
    PhoneFormater,
    DateFormater,
    OrderModel,
    FranchiseEmployeeModel,
    OrderHandler, RestaurantHandler, CustomerHandler, CustomerModel
} from "../../../internal";
import SVGIcon from "../../../components/elements/icon/SVGIcon";
import {isBefore} from "date-fns";
import {addLog} from "../../../redux/actions/logger";
import {search} from "../../../redux/actions/admin_table";
import {useDispatch} from "react-redux";

type Props = {
    data: any,
    errors: any,
    restaurants: any,
    customers: any,
    token?: string
}

const phoneFormater = new PhoneFormater();
const dateFormater = new DateFormater();

const AdminReservationPage = (props: Props) => {

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    const dispatch = useDispatch();

    const errors = props.errors;
    if (errors && errors?.length > 0) {
        for (let error of errors) {
            dispatch(addLog({
                gravity: MessageGravity.ERROR,
                message: error
            }))
        }
    }

    let restaurantsOptions = [];

    if (props?.restaurants?.data) {
        restaurantsOptions = props.restaurants.data.map((restaurant: RestaurantModel) => {
            return {
                value: restaurant.slug,
                label: restaurant.name
            }
        })
    }

    let customersOptions = [];

    if (props?.customers) {
        customersOptions = props?.customers?.data?.map((customer: CustomerModel) => {
            return {
                value: customer.email,
                label: customer.email
            }
        })
    }

    const reservationHandler = new ReservationHandler<ReservationModel>(token);

    const actions: AdminHeaderAction[] = [];

    const tableProps: TableProps = {
        columns: [
            {
                name: 'Restaurant',
                pathToObject: ['restaurant'],
                propertiesToDisplay: ['name'],
                searchFilter: {
                    name: 'restaurant.slug',
                    type: TableSearchFilterType.SELECT,
                    options: restaurantsOptions,
                    label: 'Restaurants'
                }
            },
            {
                name: 'Client',
                pathToObject: ['customer'],
                propertiesToDisplay: ['email'],
                notCapitalize: true,
                searchFilter: {
                    name: 'customer.email',
                    type: TableSearchFilterType.SELECT,
                    options: customersOptions,
                    label: 'Clients'
                }
            },
            {
                name: 'Téléphone',
                pathToObject: [],
                propertiesToDisplay: ['phoneNumber'],
                formater: phoneFormater,
                formaterGetParams: (value: any, object: RestaurantModel) => {
                    return {
                        country: object.country
                    }
                }
            },
            {
                name: 'Réservé pour le',
                pathToObject: [],
                propertiesToDisplay: ['reservedFor'],
                formater: dateFormater
            },
            {
                name: 'Nb. personnes',
                pathToObject: [],
                propertiesToDisplay: ['nbPerson']
            },
            {
                name: 'Date d\'annulation',
                pathToObject: [],
                propertiesToDisplay: ['canceledAt'],
                formater: dateFormater,
                searchFilter: {
                    name: 'canceledAt',
                    type: TableSearchFilterType.BOOLEAN,
                    label: 'Est en cours',
                    reverseBool: true
                }
            }
        ],
        actions: [
            // {
            //     name: 'Edit',
            //     icon: '',
            //     action: () => {
            //     }
            // }
        ],
        canSelectLines: true,
        collection: new Collection<ReservationModel>(),
        dataIDField: 'id',
        apiHandler: reservationHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const user = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    if (user?.isGranted(Role.ROLE_ADMIN)) {
                        actions.push({
                                name: 'add',
                                icon: <SVGIcon icon='add' width="34px" height="34px"/>,
                                link: '/admin/reservation/add'
                            }
                        )
                    }

                    tableProps.actions.push({
                        name: 'Annuler',
                        icon: 'cancel',
                        action: (element: any, reservationModel: ReservationModel) => {
                            if (confirm('Voulez vous vraiment annuler la réservation ? ')) {

                                const context = new Context(false);
                                const reservationHandler = new ReservationHandler(context.getAdminToken());

                                reservationHandler.cancel(reservationModel).then(() => {
                                    dispatch(addLog({
                                        gravity: MessageGravity.SUCCESS,
                                        message: 'La réservation a bien été annulée'
                                    }))
                                    dispatch(search(true))
                                }).catch((error: any) => {
                                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                                })


                            }
                        },
                        checkCanDisplayAction: (reservationModel: ReservationModel) => {

                            const reservedFor = reservationModel.reservedFor instanceof Date ? reservationModel.reservedFor : new Date(reservationModel.reservedFor as any);

                            if (isBefore(reservedFor, new Date())) {

                                return false;

                            } else if (reservationModel.canceledAt) {

                                return false;
                            }

                            return true;
                        }
                    });

                    return (

                        <AdminLayout title="Réservations" actions={actions}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(false);

    if (context.req && context.res) {
        appContext = new Context(true, context.req, context.res);
    }

    let token = appContext.getAdminToken();

    const reservationRequest = new ReservationHandler(token);
    const restaurantHandler = new RestaurantHandler(token);
    const customerHandler = new CustomerHandler(token);

    let props: Props = {
        data: [],
        restaurants: [],
        customers: [],
        errors: new Array<string>(),
        token: token ?? ''
    };

    await Promise.all(
        [
            reservationRequest.getAll(),
            restaurantHandler.getAllInReservations(),
            customerHandler.getAllInReservations(),
        ]
    ).then((values: any) => {
        props.data = values[0].prepareJSON(true);
        props.restaurants = values[1].prepareJSON(true);
        props.customers = values[2].prepareJSON(true);
    }).catch((errors: any) => {
        props.errors.push(ApiPlatformModel.getFailedError(errors) ?? '');
    });

    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminReservationPage;
export const ADMIN_RESERVATION_PATH = '/admin/reservation'