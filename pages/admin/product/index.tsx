import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps, TableSearchFilterType} from "../../../components/elements/table/AdminTable";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {
    Collection,
    Context,
    CustomContext,
    PhoneFormater,
    ProductModel,
    PurchasableProductHandler,
    PurchasableProductModel
} from "../../../internal";
import {useDispatch} from "react-redux";

type Props = {
    data: any,
    errors: any,
    countries: any,
    currencies: any,
    token?: string
}

// const phoneFormater = new PhoneFormater();
// const dateFormater = new DateFormater();

const AdminProductPage = (props: Props) => {

    const dispatch = useDispatch();

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    const productHandler = new PurchasableProductHandler(token);
    const actions: AdminHeaderAction[] = [];
    const tableProps: TableProps = {
        columns: [
            {
                name: 'Référence',
                pathToObject: [],
                propertiesToDisplay: ['reference'],
                orderFilter: 'reference',
                searchFilter: {
                    name: 'reference',
                    type: TableSearchFilterType.TEXT,
                }
            },
            {
                name: 'Libellé',
                pathToObject: [],
                propertiesToDisplay: ['name'],
                orderFilter: 'name',
                searchFilter: {
                    name: 'name',
                    type: TableSearchFilterType.TEXT,
                }
            },
            {
                name: 'Catégorie',
                pathToObject: [],
                propertiesToDisplay: ['mainCategory'],
                // orderFilter: 'mainCategory',
            },
            {
                name: 'Prix',
                pathToObject: [],
                propertiesToDisplay: ['priceTaxExcluded'],
                orderFilter: 'priceTaxExcluded',
            },
            {
                name: 'Devise',
                pathToObject: ['currency'],
                propertiesToDisplay: ['symbol'],
                orderFilter: 'currency.isoCode',
            }
        ],
        actions: [],
        canSelectLines: true,
        collection: new Collection<PurchasableProductModel>(),
        dataIDField: 'reference',
        apiHandler: productHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const product = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    // let isAdmin = product?.isGranted(Role.ROLE_ADMIN);
                    //
                    // tableProps.actions.push({
                    //     name: 'Editer',
                    //     icon: 'edit',
                    //     url: {
                    //         url: ADMIN_PRODUCT_EDIT_PATH,
                    //         urlParams: [
                    //             {
                    //                 param: 'slug',
                    //                 pathToProperty: ['slug']
                    //             }
                    //         ]
                    //     },
                    //     checkCanDisplayAction: (item: ProductModel) => {
                    //
                    //         return isAdmin ||
                    //             (
                    //                 product?.isGranted(Role.ROLE_LEAD) &&
                    //                 product instanceof FranchiseEmployeeModel
                    //             );
                    //     }
                    // });
                    //
                    // if (isAdmin) {
                    //
                    //     actions.push({
                    //         name: 'add',
                    //         icon: <SVGIcon icon='add'/>,
                    //         link: '/admin/product/add'
                    //     })
                    //
                    //     actions.push({
                    //         name: 'remove',
                    //         icon: <SVGIcon icon='remove'/>,
                    //         onClick: () => {
                    //             if (confirm('Voulez vous vraiment supprimer ces éléments ? ')) {
                    //                 dispatch(deleteLines());
                    //             }
                    //         }
                    //     })
                    //
                    //     tableProps.actions.push({
                    //         name: 'Supprimer',
                    //         icon: 'remove',
                    //         action: (element: any) => {
                    //             if (confirm('Voulez vous vraiment supprimer cet élément ? ')) {
                    //                 let iri = element.target.getAttribute('entity-iri');
                    //                 doDelete(productHandler, iri, dispatch).then(
                    //                     () => {
                    //                         dispatch(search(true))
                    //                     }
                    //                 ).catch((err) => {
                    //                     console.error(err);
                    //                 });
                    //             }
                    //         }
                    //     });
                    // }

                    return (
                        <AdminLayout title="Produits" actions={actions}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(true, context.req, context.res);
    let token = appContext.getAdminToken();

    const productHandler = new PurchasableProductHandler(token);

    let props = {
        roleList: [],
        data: [],
        products: [],
        errors: '',
        token
    };

    await Promise.all(
        [
            productHandler.getAll()
        ]
    ).then(
        (values:any) =>{
            props.data = values[0].prepareJSON(true);
            props.products = values[1].prepareJSON(true);
            props.products = Array.from(
                new Set(
                    values[1]?.data?.map(
                        (product: ProductModel) => product.prepareJSON(true)
                    )
                )
            )
        }
    ).catch((errors: any) => {
        props.errors = JSON.stringify(errors);
    });


    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminProductPage;
export const ADMIN_RESTAURANT_PATH = '/admin/product'