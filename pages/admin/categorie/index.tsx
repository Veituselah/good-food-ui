import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps, TableSearchFilterType} from "../../../components/elements/table/AdminTable";
import SVGIcon from "../../../components/elements/icon/SVGIcon";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {
    Collection,
    Context,
    CategoryModel,
    CustomContext,
    CategoryHandler,
    FranchiseEmployeeModel,
    PhoneFormater,
    Role
} from "../../../internal";
import {ADMIN_CATEGORY_EDIT_PATH} from "./edit/[slug]";
import {useDispatch} from "react-redux";
import {deleteLines, search} from "../../../redux/actions/admin_table";
import {doDelete} from "../../../redux/sagas/request";
import { values } from "lodash";

type Props = {
    data: any,
    errors: any,
    countries: any,
    currencies: any,
    token?: string
}

const phoneFormater = new PhoneFormater();
// const dateFormater = new DateFormater();

const AdminCategoryPage = (props: Props) => {

    const dispatch = useDispatch();

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    const categoryHandler = new CategoryHandler(token);
    const actions: AdminHeaderAction[] = [];
    const tableProps: TableProps = {
        columns: [
            {
                name: 'Id',
                pathToObject: [],
                propertiesToDisplay: ['id'],
                orderFilter: 'id',
                searchFilter: {
                    name: 'id',
                    type: TableSearchFilterType.TEXT,
                }
            },
            {
                name: 'Libellé',
                pathToObject: [],
                propertiesToDisplay: ['name'],
                orderFilter: 'name',
                searchFilter: {
                    name: 'name',
                    type: TableSearchFilterType.TEXT,
                },
            }
        ],
        actions: [],
        canSelectLines: true,
        collection: new Collection<CategoryModel>(),
        dataIDField: 'slug',
        apiHandler: categoryHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const category = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    let isAdmin = category?.isGranted(Role.ROLE_ADMIN);

                    tableProps.actions.push({
                        name: 'Editer',
                        icon: 'edit',
                        url: {
                            url: ADMIN_CATEGORY_EDIT_PATH,
                            urlParams: [
                                {
                                    param: 'slug',
                                    pathToProperty: ['slug']
                                }
                            ]
                        },
                        checkCanDisplayAction: (item: CategoryModel) => {

                            return isAdmin ||
                                (
                                    category?.isGranted(Role.ROLE_LEAD) &&
                                    category instanceof FranchiseEmployeeModel
                                );
                        }
                    });

                    if (isAdmin) {

                        actions.push({
                            name: 'add',
                            icon: <SVGIcon icon='add'/>,
                            link: '/admin/category/add'
                        })

                        // actions.push({
                        //     name: 'remove',
                        //     icon: <SVGIcon icon='remove'/>,
                        //     onClick: () => {
                        //         if (confirm('Voulez vous vraiment supprimer ces éléments ? ')) {
                        //             dispatch(deleteLines());
                        //         }
                        //     }
                        // })
                        //
                        // tableProps.actions.push({
                        //     name: 'Supprimer',
                        //     icon: 'remove',
                        //     action: (element: any) => {
                        //         if (confirm('Voulez vous vraiment supprimer cet élément ? ')) {
                        //             let iri = element.target.getAttribute('entity-iri');
                        //             doDelete(categoryHandler, iri, dispatch).then(
                        //                 () => {
                        //                     dispatch(search(true))
                        //                 }
                        //             ).catch((err) => {
                        //                 console.error(err);
                        //             });
                        //         }
                        //     }
                        // });

                    }


                    return (
                        <AdminLayout title="Catégories" actions={actions}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(true, context.req, context.res);
    let token = appContext.getAdminToken();

    const categoryHandler = new CategoryHandler(token);

    let props = {
        roleList: [],
        data: [],
        categorys: [],
        errors: '',
        token
    };

    await Promise.all(
        [
            categoryHandler.getAll()
        ]
    ).then(
        (values:any) =>{
            props.data = values[0].prepareJSON(true);
            props.categorys = values[1].prepareJSON(true);
            props.categorys = Array.from(
                new Set(
                    values[1]?.data?.map(
                        (category: CategoryModel) => category.prepareJSON(true)
                    )
                )
            )
        }
    ).catch((errors: any) => {
        props.errors = JSON.stringify(errors);
    });


    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminCategoryPage;
export const ADMIN_RESTAURANT_PATH = '/admin/category'