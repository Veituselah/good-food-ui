import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps, TableSearchFilterType} from "../../../components/elements/table/AdminTable";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {
    ApiPlatformModel,
    Collection,
    Context,
    CustomContext,
    CustomerHandler,
    CustomerModel,
    DateFormater,
    MessageGravity,
    OrderHandler,
    OrderModel, OrderStatusHandler, OrderStatusModel,
    RestaurantHandler,
    RestaurantModel,
    Role
} from "../../../internal";
import {useDispatch} from "react-redux";
import {addLog} from "../../../redux/actions/logger";
import {search} from "../../../redux/actions/admin_table";
import {INVOICE_PATH} from "../../invoice/[order_id]";
import {useRouter} from "next/router";
import Restaurant from "../restaurant";

type Props = {
    data: any,
    errors: any,
    restaurants: any,
    customers: any,
    statuses: any,
    token?: string
}

const AdminOrderPage = (props: Props) => {

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    const orderHandler = new OrderHandler(token);

    const router = useRouter();
    const dispatch = useDispatch();

    const errors = props.errors;
    if (errors && errors?.length > 0) {
        for (let error of errors) {
            dispatch(addLog({
                gravity: MessageGravity.ERROR,
                message: error
            }))
        }
    }

    let restaurantsOptions = [];

    if (props?.restaurants?.data) {
        restaurantsOptions = props.restaurants.data.map((restaurant: RestaurantModel) => {
            return {
                value: restaurant.slug,
                label: restaurant.name
            }
        })
    }

    let customersOptions = [];

    if (props?.customers) {
        customersOptions = props?.customers?.data?.map((customer: CustomerModel) => {
            return {
                value: customer.email,
                label: customer.email
            }
        })
    }

    let orderStatuses = [];
    if (props?.statuses) {
        orderStatuses = props?.statuses?.data?.map((status: OrderStatusModel) => {
            return {
                value: status.slug,
                label: status.name
            }
        })
    }


    const tableProps: TableProps = {
        columns: [
            {
                name: 'Référence',
                pathToObject: [],
                propertiesToDisplay: ['reference'],
                orderFilter: 'reference',
                searchFilter: {
                    name: 'reference',
                    type: TableSearchFilterType.TEXT,
                }
            },
            {
                name: 'Restaurant',
                pathToObject: ['restaurant'],
                propertiesToDisplay: ['name'],
                searchFilter: {
                    name: 'restaurant.slug',
                    type: TableSearchFilterType.SELECT,
                    options: restaurantsOptions,
                    label: 'Restaurants'
                }
            },
            {
                name: 'Client',
                pathToObject: ['customer'],
                propertiesToDisplay: ['email'],
                notCapitalize: true,
                searchFilter: {
                    name: 'customer.email',
                    type: TableSearchFilterType.SELECT,
                    options: customersOptions,
                    label: 'Clients'
                }
            },
            {
                name: 'Date',
                pathToObject: [],
                propertiesToDisplay: ['orderedAt'],
                orderFilter: 'orderedAt',
                // searchFilter: {
                //     name: 'orderedAt',
                //     type: TableSearchFilterType.TEXT,
                // },
                formater: new DateFormater()
            },
            {
                name: 'Status',
                pathToObject: ['status'],
                propertiesToDisplay: ['name'],
                searchFilter: {
                    name: 'status.slug',
                    type: TableSearchFilterType.SELECT,
                    options: orderStatuses,
                    label: 'Statut'
                }
            }
        ],
        actions: [],
        canSelectLines: true,
        collection: new Collection<OrderModel>(),
        dataIDField: 'reference',
        apiHandler: orderHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const user = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    let isAdmin = user?.isGranted(Role.ROLE_ADMIN);

                    tableProps.actions.push({
                        name: 'Annuler',
                        icon: 'cancel',
                        action: (element: any, orderModel: OrderModel) => {
                            if (confirm('Voulez vous vraiment annuler la commande ? ')) {

                                const context = new Context(false);
                                const orderHandler = new OrderHandler(context.getAdminToken());

                                orderHandler.cancel(orderModel).then(() => {
                                    dispatch(addLog({
                                        gravity: MessageGravity.SUCCESS,
                                        message: 'La commande a bien été annulée'
                                    }))
                                    dispatch(search(true))
                                }).catch((error: any) => {
                                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                                })


                            }
                        },
                        checkCanDisplayAction: (orderModel: OrderModel) => {

                            switch (orderModel?.status?.slug) {
                                case 'payee':
                                    // case 'annulee':
                                    // case 'echec-de-paiement':
                                    // case 'disponible-en-resaurant':
                                    // case 'livree':
                                    // case 'remboursee':
                                    // case 'en-cours-de-livraison':
                                    // case 'en-cours-de-traitement':
                                    // case 'panier':
                                    return true;
                            }

                            return false;
                        }
                    });

                    tableProps.actions.push({
                        name: 'En cours de traitement',
                        icon: 'update_status',
                        action: (element: any, orderModel: OrderModel) => {
                            if (confirm('La commande a t\'elle bien été prise en charge ? ')) {

                                const context = new Context(false);
                                const orderHandler = new OrderHandler(context.getAdminToken());

                                orderHandler.processing(orderModel).then(() => {
                                    dispatch(addLog({
                                        gravity: MessageGravity.SUCCESS,
                                        message: 'Le statut de la commande a bien été mis à jour'
                                    }))
                                    dispatch(search(true))
                                }).catch((error: any) => {
                                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                                })


                            }
                        },
                        checkCanDisplayAction: (orderModel: OrderModel) => {

                            switch (orderModel?.status?.slug) {
                                case 'payee':
                                    // case 'annulee':
                                    // case 'echec-de-paiement':
                                    // case 'disponible-en-resaurant':
                                    // case 'livree':
                                    // case 'remboursee':
                                    // case 'en-cours-de-livraison':
                                    // case 'en-cours-de-traitement':
                                    // case 'panier':
                                    return true;
                            }

                            return false;
                        }
                    });

                    tableProps.actions.push({
                        name: 'En cours de livraison',
                        icon: 'update_status',
                        action: (element: any, orderModel: OrderModel) => {
                            if (confirm('La commande est elle bien en cours de livraison ? ')) {

                                const context = new Context(false);
                                const orderHandler = new OrderHandler(context.getAdminToken());

                                orderHandler.in_delivery(orderModel).then(() => {
                                    dispatch(addLog({
                                        gravity: MessageGravity.SUCCESS,
                                        message: 'Le statut de la commande a bien été mis à jour'
                                    }))
                                    dispatch(search(true))
                                }).catch((error: any) => {
                                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                                })


                            }
                        },
                        checkCanDisplayAction: (orderModel: OrderModel) => {

                            switch (orderModel?.status?.slug) {
                                // case 'payee':
                                // case 'annulee':
                                // case 'echec-de-paiement':
                                // case 'disponible-en-resaurant':
                                // case 'livree':
                                // case 'remboursee':
                                // case 'en-cours-de-livraison':
                                case 'en-cours-de-traitement':
                                    // case 'panier':
                                    return true;
                            }

                            return false;
                        }
                    });

                    tableProps.actions.push({
                        name: 'Afficher',
                        icon: 'view',
                        action: (element: any, orderModel: OrderModel) => {
                            router.push(INVOICE_PATH + orderModel.reference);
                        },
                    });

                    tableProps.actions.push({
                        name: 'Disponible en restaurant',
                        icon: 'update_status',
                        action: (element: any, orderModel: OrderModel) => {
                            if (confirm('La commande est elle bien disponible en restaurant ? ')) {

                                const context = new Context(false);
                                const orderHandler = new OrderHandler(context.getAdminToken());

                                orderHandler.available_in_restaurant(orderModel).then(() => {
                                    dispatch(addLog({
                                        gravity: MessageGravity.SUCCESS,
                                        message: 'Le statut de la commande a bien été mis à jour'
                                    }))
                                    dispatch(search(true))
                                }).catch((error: any) => {
                                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                                })


                            }
                        },
                        checkCanDisplayAction: (orderModel: OrderModel) => {

                            switch (orderModel?.status?.slug) {
                                // case 'payee':
                                // case 'annulee':
                                // case 'echec-de-paiement':
                                // case 'disponible-en-resaurant':
                                // case 'livree':
                                // case 'remboursee':
                                case 'en-cours-de-livraison':
                                case 'en-cours-de-traitement':
                                    // case 'panier':
                                    return true;
                            }

                            return false;
                        }
                    });

                    tableProps.actions.push({
                        name: 'Livrée',
                        icon: 'update_status',
                        action: (element: any, orderModel: OrderModel) => {
                            if (confirm('La commande est elle bien livrée ? ')) {

                                const context = new Context(false);
                                const orderHandler = new OrderHandler(context.getAdminToken());

                                orderHandler.delivered(orderModel).then(() => {
                                    dispatch(addLog({
                                        gravity: MessageGravity.SUCCESS,
                                        message: 'Le statut de la commande a bien été mis à jour'
                                    }))
                                    dispatch(search(true))
                                }).catch((error: any) => {
                                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                                })


                            }
                        },
                        checkCanDisplayAction: (orderModel: OrderModel) => {

                            switch (orderModel?.status?.slug) {
                                // case 'payee':
                                // case 'annulee':
                                // case 'echec-de-paiement':
                                case 'disponible-en-resaurant':
                                // case 'livree':
                                // case 'remboursee':
                                case 'en-cours-de-livraison':
                                    // case 'en-cours-de-traitement':
                                    // case 'panier':
                                    return true;
                            }

                            return false;
                        }
                    });

                    tableProps.actions.push({
                        name: 'Remboursée',
                        icon: 'update_status',
                        action: (element: any, orderModel: OrderModel) => {
                            if (confirm('La commande est elle bien remboursée ? ')) {

                                const context = new Context(false);
                                const orderHandler = new OrderHandler(context.getAdminToken());

                                orderHandler.refunded(orderModel).then(() => {
                                    dispatch(addLog({
                                        gravity: MessageGravity.SUCCESS,
                                        message: 'Le statut de la commande a bien été mis à jour'
                                    }))
                                    dispatch(search(true))
                                }).catch((error: any) => {
                                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                                })


                            }
                        },
                        checkCanDisplayAction: (orderModel: OrderModel) => {

                            switch (orderModel?.status?.slug) {
                                // case 'payee':
                                // case 'annulee':
                                // case 'echec-de-paiement':
                                //     case 'disponible-en-resaurant':
                                case 'livree':
                                    // case 'remboursee':
                                    // case 'en-cours-de-livraison':
                                    // case 'en-cours-de-traitement':
                                    // case 'panier':
                                    return true;
                            }

                            return false;
                        }
                    });

                    if (isAdmin) {

                        // actions.push({
                        //     name: 'add',
                        //     icon: <SVGIcon icon='add'/>,
                        //     link: '/admin/order/add'
                        // })

                        // actions.push({
                        //     name: 'remove',
                        //     icon: <SVGIcon icon='remove'/>,
                        //     onClick: () => {
                        //         if (confirm('Voulez vous vraiment supprimer ces éléments ? ')) {
                        //             dispatch(deleteLines());
                        //         }
                        //     }
                        // })

                        // tableProps.actions.push({
                        //     name: 'Supprimer',
                        //     icon: 'remove',
                        //     action: (element: any) => {
                        //         if (confirm('Voulez vous vraiment supprimer cet élément ? ')) {
                        //             let iri = element.target.getAttribute('entity-iri');
                        //             doDelete(orderHandler, iri, dispatch).then(
                        //                 () => {
                        //                     dispatch(search(true))
                        //                 }
                        //             ).catch((err) => {
                        //                 console.error(err);
                        //             });
                        //         }
                        //     }
                        // });

                    }


                    return (
                        <AdminLayout title="Commandes" actions={new Array<AdminHeaderAction>()}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(true, context.req, context.res);
    let token = appContext.getAdminToken();

    const orderHandler = new OrderHandler(token);
    const restaurantHandler = new RestaurantHandler(token);
    const customerHandler = new CustomerHandler(token);
    const statusHandler = new OrderStatusHandler(token);

    let props: Props = {
        data: [],
        restaurants: [],
        customers: [],
        statuses: [],
        errors: new Array<string>(),
        token: token ?? ''
    };

    await Promise.all(
        [
            orderHandler.getAll(),
            restaurantHandler.getAllInOrders(),
            customerHandler.getAllInOrders(),
            statusHandler.getAll(),
        ]
    ).then((values: any) => {
        props.data = values[0].prepareJSON(true);
        props.restaurants = values[1].prepareJSON(true);
        props.customers = values[2].prepareJSON(true);
        props.statuses = values[3].prepareJSON(true);
    }).catch((errors: any) => {
        props.errors.push(ApiPlatformModel.getFailedError(errors) ?? '');
    });

    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminOrderPage;
export const ADMIN_ORDER_PATH = '/admin/order'