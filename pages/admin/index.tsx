import AdminLayout from "../../components/layouts/AdminLayout";
import AdminDashboard from "../../components/sections/admin/dashboard/AdminDashboard";
import React from "react";

const AdminPage = () => {

    return (
        <AdminLayout title="Dashboard">
            <AdminDashboard/>
        </AdminLayout>
    )

}

export default AdminPage;
export const ADMIN_PATH = '/admin';
