import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps, TableSearchFilterType} from "../../../components/elements/table/AdminTable";
import SVGIcon from "../../../components/elements/icon/SVGIcon";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {
    Collection,
    Context,
    CountryHandler,
    CountryModel,
    CurrencyModel,
    CustomContext,
    FranchiseEmployeeModel,
    PhoneFormater,
    RestaurantHandler,
    RestaurantModel,
    Role
} from "../../../internal";
import {ADMIN_RESTAURANT_EDIT_PATH} from "./edit/[slug]";
import country from "../country";
import {useDispatch} from "react-redux";
import {deleteLines, search} from "../../../redux/actions/admin_table";
import {doDelete} from "../../../redux/sagas/request";

type Props = {
    data: any,
    errors: any,
    countries: any,
    currencies: any,
    token?: string
}

const phoneFormater = new PhoneFormater();
// const dateFormater = new DateFormater();

const AdminRestaurantPage = (props: Props) => {

    const dispatch = useDispatch();

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    let countriesOptions = [];

    if (props?.countries?.data) {
        countriesOptions = props.countries.data.map((country: CountryModel) => {
            return {
                value: country.isoCode,
                label: country.isoCode
            }
        })
    }

    let currenciesOptions = [];

    if (props?.currencies) {
        currenciesOptions = props.currencies.map((currency: CurrencyModel) => {
            return {
                value: currency.isoCode,
                label: currency.symbol
            }
        })
    }

    const restaurantHandler = new RestaurantHandler(token);
    const actions: AdminHeaderAction[] = [];
    const tableProps: TableProps = {
        columns: [
            {
                name: 'Nom',
                pathToObject: [],
                propertiesToDisplay: ['name'],
                orderFilter: 'name',
                searchFilter: {
                    name: 'name',
                    type: TableSearchFilterType.TEXT,
                }
            },
            {
                name: 'Téléphone',
                pathToObject: [],
                propertiesToDisplay: ['phoneNumber'],
                orderFilter: 'phoneNumber',
                searchFilter: {
                    name: 'phoneNumber',
                    type: TableSearchFilterType.TEXT,
                },
                formater: phoneFormater,
                formaterGetParams: (value: any, object: RestaurantModel) => {
                    return {
                        country: object.country
                    }
                }
            },
            {
                name: 'Adresse',
                pathToObject: [],
                propertiesToDisplay: ['street', 'zipCode', 'city'],
                searchFilter: {
                    name: 'address',
                    type: TableSearchFilterType.TEXT,
                },
            },
            {
                name: 'Pays',
                pathToObject: ['country'],
                propertiesToDisplay: ['isoCode'],
                orderFilter: 'country.isoCode',
                searchFilter: {
                    name: 'country.isoCode',
                    type: TableSearchFilterType.SELECT,
                    options: countriesOptions,
                    label: 'Pays'
                },
            },
            {
                name: 'Devise',
                pathToObject: ['defaultCurrency'],
                propertiesToDisplay: ['symbol'],
                orderFilter: 'defaultCurrency.name',
                searchFilter: {
                    name: 'country.defaultCurrency.isoCode',
                    type: TableSearchFilterType.SELECT,
                    options: currenciesOptions,
                    label: 'Devises'
                },
            },
            {
                name: 'Gérants(s)',
                pathToObject: ['leaders'],
                propertiesToDisplay: ['firstname', 'lastname']
            },
        ],
        actions: [],
        canSelectLines: true,
        collection: new Collection<RestaurantModel>(),
        dataIDField: 'slug',
        apiHandler: restaurantHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const user = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    let isAdmin = user?.isGranted(Role.ROLE_ADMIN);

                    tableProps.actions.push({
                        name: 'Editer',
                        icon: 'edit',
                        url: {
                            url: ADMIN_RESTAURANT_EDIT_PATH,
                            urlParams: [
                                {
                                    param: 'slug',
                                    pathToProperty: ['slug']
                                }
                            ]
                        },
                        checkCanDisplayAction: (item: RestaurantModel) => {

                            return isAdmin ||
                                (
                                    user?.isGranted(Role.ROLE_LEAD) &&
                                    user instanceof FranchiseEmployeeModel &&
                                    user.restaurant?.slug == item.slug
                                );
                        }
                    });

                    if (isAdmin) {

                        actions.push({
                            name: 'add',
                            icon: <SVGIcon icon='add'/>,
                            link: '/admin/restaurant/add'
                        })

                        actions.push({
                            name: 'remove',
                            icon: <SVGIcon icon='remove'/>,
                            onClick: () => {
                                if (confirm('Voulez vous vraiment supprimer ces éléments ? ')) {
                                    dispatch(deleteLines());
                                }
                            }
                        })

                        tableProps.actions.push({
                            name: 'Supprimer',
                            icon: 'remove',
                            action: (element: any) => {
                                if (confirm('Voulez vous vraiment supprimer cet élément ? ')) {
                                    let iri = element.target.getAttribute('entity-iri');
                                    doDelete(restaurantHandler, iri, dispatch).then(
                                        () => {
                                            dispatch(search(true))
                                        }
                                    ).catch((err) => {
                                        console.error(err);
                                    });
                                }
                            }
                        });

                    }


                    return (
                        <AdminLayout title="Restaurant" actions={actions}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(true, context.req, context.res);
    let token = appContext.getAdminToken();

    const restaurantHandler = new RestaurantHandler(token);
    const countryHandler = new CountryHandler(token);

    let props = {
        data: [],
        currencies: [],
        countries: [],
        errors: '',
        token
    };

    await Promise.all(
        [
            restaurantHandler.getAll(),
            countryHandler.getAllInRestaurants(),
        ]
    ).then((values: any) => {
        props.data = values[0].prepareJSON(true);
        props.countries = values[1].prepareJSON(true);
        props.currencies = Array.from(
            new Set(
                values[1]?.data?.map(
                    (country: CountryModel) => country?.defaultCurrency?.prepareJSON(true)
                )
            )
        );
    }).catch((errors: any) => {
        props.errors = JSON.stringify(errors);
    });

    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminRestaurantPage;
export const ADMIN_RESTAURANT_PATH = '/admin/restaurant'