import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps, TableSearchFilterType} from "../../../components/elements/table/AdminTable";
import SVGIcon from "../../../components/elements/icon/SVGIcon";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {
    Collection,
    Context,
    CurrencyModel,
    CustomContext,
    CurrencyHandler,
    FranchiseEmployeeModel,
    PhoneFormater,
    Role
} from "../../../internal";
import {ADMIN_CURRENCY_EDIT_PATH} from "./edit/[slug]";
import {useDispatch} from "react-redux";
import {deleteLines, search} from "../../../redux/actions/admin_table";
import {doDelete} from "../../../redux/sagas/request";
import { values } from "lodash";

type Props = {
    data: any,
    errors: any,
    countries: any,
    currencies: any,
    token?: string
}

const phoneFormater = new PhoneFormater();
// const dateFormater = new DateFormater();

const AdminCurrencyPage = (props: Props) => {

    const dispatch = useDispatch();

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    let currenciesOptions = [];

    if (props?.currencies) {
        currenciesOptions = props.currencies.map((currency: CurrencyModel) => {
            return {
                value: currency.isoCode,
                label: currency.symbol
            }
        })
    }

    const currencyHandler = new CurrencyHandler(token);
    const actions: AdminHeaderAction[] = [];
    const tableProps: TableProps = {
        columns: [
            {
                name: 'Iso code',
                pathToObject: [],
                propertiesToDisplay: ['isoCode'],
                orderFilter: 'isoCode',
                searchFilter: {
                    name: 'isoCode',
                    type: TableSearchFilterType.TEXT,
                }
            },
            {
                name: 'Nom',
                pathToObject: [],
                propertiesToDisplay: ['name'],
                orderFilter: 'name',
                searchFilter: {
                    name: 'isoCode',
                    type: TableSearchFilterType.TEXT,
                },
            },
            {
                name: 'Symbole',
                pathToObject: [],
                propertiesToDisplay: ['symbol'],
                /*searchFilter: {
                    name: 'symbol',
                    type: TableSearchFilterType.TEXT,
                },*/
            },
            {
                name: 'Taux',
                pathToObject: [],
                propertiesToDisplay: ['rateForOneEuro'],
                orderFilter: 'name',
                /*searchFilter: {
                    name: 'country.defaultCurrency.isoCode',
                    type: TableSearchFilterType.SELECT,
                    options: currenciesOptions,
                    label: 'Devises'
                },*/
            }
        ],
        actions: [],
        canSelectLines: true,
        collection: new Collection<CurrencyModel>(),
        dataIDField: 'isoCode',
        apiHandler: currencyHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const user = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    let isAdmin = user?.isGranted(Role.ROLE_ADMIN);

                    tableProps.actions.push({
                        name: 'Editer',
                        icon: 'edit',
                        url: {
                            url: ADMIN_CURRENCY_EDIT_PATH,
                            urlParams: [
                                {
                                    param: 'slug',
                                    pathToProperty: ['slug']
                                }
                            ]
                        },
                        checkCanDisplayAction: (item: CurrencyModel) => {

                            return isAdmin ||
                                (
                                    user?.isGranted(Role.ROLE_LEAD) &&
                                    user instanceof FranchiseEmployeeModel
                                );
                        }
                    });

                    if (isAdmin) {

                        actions.push({
                            name: 'add',
                            icon: <SVGIcon icon='add'/>,
                            link: '/admin/currency/add'
                        })

                        actions.push({
                            name: 'remove',
                            icon: <SVGIcon icon='remove'/>,
                            onClick: () => {
                                if (confirm('Voulez vous vraiment supprimer ces éléments ? ')) {
                                    dispatch(deleteLines());
                                }
                            }
                        })

                        tableProps.actions.push({
                            name: 'Supprimer',
                            icon: 'remove',
                            action: (element: any) => {
                                if (confirm('Voulez vous vraiment supprimer cet élément ? ')) {
                                    let iri = element.target.getAttribute('entity-iri');
                                    doDelete(currencyHandler, iri, dispatch).then(
                                        () => {
                                            dispatch(search(true))
                                        }
                                    ).catch((err) => {
                                        console.error(err);
                                    });
                                }
                            }
                        });

                    }


                    return (
                        <AdminLayout title="Devises" actions={actions}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(true, context.req, context.res);
    let token = appContext.getAdminToken();

    const currencyHandler = new CurrencyHandler(token);

    let props = {
        data: [],
        currencies: [],
        errors: '',
        token
    };

    await Promise.all(
        [
            currencyHandler.getAll()
        ]
    ).then(
        (values:any) =>{
            props.data = values[0].prepareJSON(true);
            props.currencies = values[1].prepareJSON(true);
            props.currencies = Array.from(
                new Set(
                    values[1]?.data?.map(
                        (currency: CurrencyModel) => currency.prepareJSON(true)
                    )
                )
            )
        }
    ).catch((errors: any) => {
        props.errors = JSON.stringify(errors);
    });


    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminCurrencyPage;
export const ADMIN_RESTAURANT_PATH = '/admin/currency'