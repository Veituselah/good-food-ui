import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps} from "../../../components/elements/table/AdminTable";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {Collection, Context, CountryHandler, CountryModel, CustomContext, Role} from "../../../internal";
import SVGIcon from "../../../components/elements/icon/SVGIcon";

type Props = {
    data: any,
    token?: string
}

const AdminCountryPage = (props: Props) => {

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    const countryHandler = new CountryHandler<CountryModel>(token);

    const actions: AdminHeaderAction[] = [];

    const tableProps: TableProps = {
        columns: [
            {
                name: 'Code ISO',
                pathToObject: [],
                propertiesToDisplay: ['isoCode']
            },
            {
                name: 'Nom du pays',
                pathToObject: [],
                propertiesToDisplay: ['name']
            },
            {
                name: 'Devise',
                pathToObject: ["defaultCurrency"],
                propertiesToDisplay: ['name']
            },
            {
                name: 'Préfixe de téléphone',
                pathToObject: [],
                propertiesToDisplay: ['phonePrefix']
            }
        ],
        actions: [
            {
                name: 'Edit',
                icon: '',
                action: () => {
                }
            }
        ],
        canSelectLines: true,
        collection: new Collection<CountryModel>(),
        dataIDField: 'isoCode',
        apiHandler: countryHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const user = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    if (user?.isGranted(Role.ROLE_ADMIN)) {
                        actions.push({
                                name: 'add',
                                icon: <SVGIcon icon='add' width="34px" height="34px"/>,
                                link: '/admin/country/add'
                            }
                        )
                    }

                    return (

                        <AdminLayout title="Pays" actions={actions}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(false);

    if (context.req && context.res) {
        appContext = new Context(true, context.req, context.res);
    }

    let token = appContext.getAdminToken();

    const countryRequest = new CountryHandler(token);

    let props = {
        data: {},
        token: token
    };

    await countryRequest.getAll().then(async (collection: Collection<CountryModel>) => {
        props.data = collection.prepareJSON(true);
    }).catch(function () {
    })

    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminCountryPage;
export const ADMIN_COUNTRY_PATH = '/admin/country'