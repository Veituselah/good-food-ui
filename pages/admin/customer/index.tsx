import AdminLayout from "../../../components/layouts/AdminLayout";
import React from "react";
import AdminTable, {TableProps, TableSearchFilterType} from "../../../components/elements/table/AdminTable";
import SVGIcon from "../../../components/elements/icon/SVGIcon";
import {AdminHeaderAction} from "../../../components/sections/admin/header/AdminHeader";
import {
    Collection,
    Context,
    UserModel,
    CustomContext,
    UserHandler,
    FranchiseEmployeeModel,
    PhoneFormater,
    Role, CustomerHandler
} from "../../../internal";
import {ADMIN_USER_EDIT_PATH} from "./edit/[slug]";
import {useDispatch} from "react-redux";
import {deleteLines, search} from "../../../redux/actions/admin_table";
import {doDelete} from "../../../redux/sagas/request";
import { values } from "lodash";

type Props = {
    data: any,
    errors: any,
    countries: any,
    currencies: any,
    token?: string
}

const AdminUserPage = (props: Props) => {

    let token;

    if (!props.token) {
        let appContext = new Context(false);
        token = appContext.getAdminToken();
    } else {
        token = props.token;
    }

    const userHandler = new CustomerHandler(token);
    const actions: AdminHeaderAction[] = [];
    const tableProps: TableProps = {
        columns: [
            {
                name: 'Nom',
                pathToObject: [],
                propertiesToDisplay: ['lastname'],
                orderFilter: 'lastname',
                searchFilter: {
                    name: 'lastname',
                    type: TableSearchFilterType.TEXT,
                }
            },
            {
                name: 'Prénom',
                pathToObject: [],
                propertiesToDisplay: ['firstname'],
                orderFilter: 'firstname',
                searchFilter: {
                    name: 'firstname',
                    type: TableSearchFilterType.TEXT,
                },
            },
            {
                name: 'Mail',
                pathToObject: [],
                propertiesToDisplay: ['email'],
                notCapitalize: true,
                orderFilter: 'email',
                searchFilter: {
                    name: 'email',
                    type: TableSearchFilterType.TEXT,
                },
            },
        ],
        actions: [],
        canSelectLines: true,
        collection: new Collection<UserModel>(),
        dataIDField: 'uuid',
        apiHandler: userHandler
    }

    return (
        <CustomContext.Consumer>
            {
                (context) => {
                    const user = context.getAdmin();

                    if (props.data) {
                        tableProps.collection = props.data;
                    }

                    // let isAdmin = user?.isGranted(Role.ROLE_ADMIN);

                    // tableProps.actions.push({
                    //     name: 'Editer',
                    //     icon: 'edit',
                    //     url: {
                    //         url: ADMIN_USER_EDIT_PATH,
                    //         urlParams: [
                    //             {
                    //                 param: 'slug',
                    //                 pathToProperty: ['slug']
                    //             }
                    //         ]
                    //     },
                    //     checkCanDisplayAction: (item: UserModel) => {
                    //
                    //         return isAdmin ||
                    //             (
                    //                 user?.isGranted(Role.ROLE_LEAD) &&
                    //                 user instanceof FranchiseEmployeeModel
                    //             );
                    //     }
                    // });
                    //
                    // if (isAdmin) {
                    //
                    //     actions.push({
                    //         name: 'add',
                    //         icon: <SVGIcon icon='add'/>,
                    //         link: '/admin/user/add'
                    //     })
                    //
                    //     actions.push({
                    //         name: 'remove',
                    //         icon: <SVGIcon icon='remove'/>,
                    //         onClick: () => {
                    //             if (confirm('Voulez vous vraiment supprimer ces éléments ? ')) {
                    //                 dispatch(deleteLines());
                    //             }
                    //         }
                    //     })
                    //
                    //     tableProps.actions.push({
                    //         name: 'Supprimer',
                    //         icon: 'remove',
                    //         action: (element: any) => {
                    //             if (confirm('Voulez vous vraiment supprimer cet élément ? ')) {
                    //                 let iri = element.target.getAttribute('entity-iri');
                    //                 doDelete(userHandler, iri, dispatch).then(
                    //                     () => {
                    //                         dispatch(search(true))
                    //                     }
                    //                 ).catch((err) => {
                    //                     console.error(err);
                    //                 });
                    //             }
                    //         }
                    //     });
                    //
                    // }
                    //

                    return (
                        <AdminLayout title="Clients" actions={actions}>
                            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <AdminTable {...tableProps}/>
                            </div>
                        </AdminLayout>
                    )
                }
            }
        </CustomContext.Consumer>
    )
}

export async function getServerSideProps(context: any) {

    let appContext = new Context(true, context.req, context.res);
    let token = appContext.getAdminToken();

    const customerHandler = new CustomerHandler(token);

    let props = {
        roleList: [],
        data: [],
        users: [],
        errors: '',
        token
    };

    await Promise.all(
        [
            customerHandler.getAll()
        ]
    ).then(
        (values:any) =>{
            props.data = values[0].prepareJSON(true);
            props.users = values[1].prepareJSON(true);
        }
    ).catch((errors: any) => {
        props.errors = JSON.stringify(errors);
    });


    return {
        props: props, // will be passed to the page component as props
    }
}

export default AdminUserPage;
export const ADMIN_RESTAURANT_PATH = '/admin/user'