import React, {useEffect, useState} from 'react';
import dynamic from 'next/dynamic';
import AppRestaurantAbout from '../../components/sections/restaurant/AppRestaurantAbout';
import AppRestaurantReview from '../../components/sections/restaurant/AppRestaurantReview';
import HomeLayout from '../../components/layouts/HomeLayout';
import MapRestaurant from '../../components/elements/maps/MapRestaurant';
import {SvgBooking, SvgDots, SvgInfo, SvgLocate, SvgMenu, SvgRestaurant, SvgReview} from '../../components/elements/icon/SvgItems';
import {RestaurantHandler} from "../../utils/handlers/ApiPlatform/Restaurant/RestaurantHandler";
import {RestaurantModel} from "../../utils/models/ApiPlatform/Restaurant/RestaurantModel";
import {NOT_FOUND_PATH} from "../404";
import {ERROR_PATH} from "../500";
import {Collection} from "../../utils/handlers/Collection";
import {CommentModel} from "../../utils/models/ApiPlatform/User/CommentModel";
import {useDispatch} from "react-redux";
import {setComments} from "../../redux/actions/comments";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import AppRestaurantMenu from "../../components/sections/restaurant/AppRestaurantMenu";
import {ProductModel} from "../../utils/models/ApiPlatform/Product/ProductModel";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {setProducts} from "../../redux/actions/products";
import {PurchasableProductHandler} from "../../utils/handlers/ApiPlatform/Product/PurchasableProduct/PurchasableProductHandler";
import {OrderHandler} from "../../utils/handlers/ApiPlatform/Order/OrderHandler";
import {Context} from "../../utils/middlewares/Context/Context";
import {OrderModel} from "../../utils/models/ApiPlatform/Order/OrderModel";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import AppRestaurantBooking from "../../components/sections/restaurant/AppRestaurantBooking";
import {HOME_PATH} from "../index";
import {setCart} from "../../redux/actions/cart";

type Props = {
    restaurant?: any,
    comments?: any,
    products?: any,
    errors?: Array<string | null>,
    cart?: any
}

const RestaurantPage = ({cart, restaurant, comments, products, errors}: Props) => {

    const dispatch = useDispatch();

    useEffect(() => {

        dispatch(setComments(comments.data));

        dispatch(setProducts(products));

        if (errors) {

            for (let error of errors) {
                if (error) {
                    dispatch(addLog({
                        gravity: MessageGravity.ERROR,
                        message: error
                    }))
                }
            }
        }

    }, [])

    const [currentTab, setCurrentTab] = useState('1');

    const MapRestaurant = React.useMemo(
        () => dynamic(
            () => import('../../components/elements/maps/MapRestaurant'), {
                loading: () => <p>La carte est en chargement, merci de patienter</p>,
                ssr: false,
            }
        ),
        undefined
    );

    const ButtonRestaurantUserDistance = React.useMemo(
        () => dynamic(
            () => import('../../components/elements/buttons/ButtonRestaurantUserDistance'), {
                ssr: false,
            }
        ),
        undefined
    );

    const handleTabClick = (e: any) => {
        setCurrentTab(e.target.id);
    }

    const tabs = [
        {
            id: 1,
            tabTitle: 'Menu',
            title: 'Menu',
            icon: <SvgMenu/>,
            content: <AppRestaurantMenu restaurant={restaurant}/>
        },
        {
            id: 2,
            tabTitle: 'A propos',
            title: 'A propos',
            icon: <SvgInfo/>,
            content: <AppRestaurantAbout restaurant={restaurant}/>
        },
        {
            id: 3,
            tabTitle: 'Avis',
            title: 'Avis',
            icon: <SvgReview/>,
            content: <AppRestaurantReview restaurant={restaurant}/>
        },
        {
            id: 4,
            tabTitle: 'Réservation',
            title: 'Réservation',
            icon: <SvgBooking/>,
            content: <AppRestaurantBooking restaurant={restaurant}/>
        },
        {
            id: 5,
            tabTitle: "Plan d'accès",
            title: "Plan d'accès",
            icon: <SvgLocate/>,
            content: <MapRestaurant restaurant={restaurant}/>
        },
    ];

    if(cart){
        dispatch(setCart(cart));
    }

    return (
        <HomeLayout>
            <div className="flex flex-wrap w-full">
                <div className="xs:px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20 w-10/12">
                    <div className="max-w-screen-xl sm:mx-auto">
                        {/*RestaurantHeader*/}
                        <div className="grid grid-cols-1 gap-16 row-gap-8 lg:grid-cols-2">
                            {/*RestaurantHeaderCol1*/}
                            <div className="space-y-8 text-base dark:text-neutral-100">
                                {/*RestaurantName*/}
                                <h2 className="max-w-lg font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                                    <span className="relative inline-block">
                                        <SvgDots/>
                                        <span className="relative">{restaurant?.name}</span>
                                    </span>
                                </h2>
                                {/*RestaurantInfos*/}
                                <div className="space-y-4 space-x-2 pb-6">
                                    <button
                                        type="submit"
                                        className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                                    >
                                        <span
                                            className="w-full">{restaurant?.street}, {restaurant?.zipCode} {restaurant?.city}</span>
                                    </button>
                                    <ButtonRestaurantUserDistance restaurant={restaurant}/>
                                    <button
                                        className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                                    >
                                        <span className="w-full">Ouvert</span>
                                    </button>
                                    {/*<button*/}
                                    {/*    type="submit"*/}
                                    {/*    className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"*/}
                                    {/*>*/}
                                    {/*    <span className="w-full">Réserver</span>*/}
                                    {/*</button>*/}
                                </div>
                                {/*RestaurantDescription*/}
                                {restaurant?.description}
                            </div>
                            {/*RestaurantHeaderCol2*/}
                            <div className="hidden md:block space-y-4 text-org-500 dark:text-gre-500 w-2/3 h-2/3">
                                {/*RestaurantImage*/}
                                <SvgRestaurant/>
                            </div>
                        </div>
                        {/*RestaurantDetailsTabsActions*/}
                        <div className="tabs rounded-t-2xl rounded-b-md mt-6 py-2 bg-white dark:bg-neutral-500 border-b border-solid border-org-500 dark:border-gre-500 flex flex-col md:flex-row items-center justify-center">
                            {tabs.map((tab, i) =>
                                <button key={i} id={tab?.id.toString()} disabled={currentTab === `${tab.id}`}
                                        onClick={(handleTabClick)}
                                        className="flex-wrap flex-row w-full md:w-1/5 justify-center inline-flex items-center my-4">{tab.icon}{tab.tabTitle}</button>
                            )}
                        </div>
                        {/*RestaurantDetailsTabsContent*/}
                        {tabs.map((tab, i) =>
                            <div className="rounded-b-2xl shadow-md bg-white dark:bg-neutral-800 bg-opacity-60 dark:bg-opacity-20" key={i}>{
                                currentTab === `${tab.id}` && (
                                    <div>
                                        {tab.content}
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </HomeLayout>
    );
};

export default RestaurantPage;
export const RESTAURANT_PAGE = '/restaurant';

export const getServerSideProps = async (appContext: any) => {

    let context = new Context(false);

    if (appContext.req && appContext.res) {
        context = new Context(true, appContext.req, appContext.res);
    }

    let customerToken = context.getCustomerToken();

    const restaurantHandler = new RestaurantHandler(customerToken);

    let slug = appContext?.query?.slug;
    let restaurantIri = restaurantHandler.buildIri(slug);

    let props: Props = {
        restaurant: {},
        comments: {},
        products: {},
        errors: new Array<string>(),
    };

    await restaurantHandler.get(restaurantIri).then(async (restaurant: RestaurantModel) => {
        props.restaurant = restaurant?.prepareJSON(true);

        // Récupération du panier et changement du restaurant si besoin
        const customerHandler = new CustomerHandler(customerToken);

        if (context.getCustomer()) {

            customerHandler.getCart().then((order: any) => {

                const orderHandler = new OrderHandler(customerToken);

                if (order?.restaurant?.slug !== restaurant?.slug) {

                    if (!order.restaurant) {
                        order.restaurant = {};
                    }

                    order.restaurant.iri = restaurantIri;

                    orderHandler.patch(order).then((order: OrderModel) => {
                        props.cart = order.prepareJSON(true);
                    }).catch(function (err: any) {
                        props.errors?.push(ApiPlatformModel.getFailedError(err));
                    })

                }

            }).catch((err: any) => {
                props.errors?.push(ApiPlatformModel.getFailedError(err));
            });

        }

        await restaurantHandler.getComments(restaurantIri).then(async (comments: Collection<CommentModel>) => {
            props.comments = comments?.prepareJSON(true);
        }).catch(function (err: any) {
            props.errors?.push(ApiPlatformModel.getFailedError(err));
        })

        const productHandler = new PurchasableProductHandler(customerToken);

        await productHandler.getAll().then(async (products: Collection<ProductModel>) => {
            props.products = products?.prepareJSON(true);
        }).catch(function (err: any) {
            props.errors?.push(ApiPlatformModel.getFailedError(err));
        })

    }).catch(function (err: any) {

        let response = err.response;

        switch (response?.status) {

            case 404:
                return {
                    redirect: {
                        permanent: false,
                        destination: NOT_FOUND_PATH
                    }
                }

            case 500:
                return {
                    redirect: {
                        permanent: false,
                        destination: ERROR_PATH
                    }
                }

            case 401:
                return {
                    redirect: {
                        permanent: false,
                        destination: HOME_PATH
                    }
                }

        }

        props.errors?.push(ApiPlatformModel.getFailedError(err));

    })

    return {
        props: props, // will be passed to the page component as props
    }
}
