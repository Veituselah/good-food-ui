import {useRouter} from 'next/router'
import Button from '../../components/elements/buttons/Button'
import HomeLayout from "../../components/layouts/HomeLayout";

const ERROR_MESSAGE = "Nous apprécions au plus haut point chacun d'entre-vous. Nous vous recommandons cependant de présenter une identité numérique en accord avec votre demande d'accès à cette page.";
const CODE = 401;

const UnauthorizedPage = () => {
    const router = useRouter()
    return (
        <HomeLayout title={CODE.toString()}>
            <div className="flex flex-wrap w-full shadow">
                <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-12 lg:px-8 lg:py-20">
                    <div className="sm:mx-auto">
                        <div className="flex flex-row md:mx-auto sm:text-center space-y-4 items-center justify-center">
                            <h2 className="max-w-lg font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                                <span className="flex flex-col relative inline-block mr-4">
                                    <span className="relative">Erreur</span>
                                    <span className="relative text-gre-700 dark:text-org-500">{CODE.toString()}</span>
                                </span>
                            </h2>
                            <p className="border-l-8 border-org-500 dark:border-gre-500 text-base md:text-lg">
                                {ERROR_MESSAGE}
                            </p>
                        </div>
                        <div className="pt-4 w-full sm:w-1/2 md:w-1/4">
                            <Button color="gre" onClick={() => router.back()} label="Revenir en sécurité"/>
                        </div>
                    </div>
                    <a className="flex items-center">
                        <img className="w-auto block dark:hidden" src="/icons/Error401AnimatedOrg.svg" alt="site"/>
                        <img className="w-auto hidden dark:block" src="/icons/Error401AnimatedGre.svg" alt="site"/>
                    </a>
                </div>
            </div>
        </HomeLayout>
    )

}
export default UnauthorizedPage;
export const UNAUTHORIZED_PATH = '/' + CODE;

export const getServerSideProps = async ({res}: {res: any}) => {

    res.statusCode = CODE;

    return {
        props: {},
    };
};