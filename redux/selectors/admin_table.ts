import {AdminTableState} from "../reducers/admin_table";

export const getAdminTable = (state: any): AdminTableState => state.admin_table;