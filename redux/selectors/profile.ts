import {ProfileState} from "../reducers/profile";

export const getProfile = (state: any): ProfileState => state.profile;