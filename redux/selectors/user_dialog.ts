import {UserDialogState} from "../reducers/user_dialog";

export const getUserDialog = (state: any): UserDialogState => state.user_dialog;