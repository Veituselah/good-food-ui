import {LoaderState} from "../reducers/loader";

export const getLoader = (state: any): LoaderState => state.loader;