import {CartState} from "../reducers/cart";

export const getCart = (state: any): CartState => state.cart;