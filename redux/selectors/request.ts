import {RequestState} from "../reducers/request";

export const getRequest = (state: any): RequestState => state.request;