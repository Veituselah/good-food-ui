import {ContextState} from "../reducers/context";

export const getContext = (state: any): ContextState => state.context;