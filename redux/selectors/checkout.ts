import {CheckoutState} from "../reducers/checkout";

export const getCheckout = (state: any): CheckoutState => state.checkout;