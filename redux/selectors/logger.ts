import {LoggerState} from "../reducers/logger";

export const getLogger = (state: any): LoggerState => state.logger;