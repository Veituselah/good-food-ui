import {CommentState} from "../reducers/comments";

export const getComments = (state: any): CommentState => state.comments;