import {ProductState} from "../reducers/products";

export const getProducts = (state: any): ProductState => state.products;