export enum ActionRequest {
    NEW_REQUEST = 'NEW_REQUEST',
    END_REQUEST = 'END_REQUEST',
}

export function newRequest() {
    return {
        type: ActionRequest.NEW_REQUEST,
    };
}

export function endRequest() {
    return {type: ActionRequest.END_REQUEST};
}