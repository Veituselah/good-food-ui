import {Context} from "../../internal";

export enum ActionContext {
    SET_CUSTOMER = 'SET_CUSTOMER',
    SET_EXPIRED_CUSTOMER = 'SET_EXPIRED_CUSTOMER',
    LOGOUT_CUSTOMER = 'LOGOUT_CUSTOMER',
    SET_ADMIN = 'SET_ADMIN',
    SET_EXPIRED_ADMIN = 'SET_EXPIRED_ADMIN',
    LOGOUT_ADMIN = 'LOGOUT_ADMIN',
}


export function loginCustomer(token: string) {

    return {
        type: ActionContext.SET_CUSTOMER,
        token
    }
}

export function setExpiredCustomer(context: Context) {

    context.logout(true, false);

    return {
        type: ActionContext.SET_EXPIRED_CUSTOMER,
    }
}

export function logoutCustomer(context: Context) {

    context.logout(true, false);

    return {
        type: ActionContext.LOGOUT_CUSTOMER,
    }
}

export function loginAdmin(token: string) {

    return {
        type: ActionContext.SET_ADMIN,
        token
    }
}

export function setExpiredAdmin(context: Context) {

    context.logout(false, true);

    return {
        type: ActionContext.SET_EXPIRED_ADMIN,
    }
}

export function logoutAdmin(context: Context) {

    context.logout(false, true);

    return {
        type: ActionContext.LOGOUT_ADMIN,
    }
}