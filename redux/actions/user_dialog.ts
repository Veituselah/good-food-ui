import {UserDialogSection} from "../reducers/user_dialog";

export enum ActionUserDialog {
    SHOW_DIALOG_SECTION = 'SHOW_DIALOG_SECTION',
    SHOW_DIALOG = 'SHOW_DIALOG',
    HIDE_DIALOG = 'HIDE_DIALOG',
}

export function showUserDialog() {
    return {
        type: ActionUserDialog.SHOW_DIALOG,
    };
}

export function showUserDialogSection(section: UserDialogSection) {
    return {
        type: ActionUserDialog.SHOW_DIALOG_SECTION,
        section
    };
}

export function hideUserDialog() {
    return {
        type: ActionUserDialog.HIDE_DIALOG,
    };
}

