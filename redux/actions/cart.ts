export enum ActionCart {
    SET_CART = 'SET_CART',
    SAVE_PRODUCT_INTO_CART = 'SAVE_PRODUCT_INTO_CART',
    REMOVE_CART_LINE = 'REMOVE_CART_LINE',
}

export function setCart(cart: any){

    return {
        type: ActionCart.SET_CART,
        cart
    }

}

export function saveCartLine(cartLine: any){

    return {
        type: ActionCart.SAVE_PRODUCT_INTO_CART,
        cartLine,
    }

}

export function removeCartLine(cartLineID: any){

    return {
        type: ActionCart.REMOVE_CART_LINE,
        cartLineID,
    }

}