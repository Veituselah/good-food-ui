import {CustomerModel} from "../../utils/models/ApiPlatform/User/Customer/CustomerModel";
import {Collection} from "../../utils/handlers/Collection";
import {OrderModel} from "../../utils/models/ApiPlatform/Order/OrderModel";

export enum ActionProfile {
    SET_PROFILE_CUSTOMER = 'SET_PROFILE_CUSTOMER',
    SET_PROFILE_ORDERS = 'SET_PROFILE_ORDERS',
}

export function setProfileCustomer(customer: CustomerModel){
    return {
        type: ActionProfile.SET_PROFILE_CUSTOMER,
        customer
    }
}

export function setProfileOrders(orders: Collection<OrderModel>){
    return {
        type: ActionProfile.SET_PROFILE_ORDERS,
        orders
    }
}
