import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {Collection} from "../../utils/handlers/Collection";
import {ApiPlatformHandler} from "../../utils/handlers/ApiPlatform/ApiPlatformHandler";
import {OrderFilterTypeEnum} from "../../utils/filters/ApiPlatform/OrderFilter";

export enum ActionAdminTable {
    SEARCH = 'SEARCH',
    TOGGLE_ORDER_FILTER = 'TOGGLE_ORDER_FILTER',
    SET_ORDER_FILTER = 'SET_ORDER_FILTER',
    MANAGE_SEARCH_FILTER = 'MANAGE_SEARCH_FILTER',
    MANAGE_EXISTS_FILTER = 'MANAGE_EXISTS_FILTER',
    SET_PAGE = 'SET_PAGE',
    SET_API_HANDLER = 'SET_API_HANDLER',
    SEARCH_SUCCESS = 'SEARCH_SUCCESS',
    SEARCH_FAILED = 'SEARCH_FAILED',
    TOOGLE_SELECT_IRI = 'TOOGLE_SELECT_IRI',
    RESET_SELECT_IRI = 'RESET_SELECT_IRI',
    DELETE_SELECTED_LINES = 'DELETE_SELECTED_LINES',
}

export function search(forceSearch = false) {
    return {
        type: ActionAdminTable.SEARCH,
        forceSearch
    };
}


export function setApiHandler(apiHandler: ApiPlatformHandler<ApiPlatformModel>) {
    return {
        type: ActionAdminTable.SET_API_HANDLER,
        apiHandler
    };
}

export function toggleFilterOnField(fieldName: string) {

    return {
        type: ActionAdminTable.TOGGLE_ORDER_FILTER,
        fieldName
    };

}

export function setOrderFilterOnField(fieldName: string, type: OrderFilterTypeEnum) {

    return {
        type: ActionAdminTable.SET_ORDER_FILTER,
        fieldName,
        orderFilterType: type
    };

}

export function manageSearchFilterOnField(fieldName: string, value: any) {

    return {
        type: ActionAdminTable.MANAGE_SEARCH_FILTER,
        fieldName,
        value
    };

}

export function manageExistsFilterOnField(fieldName: string, value: any) {

    return {
        type: ActionAdminTable.MANAGE_EXISTS_FILTER,
        fieldName,
        value
    };

}

export function searchSuccess(collection: Collection<ApiPlatformModel>) {
    return {
        type: ActionAdminTable.SEARCH_SUCCESS,
        collection
    };
}

export function searchFailed(error: any) {
    return {
        type: ActionAdminTable.SEARCH_FAILED,
        error
    };
}

export function setPage(page: number) {
    return {
        type: ActionAdminTable.SET_PAGE,
        page
    };
}

export function toggleSelectIri(iri: string) {
    return {
        type: ActionAdminTable.TOOGLE_SELECT_IRI,
        iri
    };
}

export function deleteLines() {
    return {
        type: ActionAdminTable.DELETE_SELECTED_LINES,
    };
}

export function resetSelectedLines() {
    return {
        type: ActionAdminTable.RESET_SELECT_IRI,
    };
}