export enum ActionProducts {
    SET_PRODUCTS = 'SET_PRODUCTS',
}

export function setProducts(products: any) {
    return {
        type: ActionProducts.SET_PRODUCTS,
        products: products
    };
}
