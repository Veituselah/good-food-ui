export enum ActionLoader {
    TOGGLE_LOADER = 'TOGGLE_LOADER',
    SHOW_LOADER = 'SHOW_LOADER',
    HIDE_LOADER = 'HIDE_LOADER',
}

export function showLoader(title?: any) {
    return getLoaderActionData(true, title);
}

export function hideLoader() {
    return getLoaderActionData(false, null);
}

export function toggleLoader(){
    return {type: ActionLoader.TOGGLE_LOADER};
}

function getLoaderActionData(showLoader: boolean, title?: any) {
    return {
        type: showLoader ? ActionLoader.SHOW_LOADER : ActionLoader.HIDE_LOADER,
        showLoader,
        title
    }
}