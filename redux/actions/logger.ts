import {generateUniqueRandomKey, Message} from "../../internal";

export enum ActionLogger {
    LOG_MESSAGE = 'LOG_MESSAGE',
    REMOVE_MESSAGE = 'REMOVE_MESSAGE',
}

export function addLog(message: Message) {
    if (!message.key) {
        message.key = generateUniqueRandomKey();
    }

    return {
        type: ActionLogger.LOG_MESSAGE,
        message
    }
}

export function removeMessageAtIndex(message: Message) {
    return {
        type: ActionLogger.REMOVE_MESSAGE,
        message
    }
}
