export enum ActionComments {
    NEW_COMMENT = 'NEW_COMMENT',
    SET_COMMENTS = 'SET_COMMENTS',
}

export function newComment(comment: any) {
    return {
        type: ActionComments.NEW_COMMENT,
        comment: comment
    };
}

export function setComments(comments: any) {
    return {
        type: ActionComments.SET_COMMENTS,
        comments: comments
    };
}
