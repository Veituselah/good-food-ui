export enum ActionCheckout {
    GO_TO_STEP = 'GO_TO_STEP',
    NEXT_STEP = 'NEXT_STEP',
    SET_ADDRESSES = 'SET_ADDRESSES',
    SET_COUNTRIES = 'SET_COUNTRIES',
    SET_CHECKOUT_CUSTOMER = 'SET_CHECKOUT_CUSTOMER',
    SET_INVOICE_ADDRESS = 'SET_INVOICE_ADDRESS',
    SET_DELIVERY_ADDRESS = 'SET_DELIVERY_ADDRESS',
}

export function goToStep(step: number) {
    return {
        type: ActionCheckout.GO_TO_STEP,
        step
    };
}

export function nextStep() {
    return {
        type: ActionCheckout.NEXT_STEP,
    };
}

export function setAddresses(addresses: any) {
    return {
        type: ActionCheckout.SET_ADDRESSES,
        addresses
    };
}

export function setCountries(countries: any) {
    return {
        type: ActionCheckout.SET_COUNTRIES,
        countries
    };
}

export function setCustomer(customer: any) {
    return {
        type: ActionCheckout.SET_CHECKOUT_CUSTOMER,
        customer
    };
}

export function setCheckoutInvoiceAddress(address: any) {
    return {
        type: ActionCheckout.SET_INVOICE_ADDRESS,
        address
    };
}

export function setCheckoutDeliveryAddress(address: any) {
    return {
        type: ActionCheckout.SET_DELIVERY_ADDRESS,
        address
    };
}
