import {ActionComments} from "../actions/comments";

export type CommentState = {
    comments: Array<any>
}

const initialState: CommentState = {
    comments: []
}

function reducer(state = initialState, action: any) {

    switch (action.type) {

        case ActionComments.NEW_COMMENT:

            let comments = state.comments;
            comments.push(action.comment);

            return {
                ...state,
                comments
            }

        case ActionComments.SET_COMMENTS:
            return {
                ...state,
                comments: action.comments
            }

        default:
            return state
    }
}

export default reducer