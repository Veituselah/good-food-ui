import {ActionCart} from "../actions/cart";
import {Context} from "../../utils/middlewares/Context/Context";

export interface CartState {
    cart: any,
}

const initialState: CartState = {
    cart: null,
}

function reducer(state = initialState, action: any) {

    const getCartLineIndex = (orderLineID: number): number | null => {

        const cart = state.cart;

        let sameCartLineIndex: any = null;

        for (let cartLineIndex in cart?.orderLines) {
            const cartLine = cart?.orderLines[cartLineIndex];

            if (cartLine?.id === orderLineID) {
                sameCartLineIndex = cartLineIndex;
                break;
            }

        }

        return sameCartLineIndex;
    }

    switch (action.type) {

        case ActionCart.SET_CART : {

            const context = new Context(false);

            if(action?.cart){
                context.setCartIdentifier(action?.cart.reference);
            } else {
                context.removeCart()
            }

            return {
                ...state,
                cart: action.cart
            };
        }

        case ActionCart.SAVE_PRODUCT_INTO_CART : {

            const cart = state.cart;

            let sameCartLineIndex: any = getCartLineIndex(action.cartLine.id);

            if (sameCartLineIndex) {
                cart.orderLines[sameCartLineIndex] = action.cartLine;
            } else {
                cart.orderLines.push(action.cartLine);
            }

            return {
                ...state,
                cart
            };
        }

        case ActionCart.REMOVE_CART_LINE : {

            const cart = state.cart;

            let sameCartLineIndex: any = getCartLineIndex(action.cartLineID);

            if (sameCartLineIndex) {
                cart.orderLines.splice(sameCartLineIndex, 1);
            }

            return {
                ...state,
                cart
            };
        }

        default:
            return state
    }
}


export default reducer