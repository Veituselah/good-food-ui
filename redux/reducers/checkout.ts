import {ActionCheckout} from "../actions/checkout";


export type CheckoutState = {
    step: number,
    addresses: any,
    countries: any,
    customer: any,
    deliveryAddress: any,
    invoiceAddress: any,
}

const initialState: CheckoutState = {
    step: 0,
    addresses: [],
    countries: [],
    customer: null,
    deliveryAddress: null,
    invoiceAddress: null,
}

function reducer(state = initialState, action: any) {

    switch (action.type) {

        case ActionCheckout.GO_TO_STEP:
            return {
                ...state,
                step: action.step
            }

        case ActionCheckout.NEXT_STEP:
            return {
                ...state,
                step: state.step + 1
            }

        case ActionCheckout.SET_ADDRESSES:
            return {
                ...state,
                addresses: action.addresses
            }

        case ActionCheckout.SET_COUNTRIES:
            return {
                ...state,
                countries: action.countries
            }

        case ActionCheckout.SET_CHECKOUT_CUSTOMER:
            return {
                ...state,
                customer: action.customer
            }

        case ActionCheckout.SET_INVOICE_ADDRESS:
            return {
                ...state,
                invoiceAddress: action.address
            }

        case ActionCheckout.SET_DELIVERY_ADDRESS:
            return {
                ...state,
                deliveryAddress: action.address
            }

        default:
            return state
    }
}

export default reducer