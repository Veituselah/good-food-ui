import {CustomerModel} from "../../utils/models/ApiPlatform/User/Customer/CustomerModel";
import {Collection} from "../../utils/handlers/Collection";
import {OrderModel} from "../../utils/models/ApiPlatform/Order/OrderModel";
import {CommentModel} from "../../utils/models/ApiPlatform/User/CommentModel";
import {AddressModel} from "../../utils/models/ApiPlatform/User/Customer/AddressModel";
import {ActionProfile} from "../actions/profile";

export type ProfileState = {
    customer?: CustomerModel,
    orders?: Collection<OrderModel>,
    comments?: Collection<CommentModel>,
    addresses?: Collection<AddressModel>,
}

const initialState: ProfileState = {}

function reducer(state = initialState, action: any) {

    switch (action.type) {

        case ActionProfile.SET_PROFILE_CUSTOMER:
            return {
                ...state,
                customer: action?.customer
            }

        case ActionProfile.SET_PROFILE_ORDERS:
            return {
                ...state,
                orders: action?.orders
            }

        default:
            return state
    }
}

export default reducer