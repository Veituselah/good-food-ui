import {ActionUserDialog} from "../actions/user_dialog";

export enum UserDialogSection {
    LOGIN,
    REGISTER,
    LOST_PASSWORD
}

export type UserDialogState = {
    section: UserDialogSection,
    show: boolean
}

const initialState: UserDialogState = {
    section: UserDialogSection.LOGIN,
    show: false
}

function reducer(state = initialState, action: any) {

    switch (action.type) {

        case ActionUserDialog.SHOW_DIALOG:
            return {
                ...state,
                show: true
            }

        case ActionUserDialog.HIDE_DIALOG:
            return {
                ...state,
                show: false
            }

        case ActionUserDialog.SHOW_DIALOG_SECTION:

            return {
                ...state,
                show: true,
                section: action.section
            }

        default:
            return state
    }
}

export default reducer