import {ActionProducts} from "../actions/products";

export type ProductState = {
    products: any
}

const initialState: ProductState = {
    products: {}
}

function reducer(state = initialState, action: any) {

    switch (action.type) {

        case ActionProducts.SET_PRODUCTS:
            return {
                ...state,
                products: action.products
            }

        default:
            return state
    }
}

export default reducer