import {ActionLogger} from '../actions/logger'
import {Message} from "../../internal";

export interface LoggerState {
    messagesToLogg: Array<Message>,
}

const initialState: LoggerState = {
    messagesToLogg: new Array<Message>()
}

function reducer(state = initialState, action: any) {
    switch (action.type) {

        case ActionLogger.LOG_MESSAGE:
            state.messagesToLogg.push(action.message);
            return {...state};

        case ActionLogger.REMOVE_MESSAGE:

            let messageIndex = state.messagesToLogg.indexOf(action.message);

            state.messagesToLogg.splice(messageIndex, 1);
            return {...state};

        default:
            return state
    }
}

export default reducer