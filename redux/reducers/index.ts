import request from './request';
import loader from './loader';
import logger from './logger';
import admin_table from './admin_table';
import user_dialog from './user_dialog';
import comments from './comments';
import cart from './cart';
import context from './context';
import products from './products';
import checkout from './checkout';
import profile from './profile';
import {combineReducers} from "redux";

export default combineReducers({
    request,
    loader,
    logger,
    admin_table,
    user_dialog,
    comments,
    products,
    cart,
    context,
    checkout,
    profile,
})
