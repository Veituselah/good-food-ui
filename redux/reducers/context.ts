import {ActionContext} from "../actions/context";
import {Dispatch} from "@reduxjs/toolkit";
import {addLog} from "../actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import jwtDecode from "jwt-decode";

export interface ContextState {
    admin: string | null,
    customer: string | null,
}

const initialState: ContextState = {
    admin: null,
    customer: null,
}

function reducer(state = initialState, action: any) {
    switch (action.type) {

        case ActionContext.SET_CUSTOMER : {
            return {
                ...state,
                customer: action.token
            };
        }

        case ActionContext.LOGOUT_CUSTOMER :
        case ActionContext.SET_EXPIRED_CUSTOMER : {
            return {
                ...state,
                customer: null,
                cart: null
            };
        }

        case ActionContext.SET_ADMIN : {
            return {
                ...state,
                admin: action.token
            };
        }

        case ActionContext.LOGOUT_ADMIN :
        case ActionContext.SET_EXPIRED_ADMIN : {
            return {
                ...state,
                admin: null
            };
        }

        default:
            return state
    }
}

export const decodeCustomer = (customerToken: any, dispatch: Dispatch): any => {

    try {
        return jwtDecode(customerToken ?? '');
    } catch (e) {

        dispatch(addLog({
            gravity: MessageGravity.INFO,
            message: 'Votre token d\'authentification est arrivé à échéance, veuillez vous reconnectez.'
        }))
    }

    return {};
}

export default reducer