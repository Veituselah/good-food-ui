import {ActionRequest} from '../actions/request'
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";

export interface RequestState {
    numberRequestInProgress: number,
    model?: ApiPlatformModel,
}

const initialState: RequestState = {
    numberRequestInProgress: 0,
}

function reducer(state = initialState, action: any) {
    switch (action.type) {

        case ActionRequest.NEW_REQUEST:
            return {
                ...state,
                ...{numberRequestInProgress: state.numberRequestInProgress + 1},
            }

        case ActionRequest.END_REQUEST:
            return {
                ...state,
                ...{numberRequestInProgress: (state.numberRequestInProgress > 0 ? state.numberRequestInProgress - 1 : 0)},
            }

        default:
            return state
    }
}

export default reducer