import {ActionAdminTable} from '../actions/admin_table'
import {OrderFilter} from "../../utils/filters/ApiPlatform/OrderFilter";
import {Collection} from "../../utils/handlers/Collection";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {ApiPlatformHandler} from "../../utils/handlers/ApiPlatform/ApiPlatformHandler";
import {SearchFilter} from "../../utils/filters/ApiPlatform/SearchFilter";
import {ExistsFilter} from "../../utils/filters/ApiPlatform/ExistsFilter";

export interface AdminTableState {
    apiHandler?: ApiPlatformHandler<ApiPlatformModel>;
    orderBy?: OrderFilter,
    searchFilters: Array<SearchFilter>,
    existsFilters: Array<ExistsFilter>,
    collection?: Collection<ApiPlatformModel>,
    error?: any,
    page: number,
    selectedIri: Array<string>,
    sometingHasChangedSinceLastRequest: boolean
}

const initialState: AdminTableState = {
    searchFilters: new Array<SearchFilter>(),
    existsFilters: new Array<ExistsFilter>(),
    page: 1,
    selectedIri: [],
    sometingHasChangedSinceLastRequest: true
}

function reducer(state = initialState, action: any) {

    switch (action.type) {

        case ActionAdminTable.SET_API_HANDLER:
            return {
                ...state,
                apiHandler: action.apiHandler
            }

        case ActionAdminTable.TOGGLE_ORDER_FILTER:

            let orderBy = state.orderBy;

            if (orderBy && orderBy.fieldName === action.fieldName) {
                orderBy.switchOrderFilterType();
            } else {
                orderBy = new OrderFilter(action.fieldName);
            }

            return {
                ...state,
                orderBy: orderBy,
                sometingHasChangedSinceLastRequest: true
            }

        case ActionAdminTable.SET_ORDER_FILTER:
            return {
                ...state,
                orderBy: new OrderFilter(action.fieldName, action.orderFilterType),
                sometingHasChangedSinceLastRequest: true
            }

        case ActionAdminTable.MANAGE_SEARCH_FILTER:

            let searchFilters = state.searchFilters ?? [];
            const searchIndex = searchFilters.findIndex(filter => filter.fieldName === action.fieldName);

            if (searchIndex > -1) {
                if (action.value) {
                    searchFilters[searchIndex].value = action.value;
                } else {
                    searchFilters.splice(searchIndex, 1);
                }
            } else if (action.value) {
                searchFilters.push(new SearchFilter(action.fieldName, action.value));
            }

            return {
                ...state,
                searchFilters,
                sometingHasChangedSinceLastRequest: true
            }

        case ActionAdminTable.MANAGE_EXISTS_FILTER:

            let existsFilters = state.existsFilters ?? [];
            const searchExistsIndex = existsFilters.findIndex(filter => filter.fieldName === action.fieldName);

            if (searchExistsIndex > -1) {
                if (typeof action.value === "boolean") {
                    existsFilters[searchExistsIndex].exists = action.value;
                } else {
                    existsFilters.splice(searchExistsIndex, 1);
                }
            } else if (action.value) {
                existsFilters.push(new ExistsFilter(action.fieldName, action.value));
            }

            return {
                ...state,
                existsFilters,
                sometingHasChangedSinceLastRequest: true
            }

        case ActionAdminTable.SEARCH:
            return {
                ...state,
                page: 1,
                sometingHasChangedSinceLastRequest: action.forceSearch ?? state.sometingHasChangedSinceLastRequest
            }

        case ActionAdminTable.SEARCH_SUCCESS:
            return {
                ...state,
                collection: action.collection,
                sometingHasChangedSinceLastRequest: false
            }

        case ActionAdminTable.SEARCH_FAILED:
            return {
                ...state,
                errors: action.errors,
                sometingHasChangedSinceLastRequest: false
            }

        case ActionAdminTable.SET_PAGE:
            return {
                ...state,
                page: action.page < 1 ? 1 : action.page,
                sometingHasChangedSinceLastRequest: true
            }

        case ActionAdminTable.TOOGLE_SELECT_IRI:

            let selectedIri = state.selectedIri;
            let indexIri = selectedIri.indexOf(action.iri);

            if (indexIri >= 0) {
                selectedIri.splice(indexIri, 1);
            } else {
                selectedIri.push(action.iri);
            }

            return {
                ...state,
                selectedIri
            }

        case ActionAdminTable.RESET_SELECT_IRI:
            return {
                ...state,
                selectedIri: []
            }

        default:
            return state
    }
}

export default reducer