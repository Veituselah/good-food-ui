import {ActionLoader} from '../actions/loader'

export interface LoaderState {
    loader: boolean,
    title?: any
}

const initialState: LoaderState = {
    loader: false,
    title: null
}

function reducer(state = initialState, action: any) {
    switch (action.type) {

        case ActionLoader.TOGGLE_LOADER:
            return {
                ...state,
                ...{loader: !state.loader},
            }

        case ActionLoader.SHOW_LOADER:
        case ActionLoader.HIDE_LOADER:
            return {
                ...state,
                ...{loader: action.showLoader},
                title: action.title
            }

        default:
            return state
    }
}

export default reducer