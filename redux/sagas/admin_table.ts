import {call, put, select, takeLatest} from 'redux-saga/effects'
import {endRequest, newRequest,} from "../actions/request";
import {ActionAdminTable, resetSelectedLines, search, searchFailed, searchSuccess} from "../actions/admin_table";
import {AdminTableState} from "../reducers/admin_table";
import {Collection} from "../../utils/handlers/Collection";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {Message} from "../../utils/middlewares/Logger/Message";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {addLog} from "../actions/logger";
import {getAdminTable} from "../selectors/admin_table";
import {ApiPlatformHandler} from "../../utils/handlers/ApiPlatform/ApiPlatformHandler";

function* searchTableSaga() {

    const state: AdminTableState = yield select(getAdminTable);

    if (state.sometingHasChangedSinceLastRequest) {

        const apiHandler = state.apiHandler;

        if (apiHandler instanceof ApiPlatformHandler) {

            try {

                yield put(newRequest());

                let orderBy = state.orderBy ? [state.orderBy] : [];
                let searchFilters = state.searchFilters ?? [];
                let existsFilter = state.existsFilters ?? [];
                let page = state.page ?? 1;
                let args: any = [orderBy, searchFilters, page, false, existsFilter];
                let res: Collection<ApiPlatformModel> = yield call([apiHandler, apiHandler.getAll], ...args);
                yield put(searchSuccess(res));

            } catch (err: any) {

                let message: Message = {
                    title: "Une erreur est survenue",
                    message: err.message ?? '',
                    gravity: MessageGravity.ERROR,
                };

                yield put(addLog(message));

                yield put(searchFailed(err));
            } finally {
                yield put(endRequest());
            }

        }
    }
}

function* deleteLinesTableSaga() {

    const state: AdminTableState = yield select(getAdminTable);
    const {apiHandler, selectedIri} = state;

    if (apiHandler instanceof ApiPlatformHandler) {

        let nbToDelete = selectedIri.length;
        let nbDelete = 0;

        for (let iri of selectedIri) {

            try {

                yield put(newRequest());

                let hasBeenDeleted: boolean = yield call([apiHandler, apiHandler.delete], [iri] as any);

                if (hasBeenDeleted) {
                    nbDelete++;
                }

            } catch (err: any) {

                console.error(err);

                let message: Message = {
                    title: "Une erreur est survenue",
                    message: err.message ?? '',
                    gravity: MessageGravity.ERROR,
                };

                yield put(addLog(message));

            } finally {
                yield put(endRequest());
            }
        }

        let message = {
            title: "La suppression a bien été réalisée",
            message: nbDelete + '/' + nbToDelete + " entité" + (nbDelete > 1 ? 's' : '') + " supprimée" + (nbDelete > 1 ? 's' : ''),
            gravity: MessageGravity.SUCCESS,
        }


        yield put(resetSelectedLines());

        yield put(addLog(message));

        yield put(search(true));

    }

}

function* adminTableSaga() {
    yield takeLatest(ActionAdminTable.SEARCH, searchTableSaga)
    yield takeLatest(ActionAdminTable.TOGGLE_ORDER_FILTER, searchTableSaga)
    yield takeLatest(ActionAdminTable.SET_ORDER_FILTER, searchTableSaga)
    yield takeLatest(ActionAdminTable.SET_PAGE, searchTableSaga)
    yield takeLatest(ActionAdminTable.DELETE_SELECTED_LINES, deleteLinesTableSaga)
}


export default adminTableSaga