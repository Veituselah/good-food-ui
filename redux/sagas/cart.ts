import {put, takeEvery, takeLatest} from 'redux-saga/effects'
import {ActionContext} from "../actions/context";
import {ActionCart, setCart} from "../actions/cart";
import jwtDecode from "jwt-decode";
import {OrderHandler} from "../../utils/handlers/ApiPlatform/Order/OrderHandler";
import {addLog} from "../actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {OrderModel} from "../../utils/models/ApiPlatform/Order/OrderModel";
import {Context} from "../../utils/middlewares/Context/Context";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";

function* removeCart() {
    yield put(setCart(null));
}

function* initCart(action: any): any {

    let token = action?.token;

    let customerHandler = new CustomerHandler(token);

    try {
        let data: OrderModel = yield customerHandler.getCart();

        if (data) {
            yield put(setCart(data));
        }


    } catch (e) {

        yield put(addLog({
            gravity: MessageGravity.ERROR,
            message: ApiPlatformModel.getFailedError(e) ?? ''
        }))

    }
}

export function getCartFromToken(token: string): any {

    let data: any = jwtDecode(token);

    if (data && data.cart) {
        return data.cart;
    }

    return null;

}

function* updateCartAfterUpdatingCartLine() {

    const context = new Context(false);

    if (context.getCustomer() && context.getCartIdentifier()) {

        let orderHandler = new OrderHandler(context.getCustomerToken());
        let iri = orderHandler.buildIri(context.getCartIdentifier());

        try {
            let data: OrderModel = yield orderHandler.get(iri);

            if (data) {
                yield put(setCart(data));

                yield put(addLog({
                    gravity: MessageGravity.SUCCESS,
                    message: 'Votre panier a bien été mis à jour'
                }))
            }

        } catch (e) {

            yield put(addLog({
                gravity: MessageGravity.ERROR,
                message: ApiPlatformModel.getFailedError(e) ?? ''
            }))

        }

    }

}

function* cartSaga() {
    yield takeEvery(ActionContext.LOGOUT_CUSTOMER, removeCart)
    yield takeEvery(ActionContext.SET_EXPIRED_CUSTOMER, removeCart)
    yield takeEvery(ActionContext.SET_CUSTOMER, initCart)
    yield takeLatest(ActionCart.SAVE_PRODUCT_INTO_CART, updateCartAfterUpdatingCartLine)
    yield takeLatest(ActionCart.REMOVE_CART_LINE, updateCartAfterUpdatingCartLine)
}


export default cartSaga