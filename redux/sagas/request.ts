import {put, select, takeEvery} from 'redux-saga/effects'
import {ActionRequest, endRequest, newRequest,} from "../actions/request";

import {hideLoader, showLoader} from "../actions/loader";
import {getRequest} from "../selectors/request";
import {RequestState} from "../reducers/request";
import {ApiPlatformHandler} from "../../utils/handlers/ApiPlatform/ApiPlatformHandler";
import {Message} from "../../utils/middlewares/Logger/Message";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {addLog} from "../actions/logger";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {Dispatch} from "redux";

function* toggleLoaderFromRequestState() {
    const state: RequestState = yield select(getRequest);

    if (state.numberRequestInProgress === 1) {
        yield put(showLoader('Chargement des données en cours...'));
    } else if (state.numberRequestInProgress === 0) {
        yield put(hideLoader());
    }
}

export async function doDelete(handler: ApiPlatformHandler<ApiPlatformModel>, iri: string, dispatch: Dispatch): Promise<boolean> {

    dispatch(newRequest());

    let hasBeenDeleted: any = false;

    try {

        hasBeenDeleted = await handler.delete(iri);

        let message: Message = {
            title: "La suppression n'a pas pu être effectuée",
            message: '',
            gravity: MessageGravity.ERROR,
        };

        if (hasBeenDeleted) {

            message.title = "La suppression a bien été réalisée";
            message.gravity = MessageGravity.SUCCESS;

        }


        dispatch(addLog(message));

    } finally {
        dispatch(endRequest());
    }

    return hasBeenDeleted;

}

export async function doCreate(
    handler: ApiPlatformHandler<ApiPlatformModel>,
    model: ApiPlatformModel,
    dispatch: Dispatch,
    displayLoader: boolean = true
): Promise<ApiPlatformModel> {

    if (displayLoader) {
        dispatch(newRequest());
    }

    let resultModel = model;

    try {

        resultModel = await handler.create(model);

    } finally {
        if (displayLoader) {
            dispatch(endRequest());
        }
    }

    return resultModel;
}

export async function doPatch(
    handler: ApiPlatformHandler<ApiPlatformModel>,
    model: ApiPlatformModel,
    dispatch: Dispatch,
    displayLoader: boolean = true
): Promise<ApiPlatformModel> {

    if (displayLoader) {
        dispatch(newRequest());
    }

    let resultModel = model;

    try {

        resultModel = await handler.patch(model);

    } finally {
        if (displayLoader) {
            dispatch(endRequest());
        }
    }

    return resultModel;
}


function* requestSaga() {
    yield takeEvery(ActionRequest.NEW_REQUEST, toggleLoaderFromRequestState)
    yield takeEvery(ActionRequest.END_REQUEST, toggleLoaderFromRequestState)
}


export default requestSaga