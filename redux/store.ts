import {applyMiddleware, createStore} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {createWrapper} from 'next-redux-wrapper'

import reducers from './reducers';
import requestSaga from "./sagas/request";
import adminTableSaga from "./sagas/admin_table";
import cartSaga from "./sagas/cart";

export const store = null;

const bindMiddleware = (middleware: any) => {
    if (process.env.NODE_ENV !== 'production') {
        const { composeWithDevTools } = require('redux-devtools-extension')
        return composeWithDevTools(applyMiddleware(...middleware))
    }

    return applyMiddleware(...middleware)
}

// TODO : créer un reducer pour les alerts (plusieurs notifications dans le initialState)

export const makeStore = () => {
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore(reducers, bindMiddleware([sagaMiddleware]))

    sagaMiddleware.run(requestSaga)
    sagaMiddleware.run(adminTableSaga)
    sagaMiddleware.run(cartSaga)

    return store
}


export const wrapper = createWrapper(makeStore, { debug: process.env.NODE_ENV !== 'production' })