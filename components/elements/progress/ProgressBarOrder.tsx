import React from 'react';

const STEPS = [
    {
        step: 'Commande créée',
        icon: (
            <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-800" xmlns="http://www.w3.org/2000/svg" fill="none"
                 viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                      d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"/>
            </svg>
        )
    },
    {
        step: 'Commande réglée',
        icon: (
            <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-800" xmlns="http://www.w3.org/2000/svg"
                 fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                <path strokeLinecap="round" strokeLinejoin="round"
                      d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z"/>
            </svg>
        )
    },
    {
        step: 'En préparation',
        icon: (
            <svg xmlns="http://www.w3.org/2000/svg" className="absolute w-6 h-6 ml-1 mt-1 text-neutral-800" fill="none"
                 viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                      d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"/>
            </svg>
        )
    },
    {
        step: 'En livraison',
        icon: (
            <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-400" xmlns="http://www.w3.org/2000/svg" fill="none"
                 viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                <path strokeLinecap="round" strokeLinejoin="round"
                      d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"/>
                <path strokeLinecap="round" strokeLinejoin="round" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"/>
            </svg>
        )
    },
    {
        step: 'Livrée, Bon appétit',
        icon: (
            <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-400" xmlns="http://www.w3.org/2000/svg" fill="none"
                 viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                      d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"/>
            </svg>
        )
    }
]


const ProgressBarOrder = ({order}: any) => {

    let stepsUntilAreDone = 1;

    switch (order?.status?.slug) {
        case 'livree':
        case 'remboursee':
            stepsUntilAreDone = 5;
            break;
        case 'disponible-en-restaurant':
        case 'en-cours-de-livraison':
            stepsUntilAreDone = 4;
            break;
        case 'en-cours-de-traitement':
            stepsUntilAreDone = 3;
            break;
        case 'payee':
            stepsUntilAreDone = 2;
            break;
    }

    return (
        <ol className="md:flex md:justify-center">
            {STEPS.map((step: any, index: number) => {

                const isCurrent = index === stepsUntilAreDone;
                const isPassed = index < stepsUntilAreDone;

                return (
                    <li className="md:w-1/5" key={index}>
                        <div className="flex md:block flex-start items-center pt-2 md:pt-0">
                            <div
                                className={"border-l md:border-l-0 md:border-t-4 md:flex md:justify-center md:gap-6 mt-4 " +
                                    (
                                        isPassed ?
                                            "border-gre-700 dark:border-org-500" :
                                            "border-neutral-200 dark:border-neutral-800"
                                    )
                                }
                            />
                            <div
                                className={"w-8 h-8 rounded-full -ml-1 md:ml-0 mr-3 md:mr-0 md:-mt-4 " +
                                    (
                                        isPassed ?
                                            "bg-gre-700 dark:bg-org-500" :
                                            "bg-neutral-200 dark:bg-neutral-800"
                                    )
                                }
                            >
                                {isCurrent ?
                                    <div className="absolute bg-gre-700 dark:bg-org-500 w-4 h-4 rounded-full ml-2 mt-2"/> :
                                    step?.icon
                                }
                            </div>
                        </div>
                        <div className="mt-0.5 ml-4 md:ml-0 pb-2">
                            <h4 className="text-neutral-400 font-semibold text-xl mb-1.5">{step?.step}</h4>
                            {/*<p className="text-neutral-400 mb-3">22 Juillet 2022, 11:44</p>*/}
                        </div>
                    </li>
                )
            })}
        </ol>
    );
};
export default ProgressBarOrder;

// <li className="md:w-1/5">
//     <div className="flex md:block flex-start items-center pt-2 md:pt-0">
//         <div
//             className="border-l md:border-l-0 md:border-t-4 border-gre-700 dark:border-org-500 md:flex md:justify-center md:gap-6 mt-4"/>
//         <div className="bg-gre-700 dark:bg-org-500 w-8 h-8 rounded-full -ml-1 md:ml-0 mr-3 md:mr-0 md:-mt-5">
//             {/* <div className="absolute bg-gre-700 dark:bg-org-500 w-4 h-4 rounded-full ml-2 mt-2"/> */}
//             <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-800" xmlns="http://www.w3.org/2000/svg" fill="none"
//                  viewBox="0 0 24 24" stroke="currentColor">
//                 <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
//                       d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"/>
//             </svg>
//         </div>
//     </div>
//     <div className="mt-0.5 ml-4 md:ml-0 pb-2">
//         <h4 className="font-semibold text-xl mb-1.5">Commande créée</h4>
//         {/*<p className="text-neutral-400 mb-3">21 Juillet 2022, 11:49</p>*/}
//     </div>
// </li>
// <li className="md:w-1/5">
//     <div className="flex md:block flex-start items-center pt-2 md:pt-0">
//         <div
//             className="border-l md:border-l-0 md:border-t-4 border-gre-700 dark:border-org-500 md:flex md:justify-center md:gap-6 mt-4"/>
//         <div className="bg-gre-700 dark:bg-org-500 w-8 h-8 rounded-full -ml-1 md:ml-0 mr-3 md:mr-0 md:-mt-4">
//             {/* <div className="absolute bg-gre-700 dark:bg-org-500 w-4 h-4 rounded-full ml-2 mt-2"/> */}
//             <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-800" xmlns="http://www.w3.org/2000/svg" fill="none"
//                  viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
//                 <path strokeLinecap="round" strokeLinejoin="round"
//                       d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z"/>
//             </svg>
//         </div>
//     </div>
//     <div className="mt-0.5 ml-4 md:ml-0 pb-2">
//         <h4 className="font-semibold text-xl mb-1.5">Commande réglée</h4>
//         {/*<p className="text-neutral-400 mb-3">21 Juillet 2022, 11:51</p>*/}
//     </div>
// </li>
// <li className="md:w-1/5">
//     <div className="flex md:block flex-start items-center pt-2 md:pt-0">
//         <div
//             className="border-l md:border-l-0 md:border-t-4 border-gre-700 dark:border-org-500 md:flex md:justify-center md:gap-6 mt-4"/>
//         <div className="bg-gre-700 dark:bg-org-500 w-8 h-8 rounded-full -ml-1 md:ml-0 mr-3 md:mr-0 md:-mt-4">
//             {/* <div className="absolute bg-gre-700 dark:bg-org-500 w-4 h-4 rounded-full ml-2 mt-2"/> */}
//             <svg xmlns="http://www.w3.org/2000/svg" className="absolute w-6 h-6 ml-1 mt-1 text-neutral-800" fill="none"
//                  viewBox="0 0 24 24" stroke="currentColor">
//                 <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
//                       d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"/>
//             </svg>
//         </div>
//     </div>
//     <div className="mt-0.5 ml-4 md:ml-0 pb-2">
//         <h4 className="font-semibold text-xl mb-1.5">En préparation</h4>
//         {/*<p className="text-neutral-400 mb-3">22 Juillet 2022, 10:44</p>*/}
//     </div>
// </li>
// <li className="md:w-1/5">
//     <div className="flex md:block flex-start items-center pt-2 md:pt-0">
//         <div
//             className="border-l md:border-l-0 md:border-t-4 border-neutral-200 dark:border-neutral-800 md:flex md:justify-center md:gap-6 mt-4"/>
//         <div className="bg-neutral-200 dark:bg-neutral-800 w-8 h-8 rounded-full -ml-1 md:ml-0 mr-3 md:mr-0 md:-mt-4">
//             <div className="absolute bg-gre-700 dark:bg-org-500 w-4 h-4 rounded-full ml-2 mt-2"/>
//             <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-400" xmlns="http://www.w3.org/2000/svg" fill="none"
//                  viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
//                 <path strokeLinecap="round" strokeLinejoin="round"
//                       d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"/>
//                 <path strokeLinecap="round" strokeLinejoin="round" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"/>
//             </svg>
//         </div>
//     </div>
//     <div className="mt-0.5 ml-4 md:ml-0 pb-2">
//         <h4 className="font-semibold text-xl mb-1.5">En livraison</h4>
//         {/*<p className="text-neutral-400 mb-3">22 Juillet 2022, 11:23</p>*/}
//     </div>
// </li>
// <li className="md:w-1/5">
//     <div className="flex md:block flex-start items-center pt-2 md:pt-0">
//         <div
//             className="border-l md:border-l-0 md:border-t-4 border-neutral-200 dark:border-neutral-800 md:flex md:justify-center md:gap-6 mt-4"/>
//         <div className="bg-neutral-200 dark:bg-neutral-800 w-8 h-8 rounded-full -ml-1 md:ml-0 mr-3 md:mr-0 md:-mt-4">
//             {/* <div className="absolute bg-gre-700 dark:bg-org-500 w-4 h-4 rounded-full ml-2 mt-2"/> */}
//             <svg className="absolute w-6 h-6 ml-1 mt-1 text-neutral-400" xmlns="http://www.w3.org/2000/svg" fill="none"
//                  viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
//                 <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
//                       d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"/>
//             </svg>
//         </div>
//     </div>
//     <div className="mt-0.5 ml-4 md:ml-0 pb-2">
//         <h4 className="text-neutral-400 font-semibold text-xl mb-1.5">Livré, bon appétit !</h4>
//         {/*<p className="text-neutral-400 mb-3">22 Juillet 2022, 11:44</p>*/}
//     </div>
// </li>