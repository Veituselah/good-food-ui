import React from 'react';
import {MessageGravity} from "../../../utils/middlewares/Logger/MessageGravity";

interface Props {
    type: MessageGravity;
    title: string;
    text: string;
    transparent?: boolean;
    pointer?: boolean;
}

const SimpleAlert = (props: Props) => {

    let cssClasses;

    switch (props.type) {
        case MessageGravity.ERROR:
            cssClasses = 'bg-red-200 border-red-600 text-red-600';
            break;
        case MessageGravity.WARNING:
            cssClasses = 'bg-yellow-200 border-yellow-600 text-yellow-600';
            break;
        case MessageGravity.SUCCESS:
            cssClasses = 'bg-green-200 border-green-600 text-green-600';
            break;
        case MessageGravity.INFO:
        default:
            cssClasses = 'bg-blue-200 border-blue-600 text-blue-600';
            break;
    }

    if (props.transparent) {
        cssClasses += ' bg-50';
    }

    if (props.pointer) {
        cssClasses += ' cursor-pointer';
    }

    return (
        <div className={`${cssClasses} border-l-4 p-4`} role="alert">
            <p className="font-bold">{props.title}</p>
            <p>{props.text}</p>
        </div>
    );
};

export default SimpleAlert;
