import React from "react";

type Props = {
    width?: string,
    height?: string,
    pathToSVG?: string,
    className?: string,
    onClick?: any,
    icon?: string
}

const SVGIcon = ({
                     width = '25px',
                     height = '25px',
                     pathToSVG = '/icons/icons.svg',
                     className = '',
                     onClick,
                     icon
                 }: Props) => {

    return (
        <svg width={width} height={height} className={className} onClick={onClick }>
            <use xlinkHref={pathToSVG + "#" + icon}/>
        </svg>
    )
}

export default SVGIcon;