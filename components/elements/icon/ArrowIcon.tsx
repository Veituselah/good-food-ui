import React from "react";
import SVGIcon from "./SVGIcon";

type props = {
    width?: string,
    height?: string,
    className?: string,
    up?: boolean,
    selected?: boolean,
    onClick: Function
}

const ArrowIcon = ({width = '8px', up, height = '8px', className = '', selected = false, onClick}: props) => {

    let fullClassName = className + ' cursor-pointer ';

    if (up) {
        fullClassName += ' hover:text-red-500 ';
    } else {
        fullClassName += ' hover:text-blue-500 ';
    }

    if (selected) {
        fullClassName += ' text-black';
    } else {
        fullClassName += ' text-gray-500';
    }

    return (
        <SVGIcon icon={up ? 'arrow_up_fill' : 'arrow_down_fill'} width={width} height={height}
                 className={fullClassName} onClick={onClick}
        />
    )
}

export default ArrowIcon;
