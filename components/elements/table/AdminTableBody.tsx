import React from "react";
import {TableProps} from "./AdminTable";
import {getAdminTable} from "../../../redux/selectors/admin_table";
import {useDispatch, useSelector} from "react-redux";
import AdminTableBodyColumnAction from "./AdminTableBodyColumnAction";
import {toggleSelectIri} from "../../../redux/actions/admin_table";

const AdminTableBody = (props: TableProps) => {

    const dispatch = useDispatch();

    let columns = props.columns ?? [];
    let actions = props.actions ?? [];

    let adminTable = useSelector(getAdminTable);
    const data: any = adminTable?.collection?.data ?? [];

    return (
        <tbody>

        {data && data?.length > 0 ? (
                data?.map((object: any) => (

                        <tr key={object[props.dataIDField]} className="bg-white border-b dark:bg-neutral-800 dark:border-neutral-700">

                            {props.canSelectLines ?
                                <td scope="col" className="px-6 py-3">
                                    <input type="checkbox" name={"item[" + object[props.dataIDField] + "]"}
                                           onClick={
                                               () => {
                                                   dispatch(toggleSelectIri(object.iri))
                                               }
                                           }/>
                                </td>
                                : null}

                            {columns.map((column) => {

                                    let targetObject = object;

                                    for (let property of column.pathToObject) {
                                        targetObject = targetObject[property] ?? null;
                                    }

                                    if (!Array.isArray(targetObject)) {
                                        targetObject = [targetObject];
                                    }

                                    let value = '';

                                    for (let data of targetObject) {

                                        if (!data) {
                                            break;
                                        }

                                        if (value) {
                                            value += ' / ';
                                        }

                                        for (let property of column.propertiesToDisplay) {
                                            let propertyValue = data[property] ?? '';

                                            value += (value ? ' ' : '') + propertyValue;
                                        }
                                    }

                                    if (column.formater && column.formater.canBeFormated(value)) {
                                        let params = column.formaterGetParams ? column.formaterGetParams(value, object) : [];
                                        value = column.formater.format(value, params);
                                    }

                                    return (
                                        <td key={object[props.dataIDField] + '_' + column.name} scope="col"
                                            className={"px-6 py-3 " + (column?.notCapitalize ? '' : "capitalize")}>
                                            {value === '' ? 'N.D.' : value}
                                        </td>
                                    )
                                }
                            )}

                            <AdminTableBodyColumnAction actions={actions} object={object}/>

                        </tr>
                    )
                )
            ) :
            <tr>
                <td colSpan={100} className="text-2xl p-4 text-center"> Aucun résultat n'a malheureusement été trouvé...</td>
            </tr>
        }

        </tbody>
    );

}

export default AdminTableBody;