import React, {MouseEventHandler} from "react";
import {ApiPlatformHandler, ApiPlatformModel, Collection, Formater, UrlInterface} from "../../../internal";
import AdminTableHeader from "./AdminTableHeader";
import AdminTableBody from "./AdminTableBody";
import {useDispatch} from "react-redux";
import {searchSuccess, setApiHandler} from "../../../redux/actions/admin_table";
import {CustomSelectOption} from "../form/select/CustomSelect/CustomSelect";
import AdminTablePagination from "./AdminTablePagination";

export interface TableAction {
    name: string,
    icon: string,
    action?: any,
    url?: UrlInterface,
    checkCanDisplayAction?: Function
}

export enum TableSearchFilterType {
    BOOLEAN,
    TEXT,
    SELECT
}

export interface TableSearchFilter {
    name: string,
    type: TableSearchFilterType
    options?: Array<CustomSelectOption>,
    reverseBool?: boolean,
    label?: string,
}

export interface TableColumn {
    name: string,
    pathToObject: Array<string>,
    propertiesToDisplay: Array<string>,
    formater?: Formater,
    formaterGetParams?: Function,
    orderFilter?: string,
    searchFilter?: TableSearchFilter,
    notCapitalize?: boolean
}

export type TableProps = {
    columns: TableColumn[],
    actions: TableAction[],
    canSelectLines: boolean,
    collection: Collection<ApiPlatformModel>,
    dataIDField: string,
    apiHandler: ApiPlatformHandler<ApiPlatformModel>
};

const AdminTable = (props: TableProps) => {

    const dispatch = useDispatch();
    dispatch(setApiHandler(props.apiHandler));
    dispatch(searchSuccess(props.collection));

    return (
        <>
            <table className="w-full text-sm text-left">
                <AdminTableHeader {...props}/>
                <AdminTableBody {...props}/>
            </table>
            <AdminTablePagination/>
        </>
    );

}

export default AdminTable;