import React from "react";
import {TableProps, TableSearchFilterType} from "./AdminTable";
import {useDispatch, useSelector} from "react-redux";
import {
    manageExistsFilterOnField,
    manageSearchFilterOnField,
    search,
    setOrderFilterOnField,
    toggleFilterOnField
} from "../../../redux/actions/admin_table";
import {OrderFilterTypeEnum} from "../../../utils/filters/ApiPlatform/OrderFilter";
import {getAdminTable} from "../../../redux/selectors/admin_table";
import InputText from "../form/inputText/InputText";
import Button from "../buttons/Button";
import SVGIcon from "../icon/SVGIcon";
import ArrowIcon from "../icon/ArrowIcon";
import CustomSelect from "../form/select/CustomSelect/CustomSelect";
import Toggle from "../form/toggle/Toggle";


const AdminTableHeader = (props: TableProps) => {
    const columns = props.columns ?? [];
    const dispatch = useDispatch();
    const adminTable = useSelector(getAdminTable);
    return (
        <thead className="text-xs uppercase bg-white dark:bg-neutral-500 border-b dark:border-neutral-700">
        <tr>
            {
                props.canSelectLines ?
                    <th scope="col" className="px-6 py-3"/>
                    : null
            }
            {columns.map((column) => {
                let classNameColumn = ' cursor-default ';
                let orderFilter = null;
                if (column.orderFilter) {
                    classNameColumn = ' cursor-pointer ';
                    if (adminTable.orderBy?.fieldName === column.orderFilter) {
                        orderFilter = adminTable.orderBy;
                    }
                }
                let searchFilterToDiplsay = null;
                if (column.searchFilter) {
                    switch (column.searchFilter?.type) {
                        case TableSearchFilterType.BOOLEAN:
                            searchFilterToDiplsay = <Toggle
                                onChange={(checked) => {
                                    manageExistsFilter(column.searchFilter?.name, column?.searchFilter?.reverseBool ? !checked : checked);
                                    dispatchSearch();
                                }}
                                label={column.searchFilter.label ?? 'Switch'}
                            />
                            break;
                        case TableSearchFilterType.SELECT:
                            searchFilterToDiplsay = <CustomSelect
                                options={column.searchFilter.options ?? []}
                                onChange={(values: Array<any>) => {
                                    manageSearchFilter(column.searchFilter?.name, values && values.length > 0 ? values : null);
                                    dispatchSearch();
                                }}
                                title={column.searchFilter.label ?? 'Sélectionner une valeur'}
                            />
                            break;
                        default:
                        case TableSearchFilterType.TEXT:
                            searchFilterToDiplsay = <InputText
                                px={2} py={1}
                                onInput={(e: any) => manageSearchFilter(column.searchFilter?.name, e.target.value)}
                                onfocusout={() => dispatchSearch()}
                            />
                            break;
                    }
                }
                return (
                    <th key={column.name} scope="col" className="px-4 py-3 align-top">
                        <div className={"mb-1 flex items-center"}>
                            { column.orderFilter ?
                                <div className={"mr-1"}>
                                    <ArrowIcon
                                        up={true}
                                        selected={(orderFilter && orderFilter.type === OrderFilterTypeEnum.ASC) ?? false}
                                        onClick={() => manageOrderFilter(column.orderFilter, OrderFilterTypeEnum.ASC)}
                                    />
                                    <ArrowIcon
                                        up={false}
                                        selected={(orderFilter && orderFilter.type === OrderFilterTypeEnum.DESC) ?? false}
                                        onClick={() => manageOrderFilter(column.orderFilter, OrderFilterTypeEnum.DESC)}
                                    />
                                </div>
                                : null
                            }
                            <span
                                className={classNameColumn}
                                onClick={() => manageOrderFilter(column.orderFilter, null)}
                            >
                                {column.name}
                            </span>
                        </div>
                        {searchFilterToDiplsay}
                    </th>
                    )
                }
            )}
            <th scope="col" className="px-6 py-3 align-baseline">
                <div className={'bt-1 mb-1'}>Actions</div>
                <div className="w-10 h-10 items-center justify-center">
                    <Button
                        color="org"
                        icon={<SVGIcon icon={"search"} width={'20px'} height={'20px'}/>}
                        width={10} height={10} py={0} px={0}
                        onClick={() => dispatchSearch(true)}
                    />
                </div>
            </th>

        </tr>

        </thead>
    );

    function manageOrderFilter(orderFilterName?: string | null, type?: OrderFilterTypeEnum | null) {
        if (orderFilterName) {

            switch (type) {
                case OrderFilterTypeEnum.ASC:
                case OrderFilterTypeEnum.DESC:
                    dispatch(
                        setOrderFilterOnField(orderFilterName, type)
                    );
                    break;
                default:
                    dispatch(
                        toggleFilterOnField(orderFilterName)
                    );
                    break;
            }
        }
    }

    function manageSearchFilter(searchFilterName?: string | null, value?: any | null) {

        if (searchFilterName) {
            dispatch(
                manageSearchFilterOnField(searchFilterName, value)
            );
        }

    }

    function manageExistsFilter(filterName?: string | null, value?: any | null) {

        if (filterName) {
            dispatch(
                manageExistsFilterOnField(filterName, value)
            );
        }

    }

    function dispatchSearch(forceSearch = false) {
        dispatch(search(forceSearch));
    }
}

export default AdminTableHeader;