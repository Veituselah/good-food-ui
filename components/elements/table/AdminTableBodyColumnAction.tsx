import React from "react";
import SVGIcon from "../icon/SVGIcon";
import {ApiPlatformModel, UrlBuilder} from "../../../internal";
import {TableAction} from "./AdminTable";
import DropDownMenu, {DDMItem} from "../ddm/DropDownMenu";

type Props = {
    actions: TableAction[],
    object: ApiPlatformModel
}

const AdminTableBodyColumnAction = (props: Props) => {

    let {actions, object} = props;

    let entityIri = object.iri;

    let ddmItems = new Array<DDMItem>();

    actions.forEach((action) => {

        let canDisplayAction = !action.checkCanDisplayAction || action.checkCanDisplayAction(object);

        if (!canDisplayAction) {
            return;
        }

        let icon = <SVGIcon width="24px" height="24px" icon={action.icon}/>;
        let label = action.name;
        let link = undefined;
        let click = undefined;

        if (action.url) {
            let urlBuilder = new UrlBuilder(action.url, object);
            link = urlBuilder.buildUrl();
        } else if (action.action) {
            click = (e: any) => action.action(e, object);
        }

        let item: DDMItem = {
            icon,
            label,
            link,
            click,
            entityIri
        };

        ddmItems.push(item);
    });

    return (
        <td scope="col" className="px-6 py-3">
            {ddmItems && ddmItems.length > 0 &&
                <DropDownMenu items={ddmItems}/>
            }
        </td>
    )

}

export default AdminTableBodyColumnAction;