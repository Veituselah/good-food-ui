import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {setPage} from "../../../redux/actions/admin_table";
import {getAdminTable} from "../../../redux/selectors/admin_table";

const AdminTablePagination = () => {

    const dispatch = useDispatch();
    let adminTable = useSelector(getAdminTable);

    return (
        <div className="px-5 uppercase bg-gray-50 dark:bg-neutral-600 py-5 flex flex-col xs:flex-row items-center xs:justify-between">
            <div className="flex items-center">
                {adminTable.collection?.firstPage && adminTable.collection?.previousPage && (
                    <>
                        <button type="button"
                                className="w-full p-4 border text-base rounded-l-xl border-neutral-200 dark:border-neutral-800 hover:text-org-500 dark:hover:text-gre-500 text-neutral-400 bg-neutral-50 dark:bg-neutral-700 hover:bg-neutral-100 dark:hover:bg-neutral-800"
                                onClick={() => pageClick(adminTable.collection?.firstPage)}
                        >
                            <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M1427 301l-531 531 531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19l-742-742q-19-19-19-45t19-45l742-742q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z">
                                </path>
                            </svg>
                        </button>
                        <button
                            type="button"
                            className="w-full px-4 py-2 border-r border-t border-b border-neutral-200 dark:border-neutral-800 hover:text-org-500 dark:hover:text-gre-500 text-base text-neutral-400 bg-neutral-50 dark:bg-neutral-700 hover:bg-neutral-100 dark:hover:bg-neutral-800"
                            onClick={() => pageClick(adminTable.collection?.previousPage)}
                        >
                            {adminTable.collection?.previousPage}
                        </button>
                    </>
                )}
                <button
                    type="button"
                    className="w-full px-4 py-2 border border-neutral-200 dark:border-neutral-800 disabled text-base text-org-500 dark:text-gre-500 bg-neutral-100 dark:bg-neutral-800 cursor-default"
                >
                    {adminTable.collection?.currentPage ?? 1    }
                </button>
                {adminTable.collection?.lastPage && adminTable.collection?.nextPage && (
                    <>
                        <button
                            type="button"
                            className="w-full px-4 py-2 border border-neutral-200 dark:border-neutral-800 hover:text-org-500 dark:hover:text-gre-500 text-base text-neutral-400 bg-neutral-50 dark:bg-neutral-700 hover:bg-neutral-100 dark:hover:bg-neutral-800"
                            onClick={() => pageClick(adminTable.collection?.nextPage)}
                        >
                            {adminTable.collection?.nextPage}
                        </button>
                        <button type="button"
                                className="w-full p-4 border-t border-b border-r border-neutral-200 dark:border-neutral-800 hover:text-org-500 dark:hover:text-gre-500 text-base rounded-r-xl text-neutral-400 bg-neutral-50 dark:bg-neutral-700 hover:bg-neutral-100 dark:hover:bg-neutral-800"
                                onClick={() => pageClick(adminTable.collection?.lastPage)}
                        >
                            <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z">
                                </path>
                            </svg>
                        </button>
                    </>
                )}
            </div>
        </div>
    )


    function pageClick(page: number|undefined|null) {

        if (page) {
            dispatch(setPage(page));
        }

    }
}

export default AdminTablePagination;