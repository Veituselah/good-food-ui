import React from 'react';

const CalculatedReadTime = () => {
    return <p className="text-neutral-300 dark:text-neutral-200">6 min read</p>;
};

export default CalculatedReadTime;
