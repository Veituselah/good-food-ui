import React from 'react';

const CalculatedComments = () => {
    return (
        <a
            href="/"
            aria-label="Comments"
            className="flex items-start transition-colors duration-200 group text-neutral-500 dark:text-neutral-200"
        >
            <div className="mr-2">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    stroke="currentColor"
                    className="w-5 h-5 transition-colors duration-200 text-org-300 dark:text-gre-300"
                >
                    <polyline points="23 5 23 18 19 18 19 22 13 18 12 18" fill="none" strokeMiterlimit="10"/>
                    <polygon
                        points="19 2 1 2 1 14 5 14 5 19 12 14 19 14 19 2"
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                    />
                </svg>
            </div>
            <p className="font-semibold">81</p>
        </a>
    );
};

export default CalculatedComments;
