import React from 'react';

const CalculatedLikes = () => {
    return (
        <a
            href="/"
            aria-label="Likes"
            className="flex items-start transition-colors duration-200 group text-neutral-500 dark:text-neutral-200"
        >
            <div className="mr-2">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    className="w-5 h-5 transition-colors duration-200 text-gre-300 dark:text-org-300"
                >
                    <polyline points="6 23 1 23 1 12 6 12" fill="none" strokeMiterlimit="10"/>
                    <path
                        d="M6,12,9,1H9a3,3,0,0,1,3,3v6h7.5a3,3,0,0,1,2.965,3.456l-1.077,7A3,3,0,0,1,18.426,23H6Z"
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                    />
                </svg>
            </div>
            <p className="font-semibold">7.4K</p>
        </a>
    );
};

export default CalculatedLikes;
