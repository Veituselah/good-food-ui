import React from 'react';

const CalculatedAllergens = () => {
    return (
        <div className="p-4 text-sm text-neutral-400 flex flex-row justify-around flex-wrap">
            <div className="flex flex-row items-center m-2">
                {/* <FaReact size={20} color="#61DBFB" /> */}
                <h1 className="ml-2">Halal</h1>
            </div>
            <div className="flex flex-row items-center m-2">
                {/* <SiTypescript size={20} color="#007acc" /> */}
                <h1 className="ml-2">Vegan</h1>
            </div>
            <div className="flex flex-row items-center m-2">
                {/* <AiOutlineClockCircle size={20} className="dark:text-white" /> */}
                <h1 className="ml-2">Casher</h1>
            </div>
            <div className="flex flex-row items-center m-2">
                {/* <VscChecklist size={20} className="dark:text-white" /> */}
                <h1 className="ml-2">Allergènes</h1>
            </div>
        </div>
    );
};

export default CalculatedAllergens;
