import React from 'react';

const CalculatedProductCounter = ({onChange, value, disabled = false, displayActionButton = true}: any) => {

    const more = () => {
        onChange(value + 1);
    }

    const less = () => {
        onChange(Math.max(1, value - 1));
    }

    return (
        <div className="flex items-center">
            {displayActionButton &&
                <button
                    type={"button"}
                    className={"text-neutral-700 dark:text-neutral-200 hover:text-neutral-500 focus:outline-none focus:text-gray-600 " +
                        (disabled ? 'cursor-not-allowed' : 'cursor-pointer')
                    }
                    onClick={less}
                    disabled={disabled}
                >
                    <svg
                        className="h-5 w-5"
                        fill="none"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                </button>
            }

            <span className="text-neutral-700 dark:text-neutral-100 font-semibold text-lg mx-2">{value}</span>

            {displayActionButton &&
                <button
                    type={"button"}
                    className={"text-neutral-700 dark:text-neutral-200 hover:text-neutral-500 focus:outline-none focus:text-gray-600 " +
                        (disabled ? 'cursor-not-allowed' : 'cursor-pointer')
                    }
                    onClick={more}
                    disabled={disabled}
                >
                    <svg
                        className="h-5 w-5"
                        fill="none"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                </button>
            }
        </div>
    );
};

export default CalculatedProductCounter;
