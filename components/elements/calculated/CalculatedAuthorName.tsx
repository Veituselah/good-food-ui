import React from 'react';

const CalculatedAuthorName = () => {
    return <p className="text-neutral-400">Jean Jacques</p>;
};

export default CalculatedAuthorName;
