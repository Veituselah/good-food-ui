import React from 'react';

const CalculatedOpeningHours = () => {
    return (
        <div className="p-4 text-xs text-neutral-400 mt-4">
            <span className="flex items-center mb-1">
                <i className="far fa-clock fa-fw mr-2 text-gray-900"></i> 11:30 - 15:00 / 18:00 - 22:30
            </span>
            {/*<span className="flex items-center">*/}
            {/*    <i className="far fa-address-card fa-fw text-gray-900 mr-2"></i> Booking available*/}
            {/*</span>*/}
        </div>
    );
};

export default CalculatedOpeningHours;
