type Props = {
    nbPage: number,
    currentPage: number,
    onPageChange: any,
}

export const Pagination = ({nbPage, currentPage, onPageChange}: Props) => {

    const createButton = (page: number) => {

        const isDisabled = page === currentPage;

        return (
            <button
                type="button"
                className={
                    "w-full px-4 py-2 border-t border-b border-neutral-200 dark:border-neutral-800 text-base " +
                    (isDisabled ? ' cursor-not-allowed bg-neutral-100 text-org-500 dark:text-neutral-500 ' : ' hover:bg-neutral-100 bg-white dark:bg-neutral-600 ')
                }
                disabled={isDisabled}
                onClick={() => goToPage(page)}
            >
                {page}
            </button>
        )
    }

    const nextPage = () => {
        goToPage(currentPage + 1);
    }

    const previousPage = () => {
        goToPage(currentPage - 1);
    }

    const goToPage = (page: number) => {
        if (onPageChange) {
            onPageChange(page);
        }
    }

    const isFirstPage = currentPage === 1;
    const isLastPage = currentPage === nbPage;

    return (
        <div
            className="px-5 bg-white dark:bg-neutral-800 py-4 flex flex-col xs:flex-row items-center xs:justify-between">
            <div className="flex items-center">

                <button type="button"
                        disabled={isFirstPage}
                        className={
                            "w-full p-4 border border-neutral-200 dark:border-neutral-800 text-base rounded-l-xl bg-white dark:bg-neutral-600 hover:bg-neutral-100 dark:hover:bg-neutral-800"
                            + (isFirstPage ? ' cursor-not-allowed ' : ' ')
                        }
                        onClick={previousPage}
                >
                    <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M1427 301l-531 531 531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19l-742-742q-19-19-19-45t19-45l742-742q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z">
                        </path>
                    </svg>
                </button>

                {!isFirstPage && createButton(currentPage - 1)}

                {createButton(currentPage)}

                {!isLastPage && createButton(currentPage + 1)}

                <button type="button"
                        className={
                            "w-full p-4 border-t border-b border-r border-l border-neutral-200 dark:border-neutral-800 text-base  rounded-r-xl bg-white dark:bg-neutral-600 hover:bg-neutral-100 dark:hover:bg-neutral-800"
                            + (isLastPage ? ' cursor-not-allowed ' : ' ')
                        }
                        disabled={isLastPage}
                        onClick={nextPage}
                >
                    <svg width="9" fill="currentColor" height="8" className="" viewBox="0 0 1792 1792"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z">
                        </path>
                    </svg>
                </button>

            </div>
        </div>
    )
}