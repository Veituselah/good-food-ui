import {useDispatch, useSelector} from "react-redux";
import {getCheckout} from "../../../redux/selectors/checkout";
import {goToStep} from "../../../redux/actions/checkout";

type Props = {
    title: string,
    children: any,
    step: number,
    address?: any,
    errors?: Array<string>,
}

export const Item = ({title, children, step}: Props) => {

    const checkoutState = useSelector(getCheckout);
    const currentStep = checkoutState.step;
    const disabled = step > currentStep;
    const dispatch = useDispatch();

    return (
        <div
            className={"border rounded-lg shadow-sm " + (disabled ? ' bg-neutral-200 ' : ' bg-neutral-50 ') + " dark:bg-neutral-800 "}>
            <button
                type="button"
                aria-label="Open item" title="Open item"
                className={"flex items-center justify-between w-full p-4 focus:outline-none " + (disabled ? ' cursor-not-allowed ' : null)}
                onClick={() => {
                    if (!disabled) {
                        dispatch(goToStep(step))
                    }
                }}
            >
                <p className="text-lg font-medium text-gre-700 dark:text-org-500">{title}</p>
                <div
                    className="flex items-center justify-center w-8 h-8 border rounded-full text-org-500 dark:text-gre-500">
                    <svg viewBox="0 0 24 24"
                         className={`w-3 transition-transform duration-200 ${!disabled ? 'transform rotate-180' : ''}`}>
                        <polyline fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                                  strokeMiterlimit="10" points="2,7 12,17 22,7" strokeLinejoin="round"/>
                    </svg>
                </div>
            </button>
            <div className={"p-4 pt-0 " + (step !== currentStep ? 'hidden' : '')}>
                <div className="ctext-neutral-700 dark:text-neutral-100">{children}</div>
            </div>
        </div>
    );
};
