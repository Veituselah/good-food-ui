import React from 'react';
import {useDispatch} from "react-redux";
import {Context} from "../../../internal";
import {logoutAdmin, logoutCustomer} from "../../../redux/actions/context";
import {useRouter} from "next/router";

export default function ButtonLogout() {

    const dispatch = useDispatch();
    const router = useRouter();

    const logout = () => {

        const context = new Context(false);
        router.reload();
        dispatch(logoutCustomer(context));
        dispatch(logoutAdmin(context));

    }

    return (
        <>
            <button
                className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-org-500 dark:bg-gre-700 rounded-md shadow-md focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                type="button"
                onClick={logout}
            >
                Déconnexion
            </button>
        </>
    );
}
