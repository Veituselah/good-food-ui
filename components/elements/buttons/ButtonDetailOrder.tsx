import React from 'react';
import CardOrdersDetail from '../cards/CardOrdersDetail'
import { SvgDetail, SvgDots } from '../icon/SvgItems'

export default function ButtonDetailOrder({order}: any) {
    const [showModal, setShowModal] = React.useState(false);
    return (
        <>
        <button type="button" onClick={() => setShowModal(true)} className="p-2 justify-center items-center transition ease-in duration-200 text-center text-base font-semibold shadow-md rounded-lg bg-gre-700 dark:bg-org-500 hover:bg-gre-600 dark:hover:bg-org-600 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 text-neutral-700">
            <SvgDetail/>
        </button>
        {showModal ? (
            <>
                <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                    <div className="relative md:w-4/5 my-6 mx-auto">
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white dark:bg-neutral-800 outline-none focus:outline-none">
                            <div className="content">
                                    <div>
                                        <div>
                                            <div className="p-5 border-b border-solid border-org-500 dark:border-gre-500 rounded-t">
                                                <h2 className="mt-12 mb-6 font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl text-center">
                                                    <span className="relative inline-block p-4 text-center">
                                                        <SvgDots/>
                                                        <span className="title relative">Détails de la commande n°#{order?.reference}<br/>({order?.status?.name})</span>
                                                    </span>
                                                </h2>
                                                <button
                                                    className="absolute top-5 right-5 p-1 ml-auto border-0 text-org-500 opacity-75 float-right text-3xl leading-none font-bold outline-none focus:outline-none hover:text-gre-700"
                                                    onClick={() => setShowModal(false)}
                                                >
                                                    x
                                                </button>
                                            </div>
                                            <CardOrdersDetail order={order}/>
                                        </div>
                                    </div>
                                </div>
                            <div className="relative flex-auto"/>
                        </div>
                    </div>
                </div>
                <div className="opacity-50 fixed inset-0 z-40 bg-black"/>
            </>
        ) : null}
    </>
);
}
