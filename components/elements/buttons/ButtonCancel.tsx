import React from 'react';
import {SvgCancel} from '../../elements/icon/SvgItems'

export default function ButtonCancel({onClick}: any) {
    return (
        <button type="button"
                className="p-2 justify-center items-center transition ease-in duration-200 text-center text-base font-semibold shadow-md rounded-lg bg-org-500 dark:bg-gre-500 hover:bg-org-600 dark:hover:bg-gre-600 focus:ring-org-700 dark:focus:ring-gre-500 focus:ring-offset-org-200 dark:focus:ring-offset-gre-200 text-neutral-700"
                style={{
                    width: '30px',
                    height: '30px',
                }}
                onClick={onClick}
        >
            <SvgCancel/>
        </button>
    );
}

