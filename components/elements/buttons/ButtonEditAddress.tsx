import React from 'react';
import {SvgDots, SvgEdit} from '../icon/SvgItems'
import {FormAddress} from "../../forms/FormAddress";

export default function ButtonEditAddress({countries, address, customer, onSaveAddress}: any) {
    const [showModal, setShowModal] = React.useState(false);
    return (
        <>
        <button type="button" onClick={() => setShowModal(true)} className="p-2 justify-center items-center transition ease-in duration-200 text-center text-base font-semibold shadow-md rounded-lg bg-gre-700 dark:bg-org-500 hover:bg-gre-600 dark:hover:bg-org-600 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 text-neutral-700">
            <SvgEdit/>
        </button>
        {showModal ? (
            <>
                <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                    <div className="relative w-full my-6 mx-auto max-w-3xl">
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white dark:bg-neutral-800 outline-none focus:outline-none">
                            <div className="content">
                                    <div>
                                        <div>
                                            <div className="flex items-start justify-between p-5 border-b border-solid border-org-500 dark:border-gre-500 rounded-t">
                                                <h2 className="mt-12 max-w-lg font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                                                    <span className="relative inline-block">
                                                        <SvgDots/>
                                                        <span className="title relative">Modifier l'adresse</span>
                                                    </span>
                                                </h2>
                                                <button
                                                    className="p-1 ml-auto border-0 text-org-500 opacity-75 float-right text-3xl leading-none font-bold outline-none focus:outline-none hover:text-gre-700"
                                                    onClick={() => setShowModal(false)}
                                                >
                                                    x
                                                </button>
                                            </div>
                                            <FormAddress
                                                address={address}
                                                countries={countries}
                                                customer={customer}
                                                onSaveAddress={() => {
                                                    setShowModal(false);

                                                    if(onSaveAddress){
                                                        onSaveAddress();
                                                    }

                                                }}
                                                onCancelAddress={() => setShowModal(false)}
                                            />
                                        </div>
                                    </div>
                                </div>
                            <div className="relative p-2 flex-auto"/>
                        </div>
                    </div>
                </div>
                <div className="opacity-50 fixed inset-0 z-40 bg-black"/>
            </>
        ) : null}
    </>
);
}
