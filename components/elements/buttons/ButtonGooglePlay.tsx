import React from 'react'
import {SvgGooglePlay} from '../icon/SvgItems'

const ButtonGooglePlay = () => {
    return (
        <button className="m-4 bg-neutral-100 dark:bg-neutral-800 hover:bg-neutral-200 dark:hover:bg-neutral-700 text-org-500 dark:text-gre-500 inline-flex py-3 px-5 rounded-lg items-center focus:outline-none">
            <SvgGooglePlay/>
            <span className="ml-4 flex items-start flex-col leading-none">
                <span className="text-xs mb-1 text-neutral-700 dark:text-neutral-100">OBTENIR SUR</span>
                <span className="title-font font-medium text-neutral-700 dark:text-neutral-100">Google Play</span>
            </span>
        </button>
    );
};

export default ButtonGooglePlay;
