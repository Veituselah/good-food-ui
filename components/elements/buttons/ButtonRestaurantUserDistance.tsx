import React, {useState} from "react";
import {getCurrentLocation} from "../maps/Map";

export function calcCrow(lat1: number, lon1: number, lat2: number, lon2: number) {

    let radlat1 = Math.PI * lat1 / 180;
    let radlat2 = Math.PI * lat2 / 180;
    let theta = lon1 - lon2;
    let radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

    if (dist > 1) {
        dist = 1;
    }

    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    dist = dist * 1.609344;

    return dist;
}

const ButtonRestaurantUserDistance = ({restaurant}: any) => {

    const [distance, setDistance] = useState<number>(0);

    const setUserRestaurantDistance = (userLongitude: number, userLatitude: number) => {
        let distance = calcCrow(userLatitude, userLongitude, restaurant?.latitude,  restaurant?.longitude);
        setDistance(distance);
    }

    getCurrentLocation(setUserRestaurantDistance)

    return (
        <button
            className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
        >
                                        <span className="w-full">
                                            {distance.toFixed(2)} km
                                        </span>
        </button>
    )
}

export default ButtonRestaurantUserDistance;