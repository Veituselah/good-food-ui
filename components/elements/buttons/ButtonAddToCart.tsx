import CalculatedProductCounter from "../calculated/CalculatedProductCounter";
import React, {useState} from "react";
import {
    ApiPlatformModel,
    Context, MessageGravity, OrderHandler,
    OrderLineHandler,
    OrderLineModel,
    OrderModel,
    PurchasableProductModel
} from "../../../internal";
import {Message} from "../../../utils/middlewares/Logger/Message";
import {addLog} from "../../../redux/actions/logger";
import {showUserDialogSection} from "../../../redux/actions/user_dialog";
import {UserDialogSection} from "../../../redux/reducers/user_dialog";
import {useDispatch, useSelector} from "react-redux";
import {doCreate, doPatch} from "../../../redux/sagas/request";
import {saveCartLine} from "../../../redux/actions/cart";
import {getCart} from "../../../redux/selectors/cart";

export const ButtonAddToCart = ({product}: any) => {

    const [qty, setQty] = useState<number>(1);
    const [disabled, setDisabled] = useState(false)
    const dispatch = useDispatch();

    const cartState = useSelector(getCart);

    const addToCart = () => {

        const context = new Context(false);
        const customer = context.getCustomerToken();

        if (customer) {

            setDisabled(true);

            const orderLineHandler = new OrderLineHandler(customer);
            const orderHandler = new OrderHandler();
            const cart = cartState.cart;

            cart.iri = orderHandler.buildIri(cart.reference);

            let productAlreadyInCart = null;

            for (let cartLineInCart of cart.orderLines) {

                if (cartLineInCart.product.reference === product.reference) {
                    productAlreadyInCart = cartLineInCart;
                    break;
                }

            }

            if (productAlreadyInCart) {

                productAlreadyInCart.qty += qty;

                doPatch(orderLineHandler, productAlreadyInCart, dispatch, false).then((orderLine: any) => {
                    dispatch(saveCartLine(orderLine));
                }).catch((error: any) => {
                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                }).finally(() => {
                    setDisabled(false);
                })

            } else {

                const cartLine = new OrderLineModel();
                cartLine.qty = qty;
                cartLine.product = product as PurchasableProductModel;
                cartLine.orderReference = cart as OrderModel;

                doCreate(orderLineHandler, cartLine, dispatch, false).then((orderLine: any) => {
                    dispatch(saveCartLine(orderLine));
                }).catch((error: any) => {
                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                }).finally(() => {
                    setDisabled(false);
                })

            }

        } else {

            let message: Message = {
                gravity: MessageGravity.ERROR,
                title: 'Vous devez être connecté en tant qu\'utilisateur',
                message: 'Pour réaliser un panier'
            }

            dispatch(addLog(message));
            dispatch(showUserDialogSection(UserDialogSection.LOGIN));

        }

    }

    return (
        <div className="flex item-center justify-around md:justify-between mt-4 px-1 flex-wrap">
            <CalculatedProductCounter value={qty} onChange={setQty}/>
            <button
                type={"button"}
                className={"px-6 py-2 shadow-md text-base font-semibold rounded-md " + (disabled ? ' cursor-not-allowed bg-gray-500 ' : ' bg-gradient-to-r ') + " from-org-500 to-org-600 dark:from-gre-500 dark:to-gre-600 text-neutral-700"}
                onClick={addToCart}
                disabled={disabled}
            >
                Ajouter au panier
            </button>
        </div>
    )

}