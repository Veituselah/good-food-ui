import React from 'react';
import {useDispatch} from "react-redux";
import {showUserDialog} from "../../../redux/actions/user_dialog";

export default function ButtonLogin() {

    const dispatch = useDispatch();
    const showDialog = () => {
        dispatch(showUserDialog());
    }

    return (
        <>
            <button
                className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                type="button"
                onClick={showDialog}
            >
                Connexion
            </button>
        </>
    );
}
