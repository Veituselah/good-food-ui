import React, { useState } from 'react';
import {SvgDetail} from '../icon/SvgItems'

export default function ButtonDetail() {
    return (
        <button type="button" className="p-2 justify-center items-center transition ease-in duration-200 text-center text-base font-semibold shadow-md rounded-lg bg-gre-700 dark:bg-org-500 hover:bg-gre-600 dark:hover:bg-org-600 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 text-neutral-700">
            <SvgDetail/>
        </button>
    );
}
