import React from 'react'
import Link from 'next/link'
import {SvgCookies} from "../icon/SvgItems"

export default function ButtonCookies() {
    const [showModal, setShowModal] = React.useState(false);
    return (
        <>
            <div className="z-100 absolute bottom-5 left-10 pointer-events-none back-to-top-wrapper">
                <a
                    href="#cookies"
                    className="back-to-top-link flex items-center justify-center inline-block no-underline leading-10 text-center w-20 h-20 p-1 sticky bg-none text-gre-500 dark:text-org-500"
                    aria-label="Gérer mes cookies"
                    onClick={() => setShowModal(true)}
                >
                    <SvgCookies/>
                </a>
            </div>
            {showModal ? (
            <>
                <div className="bottom-0 md:left-32 justify-center items-center flex overflow-x-hidden overflow-y-auto fixed z-50 outline-none focus:outline-none">
                    <div className="relative w-full my-6 mx-auto max-w-3xl">
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white dark:bg-neutral-800 outline-none focus:outline-none">
                            <div className="max-w-5xl sm:mx-auto flex flex-row items-center p-4 md:p-3 leading-none font-medium rounded-3xl shadow-2xl shadow-green-700">
                                <div className="flex flex-col md:flex-row justify-between items-center">
                                    <div className="flex-initial inline-flex w-full md:w-8/12 items-center text-justify text-sm pr-4">
                                        <span className="">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8 mr-2 text-org-500 dark:text-gre-500" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd" />
                                            </svg>
                                        </span>
                                        Avertissement : Good Food utilise les cookies à des fins de fonctionnement optimal et améliorer votre expérience. En utilisant notre site, vous acceptez notre politique de confidentialité et l'utilisation des cookies.
                                    </div>
                                    <div className="flex flex-row gap-x-2 md:gap-x-5 mt-4 md:mt-0">
                                        <button className="uppercase font-semibold px-2 md:px-1 lg:px-3 rounded-md py-3 text-sm bg-gre-700 dark:bg-org-500 hover:bg-gre-600 dark:hover:bg-org-600 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 text-neutral-700" onClick={() => setShowModal(false)}>J'accepte les cookies</button>
                                        <Link href='/privacy'><button className="px-2 md:px-1 rounded-md py-2 underline text-sm font-semibold text-org-500 hover:text-org-600 dark:text-gre-500 dark:hover:text-gre-700">Politique de confidentialité</button></Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="opacity-50 fixed inset-0 z-40 bg-black"/>
            </>
            ) : null}
        </>
    );
}