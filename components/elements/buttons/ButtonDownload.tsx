import React from 'react';
import {SvgDownload} from '../icon/SvgItems'
import {useRouter} from "next/router";
import {INVOICE_PATH} from "../../../pages/invoice/[order_id]";

export default function ButtonDownload({order}: any) {

    const router = useRouter();

    return (
        <button type="button" onClick={() => {router.push(INVOICE_PATH + order.reference)}} className="h-8 p-2 justify-center items-center transition ease-in duration-200 text-center text-base font-semibold shadow-md rounded-lg bg-org-500 dark:bg-gre-500 hover:bg-org-600 dark:hover:bg-gre-600 focus:ring-org-700 dark:focus:ring-gre-500 focus:ring-offset-org-200 dark:focus:ring-offset-gre-200 text-neutral-700">
            <SvgDownload/>
        </button>
    );
}
