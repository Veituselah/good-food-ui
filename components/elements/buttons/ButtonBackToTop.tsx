import React from 'react';

const ButtonBackToTop = () => {
    return (
        <div className="z-100 absolute bottom-0 right-10 pointer-events-none back-to-top-wrapper">
            <a
                href="#top"
                className="back-to-top-link flex items-center justify-center inline-block no-underline text-3xl leading-10 text-center w-12 h-12 p-1 sticky rounded-2xl bg-gradient-to-r from-gre-500 to-gre-600 dark:from-org-500 dark:to-org-600 text-neutral-700 shadow-md"
                aria-label="Haut de page"
            >
                <svg
                    className="absolute z-110 w-6 h-6 text-neutral-800 pointer-events-none fill-current sm:block items-center"
                    fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 384 512"
                >
                    <path d="M374.6 246.6C368.4 252.9 360.2 256 352 256s-16.38-3.125-22.62-9.375L224 141.3V448c0 17.69-14.33 31.1-31.1 31.1S160 465.7 160 448V141.3L54.63 246.6c-12.5 12.5-32.75 12.5-45.25 0s-12.5-32.75 0-45.25l160-160c12.5-12.5 32.75-12.5 45.25 0l160 160C387.1 213.9 387.1 234.1 374.6 246.6z" />
                </svg>
            </a>
        </div>
    );
};

export default ButtonBackToTop;
