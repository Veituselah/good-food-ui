import React, {useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {getLoader} from "../../../redux/selectors/loader";
import {useRouter} from "next/router";
import {hideLoader, showLoader} from "../../../redux/actions/loader";

const OverlayLoader = () => {

    const showLoaderState = useSelector(getLoader);

    const router = useRouter();
    const dispatch = useDispatch();

    useEffect(() => {
        const handleStart = (url: any) => {
            if(url !== router.asPath){
                dispatch(showLoader('Chargement de la page en cours...'));
            }
        }
        const handleComplete = (url: any) => {
            if(url === router.asPath){
                dispatch(hideLoader());
            }
        }

        router.events.on('routeChangeStart', handleStart)
        router.events.on('routeChangeComplete', handleComplete)
        router.events.on('routeChangeError', handleComplete)

        return () => {
            router.events.off('routeChangeStart', handleStart)
            router.events.off('routeChangeComplete', handleComplete)
            router.events.off('routeChangeError', handleComplete)
        }
    })

    return (
        <div
            className={
                (showLoaderState.loader ? '' : 'hidden') + " p-4 flex-col fixed top-0 left-0 right-0 bottom-0 z-50 flex justify-center items-center bg-black bg-opacity-50"
            }
        >
            <object data="/images/logo/logo-dark.svg" width="300rem"/>
            <h2 className={"text-white text-4xl mt-5"}>{showLoaderState?.title}</h2>
        </div>
    )

}

export default OverlayLoader;