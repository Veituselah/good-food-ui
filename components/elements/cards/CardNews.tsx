import React from 'react';
import CardNewsFooter from './cardnewsfooter';

const CardNews = () => {
    return (
        <div className="inline-block overflow-hidden duration-300 transform bg-white dark:bg-neutral-800 rounded-2xl shadow-sm hover:-translate-y-2">
            <a href="#" className="w-full block h-full">
                <img alt="blog photo" src="/images/blog/1.jpg" className="max-h-40 w-full object-cover" />
                <div className="bg-white dark:bg-neutral-800 w-full p-4">
                    <p className="mb-3 text-xs font-semibold tracking-wide uppercase">
                        <a
                            href="/"
                            className="transition-colors duration-200 text-gre-700 dark:text-org-500 hover:text-gre-600 dark:hover:text-org-600 font-medium text-md"
                            aria-label="Category"
                            title="traveling"
                        >
                            traveling
                        </a>
                        <span className="text-neutral-400"> — 12 Feb 2022</span>
                    </p>
                    <p className="text-org-500 dark:text-gre-500 text-xl font-medium mb-2">Work at home</p>
                    <p className="text-neutral-700 dark:text-neutral-100 font-light text-md">
                        Work at home, remote, is the new age of the job, every person can work at home....
                    </p>
                    <div className="flex items-center mt-4">
                        <a href="#" className="block relative">
                            <img
                                alt="profil"
                                src="/images/person/6.jpg"
                                className="mx-auto object-cover rounded-full h-10 w-10 "
                            />
                        </a>
                        <CardNewsFooter />
                    </div>
                </div>
            </a>
        </div>
    );
};

export default CardNews;
