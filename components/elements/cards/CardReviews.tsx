import {useDispatch, useSelector} from "react-redux";
import {getProfile} from "../../../redux/selectors/profile";
import {Pagination} from "../pagination/Pagination";
import CalculatedStars from "../calculated/CalculatedStars";
import React, {useEffect, useState} from "react";
import {Context} from "../../../utils/middlewares/Context/Context";
import {CustomerHandler} from "../../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {ApiPlatformModel} from "../../../utils/models/ApiPlatform/ApiPlatformModel";

const NOTES = [
    {
        note: 5,
        color: "bg-green-600"
    },
    {
        note: 4,
        color: "bg-green-500"
    },
    {
        note: 3,
        color: "bg-yellow-500"
    },
    {
        note: 2,
        color: "bg-yellow-600"
    },
    {
        note: 1,
        color: "bg-red-500"
    },
    {
        note: 0,
        color: "bg-black"
    },
]

const CardReviews = () => {

    const dispatch = useDispatch();
    const profileState = useSelector(getProfile);
    const customer: any = profileState?.customer ?? {};
    const [comments, setComments] = useState<any>({});

    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {

        const context = new Context(false);
        const customerHandler = new CustomerHandler(context.getCustomerToken());

        customerHandler.getCustomerComments(customer.iri, currentPage).then((comments: any) => {
            setComments(comments);
        }).catch((error: any) => {
            ApiPlatformModel.showFailedResponseError(error, dispatch);
        })


    }, [currentPage]);

    const nbComments = comments?.totalItems ?? 0;

    return (
        <div className="shadow-lg rounded-2xl w-full bg-neutral-50 dark:bg-neutral-500">
            <div className="shadow-md">
                <div className="flex flex-row justify-between w-full">
                    <div
                        className="flex flex-row w-full p-4 bg-neutral-50 dark:bg-neutral-500 border-t-2 border-org-500 dark:border-gre-500 rounded-lg">
                        <div className="max-w-sm mx-auto md:w-full md:mx-0">
                            <h2 className="text-2xl font-bold">
                                Mes avis ({nbComments})
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="flex flex-col lg:flex-row overflow-x-auto">
                    <div className="shadow rounded-l-lg overflow-hidden w-full lg:w-2/3">
                        <table className="min-w-full leading-normal">
                            <thead>
                            <tr>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Cible
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Commentaire
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Note
                                </th>
                                {/*<th scope="col" className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Actions</th>*/}
                            </tr>
                            </thead>
                            <tbody>
                            {comments?.data?.map((comment: any) => {
                                return (
                                    <tr key={comment.id}>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap text-left">
                                                {comment?.restaurant ? 'Restaurant:\n' + comment?.restaurant?.name : 'Produit:\n' + comment?.product?.name}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap text-left">
                                                {comment.message}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap text-center">
                                                {comment.note}
                                            </p>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>

                        <Pagination
                            currentPage={currentPage}
                            nbPage={comments?.lastPage}
                            onPageChange={setCurrentPage}
                        />

                    </div>

                    <div className="shadow rounded-r-lg overflow-hidden w-full lg:w-1/3 py-4">

                        <div
                            className="container max-w-sm mx-auto items-center text-center rounded drop-shadow-lg px-5 md:px-0">

                            <div className="font-bold text-2xl tracking-wide">Notation moyenne</div>

                            <div className="flex mt-4 justify-center">
                                <div
                                    className="flex items-center inline-block py-3 px-2.5 space-x-2 leading-none text-center whitespace-nowrap align-baseline text-sm bg-neutral-100 dark:bg-neutral-500 text-dark rounded-full">
                                    <CalculatedStars note={customer?.meanComments ?? 0} displayTotal={true}/>
                                </div>
                            </div>

                            <div className="text-xs pt-2">
                                {nbComments} avi{nbComments > 1 ? 's' : ''} déposé{nbComments > 1 ? 's' : ''}
                            </div>

                            <div className="pt-6 flex items-center justify-center">
                                <ul className="flex flex-col w-full">
                                    {NOTES.map((noteData: any) => {

                                        const rate = customer?.noteDivision[noteData.note]?.rate ?? 0;

                                        return (
                                            <li className="flex flex-row" key={noteData.note}>
                                                <div
                                                    className="flex mx-auto justify-around w-full items-center p-2 text-center ">
                                                    <div className="flex text-neutral-400 text-xs">
                                                        <span className={"pr-1"}>{noteData.note}</span>
                                                        <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                             data-icon="star" className="w-4 text-yellow-500"
                                                             role="img" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 0 576 512">
                                                            <path fill="currentColor"
                                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z">
                                                            </path>
                                                        </svg>
                                                    </div>
                                                    <div className="w-24 bg-gray-200 h-3 rounded-md">
                                                        <div className={noteData.color + " h-3 rounded-md"}
                                                             style={{width: rate + '%'}}/>
                                                    </div>
                                                    <div
                                                        className="text-neutral-400 text-xs w-8 text-aright">{parseFloat(rate).toFixed(0)}%
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    })}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default CardReviews;