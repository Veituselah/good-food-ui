import React from 'react';
import CalculatedOpeningHours from '../calculated/CalculatedOpeningHours';
import CalculatedStars from '../calculated/CalculatedStars';
import Link from "next/link";
import {RESTAURANT_PAGE} from "../../../pages/restaurant/[slug]";

const CardRestaurant = ({restaurant, distance, defaultImage = 0}: any) => {

    return (
        <div>
            <img
                src={restaurant?.image ?? "/images/restaurant/restaurant_" + defaultImage + ".jpg"}
                alt="restaurant imgee"
                className="w-full object-cover object-center rounded-lg shadow-md"
            />
            <Link href={RESTAURANT_PAGE + '/' + restaurant?.slug} className={'cursor-pointer'}>
                <div className="relative px-4 -mt-16 cursor-pointer">
                    <div
                        className="w-full inline-block overflow-hidden duration-300 transform bg-white dark:bg-neutral-800 rounded-2xl shadow-sm hover:-translate-y-2 p-6">
                        <p className="mb-3 text-xs font-semibold tracking-wide uppercase">
                            <a
                                href="/"
                                className="transition-colors duration-200 text-gre-700 dark:text-org-500 hover:text-gre-600 dark:hover:text-org-600 font-medium text-md"
                                aria-label="Category"
                                title="traveling"
                            >
                                café burger
                            </a>
                            <span className="text-neutral-400"> — {restaurant?.city}</span>
                        </p>
                        <div className="text-org-500 dark:text-gre-500 text-xl font-medium mb-2">{restaurant?.name}
                            {distance &&
                                <span className="text-xs"> ({ (distance / 1000).toFixed(2)} km)</span>
                            }
                        </div>
                        <p className="text-neutral-700 dark:text-neutral-100 font-light text-md">
                            {restaurant?.description}
                        </p>
                        <CalculatedOpeningHours/>
                        <div className="flex flex-row items-center text-sm text-neutral-300 mt-4">
                            <CalculatedStars note={restaurant.meanNote}/>
                            <span className="ml-2">{restaurant.comments.length} Avis</span>
                        </div>
                    </div>
                </div>
            </Link>
        </div>
    );
};

export default CardRestaurant;
