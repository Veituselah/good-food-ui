import React, {useEffect, useRef, useState} from 'react';
import Link from 'next/link';
import ButtonDelete from '../buttons/ButtonDelete'

interface Props {
    links: HeaderLink[];
    label: string;
}

export interface HeaderLink {
    label: string;
    link?: string;
    isSelected?: boolean;
    desc?: string;
    qte?: string;
    price?: string;
    currency?: string;
    icon?: JSX.Element;
}

const _CardCartContent = (props: Props) => {
    // const ref = useRef<any>();
    return (
        <></>
                    // <div className="flex flex-row w-full">
                    //     <div className="w-1/5">
                    //         {entry.icon}
                    //     </div>
                    //     <div className="flex flex-col w-3/5 p-2 space-y-2">
                    //         <Link href={entry.link ?? ''} key={entry.label}>
                    //             <a className="-m-3 py-2 flex items-center hover:text-org-500 dark:hover:text-gre-500" onClick={() => setIsSectionOpen(false)}>
                    //                 <div className="">
                    //                     <p className="text-xs text-bold">{entry.label}</p>
                    //                 </div>
                    //             </a>
                    //         </Link>
                    //         <div className="-m-3 flex items-center">
                    //             <div className="flex flex-row font-normal w-full">
                    //                 <p className="relative text-xs mt-0 w-2/3">{entry.qte}</p>
                    //                 <p className="relative text-xs text-bold mt-0 w-1/3">{entry.price}{entry.currency}</p>
                    //             </div>
                    //         </div>
                    //     </div>
                    //     <div className="relative w-1/5">
                    //         <ButtonDelete/>
                    //     </div>
                    // </div>
    );
};
export default _CardCartContent;