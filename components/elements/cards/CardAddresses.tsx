import React, {useEffect, useState} from 'react'
import ButtonAddresses from '../../elements/buttons/ButtonAddresses'
import ButtonDelete from '../buttons/ButtonDelete'
import ButtonEditAddress from '../buttons/ButtonEditAddress'
import {useDispatch, useSelector} from "react-redux";
import {getProfile} from "../../../redux/selectors/profile";
import {Pagination} from "../pagination/Pagination";
import {Context} from "../../../utils/middlewares/Context/Context";
import {CustomerHandler} from "../../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {ApiPlatformModel} from "../../../utils/models/ApiPlatform/ApiPlatformModel";
import {doDelete} from "../../../redux/sagas/request";
import {AddressHandler} from "../../../utils/handlers/ApiPlatform/User/Customer/AddressHandler";
import {addLog} from "../../../redux/actions/logger";
import {MessageGravity} from "../../../utils/middlewares/Logger/MessageGravity";

const CardAddresses = ({countries}: any) => {

    const dispatch = useDispatch();
    const profileState = useSelector(getProfile);
    const customer: any = profileState?.customer ?? {};
    const [addresses, setAdresses] = useState<any>({});

    const [currentPage, setCurrentPage] = useState(1);

    const refreshAddressesList = () => {

        const context = new Context(false);
        const customerHandler = new CustomerHandler(context.getCustomerToken());

        customerHandler.getCustomerAddresses(customer.iri, currentPage).then((addresses: any) => {
            setAdresses(addresses);
        }).catch((error: any) => {
            ApiPlatformModel.showFailedResponseError(error, dispatch);
        })

    }

    useEffect(refreshAddressesList, [currentPage]);

    return (
        <div className="shadow-lg rounded-2xl w-full bg-neutral-50 dark:bg-neutral-500">

            {/* <div className="container mx-auto px-4 sm:px-8 max-w-3xl"> */}
            <div className="shadow-md">
                <div className="flex flex-row justify-between w-full">
                    <div
                        className="flex flex-wrap justify-between flex-row w-full p-4 bg-neutral-50 dark:bg-neutral-500 border-t-2 border-org-500 dark:border-gre-500 rounded-lg">
                        <div className="max-w-sm mx-auto md:mx-0">
                            <h2 className="text-2xl font-bold">
                                Mes adresses ({addresses?.totalItems ?? 0})
                            </h2>
                        </div>
                        <div className="max-w-sm mx-auto md:mx-0">
                            <div className="text-end">
                                <div
                                    className="flex flex-col md:flex-row md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-center">
                                    <ButtonAddresses
                                        countries={countries?.data}
                                        customer={customer}
                                        onSaveAddress={refreshAddressesList}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="overflow-x-auto">
                    <div className="min-w-full shadow rounded-lg overflow-hidden">
                        <table className="min-w-full leading-normal">
                            <thead>
                            <tr>
                                <th scope="col"
                                    className="px-5 py-3 w-1/4 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Nom
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 w-1/2 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Adresse
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 w-1/4 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {addresses?.data?.map((address: any) => {
                                return (
                                    <tr key={address?.slug}>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {address?.name}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {address?.street}, {address?.zipCode} {address?.city}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm space-x-2">
                                            <div className={"flex space-x-2 items-center"}>
                                                <ButtonEditAddress
                                                    address={address}
                                                    countries={countries?.data}
                                                    customer={customer}
                                                    onSaveAddress={refreshAddressesList}
                                                />
                                                <ButtonDelete onClick={() => {
                                                    if (confirm('Êtes vous sûr.e de vouloir supprimer cette adresse ?')) {

                                                        const context = new Context(false);
                                                        const token = context.getCustomerToken();
                                                        const adddressHandler = new AddressHandler(token);
                                                        const iri = adddressHandler.buildIri(address.slug);

                                                        doDelete(adddressHandler, iri, dispatch).then(() => {
                                                            refreshAddressesList();
                                                        }).catch((error: any) => {

                                                            if (error?.response?.status === 403) {
                                                                dispatch(addLog({
                                                                    gravity: MessageGravity.INFO,
                                                                    message: 'Une adresse utilisée dans une commande ne peut être supprimée',
                                                                }))
                                                            } else {
                                                                ApiPlatformModel.showFailedResponseError(error, dispatch);
                                                            }

                                                        })
                                                    }
                                                }}/>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>

                        <Pagination
                            currentPage={currentPage}
                            nbPage={addresses?.lastPage}
                            onPageChange={setCurrentPage}
                        />
                    </div>
                </div>
            </div>
            {/* </div> */}

        </div>
    );
};
export default CardAddresses;