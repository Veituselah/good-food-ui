import React from 'react';
import CalculatedAuthorName from '../calculated/CalculatedAuthorName';
import CalculatedComments from '../calculated/CalculatedComments';
import CalculatedLikes from '../calculated/CalculatedLikes';
import CalculatedReadTime from '../calculated/CalculatedReadTime';

const CardNewsFooter = () => {
    return (
        <div className="flex flex-row justify-between ml-4 text-sm">
            <div className="flex flex-col space-x-4 w-2/3">
                <CalculatedAuthorName/>
                <CalculatedReadTime/>
            </div>
            <div className="flex flex-col align-left pl-8 w-1/3">
                <CalculatedLikes/>
                <CalculatedComments/>
            </div>
        </div>
    );
};

export default CardNewsFooter;
