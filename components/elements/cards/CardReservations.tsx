import React, {FC, useEffect, useState} from 'react';
import ButtonCancel from '../buttons/ButtonCancel'
import {useDispatch, useSelector} from "react-redux";
import {getProfile} from "../../../redux/selectors/profile";
import {Pagination} from "../pagination/Pagination";
import {Context} from "../../../utils/middlewares/Context/Context";
import {CustomerHandler} from "../../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {ApiPlatformModel} from "../../../utils/models/ApiPlatform/ApiPlatformModel";
import {addLog} from "../../../redux/actions/logger";
import {MessageGravity} from "../../../utils/middlewares/Logger/MessageGravity";
import {ReservationHandler} from "../../../utils/handlers/ApiPlatform/ReservationHandler";

const CardReservations: FC = () => {

    const dispatch = useDispatch();
    const profileState = useSelector(getProfile);
    const customer: any = profileState?.customer ?? {};
    const [reservations, setReservations] = useState<any>({});

    const [currentPage, setCurrentPage] = useState(1);

    const refreshReservationsList = () => {

        const context = new Context(false);
        const customerHandler = new CustomerHandler(context.getCustomerToken());

        customerHandler.getCustomerReservations(customer.iri, currentPage).then((reservations: any) => {
            setReservations(reservations);
        }).catch((error: any) => {
            ApiPlatformModel.showFailedResponseError(error, dispatch);
        })

    }

    const cancelReservation = (reservation: any) => {

        if (confirm('Êtes vous sûr.e de vouloir annuler cette réservation ?')) {

            const context = new Context(false);
            const reservationHandler = new ReservationHandler(context.getCustomerToken());

            reservationHandler.cancel(reservation).then(() => {
                dispatch(addLog({
                    gravity: MessageGravity.SUCCESS,
                    message: 'Votre réservation a été annulée'
                }))
                refreshReservationsList();
            }).catch((error: any) => {
                ApiPlatformModel.showFailedResponseError(error, dispatch);
            })

        }

    }

    useEffect(refreshReservationsList, [currentPage]);

    return (
        <div className="shadow-lg rounded-2xl w-full bg-neutral-50 dark:bg-neutral-500">
            <div className="shadow-md">
                <div className="flex flex-row justify-between w-full">
                    <div
                        className="flex flex-row w-full p-4 bg-neutral-50 dark:bg-neutral-500 breservation-t-2 breservation-org-500 dark:breservation-gre-500 rounded-lg">
                        <div className="max-w-sm mx-auto md:w-full md:mx-0">
                            <h2 className="text-2xl font-bold">
                                Mes réservations ({reservations?.totalItems ?? 0})
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="overflow-x-auto">
                    <div className="min-w-full shadow rounded-lg overflow-x-auto scrollbar scrollbar-h-2 scrollbar-thumb-neutral-400 scrollbar-track-neutral-100 dark:scrollbar-track-neutral-800">
                        <table className="min-w-full leading-normal">
                            <thead>
                            <tr>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-left text-sm uppercase font-normal"
                                >
                                    Date
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-left text-sm uppercase font-normal"
                                >
                                    Restaurant
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-left text-sm uppercase font-normal"
                                >
                                    N° Tél.
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-left text-sm uppercase font-normal"
                                >
                                    NB. Pers.
                                </th>
                                <th scope="col"
                                    className="px-5 py-3 bg-white dark:bg-neutral-800 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-left text-sm uppercase font-normal"
                                >
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {reservations?.data?.map((reservation: any) => {

                                let isPasted = new Date(reservation?.reservedFor) < new Date();
                                let canCancel = !isPasted && !reservation?.canceledAt;

                                return (
                                    <tr key={reservation?.id}>
                                        <td className="px-5 py-3 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {reservation?.reservedFor?.toLocaleDateString() + ' ' + reservation?.reservedFor?.toLocaleTimeString()}
                                            </p>
                                        </td>
                                        <td className="text-left px-5 py-3 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {reservation?.restaurant?.name ?? '-'}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {reservation?.phoneNumber ?? '-'}
                                            </p>
                                        </td>
                                        <td className="text-center px-5 py-3 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {reservation?.nbPerson ?? '-'}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 breservation-b breservation-neutral-200 dark:breservation-neutral-600 text-sm space-x-2">
                                            <div className={"flex space-x-2 items-center"}>
                                                {canCancel ?
                                                    (
                                                        <ButtonCancel onClick={() => cancelReservation(reservation)}/>
                                                    ) :
                                                    reservation?.canceledAt ? (
                                                        <span
                                                            className="relative inline-block px-3 py-1 font-semibold leading-tight text-red-500">
                                                    <span
                                                        aria-hidden="true"
                                                        className="absolute inset-0 bg-red-200 opacity-50 rounded-full"
                                                    />
                                                    <span
                                                        className="relative">
                                                    Annulée
                                                    </span>
                                                </span>
                                                    ) : (
                                                        <span
                                                            className="relative inline-block px-3 py-1 font-semibold leading-tight text-green-500">
                                                    <span
                                                        aria-hidden="true"
                                                        className="absolute inset-0 bg-green-200 opacity-50 rounded-full"
                                                    />
                                                    <span
                                                        className="relative">
                                                    Passée
                                                    </span>
                                                </span>
                                                    )
                                                }
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                        <Pagination nbPage={reservations?.lastPage} currentPage={currentPage}
                                    onPageChange={setCurrentPage}/>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default CardReservations;