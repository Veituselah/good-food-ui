import React from 'react';
import Avatar from '../../elements/avatars/Avatar';
import {useSelector} from "react-redux";
import {getProfile} from "../../../redux/selectors/profile";

const CardProfile = () => {

    const profileState = useSelector(getProfile);
    const customer: any = profileState?.customer ?? {};

    return (
        <
            div className="shadow-lg rounded-2xl w-full bg-neutral-50 dark:bg-neutral-500">
            <img alt="profil" src="/images/landscape/1.jpg" className="rounded-t-lg h-28 w-full mb-4"/>
            <div className="flex flex-col items-center justify-center p-4 -mt-16">
                <Avatar/>
                <p className="text-xl font-medium mt-2">Bonjour {customer.firstname} {customer.lastname}</p>
                {/*<div className="rounded-lg bg-org-200 dark:bg-gre-200 p-2 w-full my-8">*/}
                {/*    <div className="flex items-center justify-between text-xs text-neutral-700">*/}
                {/*        <p className="flex flex-col">*/}
                {/*            Vos commandes*/}
                {/*            <span className="text-black dark:text-indigo-500 font-bold">{customer?.nbOrders ?? 0}</span>*/}
                {/*        </p>*/}
                {/*        <p className="flex flex-col">*/}
                {/*            Vos avis*/}
                {/*            <span className="text-black dark:text-indigo-500 font-bold">{customer?.nbComments ?? 0}</span>*/}
                {/*        </p>*/}
                {/*        <p className="flex flex-col">*/}
                {/*            Vos notations*/}
                {/*            <span className="text-black dark:text-indigo-500 font-bold">{customer?.meanComments ?? 0}</span>*/}
                {/*        </p>*/}
                {/*    </div>*/}
                {/*</div>*/}
            </div>
        </div>
    );
};
export default CardProfile;