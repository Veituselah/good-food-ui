import React from 'react'
import dynamic from "next/dynamic"
import ButtonDownload from '../buttons/ButtonDownload'
import ProgressBarOrder from '../progress/ProgressBarOrder'
import {CardCartLine} from "./CardCartLine";
import {OrderModel} from "../../../utils/models/ApiPlatform/Order/OrderModel";
import MapRestaurant from "../maps/MapRestaurant";

const CardOrdersDetail = ({order} : any) => {

    const MapRestaurant = React.useMemo(() => dynamic(
        () => import('../maps/MapRestaurant'), // replace '@components/map' with your component's location
        {
            loading: () => <p>Chargement de la map en cours</p>,
            ssr: false // This line is important. It's what prevents server-side render
        }
    ), undefined)

    return (
        <div className="shadow-lg rounded-2xl w-full bg-neutral-50 dark:bg-neutral-500">
            <div>
                <div className="flex flex-row justify-between w-full">
                    <div className="flex flex-row w-full p-4 bg-neutral-50 dark:bg-neutral-500 border-t-2 border-org-500 dark:border-gre-500 rounded-lg">
                        <div className="mx-auto md:w-full md:mx-0">
                            <div className="text-end">
                                <ProgressBarOrder order={order}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bg-white dark:bg-neutral-800 p-2 rounded-lg">
                    <div className="min-w-full flex flex-col md:flex-row">
                        <div className="w-full md:w-2/3">
                            <MapRestaurant restaurant={order?.restaurant}/>
                        </div>
                        <div className="w-full md:w-1/3">
                            <div className="w-full md:w-2/3 lg:w-full transform px-2 w-screen max-w-lg sm:px-0 px-2 h-screen/2 overflow-y-auto">
                                <div className="p-4 justify-center">
                                    <div className="flex flex-row space-x-4">
                                        <h2 className="font-sans text-3xl font-bold text-left">Contenu de la commande</h2>
                                        <ButtonDownload order={order}/>
                                    </div>
                                    <div className="relative grid gap-6 py-6 px-4 divide-y divide-neutral-400">
                                        {order?.orderLines?.map((orderLine: any) => {
                                            return <CardCartLine key={orderLine?.id} cart={order} orderLine={orderLine} displayActionButton={false}/>
                                        })}
                                        <div className="relative  grid pt-6 px-4 divide-y divide-neutral-400">
                                            <div className="flex flex-row font-normal w-full">
                                                <p className="relative mt-0 w-2/3">Total panier</p>
                                                <p className="relative mt-0 w-1/3">{OrderModel.formatPrice(order?.total) + (order?.customer?.symbol ?? '€')}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default CardOrdersDetail;