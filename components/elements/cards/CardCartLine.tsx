import Link from "next/link";
import CalculatedProductCounter from "../calculated/CalculatedProductCounter";
import ButtonDelete from "../buttons/ButtonDelete";
import React, {useState} from "react";
import {OrderModel} from "../../../utils/models/ApiPlatform/Order/OrderModel";
import {OrderLineHandler} from "../../../utils/handlers/ApiPlatform/Order/Line/OrderLineHandler";
import {Context} from "../../../utils/middlewares/Context/Context";
import {useDispatch} from "react-redux";
import {removeCartLine, saveCartLine} from "../../../redux/actions/cart";
import {ApiPlatformModel} from "../../../utils/models/ApiPlatform/ApiPlatformModel";
import {PRODUCT_PATH} from "../../../pages/product/[reference]";

let endClickTimeout: any = null;

export const CardCartLine = ({cart, orderLine, displayActionButton = true}: any) => {

    const [qty, setQty] = useState<number>(orderLine.qty);
    const [disableQtyManager, setDisableQtyManager] = useState<boolean>(false);
    const dispatch = useDispatch();
    const context = new Context(false);
    const orderLineHandler = new OrderLineHandler(context.getCustomerToken());

    const updateQuantity = (qtyToSet: number) => {

        const oldQty = qty;
        setQty(qtyToSet);

        if (endClickTimeout) {
            clearTimeout(endClickTimeout);
        }

        endClickTimeout = setTimeout(() => {

            const orderLineHandler = new OrderLineHandler(context.getCustomerToken());

            orderLine.qty = qtyToSet;
            orderLine.iri = orderLineHandler.buildIri(orderLine.id);

            setDisableQtyManager(true);

            orderLineHandler.patch(orderLine).then((orderLine: any) => {
                dispatch(saveCartLine(orderLine));
            }).catch((error: any) => {
                ApiPlatformModel.showFailedResponseError(error, dispatch);
                setQty(oldQty);
            }).finally(() => {
                setDisableQtyManager(false);
            })

        }, 1000)

    }

    const deleteCartLine = () => {

        const orderLineID = orderLine.id;

        orderLineHandler.delete(orderLine.iri).then(() => {
            dispatch(removeCartLine(orderLineID));
        }).catch((error: any) => {
            ApiPlatformModel.showFailedResponseError(error, dispatch);
        })

    }

    return (
        <div className="flex flex-row w-full pt-3" key={orderLine?.id}>
            <div className="w-1/5">
                <img
                    src={
                        orderLine?.product?.medias?.length > 0 ?
                            orderLine?.product?.medias[0] :
                            '/images/food/burger.jpg'
                    }
                    className={"rounded w-12"}
                    alt={"product-image"}
                />
            </div>
            <div className="flex flex-col w-3/5">
                <div>
                    <Link href={PRODUCT_PATH + orderLine?.product?.reference}>
                        <p className="text-xs text-bold hover:text-org-500 dark:hover:text-gre-500 cursor-pointer">{orderLine?.name}</p>
                    </Link>
                </div>
                <div className="flex items-center">
                    <div className="flex flex-row font-normal w-full">
                        <CalculatedProductCounter value={qty} onChange={updateQuantity} disabled={disableQtyManager}
                                                  displayActionButton={displayActionButton}/>
                        {/*<p className="relative text-xs mt-0 w-2/3">{orderLine?.qty}</p>*/}
                    </div>
                </div>
                <p className="relative text-xs text-bold mt-0 w-1/3">{OrderModel.formatPrice(orderLine?.total) + (cart?.currency?.symbol ?? '€')}</p>

            </div>
            {displayActionButton &&
                <div className="relative w-1/5 flex justify-end">
                    <ButtonDelete
                        onClick={() => confirm('Êtes vous sûr.e de vouloir supprimer ce produit de votre panier ?') && deleteCartLine()}/>
                </div>
            }
        </div>
    )

}