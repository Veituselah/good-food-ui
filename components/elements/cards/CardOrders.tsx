import React, {FC, useEffect, useState} from 'react';
import Button from '../../elements/buttons/Button';
import ButtonCancel from '../buttons/ButtonCancel'
import ButtonDetailOrder from '../buttons/ButtonDetailOrder'
import ButtonDownload from '../buttons/ButtonDownload';
import {useDispatch, useSelector} from "react-redux";
import {getProfile} from "../../../redux/selectors/profile";
import {Pagination} from "../pagination/Pagination";
import {Context} from "../../../utils/middlewares/Context/Context";
import {CustomerHandler} from "../../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {ApiPlatformModel} from "../../../utils/models/ApiPlatform/ApiPlatformModel";
import {OrderHandler} from "../../../utils/handlers/ApiPlatform/Order/OrderHandler";
import {addLog} from "../../../redux/actions/logger";
import {MessageGravity} from "../../../utils/middlewares/Logger/MessageGravity";

const CardOrders: FC = () => {

    const dispatch = useDispatch();
    const profileState = useSelector(getProfile);
    const customer: any = profileState?.customer ?? {};
    const [orders, setOrders] = useState<any>({});

    const [searchedRef, setSearchedRef] = useState<any>();
    const [currentPage, setCurrentPage] = useState(1);

    const refreshOrdersList = () => {

        const context = new Context(false);
        const customerHandler = new CustomerHandler(context.getCustomerToken());

        customerHandler.getCustomerOrders(customer.iri, currentPage, searchedRef).then((orders: any) => {
            setOrders(orders);
        }).catch((error: any) => {
            ApiPlatformModel.showFailedResponseError(error, dispatch);
        })

    }

    const cancelOrder = (order: any) => {

        if (confirm('Êtes vous sûr.e de vouloir annuler cette commande ?')) {

            const context = new Context(false);
            const orderHandler = new OrderHandler(context.getCustomerToken());

            orderHandler.cancel(order).then(() => {
                dispatch(addLog({
                    gravity: MessageGravity.SUCCESS,
                    message: 'Votre commande a bien été mise à jour'
                }))
                refreshOrdersList();
            }).catch((error: any) => {
                ApiPlatformModel.showFailedResponseError(error, dispatch);
            })

        }

    }

    useEffect(refreshOrdersList, [currentPage, searchedRef]);

    return (
        <div className="shadow-lg rounded-2xl w-full bg-neutral-50 dark:bg-neutral-500">
            <div className="shadow-md">
                <div className="flex flex-wrap flex-row justify-between w-full">
                    <div
                        className="flex flex-row w-full p-4 bg-neutral-50 dark:bg-neutral-500 border-t-2 border-org-500 dark:border-gre-500 rounded-lg">
                        <div className="max-w-sm mx-auto md:w-full md:mx-0">
                            <h2 className="text-2xl font-bold">
                                Mes commandes ({orders?.totalItems ?? 0})
                            </h2>
                        </div>
                        <div className="max-w-sm mx-auto md:w-full md:mx-0">
                            <div className="text-end">
                                <form
                                    onSubmit={(e: any) => {
                                        e.preventDefault();
                                        let form = e.target;
                                        let elements = form.elements;
                                        setSearchedRef(elements.filter.value)
                                    }}
                                    className="flex flex-col md:flex-row md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-center">
                                    <div className=" relative ">
                                        <input
                                            type="text"
                                            name={"filter"}
                                            className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-800 text-gray-400 aa-input"
                                            placeholder="ID de commande..."
                                        />
                                    </div>
                                    <Button color="gre" label="Rechercher" submit={true}/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="overflow-x-auto">
                    <div className="min-w-full shadow rounded-lg overflow-x-auto scrollbar scrollbar-h-2 scrollbar-thumb-neutral-400 scrollbar-track-neutral-100 dark:scrollbar-track-neutral-800">
                        <table className="min-w-full leading-normal">
                            <thead>
                                <tr>
                                    <th scope="col"
                                        className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">ID
                                    </th>
                                    <th scope="col"
                                        className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Date
                                    </th>
                                    <th scope="col"
                                        className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Restaurant
                                    </th>
                                    <th scope="col"
                                        className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Statut
                                    </th>
                                    <th scope="col"
                                        className="px-5 py-3 bg-white dark:bg-neutral-800 border-b border-neutral-200 dark:border-neutral-600 text-left text-sm uppercase font-normal">Actions
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            {orders?.data?.map((order: any) => {

                                const status = order?.status?.slug;

                                let statusColor: string;
                                let canCancel = status === 'payee';
                                let canSeeOrderDetail = status !== 'panier';

                                switch (status) {
                                    case 'disponible-en-resaurant':
                                    case 'livree':
                                    case 'remboursee':
                                        statusColor = 'green';
                                        break;
                                    case 'en-cours-de-livraison':
                                    case 'en-cours-de-traitement':
                                    case 'panier':
                                    case 'payee':
                                        statusColor = 'org';
                                        break;
                                    case 'annulee':
                                    case 'echec-de-paiement':
                                    default:
                                        statusColor = 'red';
                                        break;
                                }

                                return (
                                    <tr key={order?.reference}>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {order?.reference}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {order?.orderedAt?.toLocaleDateString() ?? '-'}
                                            </p>
                                        </td>
                                        <td className="text-left px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <p className="whitespace-no-wrap">
                                                {order?.restaurant?.name ?? '-'}
                                            </p>
                                        </td>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm">
                                            <span
                                                className={"relative inline-block px-3 py-1 font-semibold leading-tight text-" + statusColor + "-500"}>
                                                <span aria-hidden="true"
                                                      className={"absolute inset-0 bg-" + statusColor + "-200 opacity-50 rounded-full"}
                                                />
                                                <span className="relative">{order?.status?.name}</span>
                                            </span>

                                        </td>
                                        <td className="px-5 py-3 border-b border-neutral-200 dark:border-neutral-600 text-sm space-x-2 space-y-2 items-center text-center">
                                          <div className={"flex space-x-2 items-center"}>
                                            {canSeeOrderDetail &&
                                                <>
                                                    <ButtonDetailOrder order={order}/>
                                                    <ButtonDownload order={order}/>
                                                </>
                                            }
                                            {canCancel &&
                                                <ButtonCancel onClick={() => cancelOrder(order)}/>
                                            }
                                          </div>
                                        </td>

                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                        <Pagination nbPage={orders?.lastPage} currentPage={currentPage} onPageChange={setCurrentPage}/>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default CardOrders;