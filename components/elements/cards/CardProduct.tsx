import React from 'react';
import CalculatedStars from '../calculated/CalculatedStars';
import {ButtonAddToCart} from "../buttons/ButtonAddToCart";
import {OrderModel} from "../../../utils/models/ApiPlatform/Order/OrderModel";

const CardProduct = ({restaurant, product}: any) => {

    let price = product.priceTaxExcluded;

    for (let tax of product?.taxType?.taxes) {

        if (tax?.country?.isoCode === restaurant?.country?.isoCode) {
            price *= 1 + parseFloat(tax?.rate);
        }

    }

    return (
        <div
            className="flex flex-col bg-white dark:bg-neutral-800 rounded-lg shadow-md transform duration-300 hover:-translate-y-2 lg:flex-row-reverse">
            <div className="flex-none lg:w-48 relative">
                {/*<div*/}
                {/*    className="text-xs text-white z-10 absolute pl-8 pr-8 pb-2 pt-2 rounded-tl-lg rounded-br-lg font-semibold uppercase bg-gradient-to-r from-org-500 to-org-600 dark:from-gre-500 dark:to-gre-600 text-neutral-700">*/}
                {/*    <h1>PROMO</h1>*/}
                {/*</div>*/}
                <img
                    src={product?.medias?.length > 0 ? product.medias[0] : '/images/food/burger.jpg'}
                    alt="shopping image"
                    className="rounded-lg inset-0 w-full h-full object-cover object-center"
                />
            </div>
            <form className="flex-auto p-6">
                <p className="mb-3 text-xs font-semibold tracking-wide uppercase">
                    <a
                        href="/"
                        className="transition-colors duration-200 text-gre-700 dark:text-org-500 hover:text-gre-600 dark:hover:text-org-600 font-medium text-md"
                        aria-label="Category"
                        title="category"
                    >
                        {(product?.mainCategory ?? 'Non catégorisé') + (product?.categories?.length > 0 ? ' : ' + product?.categories[0] : '')}
                    </a>
                    {/*<span className="text-neutral-400"> — Plat + Dessert</span>*/}
                </p>

                <div className="flex flex-wrap">
                    <h1 className="flex-auto text-xl font-medium text-org-500 dark:text-gre-500">{product.name}</h1>
                    <div
                        className="text-xl font-bold text-neutral-700 dark:text-neutral-100">
                        {OrderModel.formatPrice(price)} {product?.currency?.symbol ?? '€'}
                    </div>
                </div>

                {/*<div className="flex items-baseline m-4 text-neutral-700 dark:text-neutral-100 font-light text-md">*/}
                {/*    <div className="space-x-4 flex">*/}
                {/*        <label className="text-center">*/}
                {/*            <input*/}
                {/*                type="radio"*/}
                {/*                className="w-3 h-3 flex items-center justify-center"*/}
                {/*                name="size"*/}
                {/*                value="xs"*/}
                {/*            />*/}
                {/*            XS*/}
                {/*        </label>*/}
                {/*        <label className="text-center">*/}
                {/*            <input*/}
                {/*                type="radio"*/}
                {/*                className="w-3 h-3 flex items-center justify-center"*/}
                {/*                name="size"*/}
                {/*                value="s"*/}
                {/*            />*/}
                {/*            S*/}
                {/*        </label>*/}
                {/*        <label className="text-center">*/}
                {/*            <input*/}
                {/*                type="radio"*/}
                {/*                className="w-3 h-3 flex items-center justify-center"*/}
                {/*                name="size"*/}
                {/*                value="m"*/}
                {/*            />*/}
                {/*            M*/}
                {/*        </label>*/}
                {/*        <label className="text-center">*/}
                {/*            <input*/}
                {/*                type="radio"*/}
                {/*                className="w-3 h-3 flex items-center justify-center"*/}
                {/*                name="size"*/}
                {/*                value="l"*/}
                {/*            />*/}
                {/*            L*/}
                {/*        </label>*/}
                {/*        <label className="text-center">*/}
                {/*            <input*/}
                {/*                type="radio"*/}
                {/*                className="w-3 h-3 flex items-center justify-center"*/}
                {/*                name="size"*/}
                {/*                value="xl"*/}
                {/*            />*/}
                {/*            XL*/}
                {/*        </label>*/}
                {/*    </div>*/}
                {/*    <a href="#" className="ml-auto hidden md:block text-sm text-gray-500 dark:text-gray-300 underline">*/}
                {/*        Menu Guide*/}
                {/*    </a>*/}
                {/*</div>*/}

                {/*<CalculatedAllergens/>*/}

                <div className="flex flex-row items-center text-sm text-neutral-300 mt-4">
                    <CalculatedStars note={product?.meanNote}/>
                    <span className="ml-2">{product?.comments?.length ?? 0} Avis</span>
                </div>

                <p className="text-sm text-neutral-500 dark:text-neutral-300 mt-4">{product?.description}</p>

                {/*<p className="text-sm text-neutral-500 dark:text-neutral-300">Free delivery for 24$ orders.</p>*/}

                <ButtonAddToCart product={product}/>
            </form>
        </div>
    );
};

export default CardProduct;
