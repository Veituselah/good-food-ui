import AdminNavLink from "./AdminNavLink";
import React, {createRef, useEffect, useState} from "react";
import {CustomContext, Role} from "../../../../internal";
import {ADMIN_PATH} from "../../../../pages/admin";
import {ADMIN_RESTAURANT_PATH} from "../../../../pages/admin/restaurant";
import {ADMIN_ORDER_PATH} from "../../../../pages/admin/order";
import {ADMIN_COUNTRY_PATH} from "../../../../pages/admin/country";
import {ADMIN_RESERVATION_PATH} from "../../../../pages/admin/reservation";

const AdminDropdown = () => {
    const btnRef = createRef();
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    useEffect(() => {

        const onClickOutside = (e: any) => {
            let current: any = btnRef.current;

            if (current && !current?.contains(e.target)) {
                setIsMenuOpen(false);
            }
        };

        document.addEventListener('click', onClickOutside);

        return () => {
            document.removeEventListener('click', onClickOutside);
        }
    })
    return (
                <div className="block rounded-2xl lg:hidden relative">
                    <div className="-mr-2 -my-2 xl:hidden p-4">
                        <button type="button" ref={btnRef as React.RefObject<HTMLButtonElement>} onClick={() => setIsMenuOpen(true)} className="shadow-lg bg-white dark:bg-neutral-600 rounded-md p-2 inline-flex items-center justify-center text-gray-400 dark:text-gray-50 hover:text-gray-500 dark:hover:text-white hover:bg-neutral-100 dark:hover:bg-neutral-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                            <span className="sr-only">Ouvrir le menu</span>
                            <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"/>
                            </svg>
                        </button>
                    </div> 
                    {isMenuOpen && (           
                    <div className="absolute bg-white h-full rounded-2xl dark:bg-neutral-600 right-4">
                        <nav className="mt-6 bg-white dark:bg-neutral-600 rounded-2xl">
                            <CustomContext.Consumer>
                                {
                                    (context) => {
                                        const user = context.getAdmin();
                                        return (
                                            <ul>
                                                <AdminNavLink
                                                    label={"Dashboard"}
                                                    url={ADMIN_PATH}
                                                    icon="dashboard"
                                                />
                                                {
                                                    user?.isGranted(Role.ROLE_LOGISTICIAN) ||
                                                    user?.isGranted(Role.ROLE_ACCOUNTANT) ?
                                                        (
                                                            <AdminNavLink
                                                                label={"Commandes"}
                                                                url={ADMIN_ORDER_PATH}
                                                                icon="order"
                                                            />
                                                        )
                                                        : null
                                                }
                                                <AdminNavLink
                                                    label={"Produits"}
                                                    url={'/admin/product'}
                                                    icon="product"
                                                />
                                                {
                                                    user?.isGranted(Role.ROLE_LOGISTICIAN) ||
                                                    user?.isGranted(Role.ROLE_HR) ?
                                                        (
                                                            <AdminNavLink
                                                                label={"Clients"}
                                                                url={'/admin/customer'}
                                                                icon="user"
                                                            />
                                                        )
                                                        : null
                                                }
                                                {/* {
                                                    user?.isGranted(Role.ROLE_ADMIN) ?
                                                        (
                                                            <AdminNavLink
                                                                label={"Catégories"}
                                                                url={'/admin/categorie'}
                                                                icon="category"
                                                            />
                                                        ) : null
                                                } */}
                                                {
                                                    user?.isGranted(Role.ROLE_ADMIN) ?
                                                        (
                                                            <>
                                                                <AdminNavLink
                                                                    label={"Restaurants"}
                                                                    url={ADMIN_RESTAURANT_PATH}
                                                                    icon="restaurant"
                                                                />
                                                                <AdminNavLink
                                                                    label={"Réservations"}
                                                                    url={ADMIN_RESERVATION_PATH}
                                                                    icon="reservation"
                                                                />
                                                                <AdminNavLink
                                                                    label={"Pays"}
                                                                    url={ADMIN_COUNTRY_PATH}
                                                                    icon="country"
                                                                />
                                                                <AdminNavLink
                                                                    label={"Devises"}
                                                                    url={'/admin/currency'}
                                                                    icon="currency"
                                                                />
                                                                <AdminNavLink
                                                                    label={"Configuration"}
                                                                    url={'/admin/settings'}
                                                                    icon="configuration"
                                                                />
                                                            </>
                                                        ) : null
                                                }
                                            </ul>                                          
                                        )
                                    }
                                }
                            </CustomContext.Consumer>                          
                        </nav>
                    </div>
                )}
            </div>
    )
}

export default AdminDropdown;
