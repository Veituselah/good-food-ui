import React from "react";
import Link from "next/link";
import SVGIcon from "../../icon/SVGIcon";
import {useRouter} from "next/router";

type Props = {
    label: string,
    url: string,
    icon: string
}

const AdminNavLink = ({label, url, icon}: Props) => {

    const router = useRouter();
    const pathname = router.pathname;

    return (
        <li>
            <Link
                href={url}
            >
                <a
                    className={
                        pathname === url ?
                            "w-full font-thin uppercase text-org-500 dark:text-gre-500 flex items-center p-4 transition-colors duration-200 justify-start bg-gradient-to-r from-white to-org-200 border-r-4 border-org-500 dark:from-neutral-600 dark:to-gre-300 border-r-4 dark:border-gre-500" :
                            "w-full font-thin uppercase flex items-center p-4 transition-colors duration-200 justify-start hover:text-org-500 dark:hover:text-gre-500"
                    }
                >
                    <span className="text-left">
                        <SVGIcon width="24px" height="24px" icon={icon}/>
                    </span>
                    <span className="mx-4 text-sm font-normal">{label}</span>
                </a>
            </Link>
        </li>
    );

}

export default AdminNavLink;