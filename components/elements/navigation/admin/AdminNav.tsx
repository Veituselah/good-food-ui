import AdminNavLink from "./AdminNavLink";
import React from "react";
import {CustomContext, Role} from "../../../../internal";
import {ADMIN_PATH} from "../../../../pages/admin";
import {ADMIN_RESTAURANT_PATH} from "../../../../pages/admin/restaurant";
import {ADMIN_ORDER_PATH} from "../../../../pages/admin/order";
import {ADMIN_COUNTRY_PATH} from "../../../../pages/admin/country";
import {ADMIN_RESERVATION_PATH} from "../../../../pages/admin/reservation";

const AdminNav = () => {

    return (
        <div className="hidden rounded-2xl lg:block shadow-lg relative">
            <div className="bg-white h-full rounded-2xl dark:bg-neutral-600">
                {/* Logo */}
                <div className="flex items-center justify-center pt-6">
                    <a href="/">
                        <object className="block dark:hidden pointer-events-none " data="/images/logo/logo-light.svg" width="100px" />
                        <object className="hidden dark:block pointer-events-none " data="/images/logo/logo-dark.svg" width="100px"/>
                    </a>
                </div>
                <nav className="mt-6">
                    <CustomContext.Consumer>
                        {
                            (context) => {
                                const user = context.getAdmin();

                                return (
                                    <ul>
                                        <AdminNavLink
                                            label={"Dashboard"}
                                            url={ADMIN_PATH}
                                            icon="dashboard"
                                        />
                                        {
                                            user?.isGranted(Role.ROLE_LOGISTICIAN) ||
                                            user?.isGranted(Role.ROLE_ACCOUNTANT) ?
                                                (
                                                    <>
                                                        <AdminNavLink
                                                            label={"Commandes"}
                                                            url={ADMIN_ORDER_PATH}
                                                            icon="order"
                                                        />
                                                        <AdminNavLink
                                                            label={"Réservations"}
                                                            url={ADMIN_RESERVATION_PATH}
                                                            icon="reservation"
                                                        />
                                                    </>
                                                )
                                                : null
                                        }
                                        <AdminNavLink
                                            label={"Produits"}
                                            url={'/admin/product'}
                                            icon="product"
                                        />
                                        {
                                            user?.isGranted(Role.ROLE_LOGISTICIAN) ||
                                            user?.isGranted(Role.ROLE_HR) ?
                                                (
                                                    <>
                                                        <AdminNavLink
                                                            label={"Clients"}
                                                            url={'/admin/customer'}
                                                            icon="user"
                                                        />
                                                        <AdminNavLink
                                                            label={"Employées"}
                                                            url={'/admin/employee'}
                                                            icon="user"
                                                        />
                                                    </>
                                                )
                                                : null
                                        }
                                        {/* {
                                            user?.isGranted(Role.ROLE_ADMIN) ?
                                                (
                                                    <AdminNavLink
                                                        label={"Catégories"}
                                                        url={'/admin/categorie'}
                                                        icon="category"
                                                    />
                                                ) : null
                                        } */}
                                        {
                                            user?.isGranted(Role.ROLE_ADMIN) ?
                                                (
                                                    <>
                                                        <AdminNavLink
                                                            label={"Restaurants"}
                                                            url={ADMIN_RESTAURANT_PATH}
                                                            icon="restaurant"
                                                        />
                                                        <AdminNavLink
                                                            label={"Pays"}
                                                            url={ADMIN_COUNTRY_PATH}
                                                            icon="country"
                                                        />
                                                        <AdminNavLink
                                                            label={"Devises"}
                                                            url={'/admin/currency'}
                                                            icon="currency"
                                                        />
                                                        <AdminNavLink
                                                            label={"Configuration"}
                                                            url={'/admin/settings'}
                                                            icon="configuration"
                                                        />
                                                    </>
                                                ) : null
                                        }
                                    </ul>
                                )
                            }
                        }
                    </CustomContext.Consumer>
                </nav>
            </div>
        </div>
    )
}

export default AdminNav;
