import React, {createRef, useEffect, useState} from 'react';
import CustomSelectArrow from "./CustomSelectArrow";
import CustomSelectValid from "./CustomSelectValid";

export interface CustomSelectOption {
    value: any,
    label: string,
    selected?: false
}

interface Props {
    hideImage?: boolean;
    options?: Array<CustomSelectOption>;
    title?: string;
    onChange?: Function
}

const CustomSelect = (props: Props) => {

    const [selectedOptions, setSelectedOptions] = useState(new Array<any>());

    const [showList, setShowList] = useState(false);

    const ref = createRef();

    useEffect(() => {

        const onClickOutside = (e: any) => {
            let current: any = ref.current;

            if (current && !current?.contains(e.target)) {
                setShowList(false);
            }
        };

        document.addEventListener('click', onClickOutside);

        return () => {
            document.removeEventListener('click', onClickOutside);
        }
    })

    return (
        <div
            ref={ref as any}
        >
            <div className="mt-1 relative">
                <button
                    type="button"
                    onClick={() => setShowList(!showList)}
                    className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-800 text-gray-500 aa-input"
                >
                    {props.title}
                    <CustomSelectArrow/>
                </button>

                {(showList) && (
                    <div
                        className="absolute mt-1 w-full z-90 rounded-md bg-neutral-100 dark:bg-neutral-800">
                        <ul
                            tabIndex={-1}
                            role="listbox"
                            aria-labelledby="listbox-label"
                            aria-activedescendant="listbox-item-3"
                            className="max-h-56 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm scrollbar scrollbar-w-2 scrollbar-thumb-neutral-400 scrollbar-track-neutral-100 dark:scrollbar-track-neutral-800"
                        >
                            {
                                props?.options?.map(
                                    (option: CustomSelectOption) =>

                                        <li
                                            key={option.value}
                                            role="option"
                                            className="cursor-default hover:bg-org-500 select-none relative py-2 pl-3 pr-9 font-light"
                                            onClick={
                                                () => {
                                                    let selectedOptionsSet = new Set<any>(selectedOptions);
                                                    let selectedValue: any = option.value;

                                                    if (selectedOptionsSet.has(selectedValue)) {
                                                        selectedOptionsSet.delete(selectedValue);
                                                    } else {
                                                        selectedOptionsSet.add(selectedValue);
                                                    }

                                                    let newSelectedOptions: Array<any> = Array.from(selectedOptionsSet);
                                                    setSelectedOptions(newSelectedOptions)

                                                    if (props.onChange) {
                                                        props.onChange(newSelectedOptions);
                                                    }
                                                }
                                            }
                                        >
                                            {option.label}
                                            {(selectedOptions.includes(option.value)) && <CustomSelectValid/>}
                                        </li>
                                )
                            }
                        </ul>
                    </div>
                )}
            </div>
        </div>
    );
};
export default CustomSelect;
