import React from "react";
import {Message} from "../../../../utils/middlewares/Logger/Message";

type Props = {
    error?: Message
}

export const InputError = ({error}: Props) => {

    return (
        <div className="text-sm text-red-400 mt-1 font-bold">{error?.message ?? ''}&nbsp;</div>
    )

}