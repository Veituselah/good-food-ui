import React from "react";
import {InputError} from "./InputError";
import {Message} from "../../../../utils/middlewares/Logger/Message";

type Props = {
    errors: Array<Message>,
    field: string
}

export const ErrorsForField = ({errors, field}: Props) => {

    let filtered = errors.filter(
        (inputError: Message) => inputError.field === field
    )

    return (
        <>
            {
                filtered.length > 0 ?
                    filtered.map(
                        (inputError: Message, index) =>
                            <InputError key={index + (inputError.field ?? '')} error={inputError}/>
                    )
                    :
                    <InputError/>
            }
        </>
    )
}