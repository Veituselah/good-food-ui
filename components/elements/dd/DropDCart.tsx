import React, {createRef, useEffect, useState} from 'react';
import {getCart} from "../../../redux/selectors/cart";
import {useSelector} from "react-redux";
import {OrderModel} from "../../../utils/models/ApiPlatform/Order/OrderModel";
import {CardCartLine} from "../cards/CardCartLine";
import Link from "next/link";
import {CART_PATH} from "../../../pages/cart";

export interface HeaderLink {
    label: string;
    link?: string;
    isSelected?: boolean;
    desc?: string;
    qte?: string;
    price?: string;
    currency?: string;
    icon?: JSX.Element;
}

const DropDCart = () => {

    const [isSectionOpen, setIsSectionOpen] = useState(false);

    const ref = createRef<any>();

    const cartState = useSelector(getCart);
    const cart = cartState?.cart;

    useEffect(() => {

        const onClickOutside = (e: any) => {
            let current: any = ref.current;
            if (current && !current?.contains(e.target)) {
                setIsSectionOpen(false);
            }
        };

        document.addEventListener('click', onClickOutside);

        return () => {
            document.removeEventListener('click', onClickOutside);
        }
    })

    return (
        <div className="md:relative"
             ref={ref}
        >

            <button type="button" onClick={() => setIsSectionOpen(!isSectionOpen)}
                    className="group text-neutral-700 dark:text-neutral-100 hover:text-org-500 dark:hover:text-gre-500 inline-flex items-center text-sm font-bold">
                <svg className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                          d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"/>
                </svg>
            </button>

            {isSectionOpen && (
                <div
                    className="absolute z-10 mt-4 right-0 transform px-2 w-screen rounded-md max-w-md sm:px-0 bg-white dark:bg-neutral-600">
                    <div
                        className="rounded-md shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden p-4 justify-center">
                        <h2 className="font-sans text-3xl font-bold">Mon panier</h2>
                        <p>{cart?.restaurant?.name ?? 'Veuillez sélectionner un restaurant'}</p>
                        <div className="relative grid gap-6 py-6 px-4 divide-y divide-neutral-400">

                            {cart?.orderLines?.map((orderLine: any) => {

                                return (
                                    <CardCartLine cart={cart} orderLine={orderLine} key={orderLine.id}/>
                                );

                            })}

                            <div className="relative grid pt-6 px-4 divide-y divide-neutral-400">
                                <div className="flex flex-row font-normal w-full">
                                    <p className="relative mt-0 w-2/3">Total panier</p>
                                    <p className="relative mt-0 w-1/3">{OrderModel.formatPrice(cart?.total ?? 0) + (cart?.currency?.symbol ?? '€')}</p>
                                </div>
                            </div>
                        </div>
                        <Link href={CART_PATH}>
                            <button
                                type={'button'}
                                className={'px-4 py-2 w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-md bg-gre-700 dark:bg-org-500 hover:bg-gre-600 dark:hover:bg-org-600 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 text-neutral-700'}
                            >
                                Accéder au panier
                            </button>
                        </Link>
                    </div>
                </div>
            )}
        </div>
    );
};
export default DropDCart;
