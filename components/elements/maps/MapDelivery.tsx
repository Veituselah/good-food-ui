import 'leaflet/dist/leaflet.css'
import CardRestaurant from '../cards/CardRestaurant'
import dynamic from "next/dynamic";
import {createRef, useEffect, useState} from "react";
import {MapContainer, Marker, Popup, TileLayer} from "react-leaflet";
import {RestaurantHandler} from "../../../utils/handlers/ApiPlatform/Restaurant/RestaurantHandler";
import {useDispatch} from "react-redux";
import {Message} from "../../../utils/middlewares/Logger/Message";
import {addLog} from "../../../redux/actions/logger";

const L = require('leaflet');

export const restaurantIcon = L.icon({
    iconUrl: '/leaf-red.png',
    shadowUrl: '/leaf-shadow.png',

    iconSize: [38, 95], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

export const userIcon = L.icon({
    iconUrl: '/leaf-orange.png',
    shadowUrl: '/leaf-shadow.png',

    iconSize: [38, 95], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});


export const getCurrentLocation = (setLocation: Function) => {
    if (typeof window !== 'undefined') {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                setLocation(
                    position.coords.longitude,
                    position.coords.latitude
                )
            },
            () => {
                fetch('https://ipinfo.io/json?token=ad744892fddc7b').then(
                    (reponse: any) => reponse.json()
                ).then((jsonResponse) => {
                    const loc = jsonResponse.loc;
                    const data = loc.split(',');
                    setLocation(
                        data[1],
                        data[0]
                    )
                }).catch((result: any) => {
                    console.error(result)
                })
            }
        );
    }
}


const MapDelivery = () => {
    const dispatch = useDispatch();
    const map: any = createRef();
    const [latitude, setLatitude] = useState<number>(47.311);
    const [longitude, setLongitude] = useState<number>(5.092);

    const [restaurants, setRestaurants] = useState<any>([]);

    const getLocation: any = () => [latitude, longitude];
    const location = getLocation();

    const zoom = 15;
    const setLocation = (longitude: number, latitude: number) => {
        setLongitude(longitude);
        setLatitude(latitude);
        map?.current?.setView([latitude, longitude], zoom)

        let restaurantHandler = new RestaurantHandler();
        restaurantHandler.getRestaurantAroundLocation(longitude, latitude).then((response) => {
            setRestaurants(response.data ?? []);
        }).catch(() => {
            let message: Message = {
                title: 'Impossible de charger les restaurants autour de vous',
                message: 'Veuillez réessayer ultérieurement'
            }

            dispatch(addLog(message));
        })
    }

    useEffect(() => {
        getCurrentLocation(setLocation);
    }, [])

    return (
        <div>
            <MapContainer className='right-0 z-10'
                          center={location}
                          zoom={zoom}
                          scrollWheelZoom={false}
                          style={
                              {height: "50vh", width: "100%"}
                          }
                          ref={map}
            >
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />

                <Marker position={getLocation()}
                        icon={userIcon}
                >
                    <Popup>Vous</Popup>
                </Marker>

                {restaurants.map((restaurant: any) => {
                    let position: any = [
                        restaurant?.restaurant?.latitude,
                        restaurant?.restaurant?.longitude
                    ];

                    return (
                        <Marker position={position}
                                key={restaurant?.restaurant?.slug}
                                icon={restaurantIcon}
                        >
                            <Popup>
                                {restaurant?.restaurant?.name}
                            </Popup>
                        </Marker>
                    )
                })}

            </MapContainer>
        </div>
    )
}

export default dynamic(() => Promise.resolve(MapDelivery), {
    loading: () => <p>La carte est en chargement, merci de patienter</p>,
    ssr: false
})
