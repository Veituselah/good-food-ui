import 'leaflet/dist/leaflet.css'
import CardRestaurant from '../cards/CardRestaurant'
import dynamic from "next/dynamic";
import {createRef, useEffect, useState} from "react";
import {MapContainer, Marker, Popup, TileLayer} from "react-leaflet";
import {RestaurantHandler} from "../../../utils/handlers/ApiPlatform/Restaurant/RestaurantHandler";
import {useDispatch} from "react-redux";
import {Message} from "../../../utils/middlewares/Logger/Message";
import {addLog} from "../../../redux/actions/logger";

const L = require('leaflet');

export const restaurantIcon = L.icon({
    iconUrl: '/leaf-red.png',
    shadowUrl: '/leaf-shadow.png',

    iconSize: [38, 95], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

export const userIcon = L.icon({
    iconUrl: '/leaf-orange.png',
    shadowUrl: '/leaf-shadow.png',

    iconSize: [38, 95], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});


export const getCurrentLocation = (setLocation: Function) => {
    if (typeof window !== 'undefined') {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                setLocation(
                    position.coords.longitude,
                    position.coords.latitude
                )
            },
            () => {
                fetch('https://ipinfo.io/json?token=ad744892fddc7b').then(
                    (reponse: any) => reponse.json()
                ).then((jsonResponse) => {
                    const loc = jsonResponse.loc;
                    const data = loc.split(',');
                    setLocation(
                        data[1],
                        data[0]
                    )
                }).catch((result: any) => {
                    console.error(result)
                })
            }
        );
    }
}


const Map = () => {
    const dispatch = useDispatch();
    const map: any = createRef();
    const [latitude, setLatitude] = useState<number>(47.311);
    const [longitude, setLongitude] = useState<number>(5.092);

    const [restaurants, setRestaurants] = useState<any>([]);

    const getLocation: any = () => [latitude, longitude];
    const location = getLocation();

    const zoom = 15;

    const [searchDone, setSearchDone] = useState(false);

    const setLocation = (longitude: number, latitude: number) => {
        setLongitude(longitude);
        setLatitude(latitude);
        map?.current?.setView([latitude, longitude], zoom)

        let restaurantHandler = new RestaurantHandler();
        restaurantHandler.getRestaurantAroundLocation(longitude, latitude).then((response) => {
            setRestaurants(response.data ?? []);
        }).catch(() => {
            let message: Message = {
                title: 'Impossible de charger les restaurants autour de vous',
                message: 'Veuillez réessayer ultérieurement'
            }

            dispatch(addLog(message));
        }).finally(() => {
            setSearchDone(true)
        })
    }

    useEffect(() => {
        getCurrentLocation(setLocation);
    }, [])

    return (
        <div className={"flex flex-col md:flex-row"}>
            <div
                className="w-100 md:w-1/3 shadow-md bg-white dark:bg-neutral-500 p-2 left-0 z-20 flex flex-col overflow-x-auto scrollbar scrollbar-w-2 scrollbar-thumb-neutral-400 scrollbar-track-neutral-100 dark:scrollbar-track-neutral-800"
                style={{maxHeight: '95vh'}}
            >
                <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight md:mx-auto">
                    <span className="relative inline-block mt-6">
                        <span className="relative">Les restaurants du coin !</span>
                    </span>
                </h2>
                {!restaurants || restaurants.length < 1 ?
                    <>
                        <span className={"mt-3"}>Chargement des restaurants en cours...</span>
                        <span className={"mt-3"}>
                            {searchDone ?
                                "Aucun restaurant n'a été trouvé, essayer de faire une recherche via le champ de recherche !" :
                                "Si cela met trop de temps ou qu'une erreur s'affiche veuillez recharger votre page."
                            }
                        </span>
                    </>
                    :
                    restaurants.map((restaurant: any, index: number) => {
                        return <CardRestaurant
                            key={restaurant?.restaurant?.slug}
                            restaurant={restaurant?.restaurant}
                            distance={restaurant?.distance}
                            defaultImage={index}
                        />
                    })
                }

            </div>
            <MapContainer className='right-0 z-10'
                          center={location}
                          zoom={zoom}
                          scrollWheelZoom={false}
                          style={
                              {height: "95vh", width: "100%"}
                          }
                          ref={map}
            >
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />

                <Marker position={getLocation()}
                        icon={userIcon}
                >
                    <Popup>Vous</Popup>
                </Marker>

                {restaurants.map((restaurant: any) => {
                    let position: any = [
                        restaurant?.restaurant?.latitude,
                        restaurant?.restaurant?.longitude
                    ];

                    return (
                        <Marker position={position}
                                key={restaurant?.restaurant?.slug}
                                icon={restaurantIcon}
                        >
                            <Popup>
                                {restaurant?.restaurant?.name}
                            </Popup>
                        </Marker>
                    )
                })}

            </MapContainer>
        </div>
    )
}

export default dynamic(() => Promise.resolve(Map), {
    loading: () => <p>La carte est en chargement, merci de patienter</p>,
    ssr: false
})
