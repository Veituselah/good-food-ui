import {MapContainer, Marker, Popup, TileLayer} from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import {getCurrentLocation, restaurantIcon, userIcon} from "./Map";
import {createRef, useEffect, useState} from "react";

const MapRestaurant = ({restaurant}: any) => {

    const [location, setLocation] = useState<any>([47.311, 5.092]);
    const map: any = createRef();
    const zoom = 12;

    const locateUser = (userLongitude: number, userLatitude: number) => {
        const location = [userLatitude, userLongitude];
        setLocation(location);
    }

    useEffect(() => {
        getCurrentLocation(locateUser)
    }, [])

    const restaurantLocation: any = [restaurant?.latitude, restaurant?.longitude];

    return (
        <MapContainer className='z-10'
                      center={restaurantLocation}
                      zoom={zoom}
                      scrollWheelZoom={false}
                      style={
                          {height: "50vh", width: "100%"}
                      }
                      ref={map}
        >
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={location} icon={userIcon}>
                <Popup>Vous</Popup>
            </Marker>
            <Marker position={restaurantLocation} icon={restaurantIcon}>
                <Popup>
                    {restaurant?.name}
                </Popup>
            </Marker>
        </MapContainer>
    )
}

export default MapRestaurant