import Link from 'next/link';
import React, {useState} from 'react';
import DropDCart from '../../elements/dd/DropDCart';
import ButtonThemeToggler from '../../elements/buttons/ButtonThemeToggler';
import FormSearchbar from '../../forms/FormSearchbar';
import ButtonLogin from '../../elements/buttons/ButtonLogin';
import Avatar from "../../elements/avatars/Avatar";
import ButtonLogout from "../../elements/buttons/ButtonLogout";
import {useSelector} from "react-redux";
import {getContext} from "../../../redux/selectors/context";
import {useRouter} from "next/router";
import {SEARCH_PATH} from "../../../pages/search/[search]";
import {SvgDots, SvgForgottenPassword, SvgLogin, SvgRegister} from "../../elements/icon/SvgItems";
import {getUserDialog} from "../../../redux/selectors/user_dialog";
import {UserDialogSection} from "../../../redux/reducers/user_dialog";
import FormLogin from "../../forms/FormLogin";
import FormRegister from "../../forms/FormRegister";
import FormLostPassword from "../../forms/FormLostPassword";
import {hideUserDialog, showUserDialogSection} from "../../../redux/actions/user_dialog";
import {useDispatch} from "react-redux";

const AppHeader = () => {

        const contextState = useSelector(getContext);
        const [isMenuOpen, setIsMenuOpen] = useState(false);

        const dispatch = useDispatch();
        const userDialog = useSelector(getUserDialog);

        interface TabsInterface {
            id: UserDialogSection,
            tabTitle: string,
            title: string,
            icon: any,
            content: any
        }

        const tabs: Array<TabsInterface> = [
            {
                id: UserDialogSection.LOGIN,
                tabTitle: 'Connexion',
                title: 'Connexion',
                icon: <SvgLogin/>,
                content: <FormLogin/>
            },
            {
                id: UserDialogSection.REGISTER,
                tabTitle: 'Inscription',
                title: 'Inscription',
                icon: <SvgRegister/>,
                content: <FormRegister/>
            },
            {
                id: UserDialogSection.LOST_PASSWORD,
                tabTitle: 'Mot de passe oublié',
                title: 'Mot de passe oublié',
                icon: <SvgForgottenPassword/>,
                content: <FormLostPassword/>
            },
        ];


        const setDialogSection = (section: UserDialogSection) => {
            dispatch(showUserDialogSection(section));
        }

        const hideDialog = () => {
            dispatch(hideUserDialog());
        }

        const router = useRouter();

        const search = (search: any) => {
            router.push(SEARCH_PATH + search);
        }

        return (
            <header
                className="w-full shadow bg-white dark:bg-neutral-800 items-center rounded-b-2xl z-40 sticky top-0 border-b-2 border-org-500 dark:border-gre-500">
                <div className="relative z-20 flex flex-col justify-center h-full px-3 mx-auto flex-center">
                    <div className="relative items-center pl-1 flex w-full lg:max-w-68 sm:pr-2 sm:ml-0">
                        <div className="container items-center relative left-0 flex w-2/3 h-auto h-full">
                            <Link href="/">
                                <a className="flex items-center pr-4">
                                    <img className="h-8 w-auto sm:h-12 hidden dark:block" src="/icons/GoodFoodD.png" alt="site"/>
                                    <img className="h-8 w-auto sm:h-12 block dark:hidden" src="/icons/GoodFoodL.png" alt="site"/>
                                </a>
                            </Link>
                            <FormSearchbar onSearch={search} placeholder={"Je recherche un restaurant (par nom ou ville)"}/>
                        </div>
                        <div className="relative p-1 flex items-center justify-end w-1/3 ml-5 mr-4 sm:mr-0 sm:right-auto">
                            <nav className="hidden xl:flex space-x-4 p-2 pr-4 items-center">
                                {/*<DropD label="Devises" links={menuCurrency}/>*/}
                                <ButtonThemeToggler/>
                                {contextState?.customer ?
                                    <>
                                        <DropDCart/>
                                        <Link href={'/account'}>
                                            <div className={"mr-4"}>
                                                <Avatar size="small"/>
                                            </div>
                                        </Link>
                                        <ButtonLogout/>
                                    </> :
                                    (contextState?.admin ?
                                            <>
                                                <Link href={'/admin'}>
                                                    <div className={"mr-4"}>
                                                        <Avatar size="small"/>
                                                    </div>
                                                </Link>
                                                <ButtonLogout/>
                                            </> :
                                            <ButtonLogin/>
                                    )
                                }
                            </nav>
                            <div className="-mr-2 -my-2 xl:hidden p-4">
                                <button type="button" onClick={() => setIsMenuOpen(true)}
                                        className="bg-white dark:bg-neutral-600 rounded-md p-2 inline-flex items-center justify-center text-gray-400 dark:text-gray-50 hover:text-gray-500 dark:hover:text-white hover:bg-neutral-100 dark:hover:bg-neutral-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                                    <span className="sr-only">Ouvrir le menu</span>
                                    <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                              d="M4 6h16M4 12h16M4 18h16"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {isMenuOpen && (
                    <div className="absolute top-0 z-20 inset-x-0 transition transform origin-top-right xl:hidden">
                        <div
                            className="w-full shadow bg-white dark:bg-neutral-800 items-center rounded-b-2xl z-40 top-0 border-b-2 border-org-500 dark:border-gre-500">
                            <div className="px-5">
                                <div className="flex items-center justify-between">
                                    <Link href="/">
                                        <a className="flex items-center pr-4">
                                            <img className="h-8 w-auto sm:h-12 hidden dark:block" src="/icons/GoodFoodD.png"
                                                 alt="site"/>
                                            <img className="h-8 w-auto sm:h-12 block dark:hidden" src="/icons/GoodFoodL.png"
                                                 alt="site"/>
                                        </a>
                                    </Link>

                                    <div className={"flex flex-col"}>

                                        <div className={"flex justify-evenly items-center my-2"}>

                                            <ButtonThemeToggler/>

                                            {
                                                contextState?.customer &&
                                                <>
                                                    <DropDCart/>
                                                    <Link href={'/account'}>
                                                        <div>
                                                            <Avatar size="small"/>
                                                        </div>
                                                    </Link>
                                                </>
                                            }

                                        </div>

                                        <div className={"my-2"}>
                                            {
                                                contextState?.customer ?
                                                    <ButtonLogout/> :
                                                    <ButtonLogin/>
                                            }
                                        </div>

                                    </div>

                                    <div className="-my-2 p-4">
                                        <button type="button" onClick={() => setIsMenuOpen(false)}
                                                className="bg-white dark:bg-neutral-600 rounded-md p-2 inline-flex items-center justify-center text-gray-400 dark:text-gray-50 hover:text-gray-500 dark:hover:text-white hover:bg-neutral-100 dark:hover:bg-neutral-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                                            <span className="sr-only">Fermer le menu</span>
                                            <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                                 viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                                      d="M6 18L18 6M6 6l12 12"/>
                                            </svg>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                )}
                {userDialog.show ? (
                        <div
                            className={"bottom-0 items-center justify-center fixed flex h-full left-0 right-0 top-0 w-full z-50 bg-black bg-opacity-50 inset-0"}>
                            <div
                                className="
                        flex focus:outline-none items-center justify-center outline-none w-full md:w-3/5 xl:w-2/5
                         rounded-lg overflow-auto shadow-lg flex-col bg-neutral-50 dark:bg-neutral-800 outline-none focus:outline-none"
                            >
                                {/*tabcontent*/}
                                <div className=" content border-b border-solid border-org-500 dark:border-gre-500 pb-6 w-full">
                                    {/*tabtitle*/}
                                    {tabs.map((tab, i) => {
                                        return (
                                            <div key={i}>
                                                {
                                                    userDialog.section === tab.id &&
                                                    <>
                                                        <div
                                                            className=" flex items-start justify-between p-5 border-b
                        border-solid border-org-500 dark:border-gre-500 rounded-t w-full">
                                                            <h2 className=" mt-12 max-w-lg font-sans text-3xl font-bold
                        leading-none tracking-tight sm:text-4xl md:mx-auto">
                                                        <span className=" relative inline-block">
                                                            <SvgDots/>
                                                            <span className=" title relative">{tab.title}</span>
                                                        </span>
                                                            </h2>
                                                            <button
                                                                className=" p-1 ml-auto border-0 text-org-500 opacity-75
                        float-right text-3xl leading-none font-bold outline-none focus:outline-none hover:text-gre-700"
                                                                onClick={hideDialog}
                                                            >
                                                                x
                                                                {/* <span className=" bg-org-500 text-black opacity-5 h-6 w-6
                        text-2xl block outline-none focus:outline-none">
                                                            x
                                                        </span> */}
                                                            </button>
                                                        </div>
                                                        <div className={'max-h-96 overflow-auto p-8'}>
                                                            {/*tabbody*/}
                                                            {tab.content}
                                                        </div>
                                                    </>
                                                }
                                            </div>
                                        )
                                    })}
                                </div>
                                {/*tabactions*/}
                                <div className=" tabsw w-full">
                                    {tabs.map((tab: any, i: number) =>
                                        <button key={i} id={tab.id} disabled={userDialog.section === tab.id}
                                                onClick={() => setDialogSection(tab.id)}
                                                className=" pt-6 pb-6 flex-wrap flex-row w-1/3 justify-center inline-flex items-center">{tab.icon}{tab.tabTitle}</button>
                                    )}
                                </div>
                            </div>
                        </div>
                    ) :
                    null
                }
            </header>
        )
            ;
    }
;

export default AppHeader;
