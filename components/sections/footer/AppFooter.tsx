import React from 'react'
import FooterCopyright from './FooterCopyright'
import FooterAddress from './FooterAddress'
import FooterApp from './FooterApp'
import FooterLinks from './FooterLinks'

export const footerLink = [
    {
        label: 'F.A.Q',
        link: '/faq',
    },
    {
        label: 'Confidentialité',
        link: '/privacy',
    },
    {
        label: "Conditions d'utilisation",
        link: '/terms',
    },
    {
        label: 'Nous contacter',
        link: '/contact',
    },
];


const AppFooter = () => {
    return (
        <footer className="bg-white dark:bg-neutral-500 rounded-t-2xl border-t-2 border-org-500 dark:border-gre-500">
            <div className="px-4 pt-8 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8">
                <div className="grid gap-10 row-gap-6 mb-4 sm:grid-cols-2 lg:grid-cols-4">
                    <FooterApp />
                    <FooterAddress />
                    <FooterLinks links={footerLink} />
                </div>
            </div>
            <FooterCopyright />
        </footer>
    );
};

export default AppFooter;
