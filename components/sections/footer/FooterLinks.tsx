import React from 'react';
import Link from 'next/link';

interface Props {
    links: FooterLink[];
    showSubLinks?: boolean;
}
interface FooterLink {
    label: string;
    link?: string;
}

const FooterLinks = (props: Props) => {
    return (
        <div className="space-y-2">
            <span className="text-base font-bold tracking-wide">Liens utiles</span>
            <div className="flex items-center mt-1 space-x-3">
                <nav className="flex flex-col mb-3 space-y-2 lg:mb-0 text-sm text-neutral-700 dark:text-neutral-100 transition-colors duration-300">
                    {props.links.map((link) => {
                        return (
                            <a
                                key={link.label}
                                aria-current="page"
                                href={link.link || '#'}
                                className="hover:text-org-500 dark:hover:text-gre-500 hover:underline"
                            >
                                {link.label}
                            </a>
                        );
                    })}
                </nav>
            </div>
        </div>
    );
};

export default FooterLinks;
