import React from 'react';
import ButtonSocial from '../../elements/buttons/ButtonSocial'

const FooterCopyright = () => {
    return (
        <div className="border-t border-org-500 dark:border-gre-700 bg-neutral-100 dark:bg-neutral-800">
            <div className="flex flex-col-reverse lg:flex-row justify-between pt-5 pb-5 px-4 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8">
                <p className="text-sm">
                    © Copyright 2022 Good Food réalisé par {' '}                       
                    <a href="https://phoenixia-prods.com" target="_blank" className="font-bold transition-colors duration-300 hover:text-org-500 dark:hover:text-gre-500 hover:underline">
                        Phoenixia Productions
                    </a>
                    . Tous droits réservés.
                </p>
                <ButtonSocial/>
            </div>
        </div>
    );
};

export default FooterCopyright;
