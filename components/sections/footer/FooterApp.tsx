import React from 'react';
import ButtonAppStore from '../../elements/buttons/ButtonAppStore';
import ButtonGooglePlay from '../../elements/buttons/ButtonGooglePlay';

const FooterApp = () => {
    return (
        <div className="sm:col-span-2">
            <div className="inline-flex items-center">
            <a href="/" aria-label="Retour à l'accueil" title="Good Food" className="flex w-1/5 mr-4">
                <img className="h-8 w-auto sm:h-12 hidden dark:block" src="/icons/GoodFoodD.png" alt="site"/>
                <img className="h-8 w-auto sm:h-12 block dark:hidden" src="/icons/GoodFoodL.png" alt="site"/>
            </a>
            <p className="text-sm text-justify text-neutral-700 dark:text-neutral-100">
                    Profitez de la saveur de nos régions directement depuis chez vous et en toute simplicité grâce à notre application mobile !
            </p>
            </div>
                <div className="flex-col sm:flex-row text-center pt-4 sm:pt-4 font-light flex items-center">
                    <ButtonGooglePlay />
                    <ButtonAppStore />
                </div>
        </div>
    );
};

export default FooterApp;
