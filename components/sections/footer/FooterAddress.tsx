import React from 'react';

const FooterAddress = () => {
    return (
        <div className="space-y-2 text-sm">
            <p className="text-base font-bold tracking-wide">Contact</p>
            <div className="flex">
                <p className="mr-1">Téléphone :</p>
                <a
                    href="tel:850-123-5021"
                    aria-label="Our phone"
                    title="Our phone"
                    className="transition-colors duration-300 hover:text-org-500 dark:hover:text-gre-500"
                >
                    +33 642 780 170
                </a>
            </div>
            <div className="flex">
                <p className="mr-1">Mail :</p>
                <a
                    href="mailto:contact@good-food.com"
                    aria-label="Our email"
                    title="Our email"
                    className="transition-colors duration-300 hover:text-org-500 dark:hover:text-gre-500"
                >
                    contact@good-food.com
                </a>
            </div>
            <div className="flex">
                <p className="mr-1">Adresse :</p>
                <a
                    href="https://www.google.com/maps"
                    target="_blank"
                    rel="noopener noreferrer"
                    aria-label="Our address"
                    title="Our address"
                    className="transition-colors duration-300 hover:text-org-500 dark:hover:text-gre-500"
                >
                    312 Lovely Street, NY
                </a>
            </div>
        </div>
    );
};

export default FooterAddress;
