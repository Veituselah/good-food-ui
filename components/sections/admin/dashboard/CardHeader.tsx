import React from "react";

const CardHeader = ({title = ''}) => {
    return (
        <div className="flex items-center justify-between mb-6">
            <div className="flex items-center">
                <div className="flex flex-col">
                    <span className="font-bold text-md ml-2">{title}</span>
                </div>
            </div>
        </div>
    )
}

export default CardHeader;