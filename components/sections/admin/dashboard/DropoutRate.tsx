import {Bar, BarChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import CardHeader from "./CardHeader";

export const fakeData = [
    {month: 'Jan. 2021', dropoutRate: 81.32,},
    {month: 'Jui. 2021', dropoutRate: 79.28},
    {month: 'Jan. 2022', dropoutRate: 84.86,},
    {month: 'Jui. 2022', dropoutRate: 76.65},
]

type Props = {
    data: any
}

export const DropoutRate = ({data}: Props) => {

    return (

        <div className="shadow-lg rounded-2xl p-4 bg-white dark:bg-neutral-600 w-full">
            <CardHeader title={"Taux d'abandon"}/>

            <ResponsiveContainer height={200}>
                <BarChart data={data}>
                    <Bar
                        dataKey="dropoutRate"
                        fill="#FF9B00"
                        strokeWidth={2}
                        unit={"%"}
                    />
                    <Tooltip/>
                    <XAxis dataKey="month"/>
                    <YAxis dataKey="dropoutRate"/>
                </BarChart>
            </ResponsiveContainer>
        </div>
    )

}