import {Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import CardHeader from "./CardHeader";

export const fakeData = [
    {year: '2019', turnover: 1456012},
    {year: '2020', turnover: 1568792},
    {year: '2021', turnover: 1684585},
    {year: '2022', turnover: 1254862},
]

type Props = {
    data: any
}

export const Turnover = ({data}: Props) => {

    return (

        <div className="shadow-lg rounded-2xl p-4 bg-white dark:bg-neutral-600 w-full">
            <CardHeader title={"Chiffre d'affaires"}/>

            <ResponsiveContainer height={200}>
                <LineChart
                    data={data}
                    margin={{top: 0, right: 5, left: 5, bottom: 0}}
                >
                    <Line
                        type="monotone"
                        dataKey="turnover"
                        stroke="#6ADB00"
                        strokeWidth={2}
                        name="Chiffre d'affaires"
                        unit={"€"}
                    />
                    <Tooltip/>
                    <XAxis dataKey={"year"}/>
                    <YAxis/>
                </LineChart>
            </ResponsiveContainer>
        </div>
    )
}