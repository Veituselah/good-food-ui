import {Bar, BarChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import CardHeader from "./CardHeader";

export const fakeData = [
    {month: 'Jan. 2021', conversionRate: 15.68,},
    {month: 'Jui. 2021', conversionRate: 17.62},
    {month: 'Jan. 2022', conversionRate: 14.14,},
    {month: 'Jui. 2022', conversionRate: 19.35},
]

type Props = {
    data: any
}

export const ConversionRate = ({data}: Props) => {

    return (
        <div className="shadow-lg rounded-2xl p-4 bg-white dark:bg-neutral-600 w-full">
            <CardHeader title={"Taux de conversion"}/>

            <ResponsiveContainer height={200}>
                <BarChart data={data}>
                    <Bar
                        dataKey="conversionRate"
                        fill="#6ADB00"
                        strokeWidth={2}
                        unit={"%"}
                    />
                    <Tooltip/>
                    <XAxis dataKey="month"/>
                    <YAxis dataKey="conversionRate"/>
                </BarChart>
            </ResponsiveContainer>
        </div>
    )

}