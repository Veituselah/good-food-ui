import {Bar, CartesianGrid, ComposedChart, Line, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import CardHeader from "./CardHeader";

export const fakeData = [
    {year: '2019', acquisitionCost: 35.12, newClientNumber: 725},
    {year: '2020', acquisitionCost: 48.57, newClientNumber: 864},
    {year: '2021', acquisitionCost: 51.19, newClientNumber: 987},
    {year: '2022', acquisitionCost: 89.68, newClientNumber: 563},
]

type Props = {
    data: any
}

export const AcquisitionCost = ({data}: Props) => {

    return (
        <div className="shadow-lg rounded-2xl p-4 bg-white dark:bg-neutral-600 w-full">
            <CardHeader title={"Coût d'acquisition"}/>
            <ResponsiveContainer height={200}>
                <ComposedChart data={data}>
                    <CartesianGrid stroke="#858585"/>
                    <Bar
                        dataKey="newClientNumber"
                        barSize={16}
                        fill="#6ADB00"
                        name={"Nombre de nouveaux clients"}
                    />
                    <Line
                        type="monotone"
                        dataKey="acquisitionCost"
                        strokeWidth="3"
                        stroke="#FF9B00"
                        unit={"€"}
                        name="Prix d'acquisition d'un client"
                    />
                    <XAxis dataKey={"year"}/>
                    <YAxis/>
                    <Tooltip/>
                </ComposedChart>
            </ResponsiveContainer>
        </div>
    )

}