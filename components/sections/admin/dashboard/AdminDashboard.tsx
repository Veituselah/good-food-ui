import React from "react";
import {BounceRate, fakeData as bounceRateFakeData} from "./BounceRate";
import {ConversionRate, fakeData as conversionRateFakeData} from "./ConversionRate";
import {AcquisitionCost, fakeData as acquisitionCostFakeData} from "./AcquisitionCost";
import {DropoutRate, fakeData as dropoutRateFakeData} from "./DropoutRate";
import {MeanPurchase, fakeData as meanCartFakeData} from "./MeanPurchase";
import {NumberOfOrder, fakeData as numberOfOrderFakeData} from "./NumberOfOrder";
import {Turnover, fakeData as turnoverFakeData} from "./Turnover";

const AdminDashboard = () => {

    return (
        <div className="overflow-auto pb-24 pt-2 pr-2 pl-2 md:pt-0 md:pr-0 md:pl-0">
            <div className="flex flex-col flex-wrap sm:flex-row ">
                <div className="w-full">
                    <div className="mb-4">
                        <NumberOfOrder data={numberOfOrderFakeData}/>
                    </div>
                </div>

                <div className="w-full sm:w-1/2 xl:w-1/3">
                    <div className="mb-4">
                        <BounceRate data={bounceRateFakeData}/>
                    </div>
                    <div className="mb-4">
                        <AcquisitionCost data={acquisitionCostFakeData}/>
                    </div>
                </div>

                <div className="w-full sm:w-1/2 xl:w-1/3">
                    <div className="mb-4 mx-0 sm:ml-4 xl:mr-4">
                        <ConversionRate data={conversionRateFakeData}/>
                    </div>
                    <div className="mb-4 sm:ml-4 xl:mr-4">
                        <DropoutRate data={dropoutRateFakeData}/>
                    </div>
                </div>
                <div className="w-full sm:w-1/2 xl:w-1/3">
                    <div className="mb-4">
                        <MeanPurchase data={meanCartFakeData}/>
                    </div>
                    <div className="mb-4">
                        <Turnover data={turnoverFakeData}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AdminDashboard;