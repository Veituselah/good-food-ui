import {CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import CardHeader from "./CardHeader";

export const fakeData = [
    {month: 'Jan.', nbOrder: 114862, nbOrderLastYear: 65123},
    {month: 'Fév.', nbOrder: 156320, nbOrderLastYear: 78452},
    {month: 'Mars', nbOrder: 82153, nbOrderLastYear: 42362},
    {month: 'Avr.', nbOrder: 124305, nbOrderLastYear: 82125},
    {month: 'Mai', nbOrder: 185320, nbOrderLastYear: 14236},
    {month: 'Juin', nbOrder: 321582, nbOrderLastYear: 24685},
    {month: 'Jui.', nbOrder: 251480, nbOrderLastYear: 36548},
    {month: 'Août', nbOrder: 246895, nbOrderLastYear: 67895},
    {month: 'Sep.', nbOrderLastYear: 84521},
    {month: 'Oct.', nbOrderLastYear: 78565},
    {month: 'Nov.', nbOrderLastYear: 67542},
    {month: 'Déc.', nbOrderLastYear: 95842},
]

type Props = {
    data: any
}

export const NumberOfOrder = ({data}: Props) => {

    return (

        <div className="shadow-lg rounded-2xl p-4 bg-white dark:bg-neutral-600 w-full">
            <CardHeader title={"Commandes sur les 12 derniers mois"}/>

            <ResponsiveContainer height={300}>
                <LineChart
                    data={data}
                    margin={{top: 10, right: 10, left: 10, bottom: 10}}
                >
                    <Line
                        type="monotone"
                        dataKey="nbOrder"
                        stroke="#6ADB00"
                        strokeWidth={3}
                        name="Nombre de commandes (Année en cours)"
                    />
                    <Line
                        type="monotone"
                        dataKey="nbOrderLastYear"
                        stroke="#FF9B00"
                        strokeWidth={3}
                        name="Nombre de commandes (N-1)"
                    />
                    <Tooltip/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="month"/>
                    <YAxis dataKey="nbOrder"/>
                </LineChart>
            </ResponsiveContainer>
        </div>
    )

}