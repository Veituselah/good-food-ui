import {Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import CardHeader from "./CardHeader";

export const fakeData = [
    {month: 'Jan.', meanOrder: 29.45, meanCart: 34.75,},
    {month: 'Fév.', meanOrder: 27.75, meanCart: 25.15,},
    {month: 'Mars', meanOrder: 30.30, meanCart: 27.30,},
    {month: 'Avr.', meanOrder: 24.45, meanCart: 29,},
    {month: 'Mai', meanOrder: 29.25, meanCart: 26.25},
    {month: 'Jui.', meanOrder: 19.85, meanCart: 22.25},
    {month: 'Août', meanOrder: 32.55, meanCart: 35.45},
]

type Props = {
    data: any
}

export const MeanPurchase = ({data}: Props) => {

    return (

        <div className="shadow-lg rounded-2xl p-4 bg-white dark:bg-neutral-600 w-full">
            <CardHeader title={"Achats moyens"}/>

            <ResponsiveContainer height={200}>
                <LineChart
                    data={data}
                    margin={{top: 0, right: 5, left: 5, bottom: 0}}
                >
                    <Line
                        type="monotone"
                        dataKey="meanCart"
                        stroke="#6ADB00"
                        strokeWidth={2}
                        name="Panier moyen"
                        unit={"€"}
                    />
                    <Line
                        type="monotone"
                        dataKey="meanOrder"
                        stroke="#FF9B00"
                        strokeWidth={2}
                        unit={"€"}
                        name={"Commande moyenne"}
                    />
                    <Tooltip/>
                    <XAxis dataKey={"month"}/>
                    <YAxis/>
                </LineChart>
            </ResponsiveContainer>
        </div>
    )
}