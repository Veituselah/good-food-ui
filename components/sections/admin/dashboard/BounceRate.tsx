import {Area, AreaChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import CardHeader from "./CardHeader";

export const fakeData = [
    {month: 'Jan.', bounceRate: 74,},
    {month: 'Fév.', bounceRate: 69,},
    {month: 'Mars', bounceRate: 51,},
    {month: 'Avr.', bounceRate: 65,},
    {month: 'Mai', bounceRate: 80},
    {month: 'Juin', bounceRate: 70},
    {month: 'Jui.', bounceRate: 65},
    {month: 'Août', bounceRate: 35},
]

type Props = {
    data: any
}

export const BounceRate = ({data}: Props) => {

    return (
        <div className="shadow-lg rounded-2xl p-4 bg-white dark:bg-neutral-600 w-full">
            <CardHeader title={"Taux de rebond"}/>
            <ResponsiveContainer height={200}>
                <AreaChart
                    data={data}
                    margin={{top: 10, right: 10, left: 0, bottom: 10}}
                >
                    <defs>
                        <linearGradient
                            id="colorPv2"
                            x1="0"
                            y1="0"
                            x2="0"
                            y2="1"
                        >
                            <stop
                                offset="10%"
                                stopColor="var(--warning)"
                                stopOpacity={0.7}
                            />
                            <stop
                                offset="90%"
                                stopColor="var(--warning)"
                                stopOpacity={0}
                            />
                        </linearGradient>
                    </defs>
                    <Tooltip/>
                    <Area
                        type="monotoneX"
                        dataKey="bounceRate"
                        stroke="var(--warning)"
                        strokeWidth={2}
                        fillOpacity={1}
                        fill="url(#colorPv2)"
                        name="Taux de rebond"
                        unit="%"
                        />
                    <XAxis dataKey={"month"}/>
                    <YAxis/>
                </AreaChart>
            </ResponsiveContainer>
        </div>
    )

}