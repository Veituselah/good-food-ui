import React from "react";
import AdminDropdown from "../../../elements/navigation/admin/AdminDropdown";
import ButtonThemeToggler from '../../../elements/buttons/ButtonThemeToggler';
import DropDownMenu from "../../../elements/ddm/DropDownMenu";
import SVGIcon from "../../../elements/icon/SVGIcon";
import Link from "next/link";
import {Context} from "../../../../internal";
import {useRouter} from "next/router";
import {HOME_PATH} from "../../../../pages";
import {useDispatch} from "react-redux";
import {logoutAdmin} from "../../../../redux/actions/context";

export interface AdminHeaderAction {
    name: string;
    icon: JSX.Element;
    link?: string;
    onClick?: any;
}

interface Props {
    actions: AdminHeaderAction[];
}

const AdminHeader = (props: Props) => {

    let router = useRouter();
    const dispatch = useDispatch();

    const menuItems = [
        {
            // icon?: JSX.Element;
            label: "Mon compte",
            // desc?: string;
            link: "/admin/account"
        },
        {
            // icon?: JSX.Element;
            label: "Déconnexion",
            // desc?: string;
            click: () => {
                const context = new Context(false);
                dispatch(logoutAdmin(context))
                router.push(HOME_PATH);
            },
        }
    ]

    return (
        <header className="w-full shadow-lg bg-white dark:bg-neutral-600 items-center rounded-2xl z-40">
            <div className="relative z-20 flex flex-col justify-center h-full px-3 mx-auto flex-center">
                <div className="relative items-center pl-1 flex w-full lg:max-w-68 sm:pr-2 sm:ml-0">
                    <div className="container relative left-0  flex w-3/4 h-auto h-full">
                        {props.actions.map((action) => (
                                <div key={action.name}
                                     className="mr-1 cursor-pointer flex items-center font-thin uppercase text-gray-500 dark:text-gray-200 flex items-center transition-colors duration-200 justify-start hover:text-blue-500">
                                    {
                                        action.link ?
                                            (
                                                <Link href={action.link} key={action.name}>
                                                    <a>{action.icon}</a>
                                                </Link>
                                            ) :
                                            (
                                                <div key={action.name} onClick={action.onClick} className="cursor-pointer">
                                                    {action.icon}
                                                </div>
                                            )
                                    }
                                </div>
                            )
                        )}
                    </div>
                    <div className="relative p-1 flex items-center justify-end w-1/4 ml-5 mr-4 sm:mr-0 sm:right-auto">
                        <ButtonThemeToggler/>
                        <DropDownMenu items={menuItems} icon={
                            <SVGIcon icon={"account"} height={"25px"}/>
                        }/>
                    </div>

                    <div>
                        <AdminDropdown/>
                    </div>

                </div>
            </div>
        </header>
    )
}

export default AdminHeader;