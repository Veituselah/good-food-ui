import React from 'react';
import CalculatedStars from '../../elements/calculated/CalculatedStars';
import FormReview from '../../forms/FormReview';
import {useSelector} from "react-redux";
import {getComments} from "../../../redux/selectors/comments";

const AppRestaurantReview = ({restaurant}: any) => {

    const commentsSelector = useSelector(getComments);

    return (
        <section className="flex flex-wrap flex-row p-4">
            <div className="flex flex-wrap flex-col md:w-1/2">
                <h5 className="flex flex-row justify-center items-center text-xl font-semibold text-center mb-10 md:mb-6">
                    {/*<span className="ml-2 mr-4">3 Commentaires</span>*/}
                    <CalculatedStars note={restaurant.meanNote}/>
                    <span className="ml-2">{commentsSelector?.comments?.length ?? '0'} Avis</span>
                </h5>
                <div
                    className="flex flex-col flex-wrap h-screen/3 overflow-y-auto scrollbar scrollbar-w-2 scrollbar-thumb-neutral-400 scrollbar-track-neutral-100 dark:scrollbar-track-neutral-800">
                    <div className="flex flex-col grow-0 shrink-0 basis-auto mb-4">
                        <div className="flex flex-wrap items-center grow-0 shrink-0 basis-auto md:pl-6">
                            <p className="text-sm text-neutral-400 mr-2">Pseudo utilisateur</p>
                            <CalculatedStars/>
                            <p className="font-semibold ml-2">Super restau, livraison rapide</p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio est ab iure
                                inventore dolorum consectetur? Molestiae aperiam atque quasi consequatur aut?
                                Repellendus alias dolor ad nam, soluta distinctio quis accusantium!
                            </p>
                        </div>
                    </div>
                    {commentsSelector?.comments?.map((comment: any) => (
                        <div className="flex flex-col grow-0 shrink-0 basis-auto mb-4" key={comment?.id}>
                            <div className="flex flex-wrap items-center grow-0 shrink-0 basis-auto md:pl-6">
                                <p className="text-sm text-neutral-300 mr-2">{comment?.customer?.lastname} {comment?.customer?.firstname}</p>
                                <CalculatedStars note={comment?.note}/>
                                <p className="font-semibold ml-2"/>
                            </div>
                            <div className="flex flex-wrap items-center grow-0 shrink-0 basis-auto  md:pl-6 mb-4">
                                {comment?.message}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            {/* RightSection */}
            <div className="flex flex-col md:ml-auto w-full md:mt-0 md:w-1/2 pl-1">
                <FormReview restaurant={restaurant}/>
            </div>
        </section>
    );
};

export default AppRestaurantReview;