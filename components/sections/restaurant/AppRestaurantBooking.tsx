import React from 'react';
import FormReservation from "../../forms/FormReservation";

const AppRestaurantReview = ({restaurant}: any) => {

    return (
        <section className="flex flex-wrap flex-row p-4 w-full flex-col items-center">
            <div className="flex flex-wrap flex-col">
                <h5 className="flex flex-row justify-center items-center text-xl font-semibold text-center mb-10 md:mb-6">
                    Nous avons de la place pour vous !
                </h5>
            </div>
            {/* RightSection */}
            <div className="flex flex-col md:ml-auto w-full md:mt-0 pl-1">
                <FormReservation restaurant={restaurant}/>
            </div>
        </section>
    );
};

export default AppRestaurantReview;