import React from 'react';
import {SvgAbout} from '../../elements/icon/SvgItems';

const AppRestaurantAbout = ({restaurant}: any) => {

    return (
        <section className="body-font">
            <div className="container px-5 mx-auto flex flex-wrap">
                <div className="flex flex-wrap w-full">
                    <div className="text-org-500 dark:text-gre-500 lg:w-2/5">
                        <SvgAbout/>
                    </div>
                    <div className="lg:w-3/5 md:pr-10 md:py-6">
                        <div className="flex relative pb-12">
                            <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                                <div className="h-full w-1 bg-gre-400 dark:bg-org-200 pointer-events-none"/>
                            </div>
                            <div className="flex-shrink-0 w-10 h-10 rounded-full bg-gre-700 dark:bg-org-500 inline-flex items-center justify-center text-white relative z-10">
                                <svg fill="none" stroke="currentColor" strokeLinecap="round"
                                     strokeLinejoin="round" strokeWidth="2" className="w-5 h-5"
                                     viewBox="0 0 24 24">
                                    <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"/>
                                </svg>
                            </div>
                            <div className="flex-grow pl-4">
                                <h2 className="font-medium title-font text-sm mb-1 tracking-wider">NOUS
                                    CONTACTER</h2>
                                <p className="leading-relaxed">Nous sommes joignables pendant nos périodes
                                    d'ouverture au {restaurant?.phoneNumber}.</p>
                            </div>
                        </div>
                        <div className="flex relative pb-12">
                            <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                                <div className="h-full w-1 bg-gre-500 dark:bg-org-300 pointer-events-none"/>
                            </div>
                            <div className="flex-shrink-0 w-10 h-10 rounded-full bg-gre-700 dark:bg-org-500 inline-flex items-center justify-center text-white relative z-10">
                                <svg fill="none" stroke="currentColor" strokeLinecap="round"
                                     strokeLinejoin="round" strokeWidth="2" className="w-5 h-5"
                                     viewBox="0 0 24 24">
                                    <path d="M22 12h-4l-3 9L9 3l-3 9H2"/>
                                </svg>
                            </div>
                            <div className="flex-grow pl-4">
                                <h2 className="font-medium title-font text-sm mb-1 tracking-wider">NOS
                                    HORAIRES</h2>
                                {/*<p className="leading-relaxed">Nous sommes ouverts du Mardi au Samedi de*/}
                                {/*    11:30 - 15:00 / 18:00 - 22:30 et le Dimanche de 12:00 - 15:00 / 19:00 - 22:30.</p>*/}
                                <p className="leading-relaxed">Nous sommes ouverts du Mardi au Dimanche de
                                    11:30 - 15:00 / 18:00 - 22:30</p>
                            </div>
                        </div>
                        <div className="flex relative pb-12">
                            <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                                <div className="h-full w-1 bg-gre-600 dark:bg-org-400 pointer-events-none"/>
                            </div>
                            <div className="flex-shrink-0 w-10 h-10 rounded-full bg-gre-700 dark:bg-org-500 inline-flex items-center justify-center text-white relative z-10">
                                <svg fill="none" stroke="currentColor" strokeLinecap="round"
                                     strokeLinejoin="round" strokeWidth="2" className="w-5 h-5"
                                     viewBox="0 0 24 24">
                                    <circle cx="12" cy="5" r="3"/>
                                    <path d="M12 22V8M5 12H2a10 10 0 0020 0h-3"/>
                                </svg>
                            </div>
                            <div className="flex-grow pl-4">
                                <h2 className="font-medium title-font text-sm mb-1 tracking-wider">NOTRE
                                    ADRESSE</h2>
                                <p className="leading-relaxed">Le restaurant {restaurant?.name} est domicilié
                                    au {restaurant?.street}, {restaurant?.zipCode} {restaurant?.city}.</p>
                            </div>
                        </div>
                        <div className="flex relative pb-12">
                            <div className="h-full w-10 absolute inset-0 flex items-center justify-center">
                                <div className="h-full w-1 bg-gre-700 dark:bg-org-500 pointer-events-none"/>
                            </div>
                            <div className="flex-shrink-0 w-10 h-10 rounded-full bg-gre-700 dark:bg-org-500 inline-flex items-center justify-center text-white relative z-10">
                                <svg fill="none" stroke="currentColor" strokeLinecap="round"
                                     strokeLinejoin="round" strokeWidth="2" className="w-5 h-5"
                                     viewBox="0 0 24 24">
                                    <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"/>
                                    <circle cx="12" cy="7" r="4"/>
                                </svg>
                            </div>
                            <div className="flex-grow pl-4">
                                <h2 className="font-medium title-font text-sm mb-1 tracking-wider">NOS
                                    PRESTATIONS</h2>
                                <p className="leading-relaxed">Service de restauration de burgers élaborés, en
                                    livraison à domicile, retrait ou sur place. L'établissement assure aussi le
                                    service en bar pendant nos horaires d'ouverture.</p>
                            </div>
                        </div>
                        <div className="flex relative">
                            <div className="flex-shrink-0 w-10 h-10 rounded-full bg-gre-700 dark:bg-org-500 inline-flex items-center justify-center text-white relative z-10">
                                <svg fill="none" stroke="currentColor" strokeLinecap="round"
                                     strokeLinejoin="round" strokeWidth="2" className="w-5 h-5"
                                     viewBox="0 0 24 24">
                                    <path d="M22 11.08V12a10 10 0 11-5.93-9.14"/>
                                    <path d="M22 4L12 14.01l-3-3"/>
                                </svg>
                            </div>
                            <div className="flex-grow pl-4">
                                <h2 className="font-medium title-font text-sm mb-1 tracking-wider">A
                                    NOTRE SUJET</h2>
                                <p className="leading-relaxed">Description sympathique à insérer, thème/ambiance
                                    du restaurant si spécificité etc etc</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default AppRestaurantAbout;
