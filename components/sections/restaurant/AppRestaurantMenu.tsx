import React, {useEffect, useRef, useState} from 'react';
// @ts-ignore
import Select from 'react-select';
import CardProduct from '../../elements/cards/CardProduct';
import FormSearchbar from '../../forms/FormSearchbar';
import {useDispatch, useSelector} from "react-redux";
import {getProducts} from "../../../redux/selectors/products";
import {PurchasableProductHandler} from "../../../utils/handlers/ApiPlatform/Product/PurchasableProduct/PurchasableProductHandler";
import {Collection} from "../../../utils/handlers/Collection";
import {ProductModel} from "../../../utils/models/ApiPlatform/Product/ProductModel";
import {ApiPlatformModel} from "../../../utils/models/ApiPlatform/ApiPlatformModel";
import {setProducts} from "../../../redux/actions/products";
import {Context} from "../../../utils/middlewares/Context/Context";
import {endRequest, newRequest} from "../../../redux/actions/request";
import {SearchFilter} from "../../../utils/filters/ApiPlatform/SearchFilter";
import {EntreeHandler} from "../../../utils/handlers/ApiPlatform/Product/PurchasableProduct/Food/EntreeHandler";
import {DishHandler} from "../../../utils/handlers/ApiPlatform/Product/PurchasableProduct/Food/DishHandler";
import {DessertHandler} from "../../../utils/handlers/ApiPlatform/Product/PurchasableProduct/Food/DessertHandler";
import {DrinkHandler} from "../../../utils/handlers/ApiPlatform/Product/PurchasableProduct/Food/DrinkHandler";
import {MenuHandler} from "../../../utils/handlers/ApiPlatform/Product/PurchasableProduct/Menu/MenuHandler";
import {Pagination} from "../../elements/pagination/Pagination";

export interface ProductOption {
    readonly value: string;
    readonly label: string;
    readonly category: string;
    readonly price: string;
    readonly rating: string;
    readonly diet: string;
    readonly allergen: string;
    readonly country: string;
    readonly menu: string;
    readonly isFixed?: boolean;
    readonly isDisabled?: boolean;
}

export const productOptions: any = [
    {
        value: 'burger1',
        label: 'burger1',
        category: 'Plats',
        price: '12',
        rating: '4.8',
        diet: '',
        allergen: 'Poivre',
        country: 'France',
        menu: ''
    },
    {
        value: 'burger2',
        label: 'burger2',
        category: 'Plats',
        price: '15',
        rating: '5',
        diet: 'Halal',
        allergen: '',
        country: 'France',
        menu: ''
    },
    {
        value: 'burger3',
        label: 'burger3',
        category: 'Plats',
        price: '10',
        rating: '4.5',
        diet: '',
        allergen: '',
        country: 'France',
        menu: ''
    },
    {
        value: 'burger4',
        label: 'burger4',
        category: 'Plats',
        price: '14',
        rating: '3.3',
        diet: 'Vegan',
        allergen: 'Gluten',
        country: 'France',
        menu: ''
    },
    {
        value: 'burger5',
        label: 'burger5',
        category: 'Plats',
        price: '12',
        rating: '4.6',
        diet: 'Casher',
        allergen: 'Sésame',
        country: 'France',
        menu: ''
    },
    {
        value: 'dessert1',
        label: 'dessert1',
        category: 'Desserts',
        price: '4',
        rating: '5',
        diet: 'Halal',
        allergen: '',
        country: 'Francce',
        menu: ''
    },
    {
        value: 'dessert2',
        label: 'dessert2',
        category: 'Desserts',
        price: '6',
        rating: '2',
        diet: 'Vegan',
        allergen: '',
        country: 'Francce',
        menu: ''
    },
    {
        value: 'dessert3',
        label: 'dessert3',
        category: 'Desserts',
        price: '4.5',
        rating: '4',
        diet: 'Halal',
        allergen: '',
        country: 'Francce',
        menu: ''
    },
    {
        value: 'entree1',
        label: 'entree1',
        category: 'Entrées',
        price: '8',
        rating: '5',
        diet: 'Halal',
        allergen: '',
        country: 'France',
        menu: ''
    },
    {
        value: 'entree2',
        label: 'entree3',
        category: 'Entrées',
        price: '8',
        rating: '5',
        diet: 'Halal',
        allergen: '',
        country: 'France',
        menu: ''
    },
    {
        value: 'entree3',
        label: 'entree3',
        category: 'Entrées',
        price: '8',
        rating: '5',
        diet: 'Halal',
        allergen: '',
        country: 'France',
        menu: ''
    },
    {
        value: 'boisson1',
        label: 'boisson1',
        category: 'Boissons',
        price: '5',
        rating: '4.2',
        diet: 'Vegan',
        allergen: 'Coke',
        country: 'USA',
        menu: ''
    },
    {
        value: 'boisson2',
        label: 'boisson2',
        category: 'Boissons',
        price: '6',
        rating: '3.2',
        diet: 'Halal',
        allergen: 'Gluten',
        country: 'France',
        menu: ''
    },
    {
        value: 'boisson3',
        label: 'boisson3',
        category: 'Boissons',
        price: '7',
        rating: '4.8',
        diet: 'Casher',
        allergen: 'Coke',
        country: 'UK',
        menu: ''
    },
    {
        value: 'boisson4',
        label: 'boisson4',
        category: 'Boissons',
        price: '8',
        rating: '5',
        diet: '',
        allergen: 'Coke',
        country: 'BE',
        menu: ''
    },
]

const SECTIONS = [
    {
        title: 'Tous',
        handler: new PurchasableProductHandler()
    },
    // {
    //     title: 'Nos menus',
    //     handler: new MenuHandler()
    // },
    {
        title: 'Entrées',
        handler: new EntreeHandler()
    },
    {
        title: 'Plats',
        handler: new DishHandler()
    },
    {
        title: 'Desserts',
        handler: new DessertHandler()
    },
    {
        title: 'Boissons',
        handler: new DrinkHandler()
    }
]

export const AppRestaurantMenu = ({restaurant}: any) => {

    const productState = useSelector(getProducts);
    const [productHandler, setProductHandler] = useState<PurchasableProductHandler<any>>(SECTIONS[0].handler);
    const [valueToSearch, setValueToSearch] = useState<any>();
    const dispatch = useDispatch();
    const [page, setPage] = useState(1);
    const initialRender = useRef(true);

    useEffect(() => {

        let effectReturns: void | (() => void | undefined) = () => {};

        if (initialRender.current) {
            initialRender.current = false;
        } else {
            searchProducts();
        }

        if (effectReturns && typeof effectReturns === "function") {
            return effectReturns;
        }

    }, [productHandler, page, valueToSearch])

    const searchProducts = () => {
        const context = new Context(false);
        productHandler.token = context.getCustomerToken();
        dispatch(newRequest());
        const searchFilters = [
            new SearchFilter('search_by_name_or_reference', valueToSearch)
        ]
        productHandler.getAll([], searchFilters, page).then(async (products: Collection<ProductModel>) => {
            dispatch(setProducts(products));
        }).catch(function (err: any) {
            ApiPlatformModel.showFailedResponseError(err, dispatch);
        }).finally(() => {
            dispatch(endRequest());
        })
    }

    const products = productState?.products;

    return (
        <section>
            <div className="relative">
                <div className="flex flex-col sm:flex-row sm:justify-around">
                    {/* Filternav */}
                    <div className="w-full md:w-72 bg-white dark:bg-neutral-800">
                        <div className="container relative flex w-full p-4">
                            <FormSearchbar
                                onSearch={(value: any) => {
                                    setValueToSearch(value);
                                }}
                                placeholder={"Je recherche un produit (par nom, ou référence)"}
                            />
                        </div>
                        <nav className="mt-4 px-4">
                            {SECTIONS.map((sectionDesc: any, index) => {
                                    const isSelected = sectionDesc.handler === productHandler;
                                    return (
                                        <div key={index}>
                                            {/*<div className="max-w-sm rounded overflow-hidden shadow-lg">*/}
                                            <a className={
                                                "cursor-pointer rounded-md flex items-center p-2 my-4 transition-colors dark:hover:bg-neutral-700 duration-200 justify-start bg-gray-50 dark:bg-neutral-600 " +
                                                (isSelected ? ' shadow-inner text-underline ' : ' shadow-lg dark:hover:bg-neutral-700 hover:bg-neutral-100')
                                            }
                                               onClick={(e: any) => {
                                                   e.preventDefault();
                                                   if (!isSelected) {
                                                       setProductHandler(sectionDesc.handler);
                                                       setPage(1);
                                                   }
                                               }}
                                            >
                                                <svg width="20" height="20" className="text-org-500 dark:text-gre-500" fill="currentColor" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1024 1131q0-64-9-117.5t-29.5-103-60.5-78-97-28.5q-6 4-30 18t-37.5 21.5-35.5 17.5-43 14.5-42 4.5-42-4.5-43-14.5-35.5-17.5-37.5-21.5-30-18q-57 0-97 28.5t-60.5 78-29.5 103-9 117.5 37 106.5 91 42.5h512q54 0 91-42.5t37-106.5zm-157-520q0-94-66.5-160.5t-160.5-66.5-160.5 66.5-66.5 160.5 66.5 160.5 160.5 66.5 160.5-66.5 66.5-160.5zm925 509v-64q0-14-9-23t-23-9h-576q-14 0-23 9t-9 23v64q0 14 9 23t23 9h576q14 0 23-9t9-23zm0-260v-56q0-15-10.5-25.5t-25.5-10.5h-568q-15 0-25.5 10.5t-10.5 25.5v56q0 15 10.5 25.5t25.5 10.5h568q15 0 25.5-10.5t10.5-25.5zm0-252v-64q0-14-9-23t-23-9h-576q-14 0-23 9t-9 23v64q0 14 9 23t23 9h576q14 0 23-9t9-23zm256-320v1216q0 66-47 113t-113 47h-352v-96q0-14-9-23t-23-9h-64q-14 0-23 9t-9 23v96h-768v-96q0-14-9-23t-23-9h-64q-14 0-23 9t-9 23v96h-352q-66 0-113-47t-47-113v-1216q0-66 47-113t113-47h1728q66 0 113 47t47 113z"/>
                                                </svg>
                                                <span className="mx-4 text-md font-normal uppercase">
                                            {sectionDesc.title}
                                            </span>
                                            </a>
                                        </div>
                                    )
                                }
                            )}
                            {/*<Fragment>*/}
                            {/*    <Select*/}
                            {/*        className="basic-single"*/}
                            {/*        classNamePrefix="select"*/}
                            {/*        defaultValue="Catégorie de produit"*/}
                            {/*        isDisabled={toggleDisabled}*/}
                            {/*        isLoading={toggleLoading}*/}
                            {/*        isClearable={toggleClearable}*/}
                            {/*        isRtl={toggleRtl}*/}
                            {/*        isSearchable={toggleSearchable}*/}
                            {/*        name="color"*/}
                            {/*        options={productOptions}*/}
                            {/*        placeholder="Catégorie de produit"*/}
                            {/*    />*/}
                            {/*    /!* <div style={{ color: 'hsl(0, 0%, 40%)', display: 'inline-block', fontSize: 12, fontStyle: 'italic', marginTop: '1em', }}>*/}
                            {/*            <Checkbox checked={isClearable} onChange={toggleClearable}>Clearable</Checkbox>*/}
                            {/*            <Checkbox checked={isSearchable} onChange={toggleSearchable}>Searchable</Checkbox>*/}
                            {/*            <Checkbox checked={isDisabled} onChange={toggleDisabled}>Disabled</Checkbox>*/}
                            {/*            <Checkbox checked={isLoading} onChange={toggleLoading}>Loading</Checkbox>*/}
                            {/*            <Checkbox checked={isRtl} onChange={toggleRtl}>RTL</Checkbox>*/}
                            {/*        </div> *!/*/}
                            {/*</Fragment>*/}
                            {/*<Select*/}
                            {/*    defaultValue={''}*/}
                            {/*    isMulti*/}
                            {/*    name="products"*/}
                            {/*    options={productOptions}*/}
                            {/*    className="basic-multi-select"*/}
                            {/*    classNamePrefix="select"*/}
                            {/*    placeholder="Régime particulier"*/}
                            {/*/>*/}
                            {/*<Select*/}
                            {/*    defaultValue={''}*/}
                            {/*    isMulti*/}
                            {/*    name="products"*/}
                            {/*    options={productOptions}*/}
                            {/*    className="basic-multi-select"*/}
                            {/*    classNamePrefix="select"*/}
                            {/*    placeholder="Allergies"*/}
                            {/*/>*/}
                            {/*<Select*/}
                            {/*    defaultValue={''}*/}
                            {/*    isMulti*/}
                            {/*    name="products"*/}
                            {/*    options={productOptions}*/}
                            {/*    className="basic-multi-select"*/}
                            {/*    classNamePrefix="select"*/}
                            {/*    placeholder="Région/Pays"*/}
                            {/*/>*/}
                            {/*<Select*/}
                            {/*    defaultValue={''}*/}
                            {/*    isMulti*/}
                            {/*    name="products"*/}
                            {/*    options={productOptions}*/}
                            {/*    className="basic-multi-select"*/}
                            {/*    classNamePrefix="select"*/}
                            {/*    placeholder="Tarif"*/}
                            {/*/>*/}
                            {/*<Select*/}
                            {/*    defaultValue={''}*/}
                            {/*    isMulti*/}
                            {/*    name="products"*/}
                            {/*    options={productOptions}*/}
                            {/*    className="basic-multi-select"*/}
                            {/*    classNamePrefix="select"*/}
                            {/*    placeholder="Notation"*/}
                            {/*/>*/}
                            {/*<div>*/}
                            {/*    <a className="text-gray-600 dark:text-gray-300 hover:text-gray-800 dark:hover:text-gray-100 transition-colors duration-200 flex items-center py-2"*/}
                            {/*       href="#">*/}
                            {/*        <svg width="20" fill="currentColor" height="20" className="h-5 w-5"*/}
                            {/*             viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">*/}
                            {/*            <path*/}
                            {/*                d="M1088 1256v240q0 16-12 28t-28 12h-240q-16 0-28-12t-12-28v-240q0-16 12-28t28-12h240q16 0 28 12t12 28zm316-600q0 54-15.5 101t-35 76.5-55 59.5-57.5 43.5-61 35.5q-41 23-68.5 65t-27.5 67q0 17-12 32.5t-28 15.5h-240q-15 0-25.5-18.5t-10.5-37.5v-45q0-83 65-156.5t143-108.5q59-27 84-56t25-76q0-42-46.5-74t-107.5-32q-65 0-108 29-35 25-107 115-13 16-31 16-12 0-25-8l-164-125q-13-10-15.5-25t5.5-28q160-266 464-266 80 0 161 31t146 83 106 127.5 41 158.5z">*/}
                            {/*            </path>*/}
                            {/*        </svg>*/}
                            {/*<span className="mx-4 font-medium">*/}
                            {/*    J'ai une question*/}
                            {/*</span>*/}
                            {/*    </a>*/}
                            {/*</div>*/}
                        </nav>
                    </div>


                    {/* Displayblock */}
                    <div className="w-full space-y-4 p-4">

                        {/* Navbar : search + change displayformat */}
                        {/* <div className="relative z-20 flex flex-col justify-center p-3 mx-auto flex-center bg-neutral-100 dark:bg-neutral-800">
                <div className="relative items-center pl-1 flex w-full lg:max-w-68 sm:pr-2 sm:ml-0">
                    <div className="container relative left-0 z-50 flex w-2/3 h-auto h-full">
                        <FormSearchbar />
                    </div>
                    <div className="relative p-1 flex items-center justify-end w-1/3 ml-5 mr-4 sm:mr-0 sm:right-auto">
                        <nav className="hidden xl:flex space-x-4 p-2">
                        </nav>
                    </div>
                </div>
            </div> */}

                        {/* Products display */}
                        {products?.data?.map((product: any) =>
                            <CardProduct key={product.reference} restaurant={restaurant} product={product}/>
                        )}

                    </div>


                </div>
            </div>

            <Pagination nbPage={products.lastPage} currentPage={page} onPageChange={setPage}/>
        </section>
    );
};

export default AppRestaurantMenu;