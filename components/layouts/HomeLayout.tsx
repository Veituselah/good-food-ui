import AppFooter from '../sections/footer/AppFooter'
import AppHeader from '../sections/header/AppHeader'
import ButtonBackToTop from '../elements/buttons/ButtonBackToTop'
import ButtonCookies from '../elements/buttons/ButtonCookies'
import Logger from "../loggers/Logger"
import Meta from '../elements/seo/Meta'
import OverlayLoader from "../elements/loader/OverlayLoader"

interface Props {
    title?: string;
    description?: string;
    // element: JSX.Element;
    // component: any;
    vertical?: boolean;
    hideLinks?: boolean;
    children: any;
}

const HomeLayout = (props: Props) => {

    return (
        <>
            {/*Meta*/}
            <Meta pageTitle={ 'Good Food' + (props.title ? ' : ' + props.title : '')} description={ props.description ?? '' }/>
            {/*Maindiv Opening*/}
            <div className="relative bg-neutral-100 dark:bg-neutral-800 bgd-30 dark:bgw-90 text-neutral-700 dark:text-neutral-100 text-justify">
                {/*Header*/}
                <AppHeader />
                {/*BodyContent*/}
                <div className="relative overflow-hidden">
                    <main className="mx-auto h-full">
                        {props.children}
                    </main>
                </div>
                {/*Footer*/}
                <AppFooter/>
                {/*Maindiv Closure*/}
                <ButtonBackToTop />
                <ButtonCookies/>
                <Logger/>
                <OverlayLoader />
            </div>
        </>
    );
};

export default HomeLayout;
