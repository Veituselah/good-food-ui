import AdminNav from "../elements/navigation/admin/AdminNav";
import AdminHeader, {AdminHeaderAction} from "../sections/admin/header/AdminHeader";
import React from "react";
import Meta from "../elements/seo/Meta";
import Logger from "../loggers/Logger";
import OverlayLoader from "../elements/loader/OverlayLoader";

interface Props {
    children: any;
    title?: string;
    description?: string;
    actions?: AdminHeaderAction[];
}

const AdminLayout = (props: Props) => {
    return (

        <>
            <Meta pageTitle={'Good Food Admin' + (props.title ? ' : ' + props.title : '')} description={props.description ?? ''}/>
            <main className="relative bg-neutral-100 dark:bg-neutral-800 bgd-30 dark:bgw-90 text-neutral-700 dark:text-neutral-100 min-h-screen flex overflow-hidden">
                <div className="flex items-start justify-between w-full p-4">
                    <AdminNav/>
                    <div className="ml-4 flex flex-col w-full pl-0 md:space-y-4">
                        <AdminHeader actions={props.actions ?? []}/>
                        {props.children}
                    </div>
                </div>
            </main>
            <Logger/>
            <OverlayLoader/>
        </>
    )
}

export default AdminLayout;