import {useEffect} from "react";
import {Message} from "../../utils/middlewares/Logger/Message";
import SimpleAlert from "../elements/alert/SimpleAlert";
import {useDispatch, useSelector} from "react-redux";
import {getLogger} from "../../redux/selectors/logger";
import {removeMessageAtIndex} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";

const Logger = () => {

    const logger = useSelector(getLogger);
    const dispatch = useDispatch();

    const PREFIX_MESSAGE_ID = "message_";

    const timeouts: any = {};

    useEffect(() => {

        for (let message of logger.messagesToLogg) {
            removeMessageAfterTime(message);
        }

    })

    function removeMessageAfterTime(message: Message) {

        timeouts[message.key ?? ''] = setTimeout(() => {

            removeMessage(message);

        }, message.duration ?? (message.gravity === MessageGravity.ERROR ? 10000 : 3000 ))

    }

    function removeMessage(message: Message) {

        clearTimeout(timeouts[message.key ?? '']);
        timeouts[message.key ?? ''] = null;

        // let element = document.getElementById(PREFIX_MESSAGE_ID + message.key);
        //
        // if (element) {
        // TODO: ADD aanimation to hide element before removing by component refresh
        // }

        dispatch(removeMessageAtIndex(message));

    }

    return (
        <div className="fixed left-0 bottom-0 md:left-5 md:bottom-5 z-50 ">
            {
                logger.messagesToLogg.map(
                    (message: Message) => (
                        <div
                            className="mt-2" key={message.key} id={PREFIX_MESSAGE_ID + message.key}
                            onClick={() => removeMessage(message)}
                        >
                            <SimpleAlert type={message.gravity ?? MessageGravity.ERROR} title={message.title ?? ''} text={message.message}
                                         transparent={true} pointer={true}/>
                        </div>
                    )
                )
            }
        </div>
    );

}

export default Logger;