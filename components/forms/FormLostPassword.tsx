import React, {useState} from 'react';
import {SvgMail} from "../elements/icon/SvgItems";
import {doCreate} from "../../redux/sagas/request";
import {Message} from "../../utils/middlewares/Logger/Message";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {addLog} from "../../redux/actions/logger";
import {hideUserDialog} from "../../redux/actions/user_dialog";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {useDispatch} from "react-redux";
import {NewPasswordRequestModel} from "../../utils/models/ApiPlatform/User/UserRequest/NewPasswordRequestModel";
import {NewPasswordRequestHandler} from "../../utils/handlers/ApiPlatform/User/UserRequest/NewPasswordRequestHandler";
import {ErrorsForField} from "../elements/form/error/ErrorsForField";

const FormLostPassword = () => {

    const dispatch = useDispatch();
    const [errors, setErrors] = useState(new Array<Message>());

    const forgotPassword = (e: any) => {

        e.preventDefault();

        let form = e.target;
        let elements = form.elements;

        let passwordRequestModel = new NewPasswordRequestModel();
        passwordRequestModel.email = elements.email.value;

        let passwordRequestHandler = new NewPasswordRequestHandler();

        doCreate(passwordRequestHandler, passwordRequestModel, dispatch).then(() => {

            let message: Message = {
                title: 'Demande de réinialisation de mot de passe réalisée',
                message: 'Un mail de réinitialisation vous a été envoyé',
                gravity: MessageGravity.SUCCESS,
            }

            dispatch(addLog(message))
            dispatch(hideUserDialog());

            setErrors([])

        }).catch((error: any) => {

            ApiPlatformModel.showFailedResponseError(error, dispatch);
            const errors = ApiPlatformModel.parseConstraintVioliationListIntoInputError(error?.response);
            setErrors(errors);

        });

    }


    return (
        <div className="flex flex-col justify-center md:justify-start">
            <form className="flex flex-col" onSubmit={forgotPassword}>
                <div className="mt-6 items-center">
                Rassurez-vous, vous avez la possibilité de le réinitialiser grâce à votre adresse mail.
            </div>
                <div className="flex flex-col pt-4">
                    <div className="relative flex items-center w-full h-full group mb-2">
                        <SvgMail/>
                        <input
                            required
                            type="email"
                            name="email"
                            className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                            placeholder="Mon adresse mail"
                        />
                    </div>
                    <ErrorsForField errors={errors} field={'email'}/>
                </div>

                <button
                    type="submit"
                    className="flex-shrink-0 mt-2 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                >
                    Réinitialiser mon mot de passe
                </button>

            </form>
        </div>
    )
}

export default FormLostPassword;
