import React, {useEffect, useState} from 'react';
import {Context, ReservationHandler, ReservationModel} from "../../internal";
import {Message} from "../../utils/middlewares/Logger/Message";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {addLog} from "../../redux/actions/logger";
import {useDispatch} from "react-redux";
import {doCreate} from "../../redux/sagas/request";
import {CustomerModel} from "../../utils/models/ApiPlatform/User/Customer/CustomerModel";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {ErrorsForField} from "../elements/form/error/ErrorsForField";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {RestaurantHandler} from "../../utils/handlers/ApiPlatform/Restaurant/RestaurantHandler";
import {showUserDialogSection} from "../../redux/actions/user_dialog";
import {UserDialogSection} from "../../redux/reducers/user_dialog";
import {setHours, setMinutes} from "date-fns";
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css";

const OPEN: any = {
    midday: [
        {
            hour: 11,
            min: 30,
        },
        {
            hour: 12,
            min: 0,
        },
        {
            hour: 12,
            min: 30,
        },
        {
            hour: 13,
            min: 0,
        },
        {
            hour: 13,
            min: 30,
        },
        {
            hour: 14,
            min: 0,
        },
        {
            hour: 14,
            min: 30,
        },
        {
            hour: 15,
            min: 0,
        },
    ],
    evening: [
        {
            hour: 18,
            min: 0,
        },
        {
            hour: 18,
            min: 30,
        },
        {
            hour: 19,
            min: 0,
        },
        {
            hour: 19,
            min: 30,
        },
        {
            hour: 20,
            min: 0,
        },
        {
            hour: 20,
            min: 30,
        },
        {
            hour: 21,
            min: 0,
        },
        {
            hour: 21,
            min: 30,
        },
        {
            hour: 22,
            min: 0,
        },
        {
            hour: 22,
            min: 30,
        },
    ]
}

const FormReview = ({restaurant}: any) => {

    const dispatch = useDispatch();
    const [errors, setErrors] = useState<Array<Message>>([]);
    const [reservedFor, setReservedFor] = useState<any>(new Date());
    const [reservedDayPart, setReservedDayPart] = useState<any>('midday');

    useEffect(() => {
        setReservedFor(null)
    }, [reservedDayPart])

    function reserv(event: any) {
        event.preventDefault();

        let context = new Context(false);
        let customer = context.getCustomer() as CustomerModel;

        if (customer) {

            if (!reservedFor) {

                let message: Message = {
                    gravity: MessageGravity.ERROR,
                    message: 'Vous devez renseigner l\'horaire de la réservation'
                }

                dispatch(addLog(message));

                return;
            }

            let form = event.target;
            let elements = form.elements;

            let reservationHandler = new ReservationHandler(context.getCustomerToken());
            let customerHandler = new CustomerHandler();
            let restaurantHandler = new RestaurantHandler();

            let model: ReservationModel = new ReservationModel();
            restaurant.iri = restaurantHandler.buildIri(restaurant.slug);
            model.restaurant = restaurant;
            customer.iri = customerHandler.buildIri(customer.uuid);
            model.customer = customer;
            model.reservedDayPart = reservedDayPart;
            model.reservedFor = new Date(reservedFor);
            model.nbPerson = parseInt(elements.nbPerson.value);
            model.phoneNumber = elements.phoneNumber.value;

            doCreate(reservationHandler, model, dispatch).then(() => {

                dispatch(addLog({
                    gravity: MessageGravity.SUCCESS,
                    message: 'Votre réservation a bien été envoyée'
                }))

                form.reset();
                setReservedFor(null)
                setErrors([]);

            }).catch((error: any) => {

                ApiPlatformModel.showFailedResponseError(error, dispatch);
                const errors = ApiPlatformModel.parseConstraintVioliationListIntoInputError(error?.response);
                setErrors(errors);

            });

        } else {

            let message: Message = {
                gravity: MessageGravity.ERROR,
                message: 'Vous devez être connecté en tant qu\'utilisateur pour réserver',
            }

            dispatch(addLog(message));
            dispatch(showUserDialogSection(UserDialogSection.LOGIN));

        }

    }

    return (
        <form className="flex flex-col w-full" onSubmit={reserv}>

            <div>
                <div className=" relative ">
                    <select
                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-neutral-500 text-gray-400 aa-input"
                        value={reservedDayPart}
                        onChange={(e: any) => setReservedDayPart(e.target.value)}
                    >
                        <option value={'midday'}>Midi</option>
                        <option value={'evening'}>Soir</option>
                    </select>
                </div>
                <ErrorsForField errors={errors} field={'reservedDayPart'}/>
            </div>


            <div>
                <div className=" relative ">
                    <DatePicker
                        showTimeSelect
                        selected={reservedFor}
                        onChange={(date: any) => setReservedFor(date)}
                        timeFormat="HH:mm"
                        minDate={new Date()}
                        filterDate={(date: Date) => {
                            return date.getDay() !== 1;
                        }}
                        includeTimes={OPEN[reservedDayPart]?.map((time: any) => setHours(setMinutes(new Date(), time.min), time.hour))}
                        isClearable
                        // locale="fr-FR"
                        calendarStartDay={1}
                        placeholderText="Réserver pour le..."
                        dateFormat="dd/MM/yyyy, HH:mm"
                        className={"block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-neutral-500 text-gray-400 aa-input"}
                    />
                </div>
                <ErrorsForField errors={errors} field={'reservedFor'}/>
            </div>


            <div>
                <div className=" relative ">
                    <input
                        type="phone"
                        name="phoneNumber"
                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-neutral-500 text-gray-400 aa-input"
                        placeholder="Numéro de téléphone"
                        required
                        pattern='^\+?[1-9]\d{1,14}$'
                        title={'Votre numéro de téléphone doit être au format +33012345678'}
                        maxLength={15}
                    />
                </div>
                <ErrorsForField errors={errors} field={'phoneNumber'}/>
            </div>


            <div>
                <div className=" relative ">
                    <input
                        required
                        type={"number"}
                        min={1}
                        id="nbPerson"
                        name="nbPerson"
                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-neutral-500 text-gray-400 aa-input"
                        placeholder="Nombre de personnes"
                    />
                </div>
                <ErrorsForField errors={errors} field={'nbPerson'}/>
            </div>


            <button
                className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                type="submit"
            >
                Réserver ma place !
            </button>

            <ErrorsForField errors={errors} field={'customer'}/>
            <ErrorsForField errors={errors} field={'restaurant'}/>

        </form>
    );
};

export default FormReview;
