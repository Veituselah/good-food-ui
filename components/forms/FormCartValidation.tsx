import React, {useState} from 'react';
import Button from '../elements/buttons/Button';
import {nextStep} from "../../redux/actions/checkout";
import {useDispatch} from "react-redux";

const FormCartValidation = () => {

    const dispatch = useDispatch();
    const [CGUCheck, setCGUCheck] = useState(false);
    const [CGVCheck, setCGVCheck] = useState(false);
    const [mentionsCheck, setMentionsCheck] = useState(false);

    return (
        <form onSubmit={(e: any) => {
            e.preventDefault();
            dispatch(nextStep());
        }} className="space-y-4 rounded-2xl w-full bg-white dark:bg-neutral-800">
            <h2 className="font-medium px-4 pt-4">
                L'équipe de Good Food vous invite à vérifier le contenu de votre panier avant de procéder à la
                validation de votre commande.
            </h2>
            <h2 className="font-medium pt-4 px-4">
                Lors de la validation de votre panier, l'ordre de commande est communiqué après de nos équipes. Cet
                ordre de commande est soumis à l'acceptation de nos différentes politiques générales d'utilisation et de
                vente.
            </h2>
            <div className="form-group form-check text-center flex md:flex-row mb-6 pt-4 px-4">
                <input
                    type="checkbox"
                    className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
                    id="cgu-input"
                    name="cgu-input"
                    checked={CGUCheck}
                    onChange={(e: any) => setCGUCheck(e.target.checked)}
                    required
                />
                <label
                    htmlFor="cgu-input"
                    className="form-check-label inline-block"
                >J'ai lu et accepte les conditions générales d'utilisation et de vente*</label>
            </div>
            {/* <div className="form-group form-check text-center flex md:flex-row mb-6 px-4">
                <input
                    type="checkbox"
                    className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
                    id="cgv-input"
                    name="cgv-input"
                    checked={CGVCheck}
                    onChange={(e: any) => setCGVCheck(e.target.checked)}
                    required
                />
                <label
                    htmlFor="cgv-input"
                    className="form-check-label inline-block"
                >J'ai lu et accepte les conditions générales de vente*</label>
            </div> */}
            <div className="form-group form-check text-center flex md:flex-row mb-6 px-4">
                <input
                    type="checkbox"
                    className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
                    id="policy-input"
                    name="policy-input"
                    checked={mentionsCheck}
                    onChange={(e: any) => setMentionsCheck(e.target.checked)}
                    required
                />
                <label
                    htmlFor="policy-input"
                    className="form-check-label inline-block"
                >J'ai lu et accepte les mentions légales*</label>
            </div>
            <div
                className="flex items-start justify-between p-5 border-b border-solid border-org-500 dark:border-gre-500 rounded-t"/>
            <div className="w-full px-4 ml-auto md:w-1/2 pb-4">
                <Button color="gre" label="Je valide mon panier" submit={true}/>
            </div>
        </form>
    );
};

export default FormCartValidation;