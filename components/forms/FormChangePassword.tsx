import React, {useState} from 'react';
import {FormShowHide} from "../forms/FormShowHide";
import {NewPasswordRequestModel} from "../../utils/models/ApiPlatform/User/UserRequest/NewPasswordRequestModel";
import {NewPasswordRequestHandler} from "../../utils/handlers/ApiPlatform/User/UserRequest/NewPasswordRequestHandler";
import {Message} from "../../utils/middlewares/Logger/Message";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {addLog} from "../../redux/actions/logger";
import {showUserDialogSection} from "../../redux/actions/user_dialog";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {useDispatch} from "react-redux";
import {ErrorsForField} from "../elements/form/error/ErrorsForField";
import {useRouter} from "next/router";
import {endRequest, newRequest} from "../../redux/actions/request";
import dynamic from "next/dynamic";
import {HOME_PATH} from "../../pages";
import {UserDialogSection} from "../../redux/reducers/user_dialog";
import {logoutCustomer} from "../../redux/actions/context";
import {Context} from "../../utils/middlewares/Context/Context";


const FormChangePassword = () => {

    const dispatch = useDispatch();
    const router = useRouter();
    const [errors, setErrors] = useState<Array<Message>>([]);


    const submit = (e: any) => {

        e.preventDefault();

        let form = e.target;
        let elements = form.elements;

        if (elements['new-password'].value === elements['confirm-new-password'].value) {

            const context = new Context(false);
            const customer = context.getCustomerToken();

            if (!customer || confirm('Après avoir modifié votre mot de passe, vous serez déconnecté.\nÊtes vous sûr de vouloir continuer ?')) {

                let id = router?.query?.request?.toString();

                let passwordRequestModel = new NewPasswordRequestModel();
                passwordRequestModel.id = parseInt(id ?? '');
                passwordRequestModel.password = elements['new-password'].value;

                let passwordRequestHandler = new NewPasswordRequestHandler();

                dispatch(newRequest());

                passwordRequestHandler.validate(passwordRequestModel).then(() => {

                    let message: Message = {
                        title: 'Réinitialisaiton de mot de passe effectuée',
                        message: 'Vous allez être redirigé vers l\'accueil',
                        gravity: MessageGravity.SUCCESS,
                    }

                    dispatch(addLog(message));

                    dispatch(logoutCustomer(context))

                    router.push(HOME_PATH);

                    dispatch(showUserDialogSection(UserDialogSection.LOGIN))


                }).catch((error: any) => {

                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                    const errors = ApiPlatformModel.parseConstraintVioliationListIntoInputError(error?.response);
                    setErrors(errors);

                }).finally(() => {

                    dispatch(endRequest());

                })
            }

        } else {
            setErrors(
                [
                    {
                        gravity: MessageGravity.ERROR,
                        field: 'password',
                        message: 'Les champs mot de passe doivent être identiques !'
                    }
                ]
            )
        }

    }

    return (
        <form onSubmit={submit} className="flex flex-col md:ml-auto">
            <FormShowHide
                name="new-password"
                pattern="^((?=.*[a-z])(?=.*[A-Z].*)(?=.*[\d])(?=.*[@&\(\)\/!\-<>,;:=\+\.\?])){2,}.{8,}$"
                title="Le mot de passe doit comprendre (2 majuscules, 2 minuscules, 2 chiffres, 2 caractères dans la liste suivante [@&()\/!-<>,;:=+.?])."
                placeholder="Mon nouveau mot de passe"
            />

            <FormShowHide
                name="confirm-new-password"
                title="Le mot de passe doit comprendre (2 majuscules, 2 minuscules, 2 chiffres, 2 caractères dans la liste suivante [@&()\/!-<>,;:=+.?])."
                placeholder="Confirmation de mon nouveau mot de passe"
            />

            <ErrorsForField errors={errors} field={'password'}/>

            <button
                className="flex-shrink-0 mt-4 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                type="submit"
            >
                Valider la modification
            </button>
        </form>
    );

};

export default dynamic(() => Promise.resolve(FormChangePassword), {
    ssr: false
})