import {ErrorsForField} from "../elements/form/error/ErrorsForField";
import React, {useState} from "react";
import {CountryModel} from "../../utils/models/ApiPlatform/Country/CountryModel";
import {Context} from "../../utils/middlewares/Context/Context";
import {AddressHandler} from "../../utils/handlers/ApiPlatform/User/Customer/AddressHandler";
import {doCreate, doPatch} from "../../redux/sagas/request";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {useDispatch} from "react-redux";
import Button from "../elements/buttons/Button";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";

export const FormAddress = ({address = {}, countries, customer, onSaveAddress, onCancelAddress}: any) => {

    const [errors, setErrors] = useState<any>([]);

    const dispatch = useDispatch();

    const cancelForm = () => {
        if (onCancelAddress) {
            onCancelAddress();
        }
    }

    const submit = (e: any) => {

        e.preventDefault();

        let form = e.target;
        let elements = form.elements;

        address.name = elements.name.value;
        address.street = elements.street.value;
        address.additionalAddress = elements.additionalAddress.value;
        address.zipCode = elements.zipCode.value;
        address.city = elements.city.value;
        const countriesFiltered = countries.filter((country: CountryModel) => elements.country.value === country.iri);
        address.country = countriesFiltered.length > 0 ? countriesFiltered[0].iri : null;
        address.customer = customer.iri;
        address.phoneNumber = elements.phone.value;

        const context = new Context(false);
        const token = context.getCustomerToken();
        const addressHandler = new AddressHandler(token);

        let promise: any;

        if (address?.slug) {

            promise = doPatch(addressHandler, address, dispatch);

        } else {

            promise = doCreate(addressHandler, address, dispatch);

        }

        promise.then((address: any) => {

            dispatch(addLog({
                gravity: MessageGravity.SUCCESS,
                message: 'Votre adresse a bien été enregistrée'
            }))

            if (onSaveAddress) {
                onSaveAddress(address.prepareJSON(true));
            }

        }).catch((error: any) => {

            if(error?.response?.status === 403){
                dispatch(addLog({
                    gravity: MessageGravity.INFO,
                    message: 'Une adresse utilisée dans une commande ne peut être modifiée',
                }))
            } else {
                ApiPlatformModel.showFailedResponseError(error, dispatch);
            }

            const errors = ApiPlatformModel.parseConstraintVioliationListIntoInputError(error?.response);
            setErrors(errors);
        })

    }

    const [countryValue, setCountryValue] = useState(address?.country ? address?.country?.iri : '/api/countries/FRA');

    return (
        <form onSubmit={submit}
        >
            <div
                className="items-center w-full p-4 pb-6 space-y-4 text-gray-500 md:inline-flex md:space-y-0 border-b border-solid border-org-500 dark:border-gre-500"
            >
                <h2 className="hidden md:block max-w-sm mx-auto space-y-14 -mt-2 md:w-1/3">
                    <p>Libellé de l'adresse</p>
                    <p>Téléphone du client</p>
                    <p>Rue du client</p>
                    <p>N° du logement client</p>
                    <p>Code postal client</p>
                    <p>Ville client</p>
                    <p>Pays client</p>
                </h2>
                <div className="max-w-sm mx-auto space-y-5 md:w-2/3">
                    <div>
                        <div className=" relative ">
                            <input
                                type="text"
                                id="name"
                                name="name"
                                className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="Maison, travail, ..."
                                required
                                defaultValue={address?.name}
                                maxLength={255}
                            />
                            <ErrorsForField errors={errors} field={'name'}/>
                        </div>
                    </div>
                    <div>
                        <div className=" relative ">
                            <input
                                type="phone"
                                id="phone"
                                name="phone"
                                className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="Numéro de téléphone"
                                required
                                defaultValue={address?.phoneNumber}
                                pattern='^\+?[1-9]\d{1,14}$'
                                title={'Votre numéro de téléphone doit être au format +33012345678'}
                                maxLength={15}
                            />
                            <ErrorsForField errors={errors} field={'phone'}/>
                        </div>
                    </div>
                    <div>
                        <div className=" relative ">
                            <input
                                type="text"
                                id="street"
                                name="street"
                                className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="Rue"
                                required
                                defaultValue={address?.street}
                                maxLength={255}
                            />
                            <ErrorsForField errors={errors} field={'street'}/>
                        </div>
                    </div>
                    <div>
                        <div className=" relative ">
                            <input
                                type="text"
                                id="additionalAddress"
                                name="additionalAddress"
                                className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="N° de porte"
                                defaultValue={address?.additionalAddress}
                                maxLength={255}
                            />
                            <ErrorsForField errors={errors} field={'additionnalAddress'}/>
                        </div>
                    </div>
                    <div>
                        <div className=" relative ">
                            <input
                                type="text"
                                id="zipCode"
                                name="zipCode"
                                className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="Code postal"
                                required
                                defaultValue={address?.zipCode}
                                minLength={5}
                                maxLength={5}
                            />
                            <ErrorsForField errors={errors} field={'zipCode'}/>
                        </div>
                    </div>
                    <div>
                        <div className=" relative ">
                            <input
                                type="text"
                                id="city"
                                name="city"
                                className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="Ville"
                                required
                                defaultValue={address?.city}
                                maxLength={255}
                            />
                            <ErrorsForField errors={errors} field={'city'}/>
                        </div>
                    </div>
                    <div>
                        <div className=" relative ">
                            <select
                                id="country"
                                name="country"
                                className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="Pays"
                                required
                                value={countryValue}
                                onChange={(e: any) => {
                                    setCountryValue(e.target.value)
                                }}
                            >
                                {countries?.map((country: any) =>
                                    <option
                                        value={country.iri}
                                        key={country.iri}
                                    >
                                        {country.name}
                                    </option>
                                )}
                            </select>
                            <ErrorsForField errors={errors} field={'country'}/>
                        </div>
                    </div>
                </div>
            </div>

            <div className={"mt-4 flex items-end"}>

                <div className="w-full px-4 ml-auto md:w-1/2 pb-4">
                    <Button color="org" label="Annuler" submit={false} onClick={cancelForm}/>
                </div>

                <div className="w-full px-4 ml-auto md:w-1/2 pb-4">
                    <Button color="gre" label="Enregistrer l'adresse" submit={true}/>
                </div>

            </div>
        </form>
    )
}