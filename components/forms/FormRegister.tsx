import React, {useState} from 'react';
import {SvgMail, SvgUser} from "../elements/icon/SvgItems";
import {CustomerModel} from "../../utils/models/ApiPlatform/User/Customer/CustomerModel";
import {ErrorsForField} from "../elements/form/error/ErrorsForField";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {useDispatch} from "react-redux";
import {doCreate} from "../../redux/sagas/request";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {showUserDialogSection} from "../../redux/actions/user_dialog";
import {UserDialogSection} from "../../redux/reducers/user_dialog";
import {addLog} from "../../redux/actions/logger";
import {Message} from "../../utils/middlewares/Logger/Message";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {FormShowHide} from "./FormShowHide";

const FormRegister = () => {

    const [errors, setErrors] = useState<Array<Message>>([]);

    const dispatch = useDispatch();

    const createUser = (e: any) => {

        e.preventDefault();

        let form = e.target;
        let elements = form.elements;

        if (elements.password.value === elements['confirm-password'].value) {

            let customer = new CustomerModel();

            customer.birthdate = new Date(elements.birthdate.value);
            customer.plainPassword = elements.password.value;
            customer.firstname = elements.firstname.value;
            customer.lastname = elements.lastname.value;
            customer.email = elements.email.value;

            let customerHandler = new CustomerHandler();

            doCreate(customerHandler, customer, dispatch).then(() => {

                let message: Message = {
                    title: 'Votre compte a bien été créé',
                    message: 'Un mail de confirmation vous a été envoyé',
                    gravity: MessageGravity.SUCCESS,
                }

                dispatch(addLog(message))
                dispatch(showUserDialogSection(UserDialogSection.LOGIN));

                setErrors([])

            }).catch(({response}) => {
                setErrors(ApiPlatformModel.parseConstraintVioliationListIntoInputError(response))
            });


        } else {
            setErrors(
                [
                    {
                        gravity: MessageGravity.ERROR,
                        field: 'password',
                        message: 'Les champs mot de passe doivent être identiques !'
                    }
                ]
            )
        }
    }

    return (
        <div className="flex flex-col justify-center md:justify-start">
            <form onSubmit={createUser} className="flex flex-col">
                <div className="relative flex flex-col md:flex-row items-center w-full h-full group pt-4 gap-4 mb-4">
                    <div className="relative flex md:flex-row items-center w-full h-full group mb-4">
                        <SvgUser/>
                        <input
                            required
                            type="text"
                            className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                            aria-describedby="emailHelp123"
                            placeholder="Mon nom"
                            name={"lastname"}
                        />
                    </div>
                    <div className="relative flex md:flex-row items-center w-full h-full group mb-4">
                        <SvgUser/>
                        <input
                            required
                            type="text"
                            className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                            placeholder="Mon prénom"
                            name={"firstname"}
                        />
                    </div>
                </div>
                <div className="flex flex-col">
                    <div className="relative flex items-center w-full h-full group mb-4">
                        <SvgMail/>
                        <input
                            required
                            type="text"
                            id="design-login-email"
                            className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                            placeholder="Mon adresse mail"
                            name={"email"}
                        />
                    </div>
                    <ErrorsForField errors={errors} field={'email'}/>
                </div>
                <div className="flex flex-col">
                    <FormShowHide
                        name="password"
                        pattern="^((?=.*[a-z])(?=.*[A-Z].*)(?=.*[\d])(?=.*[@&\(\)\/!\-<>,;:=\+\.\?])){2,}.{8,}$"
                        title="Le mot de passe doit comprendre (2 majuscules, 2 minuscules, 2 chiffres, 2 caractères dans la liste suivante [@&()\/!-<>,;:=+.?])."
                        placeholder="Mon mot de passe"
                    />
                    <ErrorsForField errors={errors} field={'password'}/>
                </div>
                <div className="flex flex-col">
                    <FormShowHide
                        name="confirm-password"
                        title="Le mot de passe doit comprendre (2 majuscules, 2 minuscules, 2 chiffres, 2 caractères dans la liste suivante [@&()\/!-<>,;:=+.?])."
                        placeholder="Confirmation de mot de passe"
                    />
                    <ErrorsForField errors={errors} field={'password'}/>
                </div>
                <div className="flex flex-col pb-2">
                    <div className="relative flex items-start flex-col w-full h-full group">
                        <label htmlFor="datetime">Date de naissance</label>
                        <input
                            required
                            type="date"
                            name={"birthdate"}
                            min='1900-01-01' max='2005-01-01'
                            className="appearance-none w-full py-2 px-4 rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 flex-1"/>
                    </div>
                    <ErrorsForField errors={errors} field={'birthdate'}/>
                </div>

                <div className="form-group form-check text-center flex md:flex-row mb-6">
                    <input
                        type="checkbox"
                        className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
                        id="cgu-input"
                        name="cgu-input"
                        required
                    />
                    <label
                        htmlFor="cgu-input"
                        className="form-check-label inline-block"
                    >J'ai lu et accepte les conditions générales d'utilisation et de vente*</label>
                </div>

                {/* <div className="form-group form-check text-center flex md:flex-row mb-6">
                    <input
                        type="checkbox"
                        className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
                        id="cgv-input"
                        name="cgv-input"
                        required
                    />
                    <label
                        htmlFor="cgv-input"
                        className="form-check-label inline-block"
                    >J'ai lu et accepte les conditions générales de vente*</label>
                </div> */}

                <div className="form-group form-check text-center flex md:flex-row mb-6">
                    <input
                        type="checkbox"
                        className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
                        id="policy-input"
                        name="policy-input"
                        required
                    />
                    <label
                        htmlFor="policy-input"
                        className="form-check-label inline-block"
                    >J'ai lu et accepte les mentions légales*</label>
                </div>

                <div className="form-group form-check text-center flex md:flex-row mb-6">
                    <input
                        type="checkbox"
                        className="form-check-input h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain mr-2 cursor-pointer"
                        id="newsletter-input"
                        name="newsletter-input"
                    />
                    <label
                        htmlFor="newsletter-input"
                        className="form-check-label inline-block"
                    >Je m'inscris à la newsletter</label>
                </div>


                <button
                    type="submit"
                    className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                >
                    Inscription
                </button>
            </form>
        </div>
    );
};

export default FormRegister;
