import React, {useEffect, useState} from 'react'
import Button from '../elements/buttons/Button'
import {useDispatch, useSelector} from "react-redux";
import {getCheckout} from "../../redux/selectors/checkout";
import {
    nextStep,
    setAddresses,
    setCheckoutDeliveryAddress,
    setCheckoutInvoiceAddress
} from "../../redux/actions/checkout";
import {FormAddress} from "./FormAddress";
import {addLog} from "../../redux/actions/logger";

const FormCheckoutAddresses = () => {

    const checkoutState = useSelector(getCheckout);
    const countries = checkoutState?.countries ?? [];
    const addresses = checkoutState?.addresses ?? [];
    const customer = checkoutState.customer;
    const [addressToManage, setAddressToManage] = useState<any>(null);
    const [addressToEdit, setAddressToEdit] = useState<any>();

    const dispatch = useDispatch();

    const [invoiceAddress, setInvoiceAddress] = useState();
    const [deliveryAddress, setDeliveryAddress] = useState();

    useEffect(() => {

        if (addresses?.length > 0) {

            setAddressToManage(null)

        } else {

            setAddressToManage({})

        }

    }, [checkoutState.addresses])

    const submit = (e: any) => {
        e.preventDefault();

        if (invoiceAddress) {
            dispatch(setCheckoutDeliveryAddress(deliveryAddress));
            dispatch(setCheckoutInvoiceAddress(invoiceAddress));
            dispatch(nextStep());
        } else {
            dispatch(addLog({
                message: 'Vous devez renseigner l\'adresse de facturation et de livraison'
            }))
        }

    }

    return (
        <div className="pt-4 space-y-4 rounded-2xl w-full bg-white dark:bg-neutral-800">
            <form className={'space-y-4'} onSubmit={submit}>

                <h2 className="font-medium px-4">
                    Mes informations personnelles
                </h2>
                <div
                    className="items-center w-full p-4 pb-6 space-y-4 text-gray-500 md:inline-flex md:space-y-0 border-b border-solid border-org-500 dark:border-gre-500">
                    <h2 className="max-w-sm mx-auto space-y-9 md:w-1/3">
                        <p>Nom client</p>
                        <p>Prénom client</p>
                        <p>Adresse mail du client</p>
                    </h2>
                    <div className="max-w-sm mx-auto space-y-5 md:w-2/3">
                        <div>
                            <div className=" relative ">
                                <input
                                    type="text"
                                    disabled
                                    defaultValue={customer?.lastname}
                                    className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                    placeholder="Nom"/>
                            </div>
                        </div>
                        <div>
                            <div className=" relative ">
                                <input
                                    type="text"
                                    disabled
                                    defaultValue={customer?.firstname}
                                    className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                    placeholder="Prénom"/>
                            </div>
                        </div>
                        <div>
                            <div className=" relative ">
                                <input
                                    type="email"
                                    disabled
                                    defaultValue={customer?.email}
                                    className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                    placeholder="Email"/>
                            </div>
                        </div>
                    </div>
                </div>

                <h2 className="font-medium px-4">
                    Mes adresses
                </h2>
                <div
                    className="items-center w-full p-4 pb-6 space-y-4 text-gray-500 md:inline-flex md:space-y-0 border-b border-solid border-org-500 dark:border-gre-500">
                    <h2 className="max-w-sm mx-auto space-y-9 md:w-1/3">
                        <p>Adresse de facturation</p>
                        <p>Adresse de livraison</p>
                    </h2>
                    <div className="max-w-sm mx-auto space-y-5 md:w-2/3">
                        <div>
                            <div className=" relative ">
                                <select
                                    id="invoice"
                                    name="invoice"
                                    className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                    placeholder="Adresse de facturation"
                                    required
                                    value={invoiceAddress}
                                    onChange={(e: any) => {
                                        setInvoiceAddress(e.target.value)
                                    }}
                                >
                                    {addresses?.map((address: any) =>
                                        <option
                                            value={address.slug}
                                            key={address.slug}
                                        >
                                            {address.name}
                                        </option>
                                    )}
                                </select>
                            </div>
                        </div>
                        <div>
                            <div className=" relative ">
                                <select
                                    id="delivery"
                                    name="delivery"
                                    className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                    placeholder="Adresse de livraison"
                                    required
                                    value={deliveryAddress}
                                    onChange={(e: any) => {
                                        setDeliveryAddress(e.target.value)
                                    }}
                                >
                                    <option>Je souhaite venir chercher ma commande</option>
                                    {addresses?.map((address: any) =>
                                        <option
                                            value={address.slug}
                                            key={address.slug}
                                        >
                                            {address.name}
                                        </option>
                                    )}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div className={"flex items-end flex-wrap"}>

                    {!addressToManage && (
                        <div className="w-full px-4 ml-auto md:w-1/3 pb-4">
                            <Button color="gre" label="Créer une adresse" submit={false}
                                    onClick={() => setAddressToManage({})}/>
                        </div>
                    )}

                    {!addressToManage && (
                        <div className="w-full px-4 ml-auto md:w-1/3 pb-4">
                            <select
                                id="addressToEdit"
                                name="addressToEdit"
                                className="mb-4 block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                placeholder="Addresse à modifier"
                                value={addressToEdit}
                                onChange={(e: any) => {
                                    setAddressToEdit(e.target.value)
                                }}
                            >
                                <option/>
                                {addresses?.map((address: any) =>
                                    <option
                                        value={address.slug}
                                        key={address.slug}
                                    >
                                        {address.name}
                                    </option>
                                )}
                            </select>
                            <Button
                                color="gre"
                                label="Modifier l'adresse"
                                submit={false}
                                onClick={() => {
                                    const searchedAddress = addresses.filter((address: any) => addressToEdit === address.slug);

                                    if (searchedAddress.length > 0) {
                                        setAddressToManage(searchedAddress[0])
                                    }
                                }}
                            />
                        </div>
                    )}

                    <div className="w-full px-4 ml-auto md:w-1/3 pb-4">
                        <Button
                            color={addressToManage ? "gray" : "gre"}
                            disabled={addressToManage}
                            label="Valider les adresses"
                            submit={true}
                        />
                    </div>
                </div>
            </form>

            {addressToManage && <FormAddress
                address={addressToManage}
                countries={countries}
                customer={customer}
                onCancelAddress={() => setAddressToManage(null)}
                onSaveAddress={(address: any) => {
                    setAddressToManage(null);

                    const addressIndex = addresses.findIndex((addressFilter: any) => addressFilter.slug === address.slug);

                    if (addressIndex > -1) {
                        addresses[addressIndex] = address;
                    } else {
                        addresses.push(address);
                    }

                    dispatch(setAddresses(addresses));
                    
                    const defaultAddress = addresses[0].slug;

                    if (!invoiceAddress) {
                        setInvoiceAddress(defaultAddress);
                    }

                }}
            />
            }

        </div>
    );
};

export default FormCheckoutAddresses;