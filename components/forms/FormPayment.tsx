import React from 'react';
import Button from '../elements/buttons/Button';
import {useDispatch, useSelector} from "react-redux";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {endRequest, newRequest} from "../../redux/actions/request";
import {getCheckout} from "../../redux/selectors/checkout";
import {OrderHandler} from "../../utils/handlers/ApiPlatform/Order/OrderHandler";
import {Context} from "../../utils/middlewares/Context/Context";
import {getCart} from "../../redux/selectors/cart";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {setCart} from "../../redux/actions/cart";
import {nextStep} from "../../redux/actions/checkout";
import {ACCOUNT_PATH} from "../../pages/account";
import {useRouter} from "next/router";
import {AddressHandler} from "../../utils/handlers/ApiPlatform/User/Customer/AddressHandler";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";

const FormPayment = () => {

    const VISA = 'visa';
    const PAYPAL = 'paypal';

    const dispatch = useDispatch();
    const checkoutState = useSelector(getCheckout);
    const cartState = useSelector(getCart);
    const router = useRouter();

    const submit = (e: any) => {

        e.preventDefault();

        let form = e.target;
        let elements = form.elements;

        dispatch(newRequest())

        if (
            elements[VISA].checked &&
            elements.cardNumber.value === '1111 2222 3333 4444' &&
            elements.monthExpiration.value === "08" &&
            elements.yearExpiration.value === "2022" &&
            elements.cryptogramme.value === "123"
        ) {

            const context = new Context(false);
            const token = context.getCustomerToken();

            let orderHandler = new OrderHandler(token);
            let addresHandler = new AddressHandler(token);
            const cart = cartState.cart;

            if (checkoutState.deliveryAddress) {
                cart.deliveryAddress = {iri: addresHandler.buildIri(checkoutState.deliveryAddress)};
            }

            cart.billingAddress = {iri: addresHandler.buildIri(checkoutState.invoiceAddress)};
            cart.orderedAt = new Date();

            orderHandler.pay(cart).then(() => {

                dispatch(addLog({
                    gravity: MessageGravity.SUCCESS,
                    title: 'Votre commande a bien été effectuée',
                    message: 'Vous allez être redirigé vers votre profil, d\'où vous pourrez consulter votre commande',
                    duration: 5000
                }))
                dispatch(nextStep())

                const customerHandler = new CustomerHandler(token);

                customerHandler.getCart().then((order: any) => {

                    setTimeout(() => {
                        router.push(ACCOUNT_PATH);
                        dispatch(setCart(order));
                    }, 3000)

                }).catch((error: any) => {
                    ApiPlatformModel.showFailedResponseError(error, dispatch);
                })

            }).catch((error) => {
                ApiPlatformModel.showFailedResponseError(error, dispatch);
            }).finally(() => {
                dispatch(endRequest())

            })

        } else {

            setTimeout(() => {

                dispatch(addLog({
                    gravity: MessageGravity.ERROR,
                    message: 'Erreur lors du paiement'
                }))
                dispatch(endRequest())

            }, 1500)

        }
    }

    return (
        <form onSubmit={submit} className="w-full mx-auto bg-white dark:text-neutral-500 shadow-lg p-5 rounded-2xl bg-white dark:bg-neutral-800">
            <div className="mb-4 flex flex-wrap -mx-2">
                <div className="px-2">
                    <label htmlFor={VISA} className="flex items-center cursor-pointer">
                        <input type="radio" className="form-radio h-5 w-5 text-indigo-500" name={VISA} id={VISA}
                               value={VISA} defaultChecked/>
                        <img alt='img' src="https://leadershipmemphis.org/wp-content/uploads/2020/08/780370.png"
                             className="h-8 ml-3"/>
                    </label>
                </div>
                <div className="px-2">
                    <label htmlFor={PAYPAL} className="flex items-center cursor-pointer">
                        <input type="radio" className="form-radio h-5 w-5 text-indigo-500" name={PAYPAL} id={PAYPAL}
                               value={PAYPAL}/>
                        <img alt='img' src="https://www.sketchappsources.com/resources/source-image/PayPalCard.png"
                             className="h-8 ml-3"/>
                    </label>
                </div>
            </div>
            <div className="mb-4">
                <label className="font-bold text-sm mb-2 ml-1">Nom sur la carte</label>
                <div>
                    <input
                        required
                        minLength={3}
                        name="cardName"
                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input"
                        placeholder="John Smith" type="text"
                    />
                </div>
            </div>
            <div className="mb-4">
                <label className="font-bold text-sm mb-2 ml-1">Numéro de carte</label>
                <div>
                    <input
                        required
                        minLength={8}
                        maxLength={30}
                        name="cardNumber"
                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input"
                        placeholder="0000 0000 0000 0000" type="text"
                    />
                </div>
            </div>
            <div className="mb-4 -mx-2 flex items-end">
                <div className="px-2 w-1/2">
                    <label className="font-bold text-sm mb-2 ml-1">Date d'expiration</label>
                    <div>
                        <select
                            required
                            name={'monthExpiration'}
                            className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input">
                            <option value="01">01 - Janvier</option>
                            <option value="02">02 - Février</option>
                            <option value="03">03 - Mars</option>
                            <option value="04">04 - Avril</option>
                            <option value="05">05 - Mai</option>
                            <option value="06">06 - Juin</option>
                            <option value="07">07 - Juillet</option>
                            <option value="08">08 - Août</option>
                            <option value="09">09 - Septembre</option>
                            <option value="10">10 - Octobre</option>
                            <option value="11">11 - Novembre</option>
                            <option value="12">12 - Décembre</option>
                        </select>
                    </div>
                </div>
                <div className="px-2 w-1/2">
                    <select
                        required
                        name={'yearExpiration'}
                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input">
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>
                        <option value="2027">2027</option>
                        <option value="2028">2028</option>
                        <option value="2029">2029</option>
                        <option value="2029">2030</option>
                    </select>
                </div>
            </div>
            <div className="mb-4">
                <label className="font-bold text-sm mb-2 ml-1">Cryptogramme de sécurité</label>
                <div>
                    <input
                        required
                        minLength={3}
                        maxLength={10}
                        name={"cryptogramme"}
                        className="block w-32 py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input"
                        placeholder="000" type="text"/>
                </div>
            </div>
            <div className="relative md:w-1/2 justify-center items-center">
                <Button color="org" type="submit" label="Confirmer et payer" submit={true}/>
            </div>
        </form>
    );
};

export default FormPayment;