import React from 'react';
import {SvgMail} from "../elements/icon/SvgItems";

const FormNewsletter = () => {
    return (
        <form className="flex flex-col md:flex-row w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-center">
            <div className="relative flex items-center w-full lg:w-64 h-full group">
                <SvgMail/>
                <input
                    type="text"
                    id='"form-subscribe-Subscribe'
                    className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-gray-800 text-gray-400 aa-input"
                    placeholder="Email"
                />
            </div>
            <button
                className="flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-purple-600 rounded-md shadow-md hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-500 focus:ring-offset-2 focus:ring-offset-purple-200"
                type="submit"
            >
                Inscription
            </button>
        </form>
    );
};

export default FormNewsletter;
