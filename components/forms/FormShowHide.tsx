import {SvgHide, SvgPassword, SvgShow} from "../elements/icon/SvgItems"
import React, {useState} from "react"

export const FormShowHide = (props:any) => {
    const [show, setShow] = useState(false);
    return (
        <div className="relative flex items-center w-full h-full group mb-4">
            <SvgPassword/>
            <input
                {...props}
                type={show ? 'text' : 'password'}
                className="block w-full py-1.5 px-10 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                required
            />
            <button className="text-org-500 dark:text-gre-500 absolute right-0 w-4 h-4 mr-4 z-20 mb-2" type="button" onClick={() => setShow(!show)}>{show ? <SvgShow/> : <SvgHide/>}</button>
        </div>
    )

}