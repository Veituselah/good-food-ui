import React from 'react';
import {SvgDots, SvgMail, SvgMessage, SvgUser} from '../elements/icon/SvgItems'

const FormContact = () => {
    return (
        <div className="flex flex-col md:ml-auto w-full">
            <div className="max-w-xl sm:mx-auto lg:max-w-2xl">
                <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                    <div>
                        <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider uppercase rounded-full">
                            Des remarques ?
                        </p>
                    </div>
                    <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight sm:text-4xl md:mx-auto">
                        <span className="relative inline-block">
                            <SvgDots/>
                            <span className="relative">Contactez-nous</span>
                        </span>
                    </h2>
                    <p className="text-base md:text-lg">
                        Vous avez une question, une remarque, ou même des idées originales à nous suggérer ? Lancez-vous
                        et partagez cela avec nous sans plus tarder ! Notre équipe sera heureuse de vous répondre.
                    </p>
                </div>
            </div>
            <div className="relative flex items-center w-full h-full group mb-4">
                <SvgUser/>
                <input
                    type="text"
                    id="name"
                    name="name"
                    className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input"
                    placeholder="Votre nom"
                />
            </div>
            <div className="relative flex items-center w-full h-full group mb-4">
                <SvgMail/>
                <input
                    type="email"
                    id="email"
                    name="email"
                    className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input"
                    placeholder="Votre adresse mail"
                />
            </div>
            <div className="relative flex items-center w-full h-full group mb-4">
                <SvgMessage/>
                <textarea
                    id="message"
                    name="message"
                    className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input h-32"
                    placeholder="Votre message"
                />
            </div>
            <button
                className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                type="submit"
            >
                Envoyer
            </button>
            <p className="text-xs text-gray-500 mt-3">
                Insérer notification de succès ou d'erreur de complétion du formulaire.
            </p>
        </div>
    );
};

export default FormContact;
