import React, {useState} from 'react';
import {Context} from "../../internal";
import {useRouter} from "next/router";
import {useDispatch} from "react-redux";
import {UserLoginModel} from "../../utils/models/ApiPlatform/User/Login/UserLoginModel";
import {SvgMail} from "../elements/icon/SvgItems";
import {InputError} from "../elements/form/error/InputError";
import {FormShowHide} from "./FormShowHide";
import {UserLoginHandler} from "../../utils/handlers/ApiPlatform/User/Login/UserLoginHandler";
import {endRequest, newRequest} from "../../redux/actions/request";
import {ADMIN_PATH} from "../../pages/admin";
import {hideUserDialog} from "../../redux/actions/user_dialog";
import {RESTAURANT_PAGE} from "../../pages/restaurant/[slug]";

const FormLogin = () => {

    const context = new Context(false);
    const router = useRouter();
    const handler = new UserLoginHandler();

    const dispatch = useDispatch();
    const [error, setError] = useState<string | null>();

    function submit(event: any) {
        event.preventDefault();

        let form = event.target;
        let elements = form.elements;

        let model = new UserLoginModel();
        model.password = elements.password.value;

        model.username = elements.username.value;

        dispatch(newRequest());

        handler.login(model).then((token: string | null) => {

            if(!token){
                throw new Error('Token incorrect');
            }

            let redirectToAdmin = context.saveUser(token, dispatch);

            dispatch(hideUserDialog())

            if(redirectToAdmin){
                router.push(ADMIN_PATH);
            } else {

                if(router.route.startsWith(RESTAURANT_PAGE)){
                    router.reload();
                }

            }

            setError(null);

        }).catch((err) => {

            if (err?.response?.data?.code === 401) {
                setError(err.response.data.message);
            } else {
                setError("Une erreur est survenue pendant la connexion");
            }

        }).finally(() => {
            dispatch(endRequest());
        });
    }

    return (
        <div className="flex flex-col justify-center my-auto md:justify-start">
            <form onSubmit={submit} className="flex flex-col">
                <div className="flex flex-col pt-4">
                    <div className="relative flex items-center w-full h-full group mb-4">
                        <SvgMail/>
                        <input
                            type="email"
                            id="design-login-email"
                            className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                            placeholder="Mon adresse mail"
                            name="username"
                            required
                        />
                    </div>
                </div>
                <div className="flex flex-col pt-4 mb-4">
                    <FormShowHide
                        name="password"
                        pattern="^((?=.*[a-z])(?=.*[A-Z].*)(?=.*[\d])(?=.*[@&\(\)\/!\-<>,;:=\+\.\?])){2,}.{8,}$"
                        title="Le mot de passe doit comprendre (2 majuscules, 2 minuscules, 2 chiffres, 2 caractères dans la liste suivante [@&()\/!-<>,;:=+.?])."
                        placeholder="Mot de passe"
                    />
                    <InputError error={{
                        message: error ?? ''
                    }}/>
                </div>
                <button
                    type="submit"
                    className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                >
                    <span className="w-full">Connexion</span>
                </button>
            </form>
        </div>
    );
};

export default FormLogin;
