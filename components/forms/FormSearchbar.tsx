import React from 'react'
import {SvgCross, SvgSearch} from '../elements/icon/SvgItems'

const FormSearchbar = ({defaultSearch, onSearch, placeholder}: any) => {

    const search = (e: any) => {

        e.preventDefault();
        const form = e.target;
        const elements = form.elements;
        const search = elements.search.value;

        if (onSearch) {
            onSearch(search)
        }
    }

    return (
        <form onSubmit={search} className="relative flex items-center w-full lg:w-96 h-full group">
            <div
                className="absolute flex items-center justify-center block w-auto h-10 p-3 pr-2 text-sm text-gray-500 uppercase cursor-pointer sm:hidden">
                <SvgCross/>
            </div>
            <SvgSearch/>
            <input
                required
                minLength={3}
                name="search"
                type="text"
                defaultValue={defaultSearch}
                className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-500 aa-input"
                placeholder={placeholder ?? "Je recherche"}
            />
            <div
                className="absolute right-0 hidden h-auto px-2 py-1 mr-2 text-xs text-gray-400 border border-gray-300 rounded-2xl md:block">
                +
            </div>
        </form>
    );
};

export default FormSearchbar;
