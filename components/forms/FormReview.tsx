import React, {useState} from 'react';
import {SvgMail, SvgMessage} from "../elements/icon/SvgItems";
import {Context} from "../../internal";
import {Message} from "../../utils/middlewares/Logger/Message";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {addLog} from "../../redux/actions/logger";
import {useDispatch} from "react-redux";
import {doCreate} from "../../redux/sagas/request";
import {CommentHandler} from "../../utils/handlers/ApiPlatform/User/CommentHandler";
import {CustomerModel} from "../../utils/models/ApiPlatform/User/Customer/CustomerModel";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {ErrorsForField} from "../elements/form/error/ErrorsForField";
import {newComment} from "../../redux/actions/comments";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {RestaurantHandler} from "../../utils/handlers/ApiPlatform/Restaurant/RestaurantHandler";
import {CommentModel} from "../../utils/models/ApiPlatform/User/CommentModel";
import {showUserDialogSection} from "../../redux/actions/user_dialog";
import {UserDialogSection} from "../../redux/reducers/user_dialog";

const FormReview = ({restaurant}: any) => {

    const dispatch = useDispatch();
    const [errors, setErrors] = useState<Array<Message>>([]);

    function comment(event: any) {
        event.preventDefault();

        let context = new Context(false);
        let customer = context.getCustomer() as CustomerModel;

        if (customer) {

            let form = event.target;
            let elements = form.elements;

            let commentHandler = new CommentHandler(context.getCustomerToken());
            let customerHandler = new CustomerHandler();
            let restaurantHandler = new RestaurantHandler();

            let model: CommentModel = new CommentModel();
            restaurant.iri = restaurantHandler.buildIri(restaurant.slug);
            model.restaurant = restaurant;
            customer.iri = customerHandler.buildIri(customer.uuid);
            model.customer = customer;
            model.note = parseInt(elements.note.value);
            model.message = elements.message.value;

            doCreate(commentHandler, model, dispatch).then((result) => {

                dispatch(newComment(result as CommentModel));
                dispatch(addLog({
                    gravity: MessageGravity.SUCCESS,
                    message: 'Votre commentaire a bien été envoyé'
                }))
                form.reset();

            }).catch((error: any) => {

                ApiPlatformModel.showFailedResponseError(error, dispatch);
                const errors = ApiPlatformModel.parseConstraintVioliationListIntoInputError(error?.response);
                setErrors(errors);

            });

        } else {

            let message: Message = {
                gravity: MessageGravity.ERROR,
                title: 'Vous devez être connecté en tant qu\'utilisateur',
                message: 'Pour envoyer un commentaire'
            }

            dispatch(addLog(message));
            dispatch(showUserDialogSection(UserDialogSection.LOGIN));

        }

    }

    return (
        <form className="pl-2 flex flex-col md:ml-auto w-full" onSubmit={comment}>
            <h5 className="text-xl font-semibold text-center mb-10 md:mb-6">Je partage mon expérience</h5>
            <div className="relative flex items-center w-full h-full group mb-4">
                <SvgMail/>
                <input
                    required
                    type="number"
                    id="note"
                    name="note"
                    min={0}
                    max={5}
                    className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input"
                    placeholder="Votre note (ente 1 et 5)"
                />
            </div>

            <ErrorsForField errors={errors} field={'note'}/>

            {/* <div className="relative flex items-center w-full h-full group mb-4">*/}
            {/*    <SvgMail/>*/}
            {/*    <input*/}
            {/*        type="email"*/}
            {/*        id="email"*/}
            {/*        name="email"*/}
            {/*        className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-gray-100 dark:bg-gray-800 text-gray-400 aa-input"*/}
            {/*        placeholder="Objet de votre commentaire"*/}
            {/*    />*/}
            {/*</div>*/}
            <div className="relative flex items-center w-full h-full group mb-4">
                <SvgMessage/>
                <textarea
                    required
                    id="message"
                    name="message"
                    className="block w-full py-1.5 pl-10 pr-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-50 dark:bg-neutral-500 text-gray-400 aa-input h-48"
                    placeholder="Votre message"
                />
            </div>

            <ErrorsForField errors={errors} field={'message'}/>

            <button
                className="flex-shrink-0 px-4 py-2 text-base font-semibold bg-gre-700 dark:bg-org-500 rounded-md shadow-md hover:bg-gre-600 dark:hover:bg-org-600 focus:outline-none focus:ring-2 focus:ring-gre-700 dark:focus:ring-org-500 focus:ring-offset-2 focus:ring-offset-gre-200 dark:focus:ring-offset-org-200 dark:text-neutral-700"
                type="submit"
            >
                Publier mon avis
            </button>
        </form>
    );
};

export default FormReview;
