import React, {FC, useState} from 'react'
import Button from '../elements/buttons/Button'
import {useDispatch, useSelector} from "react-redux";
import {getProfile} from "../../redux/selectors/profile";
import {CustomerHandler} from "../../utils/handlers/ApiPlatform/User/Customer/CustomerHandler";
import {Context} from "../../utils/middlewares/Context/Context";
import {doCreate, doPatch} from "../../redux/sagas/request";
import {ApiPlatformModel} from "../../utils/models/ApiPlatform/ApiPlatformModel";
import {setProfileCustomer} from "../../redux/actions/profile";
import {addLog} from "../../redux/actions/logger";
import {MessageGravity} from "../../utils/middlewares/Logger/MessageGravity";
import {ErrorsForField} from "../elements/form/error/ErrorsForField";
import {logoutCustomer} from "../../redux/actions/context";
import {HOME_PATH} from "../../pages";
import {useRouter} from "next/router";
import {NewPasswordRequestModel} from "../../utils/models/ApiPlatform/User/UserRequest/NewPasswordRequestModel";
import {NewPasswordRequestHandler} from "../../utils/handlers/ApiPlatform/User/UserRequest/NewPasswordRequestHandler";
import {Message} from "../../utils/middlewares/Logger/Message";

const FormUserData: FC = () => {

    const now = new Date();
    const month = now.getMonth() + 1;
    const day = now.getDate();

    const eightTeenYearsAgo =
        (now.getFullYear() - 18).toString() + '-' +
        (month < 10 ? '0' + month : month).toString() + '-' +
        (day < 10 ? '0' + day : day).toString();

    const dispatch = useDispatch();

    const profileState = useSelector(getProfile);
    const customer: any = profileState?.customer ?? {};

    const birthdateDate: Date = new Date(customer?.birthdate);

    const [errors, setErrors] = useState<any>([]);
    const router = useRouter();

    let birthdateStr = birthdateDate.toISOString().split('T')[0]

    const submit = (e: any) => {

        e.preventDefault();

        let form = e.target;
        let elements = form.elements;

        customer.birthdate = new Date(elements?.birthdate?.value);
        customer.lastname = elements?.lastname?.value;
        customer.firstname = elements?.firstname?.value;

        const newEmail = elements?.email?.value;
        let emailChanged = newEmail !== customer.email;
        let executeRequest = true;

        if (emailChanged) {

            executeRequest = confirm('Êtes vous sûr.e de vouloir modifier votre email ?\nVous devrez vous reconnecter si vous continuez...')

            if(executeRequest){
                customer.email = elements?.email?.value;
            }

        }

        if(executeRequest) {

            const context = new Context(false);
            const customerHandler = new CustomerHandler(context.getCustomerToken());

            doPatch(customerHandler, customer, dispatch).then((customer: any) => {

                dispatch(setProfileCustomer(customer))

                dispatch(addLog({
                    gravity: MessageGravity.SUCCESS,
                    message: 'Vos informations ont bien été mises à jours.'
                }))

                setErrors([]);

                if (emailChanged) {
                    dispatch(logoutCustomer(context));
                    router.push(HOME_PATH)
                }

            }).catch((error: any) => {

                ApiPlatformModel.showFailedResponseError(error, dispatch);
                const errs: any = ApiPlatformModel.parseConstraintVioliationListIntoInputError(error?.response);
                setErrors(errs);

            })

        }
    }

    const resetPassword = () => {

        let passwordRequestModel = new NewPasswordRequestModel();
        passwordRequestModel.email = customer?.email;

        let passwordRequestHandler = new NewPasswordRequestHandler();

        doCreate(passwordRequestHandler, passwordRequestModel, dispatch).then(() => {

            let message: Message = {
                title: 'Demande de réinialisation de mot de passe réalisée',
                message: 'Un mail de réinitialisation vous a été envoyé',
                gravity: MessageGravity.SUCCESS,
            }

            dispatch(addLog(message))
            setErrors([])

        }).catch((error: any) => {

            ApiPlatformModel.showFailedResponseError(error, dispatch);
            const errors = ApiPlatformModel.parseConstraintVioliationListIntoInputError(error?.response);
            setErrors(errors);

        });

    }

    return (

        <div className="shadow-lg rounded-2xl w-full bg-neutral-50 dark:bg-neutral-500">
            <div className="shadow-md">
                <div
                    className="flex flex-row w-full p-4 bg-neutral-50 dark:bg-neutral-500 border-t-2 border-org-500 dark:border-gre-500 rounded-lg">
                    <div className="max-w-sm mx-auto md:w-full md:mx-0">
                        <div className="inline-flex items-center space-x-4">
                            <h2 className="text-2xl font-bold pt-4">Mes informations personnelles</h2>
                        </div>
                    </div>
                </div>
                <form onSubmit={submit} className="space-y-4 rounded-2xl w-full bg-white dark:bg-neutral-800">
                    <h2 className="font-medium pt-4 px-4">
                        Mon compte
                    </h2>
                    <div
                        className="items-center w-full p-4 pb-6 space-y-4 text-gray-500 md:inline-flex md:space-y-0 border-b border-solid border-org-500 dark:border-gre-500">
                        <h2 className="max-w-sm mx-auto -mt-4 md:w-1/3">
                            Adresse mail :
                        </h2>
                        <div className="max-w-sm mx-auto md:w-2/3">
                            <div className=" relative ">
                                <input
                                    type="email"
                                    id="email"
                                    name="email"
                                    required
                                    maxLength={255}
                                    className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                    placeholder="Email"
                                    defaultValue={customer?.email}
                                />
                            </div>
                            <ErrorsForField errors={errors} field={'email'}/>
                        </div>
                    </div>
                    <h2 className="font-medium px-4">
                        Mes informations
                    </h2>
                    <div
                        className="items-center w-full p-4 pb-6 space-y-4 text-gray-500 md:inline-flex md:space-y-0">
                        <h2 className="hidden md:block max-w-sm mx-auto space-y-14 md:w-1/3">
                            <p>Nom :</p>
                            <p>Prénom :</p>
                            <p>Date de naissance :</p>
                        </h2>
                        <div className="max-w-sm mx-auto space-y-2 md:space-y-5 md:w-2/3">
                            <p className="block md:hidden">Nom :</p>
                            <div>
                                <div className=" relative ">
                                    <input
                                        type="text"
                                        id="lastname"
                                        name="lastname"
                                        required
                                        maxLength={255}
                                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                        placeholder="Nom"
                                        defaultValue={customer?.lastname}
                                    />
                                    <ErrorsForField errors={errors} field={'lastname'}/>
                                </div>
                            </div>
                            <p className="block md:hidden">Prénom :</p>
                            <div>
                                <div className=" relative ">
                                    <input
                                        type="text"
                                        id="firstname"
                                        name="firstname"
                                        required
                                        maxLength={255}
                                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                        placeholder="Prénom"
                                        defaultValue={customer?.firstname}
                                    />
                                    <ErrorsForField errors={errors} field={'firstname'}/>
                                </div>
                            </div>
                            <p className="block md:hidden">Date de naissance :</p>
                            <div>
                                <div className=" relative ">
                                    <input
                                        required
                                        maxLength={255}
                                        type="date"
                                        defaultValue={birthdateStr}
                                        name="birthdate"
                                        className="block w-full py-1.5 px-4 leading-normal rounded-2xl focus:border-transparent focus:outline-none focus:ring-2 focus:ring-blue-500 ring-opacity-90 bg-neutral-100 dark:bg-neutral-500 text-gray-400 aa-input"
                                        min='1900-01-01' max={eightTeenYearsAgo}
                                        placeholder="Anniversaire"
                                    />
                                    <ErrorsForField errors={errors} field={'birthdate'}/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="w-full px-4 pb-4 ml-auto border-b border-org-500 border-solid dark:border-gre-500">
                        <Button submit={true} color="gre" label="Enregistrer les modifications"/>
                    </div>

                    <h2 className="font-medium px-4">
                        Modifier mon mot de passe
                    </h2>
                    <div
                        className="items-center w-full p-4 pb-6 space-y-4 text-gray-500 md:inline-flex md:space-y-0">

                        {/*<h2 className="max-w-sm mx-auto md:w-1/3">*/}
                        {/*    Mot de passe : *********/}
                        {/*</h2>*/}
                        {/*<div className="text-center md:w-12/12">*/}

                        <Button color="org" label="Réinitialiser mon mot de passe" onClick={resetPassword}/>

                        {/*</div>*/}
                    </div>
                </form>
            </div>
        </div>
    );
};
export default FormUserData;